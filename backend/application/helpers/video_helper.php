<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function get_video_duration($file) {
	if(strpos($_SERVER['SERVER_NAME'],'localhost')===false) {
		$time = exec("ffmpeg -i $file 2>&1 | grep Duration | cut -d ' ' -f 4 | sed s/,//");
	}
		
	else {
		$time = exec("C:\\ffmpeg\\bin\\ffmpeg -i $file 2>&1 | grep Duration | cut -d ' ' -f 4 | sed s/,//");
	}
		
	return $time;
}