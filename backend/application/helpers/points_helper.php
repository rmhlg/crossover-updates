<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function encrypt_points($points)
{
	$encrypted_points = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5(ENCRYPTION_KEY), $points, MCRYPT_MODE_CBC, md5(md5(ENCRYPTION_KEY))));

	return $encrypted_points;
}

function decrypt_points($points)
{
	$decrypt_points = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5(ENCRYPTION_KEY), base64_decode($points), MCRYPT_MODE_CBC, md5(md5(ENCRYPTION_KEY))), "\0");

	return $decrypt_points;
}