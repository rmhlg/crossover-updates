<?php if(!defined('BASEPATH')){ exit('No direct script access allowed!'); }

function crop($param)
{

    $src =  $param['full_path'];
	$output_filename = $param['file_path'].'thumb_'.$param['file_name'];
	list($width2, $height2) = getimagesize($src);
	$targ_w = $targ_h = 150;
	$jpeg_quality = 100;
	$width = $param['width'];
	$height = $param['height'];
	$img_r = imagecreatefromjpeg($src);
	
	$x = $param['x'];
	$y =  $param['y'];
	$x2 = $param['x2'];
	$y2 = $param['y2'];
	$new_point_x = ($x / $width) * $width2;
	$new_point_y = ($y / $width) * $height2;
	$new_width = ($x2 - $x);
	$new_height = ($y2 - $y);
	$dst_r = ImageCreateTrueColor($new_width, $new_height);
	
	imagecopyresampled($dst_r,$img_r,0,0,$new_point_x,$new_point_y, $width,$height,$width2,$height2);
	imagejpeg($dst_r, $output_filename, $jpeg_quality); 




	
}