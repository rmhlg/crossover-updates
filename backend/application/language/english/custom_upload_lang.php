<?php

$lang['upload_invalid_filesize'] = "The photo you are trying to upload exceeds the allowable size (".((int) FILE_UPLOAD_LIMIT / 1000)."MB). Please resize your photo and try again.";