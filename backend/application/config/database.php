<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* Marlboro Crossover Database Settings */


if( in_array( $_SERVER['REMOTE_ADDR'], array( '127.0.0.1', '::1' ) ) ) 
{ //check if localhost or in staging / prod server
	$active_group = 'localhost';
}else
{
	$active_group = 'staging';
}



$active_record = TRUE;

$db['localhost']['hostname'] = 'localhost'; //'192.168.1.167';
$db['localhost']['username'] = 'root';
$db['localhost']['password'] = '';
$db['localhost']['database'] = 'marlboro_crossover';
$db['localhost']['dbdriver'] = 'mysql';
$db['localhost']['dbprefix'] = '';
$db['localhost']['pconnect'] = TRUE;
$db['localhost']['db_debug'] = TRUE;
$db['localhost']['cache_on'] = FALSE;
$db['localhost']['cachedir'] = '';
$db['localhost']['char_set'] = 'utf8';
$db['localhost']['dbcollat'] = 'utf8_general_ci';
$db['localhost']['swap_pre'] = '';
$db['localhost']['autoinit'] = TRUE;
$db['localhost']['stricton'] = FALSE;

$db['staging']['hostname'] = 'localhost';
$db['staging']['username'] = 'crossoverstage';
$db['staging']['password'] = 'DZk26SgN64t3ykqHbPHNRzbQsc3m3n';
$db['staging']['database'] = 'crossoverstage';
$db['staging']['dbdriver'] = 'mysql';
$db['staging']['dbprefix'] = '';
$db['staging']['pconnect'] = TRUE;
$db['staging']['db_debug'] = TRUE;
$db['staging']['cache_on'] = FALSE;
$db['staging']['cachedir'] = '';
$db['staging']['char_set'] = 'utf8';
$db['staging']['dbcollat'] = 'utf8_general_ci';
$db['staging']['swap_pre'] = '';
$db['staging']['autoinit'] = TRUE;
$db['staging']['stricton'] = FALSE;

$db['production']['hostname'] = '10.66.16.204';
$db['production']['username'] = 'crossover';
$db['production']['password'] = 'c8U9u92VN848a44Q9p6gb93e45s3hm';
$db['production']['database'] = 'crossover';
$db['production']['dbdriver'] = 'mysql';
$db['production']['dbprefix'] = '';
$db['production']['pconnect'] = TRUE;
$db['production']['db_debug'] = TRUE;
$db['production']['cache_on'] = FALSE;
$db['production']['cachedir'] = '';
$db['production']['char_set'] = 'utf8';
$db['production']['dbcollat'] = 'utf8_general_ci';
$db['production']['swap_pre'] = '';
$db['production']['autoinit'] = TRUE;
$db['production']['stricton'] = FALSE;

/* Marlboro Database Settings */
$db['marlboro_local']['hostname'] = '192.168.1.181'; //'192.168.1.176';
$db['marlboro_local']['username'] = 'root';
$db['marlboro_local']['password'] = '';
$db['marlboro_local']['database'] = 'marlboro_2013';
$db['marlboro_local']['dbdriver'] = 'mysql';
$db['marlboro_local']['dbprefix'] = '';
$db['marlboro_local']['pconnect'] = TRUE;
$db['marlboro_local']['db_debug'] = TRUE;
$db['marlboro_local']['cache_on'] = FALSE;
$db['marlboro_local']['cachedir'] = '';
$db['marlboro_local']['char_set'] = 'utf8';
$db['marlboro_local']['dbcollat'] = 'utf8_general_ci';
$db['marlboro_local']['swap_pre'] = '';
$db['marlboro_local']['autoinit'] = TRUE;
$db['marlboro_local']['stricton'] = FALSE;

$db['marlboro_staging']['hostname'] = 'localhost';
$db['marlboro_staging']['username'] = 'stagemarlboro';
$db['marlboro_staging']['password'] = 'uST49JBmeDFZpbrf';
$db['marlboro_staging']['database'] = 'stagemarlboro';
$db['marlboro_staging']['dbdriver'] = 'mysql';
$db['marlboro_staging']['dbprefix'] = '';
$db['marlboro_staging']['pconnect'] = TRUE;
$db['marlboro_staging']['db_debug'] = TRUE;
$db['marlboro_staging']['cache_on'] = FALSE;
$db['marlboro_staging']['cachedir'] = '';
$db['marlboro_staging']['char_set'] = 'utf8';
$db['marlboro_staging']['dbcollat'] = 'utf8_general_ci';
$db['marlboro_staging']['swap_pre'] = '';
$db['marlboro_staging']['autoinit'] = TRUE;
$db['marlboro_staging']['stricton'] = FALSE;

$db['marlboro_production']['hostname'] = '10.66.16.202';
$db['marlboro_production']['username'] = 'pmftc';
$db['marlboro_production']['password'] = 'BUmlHksWepNsT8rqFnDwlJrHLDtBUjxcBcwY';
$db['marlboro_production']['database'] = 'pmftc';
$db['marlboro_production']['dbdriver'] = 'mysql';
$db['marlboro_production']['dbprefix'] = '';
$db['marlboro_production']['pconnect'] = TRUE;
$db['marlboro_production']['db_debug'] = TRUE;
$db['marlboro_production']['cache_on'] = FALSE;
$db['marlboro_production']['cachedir'] = '';
$db['marlboro_production']['char_set'] = 'utf8';
$db['marlboro_production']['dbcollat'] = 'utf8_general_ci';
$db['marlboro_production']['swap_pre'] = '';
$db['marlboro_production']['autoinit'] = TRUE;
$db['marlboro_production']['stricton'] = FALSE;

/* End of file database.php */
/* Location: ./application/config/database.php */