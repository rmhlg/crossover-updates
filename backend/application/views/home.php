<!-- CONTENT -->
<div class="homePage">
    <div class="section" id="home_banner" style="background-image:url(<?= ASSETS_URL ?>img/california/home_banner1024.jpg)">

        <h1>Marlboro Crossover : how far will you go? you decide.</h1>

        <!-- Introduction -->
        <div class="intro">
            <!-- <h2>
			<span>HOW FAR WILL YOU GO?</span>
			<strong>YOU DECIDE</strong>
		</h2> -->
            <div class="toTrailer" data-i18n="[html]home.trailer" data-video="trailer" onclick="earnPoints('<?= encrypt_points(HOME) ?>', '<?= encrypt_points(WATCH) ?>', '<?= encrypt_points(0) ?>')"></div>

            <a href="<?= isset($row) ? SITE_URL.'activities/'.$row->experience_id : SITE_URL ?>" data-i18n="home.start"></a>
        </div>

        <!-- Links to bottom -->
        <div class="linkTo row">
            <a href="<?= SITE_URL ?>events">
                <b data-i18n="[html]home.link3.title"></b>
                <span class="subtitle" data-i18n="home.link3.subtitle"></span>
            </a>
            <a href="<?= SITE_URL ?>gifts">
                <b data-i18n="[html]home.link4.title"></b>
                <span class="subtitle" data-i18n="home.link4.subtitle"></span>
            </a>
        </div>

        <div class="imgLegend">
            <span class="lineTop"></span>
            <b data-i18n="california.surname" data-random="imgLegend"></b>
            <span data-i18n="california.location" data-random="imgLegend"></span> / <span data-i18n="california.country" data-random="imgLegend"></span>
        </div>
    </div>
</div>

<!-- MODAL VIDEO -->
<div class="modal fade modal_video" id="modalVideo">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="video-close-btn" onclick="<?= $row_first_visit === FALSE ? 'homepageVideoPopup()' : '' ?>"><span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body video">

                <img src="" alt="" class="ratio poster">

                <div id="jp_container_1" class="jp-video" role="application" aria-label="media player">
                    <div class="jp-type-single">
                        <div id="js-video" class="jp-jplayer"></div>
                        <div class="jp-gui">
                            <div class="jp-video-play">
                                <button class="jp-video-play-icon" role="button" tabindex="0">play</button>
                            </div>
                            <div class="jp-interface">
                                <div class="jp-progress">
                                    <div class="jp-seek-bar">
                                        <div class="jp-play-bar"></div>
                                    </div>
                                </div>
                                <div class="jp-current-time" role="timer" aria-label="time">&nbsp;</div>
                                <div class="jp-duration" role="timer" aria-label="duration">&nbsp;</div>
                                <div class="jp-controls-holder">
                                    <div class="jp-controls">
                                        <button class="jp-play" role="button" tabindex="0">play</button>
                                        <button class="jp-stop" role="button" tabindex="0">stop</button>
                                    </div>
                                    <div class="jp-volume-controls">
                                        <button class="jp-mute" role="button" tabindex="0">mute</button>
                                        <button class="jp-volume-max" role="button" tabindex="0">max volume</button>
                                        <div class="jp-volume-bar">
                                            <div class="jp-volume-bar-value"></div>
                                        </div>
                                    </div>
                                    <div class="jp-toggles">
                                        <button class="jp-repeat" role="button" tabindex="0">repeat</button>
                                        <button class="jp-full-screen" role="button" tabindex="0">full screen</button>
                                    </div>
                                </div>
                                <div class="jp-details">
                                    <div class="jp-title" aria-label="title">&nbsp;</div>
                                </div>
                            </div>
                        </div>
                        <div class="jp-no-solution">
                            <span>Update Required</span> To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->