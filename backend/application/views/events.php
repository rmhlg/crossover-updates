<section class="events">
    <div class="filter-menu">
        <div class="filter-menu-content">
            <ul>
                <li>
                    <select id="venue" class="select-l" onchange="filterEvent($('#venue').val(), $('#region').val(),$('#event_type').val(),$('#startdate').val(), $('#enddate').val())">
                        <option value="">Select Venue</option>
                        <?php
                        foreach($venues as $venue){
                        ?>
                            <option value="<?= $venue->name ?>"><?= $venue->name ?></option>
                        <?php
                        }
                        ?>
                    </select>
                </li>
                <li>
                    <select id="region" class="select-l" onchange="filterEvent($('#venue').val(), $('#region').val(),$('#event_type').val(),$('#startdate').val(), $('#enddate').val())">
                        <option value="">Select Region</option>
                        <?php
                        foreach($regions as $region){
                        ?>
                            <option value="<?= $region->region_id ?>"><?= $region->name ?></option>
                        <?php
                        }
                        ?>
                    </select>
                </li>
                <li>
                    <select id="event_type" class="select-l" onchange="filterEvent($('#venue').val(), $('#region').val(),$('#event_type').val(),$('#startdate').val(), $('#enddate').val())">
                        <option value="">Select Event Type</option>
                        <?php
                        foreach($eventTypes as $eventType){
                        ?>
                            <option value="<?= $eventType->name ?>"><?= $eventType->name ?></option>
                        <?php
                        }
                        ?>
                    </select>
                </li>
            </ul>
            <ul>
                <li>
                    <input type="text" class="select-l" id="startdate" onchange="filterEvent($('#venue').val(), $('#region').val(),$('#event_type').val(),$('#startdate').val(), $('#enddate').val())" />
                </li>
                <li>
                    <input type="text" class="select-l" id="enddate" onchange="filterEvent($('#venue').val(), $('#region').val(),$('#event_type').val(),$('#startdate').val(), $('#enddate').val())" />
                </li>
            </ul>
        </div>

    </div>
    <div class="events-list">
        <div class="event-content">
            <?php
            $ctr = 0;
            $att_arr = array();
            foreach($attendance as $att) {
                $att_arr[] = $att->event_id;
            }
            foreach($events as $event){
                if($ctr==0){
                    ?>
                    <div class="item">
                        <div class="thumbnails">
                            <img src="<?= UPLOADS_URL ?>backstage/events/<?= $event->image ?>" alt="thumbnail" />
                        </div>
                        <div class="content">
                            <h3><?= $event->title ?></h3>
                            <h6><?= $event->venue ?> <br/> <?= date('F d, Y ', strtotime($event->start_date)) ?> to <?= date('F d, Y ', strtotime($event->end_date)) ?></h6>
                            <br/>
                            <p>
                                <?= $event->description ?>
                            </p>
                            <?php if(!in_array($event->backstage_event_id, $att_arr)){ ?>
                                <button data-toggle="modal" data-target="#myModal" data-date="<?= date('F d, Y ', strtotime($event->start_date)) ?> to <?= date('F d, Y ', strtotime($event->end_date)) ?>" data-description="<?= $event->description ?>" data-venue="<?= $event->venue ?>" data-title="<?= $event->title ?>" onclick="imGoing($(this), <?= $event->backstage_event_id ?>, 1)"><i></i>I'M GOING</button>
                            <?php } else { ?>
                                <button disabled="disabled" class="active"><i></i>GOING</button>
                            <?php } ?>
                        </div>
                    </div>
                    <?php
                    $ctr=1;
                }else{
                    ?>
                    <div class="item">
                        <div class="thumbnails">
                            <img src="<?= UPLOADS_URL ?>backstage/events/<?= $event->image ?>" alt="thumbnail" />
                        </div>
                        <div class="content">
                            <h3><?= $event->title ?></h3>
                            <h6><?= $event->venue ?> <br/> <?= date('F d, Y ', strtotime($event->start_date)) ?> to <?= date('F d, Y ', strtotime($event->end_date)) ?></h6>
                            <br/>
                            <p>
                                <?= $event->description ?>
                            </p>
                            <?php if(!in_array($event->backstage_event_id, $att_arr)){ ?>
                                <button data-toggle="modal" data-target="#myModal" data-date="<?= date('F d, Y ', strtotime($event->start_date)) ?> to <?= date('F d, Y ', strtotime($event->end_date)) ?>" data-description="<?= $event->description ?>" data-venue="<?= $event->venue ?>" data-title="<?= $event->title ?>" onclick="imGoing($(this), <?= $event->backstage_event_id ?>, 1)"><i></i>I'M GOING</button>
                            <?php } else { ?>
                                <button disabled="disabled" class="active"><i></i>GOING</button>
                            <?php } ?>
                        </div>
                    </div>
                    <br class='clear' />
                    <?php
                    $ctr=0;
                }
            }
            ?>
        </div>
    </div>
</section>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="padding-top: 134px;">
  <div class="modal-dialog">
    <div class="modal-content" style="padding: 10px;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Thank you!</h4>
      </div>
      <div class="modal-body">
        You have confirmed your attendance to <strong><span class='eventData' id="eventTitle"></span></strong>. <br /><br />
        See you at <strong><span class='eventData' id="eventVenue"></span></strong> on  <strong><span class='eventData' id="eventDate"></span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>