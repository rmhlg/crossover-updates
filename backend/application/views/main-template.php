<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <title>Crossover</title>

    <!-- CSS -->
    <link rel="stylesheet" href="<?= ASSETS_URL ?>css/vendors/bootstrap.min.css">
    <link rel="stylesheet" href="<?= ASSETS_URL ?>css/style.css">
    <link rel="stylesheet" href="<?= ASSETS_URL ?>css/lytebox.css">
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/themes/smoothness/jquery-ui.css" />
    <link rel="stylesheet/less" href="<?= ASSETS_URL ?>css/overall.less">
    <link rel="stylesheet/less" href="<?= ASSETS_URL ?>css/events-page.less">
    <script src="<?= ASSETS_URL ?>js/less.js"></script>
    <!-- FAVICON -->
    <link rel="icon" href="<?= ASSETS_URL ?>img/favicon.ico" />

    <!-- FONTS -->
    <link href='http://fonts.googleapis.com/css?family=Lato:300' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Oswald:300,400' rel='stylesheet' type='text/css'>

    <?php if($this->uri->segment(1) == 'map'): ?>
        <!-- MAP_FONT -->
        <link rel="stylesheet" type="text/css" href="<?= ASSETS_URL ?>css/fonts/maybefont/canvasfont.css">
    <?php endif; ?>
</head>

<body class="light" data-content="index">

    <div class="mainWrapper">

        <!-- HEADER -->
        <header>

            <div class="burger">
                <span></span>
            </div>

            <div class="container">

                <div class="brand">
                    <a href="<?= SITE_URL ?>home">
                        <img src="<?= ASSETS_URL ?>img/crossover.png" alt="Malboro - Cross|Over" />
                    </a>
                </div>
                <!-- Main navigation -->
                <nav class="main-nav">
                    <span class="home">
                        <a href="<?= SITE_URL ?>home">HOME</a>
                    </span>
                    <span class="map">
                        <a href="<?= SITE_URL ?>map">THE MAP</a>
                    </span>
                    <span>
                        <a href="<?= SITE_URL ?>gifts">GIFTS</a>
                    </span>
                    <span>
                        <a href="<?= SITE_URL ?>events">EVENTS</a>
                    </span>
                    <span>
                        <a href="<?= SITE_URL ?>how-to">HOW TO PLAY</a>
                    </span>
                    <span class="sliderActivities">
                        <a href="#">ACTIVITIES</a>
                    </span>

                    <span class="active">
                        <a href="#">FORUM</a>
                    </span>

                    <!-- data user + Call modal Profile-->
                    <span class="profile">
                        <ul>
                            <!-- open modal Profile + add focus on input addCode -->
                            <li class="addCode">
                                <span class="km collected_km_points_padded"><?= str_pad($this->session->userdata('total_km_points'), 5, '0', STR_PAD_LEFT) ?> <i>KM</i></span>
                        <a href="#" class="addCodeTo">ADD CODE</a>
                        </li>
                        <!-- open modal Profile  -->
                        <li class="profileBtn">
                            <a href="#" data-toggle="modal" data-target="#myProfileInfo">
                                <span class="border">
                                        <img src="<?= $this->session->userdata('profile_picture') ?>" alt"user name" />
                                    </span>
                                <span class="arrowDown"></span>
                            </a>
                        </li>
                        </ul>
                    </span>


                    <!-- tooltip -->
                    <div class="profile-dropdown">
                        <span class="x-close">&times;</span>

                        <div class="content">
                            <div class="inputAddCode">
                                <form action="#" method="POST" onsubmit="return validateCode()" id="form-header">
                                    <div class="inputContainer">
                                        <input type="text" maxlength="8" value="Your code here" placeholder="Your code here" name="personalCode" maxlength="8">
                                        <button disabled="" type="submit"></button>
                                    </div>

                                    <!-- message to display if ...  -->
                                    <div class="notifMsg">
                                        <!-- the code is correct -->
                                        <div class="congrats">
                                            <span></span>
                                            <strong></strong>
                                        </div>
                                        <!-- the code is wrong -->
                                        <div class="error">
                                            <span></span>
                                            <strong></strong>
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <!-- data user (KM) -->
                            <div class="dataKm">
                                <div class="stats">
                                    <span><strong class="collected_km_points"><?= $this->session->userdata('total_km_points') ?></strong>&nbsp;km</span>
                                    <p>collected kilometers</p>
                                </div>
                                <div class="stats">
                                    <span><strong class="credit_km_points"><?= $this->session->userdata('credit_km_points') ?></strong>&nbsp;km</span>
                                    <p>credit kilometers</p>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <!-- list Link -->
                            <!-- <ul class="listLink">
                            <li><a href="">kilometers <strong>history</strong></a></li>
                            <li><a href="">git <strong>claimed</strong></a></li>
                        </ul> -->

                            <nav>
                                <a class="btn" href="<?= SITE_URL ?>profile">go to full profile page</a>
                                <a class="btn" href="<?= CROSSOVER_STAGING_LOGOUT ?>">logout</a>
                            </nav>
                        </div>
                    </div>
                    <!-- end tooltip -->
                </nav>

            </div>

            <!-- sub-menu: display a slider with all the activities -->
            <div class="activities">
                <div class="slider">

                    <?php if($experience): foreach($experience as $k => $v): ?>
                        <?php $points = (int)$this->session->userdata('total_km_points'); ?>
                        <?php $accessible = FALSE; ?>
                        <?php if($v->category_id == REGION_CALIFORNIA): ?>
                            <?php if($points > CALIFORNIA_START_POINTS || $points == CALIFORNIA_START_POINTS): ?>
                                <?php $accessible = TRUE; ?>
                            <?php endif; ?>
                        <?php elseif($v->category_id == REGION_ALASKA): ?>
                            <?php if($points >= ALASKA_START_POINTS): ?>
                                <?php $accessible = TRUE; ?>
                            <?php endif; ?>
                        <?php elseif($v->category_id == REGION_CANADA): ?>
                            <?php if($points >= CANADA_START_POINTS): ?>
                                <?php $accessible = TRUE; ?>
                            <?php endif; ?>
                        <?php endif; ?>

                    <div class="<?= $accessible === TRUE ? 'checked' : 'locked' ?>">
                        <?php if($accessible === TRUE): ?>
                            <a href="<?= SITE_URL ?>activities/<?= $v->experience_id.'/'.url_title($v->title) ?>">
                        <?php else: ?>
                            <a href="javascript:void(0);">
                        <?php endif; ?>
                            <div class="illu">
                                <img src="<?= $v->thumbnail ?>">
                            </div>
                            <div class="copy">
                                <span class="icon bicycle"></span>
                                <h4><?= $v->title ?></h4>
                                <span class="location"><?= unserialize(REGIONS_ALL)[$v->category_id] ?></span>
                            </div>
                        </a>
                    </div>
                <?php endforeach; endif; ?>
                </div>
            </div>

        </header>

        <!-- CONTENT -->
        
        <?php echo $content ?>
        
        <!-- FOOTER -->
        <footer>
            <nav class="bottombar">
                <em>This website is for adult smokers 18 years or older residing in the Philippines. Access to this website is subject to age verification </em>

                <ul class="nav">
                    <li><a href="#">MY ACCOUNT</a></li>
                    <li><a href="#">INVITE A FRIEND</a> </li>
                    <li><a href="javascript:void(0);" id="termsToggle">TERMS AND CONDITIONS</a></li>
                    <li><a href="javascript:void(0);" id="privacyToggle">Privacy Policy </a></li>
                    <li><a href="javascript:void(0);" id="contactusToggle">CONTACT US</a></li>
                    <li><a href="javascript:void(0);" id="mechanicsToggle">Mechanics </a></li>
                    <li><a href="http://www.pmi.com/eng/our_products/health_effects_of_smoking/pages/health_effects_of_smoking.aspx" target="blank">SMOKING &amp; HEALTH</a>
                    </li>
                </ul>
                
                <small>Copyright &copy; 2015 PMFTC INC. All rights reserved</small>
            </nav>


            <div class="warning">
                <h1>
                    <span>GOVERNMENT WARNING: CIGARETTE SMOKING</span>
                    <span> IS DANGEROUS TO YOUR HEALTH</span>
                </h1>
            </div>
        </footer>

    </div>
    
    <script>
        var siteUrl = "<?= site_url() ?>",
            baseUrl = "<?= base_url() ?>",
            assetsUrl = "<?= ASSETS_URL ?>",
            segments = "<?= @$this->uri->segment(2) ?>",
            region = "<?= @$region ?>",
            first_login = "<?= @$this->session->userdata('xover_first_login') ?>";
    </script>

    <script src="<?= ASSETS_URL ?>js/scripts.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"></script>
    <script src="<?= ASSETS_URL ?>js/nwjs.js?v=1"></script>
    <script src="<?= ASSETS_URL ?>js/lytebox.2.3.js"></script>
    <script src="<?= ASSETS_URL ?>js/jquery.slimscroll.min.js"></script>
    <script src="<?= ASSETS_URL ?>js/jquery-ui.min.js"></script>
    <script src="<?= ASSETS_URL ?>js/main.js"></script>

    <?php if($this->uri->segment(1) == 'map'): ?>
        <script src="<?= ASSETS_URL ?>js/handlebars-v1.3.0.js"></script>
        <script src="<?= ASSETS_URL ?>createjs/preloadjs-NEXT.combined.js"></script>
        <script src="<?= ASSETS_URL ?>createjs/easeljs-NEXT.combined.js"></script>
        <script src="<?= ASSETS_URL ?>createjs/tweenjs-NEXT.combined.js"></script>
        <script src="<?= ASSETS_URL ?>createjs/movieclip-NEXT.combined.js"></script>
        <script src="<?= ASSETS_URL ?>createjs/dg_map.js"></script>

        <script id="add-template" type="text/x-handlebars-template">
            <li>
                <input autocomplete="off" type="text" name="name[]" placeholder="name" class="req input">
                <input autocomplete="off" type="text" name="email[]" placeholder="email address" class="req input">
                <button class="remove"> &times; </button>
            </li>
        </script>

        <script type="text/javascript">
        $(function() {
            $('.add').click(function() {
                var items = $('ul.referrals');
                if (items.children().length >= 5) {
                    return;
                }
                var source = $("#add-template").html();
                var template = Handlebars.compile(source);

                items.append(template);
                if (items.children().length >= 5) {
                    $('.add').addClass('disabled');
                }

                $('.remove').unbind('click').bind('click', function() {
                    $('.add').removeClass('disabled');
                    $(this).parent().remove();
                });
            })
        })
        </script>
    <?php endif; ?>

    <?php if($this->uri->segment(2) == 'refer'): ?>
        <script src="js/handlebars-v1.3.0.js"></script>
        <script id="add-template" type="text/x-handlebars-template">
            <li>
                <input autocomplete="off" type="text" name="name[]" placeholder="name" class="req input">
                <input autocomplete="off" type="text" name="email[]" placeholder="email address" class="req input">
                <button class="remove"> &times; </button>
            </li>
        </script>

        <script type="text/javascript">
            $(function() {
                $('.add').click(function() {
                    var items = $('ul.referrals');
                    if (items.children().length >= 5) {
                        return;
                    }
                    var source = $("#add-template").html();
                    var template = Handlebars.compile(source);

                    items.append(template);
                    if (items.children().length >= 5) {
                        $('.add').addClass('disabled');
                    }

                    $('.remove').unbind('click').bind('click', function() {
                        $('.add').removeClass('disabled');
                        $(this).parent().remove();
                    });
                })
            })
        </script>
    <?php endif; ?>

    <script type="text/javascript">

        $(window).load(function(){

            console.log('User Session Key: <?=$session_key?>');

            <?php if($referral_offer){  ?>
                    popup.load({url:"flash_offer/get_flash_offer?prize_id=<?=$referral_offer?>&rand=<?=uniqid()?>&type=referral"});
             <?php } ?> 

             <?php if(!$referral_offer && $birthday_offer && $birthdate != '0000-00-00'){ ?>
                    popup.load({url:"birthday_offer/get_birthday_offer?prize_id=<?=$birthday_offer?>&flash_offer=<?=$flash_offer?>&rand=<?=uniqid()?>",
                                onClose:function(){     
                                    <?php if($flash_offer && !$flash_offer_confirmed) { ?>
                                             popup.load({url:"flash_offer/get_flash_offer?prize_id=<?=$flash_offer?>&rand=<?=uniqid()?>&type=flash"});
                                     <?php } ?>         
                                }
                            });
             <?php } ?>

             <?php if(!$referral_offer && !$birthday_offer && $flash_offer && !$flash_offer_confirmed) { ?>
                        popup.load({url:"flash_offer/get_flash_offer?prize_id=<?=$flash_offer?>&rand=<?=uniqid()?>&type=flash"});
             <?php } ?> 

             <?php if(!$referral_offer && !$birthday_offer && !$flash_offer && $bids){ ?>
                    popup.load({url:"perks/bid/confirm_address"});
             <?php } ?> 
        });

        

    </script>
</body>

</html>
