<!-- WIN GIFTS -->
<section class="section gifts topGifts fullscreen">
	<div class="pageName">
		<span class="icon"></span>
	</div>
	<h3>
		<span>
			<strong data-i18n="activities.gifts.title"></strong>
			<span data-i18n="activities.gifts.subtitle"></span>
		</span>
	</h3>
	<!-- TOP 3 with the gifts -->
	<div class="home_gifts trianglesArrows">
		<?php if($rows): foreach($rows as $k => $v): ?>
			<div class="gift active">
				<div class="picture">
					<img src="<?= $v->image ?>" alt="<?= $v->name ?>" />
				</div>
				<div class="infos">
					<p class="name"><span><?= $v->name ?></span><span class="km">/ <span><?= $v->km_points ?></span>  <i data-i18n="dataUser.km"></i></span></p>
					<a href="<?= SITE_URL ?>gifts" data-i18n="gifts.link"></a>
				</div>
			</div>
		<?php endforeach; endif; ?>
	</div>				
</section>

<!-- LINK GO TO THE NEXT EXPERIENCE -->
<section class="goFurther row">
	<?php if($next): ?>
		<a href="<?= SITE_URL ?>activities/<?= $next->experience_id.'/'.$next->title ?>" class="GoNextExperience">
			<div class="text" data-i18n="[html]activities.gifts.linkNextActivities"></div>	
		</a>
	<?php endif; ?>	
</section>