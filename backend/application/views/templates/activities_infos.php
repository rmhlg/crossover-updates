<!-- EXPLORE YOUR CHOICE -->
<section class="section details info">
	<div class="pageName text">
		<a href="#act_360">
			<span data-i18n="activities.explore.pageNameLeft"></span>
			<span class="icon"></span>
			<span><?= $choice ?></span>
		</a>
	</div>
	
	<?php if($rows): foreach($rows as $k => $v): ?>
		<?php if($v->media_type == 1): ?>
			<div class="line">						
				<div class="picture" style="background-image:url(<?= $v->media ?>)"></div>
				<div class="contentTxt">
					<h3><?= $v->media_title ?></h3>
					<p class="subtitle"><?= $v->media_sub_title ?></p>
					<div><?= $v->media_description ?></div>
				</div>
			</div>
		<?php else: ?>
			<div class="line">
				<div class="picture">
					<!-- button to call a video -->
					<button style="background-image:url(<?= $v->preview_if_video ?>)" data-video="<?= pathinfo($v->media, PATHINFO_FILENAME) ?>" onclick="earnPoints('<?= encrypt_points(ACTIVITY) ?>', '<?= encrypt_points($activity_id) ?>', '<?= encrypt_points(WATCH) ?>')">
						<span data-i18n="[html]alaska.one.explorations.thermal.video"></span>
						<span class="time"><?= $v->video_duration ?></span>
					</button>
				</div>
				<div class="contentTxt">
					<h3><?= $v->media_title ?></h3>
					<p class="subtitle"><?= $v->media_sub_title ?></p>
					<div><?= $v->media_description ?></div>
				</div>
			</div>
		<?php endif; ?>
	<?php endforeach; endif; ?>
</section>