<!-- PLAY THE GAME -->
<link rel="stylesheet" href="<?= ASSETS_URL ?>games/css/vendors/bootstrap.min.css">
<link rel="stylesheet" href="<?= ASSETS_URL ?>games/css/style.css">
<link href='http://fonts.googleapis.com/css?family=Lato:300' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Oswald:300,400' rel='stylesheet' type='text/css'>
<section class="section teaserGame fullscreen">
	<div class="background" style="background-image:url(<?= ASSETS_URL ?>games/img/_california/exp3/game.jpg)"></div>
	
	<div class="pageName">
		<span class="icon"></span>
	</div>

	<div class="title">	
		<span class="ico"></span>
		<div class="stroked" data-i18n="activities.game.subtitle"></div>

		<h3><span data-i18n="[html]california.exp3.game.title"></span></h3>

		<p>
			<span data-i18n="[html]california.exp3.game.text"></span>
		</p>

		<button data-i18n="activities.game.playBtn" data-game="grab"></button>
		<button data-i18n="activities.game.howToBtn" class="howTo"></button>
	</div>
	
	<div class="howToGame">
		<span class="close" data-i18n="profile.btn.close"></span>
		<h2 data-i18n="activities.game.howToBtn">How to play ?</h2>
		<p data-i18n="california.exp3.game.howToPlay"></p>
		<button class="btn" data-i18n="activities.game.playBtn" data-game="grab"></button>
	</div>

	<div class="canvasGame">
		<span class="close"></span>
		<div id="phaser-car" class="game"></div>
	</div>

	<div class="gameoutro">
		<span class="close" data-i18n="profile.btn.close"></span>
		<div class="cover" style="background-image:url(<?= ASSETS_URL ?>games/img/_california/exp3/gameOutro.jpg)"></div>
		<div class="container">
			<h3 class="gameTitle"></h3>
			<div class="stars"></div>
			<div class="kms"><span class="totalPoints"></span><span data-i18n="california.exp3.game.unit"></span></div>
			<div class="earned">
				<span data-i18n="[html]activities.game.youEarned"></span>
				<span class="grade"></span>
			</div>
			<p class="description" data-i18n="california.exp3.game.end.description"></p>
			<button class="btn" data-i18n="activities.game.tryAgain"></button>
		</div>
	</div>
	
	<div class="pageName next text">
		<a href="#gifts">
			<span data-i18n="activities.game.pageNameNextLeft"></span>
			<span class="icon"></span>
			<span data-i18n="activities.game.pageNameNextRight"></span>
		</a>
	</div>

</section>
<script type="text/javascript">
	var gamesURL = "<?= ASSETS_URL ?>games/";
</script>
<script src="<?= ASSETS_URL ?>games/js/scripts.min.js"></script>

<!-- Grab game -->
<script src="<?= ASSETS_URL ?>games/js/games/phaser-arcade-physics.min.js"></script>
<script src="<?= ASSETS_URL ?>games/js/games/landsail/config.js"></script>
<script src="<?= ASSETS_URL ?>games/js/games/landsail/boot.js"></script>
<script src="<?= ASSETS_URL ?>games/js/games/landsail/preload.js"></script>
<script src="<?= ASSETS_URL ?>games/js/games/landsail/gametitle.js"></script>
<script src="<?= ASSETS_URL ?>games/js/games/landsail/game.js"></script>
<script src="<?= ASSETS_URL ?>games/js/games/landsail/main.js"></script>
<script type="text/javascript">console.log(gamesURL);</script>