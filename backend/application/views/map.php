<div class="homePage">
    <div class="section" style="height: 100%;">

        <div class="kmap-wrapper" style="margin:0 auto; width:100%; height:100%; position:relative; margin-bottom: 145px; border-top:1px solid #ccc;border-bottom:1px solid #ccc;">

            <canvas id="xOverMap" width="1100" height="720" style="background-color:#000; overflow:hidden; position:absolute;"></canvas>

            <div class="clearfix"></div>
        </div>

        <div class="clearfix"></div>
    </div>
</div>