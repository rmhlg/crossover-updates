<form autocomplete="off" onsubmit="return validate(this)" action="<?= SITE_URL ?>refer" method="POST">
	<div class="homePage">
	    <div class="section" id="home_banner" style="background-image:url(<?= ASSETS_URL ?>img/california/home_banner1024.jpg)">

	        <div class="box">
	            <h3>REFER A FRIEND</h3>

	            <p>Invite your adult smoker friends to be part of the MARLBORO community and be rewarded with 300 points when your friend signs up and logs on.</p>
	            <p>The more friends you invite, the higher the chance for you to win in our promos!</p>


	            <div id="error" style="display: none"><span><?= $errors ?></span></div>

	            <ul class="referrals">
		            <?php if($post): ?>
		            	<?php foreach($post['name'] as $k => $v): ?>
						<li>
							<input autocomplete="off" type="text" value="<?= $v ?>" name="name[]" placeholder="name" class="req input">
							<input autocomplete="off" type="text" value="<?= $post['email'][$k] ?>" name="email[]" placeholder="email address" class="req input">
						</li>
		            	<?php endforeach; ?>
		            <?php else: ?>
					<li>
						<input autocomplete="off" type="text" name="name[]" placeholder="name" class="req input">
						<input autocomplete="off" type="text" name="email[]" placeholder="email address" class="req input">
					</li>
		      	<?php endif; ?>
	      	</ul>
	            
	            <button class="button-small add fr"> Invite More</button>

	            <div class="clearfix"></div>
	            <div class="misc">
	                <input type="checkbox" class="req" value="true"> I confirm that the person I am inviting to be part of the MARLBORO community is a legal age (min 18 years old) smoker residing in the Philippines, and that the email provided is used solely by him/her.
	            </div>

	            <button class="button">SUBMIT</button>
	        </div>


	    </div>
	</div>
</form>