<ul class="referrals">
	<?php if(isset($result['success'])) : $names = ''; ?>
	<h2>Success</h2>
	<?php foreach($result['success'] as $r) :  $names .= $r['name'].', ';?>
	<li>
		<i class="glyphicon glyphicon-ok text-success"></i> 
		<?=$r['email'];?> 
	</li>
	<?php endforeach; ?>

	<small>You have successfully invited  <?=substr($names, 0,-2)?> to be part of the MARLBORO community. 
	You will receive a notification with your points when his/her registration has been completed and fully verified.</small>
	<?php  endif; ?> 



<?php if(isset($result['registered'])) :?>
</ul><ul class="referrals failed">
<?php  foreach($result['registered'] as $r) :  ; ?>
	<li>
		<i class="glyphicon glyphicon-remove text-danger"></i> 
		<?=$r['email']?> 
		<small><?=$r['name']?> is already a member.</small>
	</li>
	<?php endforeach; endif; ?> 


	<?php if(isset($result['pending'])) : ?>
	<?php  foreach($result['pending'] as $r) :  ?>
	<li>
		<i class="glyphicon glyphicon-remove text-danger"></i> 
		<?=$r['email']?> 
		<small><?=$r['name']?> was already sent an invitation.</small>
	</li>

<?php endforeach;  endif; ?> 

<?php if(isset($result['mailing_failed'])) : ?>
	<?php  foreach($result['mailing_failed'] as $r) :  ?>
	<li>
		<i class="glyphicon glyphicon-remove text-danger"></i> 
		<?=$r['email']?> 
		<small><?=$r['message']?></small>
	</li>

<?php endforeach;  endif; ?> 

</ul>

<a  class="button" href="<?= BASE_URL ?>refer"><i>OK</i></a>