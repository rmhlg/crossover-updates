<!-- CONTENT -->

<div class="fullProfile">
    <div class="topBanner">
        <div class="inner">
            <div class="userData">

                <div class="collectedKm">
                    <span data-i18n="[html]dataUser.collected"></span>
                    <p data-i18n="mobile.profile.nav.counterCollect"></p>
                </div>

                <div class="profileImg" data-toggle="modal" data-target="#uploadPhoto">

                    <img data-i18n="[src]dataUser.profileImage;[alt]dataUser.name" />
                    <!--    At the beginning, the user hasn't a profile image, so
                                    replace the image by these <span>
                                <span class="withOutProfileImage"></span>
                     -->
                </div>

                <div class="creditKm">
                    <span data-i18n="[html]dataUser.credit"></span>
                    <p data-i18n="mobile.profile.nav.counterCredit"></p>
                </div>
            </div>
        </div>
        <div class="profileNav">
            <ul>
                <li>
                    <a href="profile.html" data-i18n="profile.nav.journey"></a>
                </li>
                <li>
                    <a href="profile-prizes.html" data-i18n="profile.nav.prizes" class="active"></a>
                </li>
                <!--<li>
                        <a href="#" data-i18n="profile.nav.notifications"></a>
                        <span class="profileNotifications">11</span>
                    </li>
                    <li class="btnRoadTrip"><a href="roadtrip.html" data-i18n="[html]profile.nav.roadtrip"></a></li>-->
            </ul>
        </div>
    </div>
    <!-- end .topBanner -->

    <div class="container">
        <div class="row">
            <div class="col-md-7 mainProfileContent prizes">
                <h1 data-i18n="[html]profile.prizes.title">Prizes <b>claimed</b></h1>
                <p data-i18n="[html]profile.prizes.subtitle">Fusce nec odio et <b>mi ornare hendrerit quis non libero</b>.
                    <br>Aliquam eget eros non quam volutpat porta ut in felis.</p>
                <div class="expandedContent">
                    <div class="checked">
                        <div class="summary">
                            <div class="col-6">
                                <div class="expDetail">
                                    <span class="icon bicycle"></span>
                                    <h4 data-i18n="alaska.one.name"></h4>
                                    <span class="location"><span data-i18n="alaska.surname"></span> / <span data-i18n="alaska.location"></span></span>
                                </div>
                                <div class="illu">
                                    <div class="figure" style="background: url(img/teaser_activities/alaska/slider1.jpg)"></div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="kmDetail">
                                    <span data-i18n="[html]profile.journey.experience1.0.totalOfKmWon"></span>
                                    <p data-i18n="modalProfile.counterCollect"></p>

                                </div>
                                <div class="btnContainer">
                                    <button class="btnToExpand">
                                        <span class="openDetails" data-i18n="[html]profile.btn.open"></span>
                                        <span class="closeDetails" data-i18n="[html]profile.btn.close"></span>
                                    </button>
                                </div>

                            </div>
                        </div>
                        <div class="details">
                            <p>
                                <span data-i18n="[html]profile.journey.details.activityUnlocked"></span>
                                <a href="activities.html" data-i18n="[html]profile.journey.experience1.0.nameOfActivityUnlocked"></a>
                                <span class="kmWon" data-i18n="[html]profile.journey.experience1.0.kmWonAfterUnlocked"></span>
                            </p>

                            <p>
                                <span data-i18n="[html]profile.journey.details.acitivityChoices"></span>
                                <a href="activities.html" data-i18n="[html]profile.journey.experience1.0.nameOfAcitivityChoices"></a>
                                <span class="kmWon" data-i18n="[html]profile.journey.experience1.0.kmWonAfterChoiced"></span>
                            </p>

                            <p>
                                <span data-i18n="[html]profile.journey.details.gamePlayed"></span>
                                <a href="activities.html" data-i18n="[html]profile.journey.experience1.0.numberofGamePlayed"></a>
                                <span class="kmWon" data-i18n="[html]profile.journey.experience1.0.kmWonAfterplayed"></span>
                            </p>
                        </div>
                    </div>

                    <div class="locked">
                        <div class="summary">
                            <div class="col-6">
                                <div class="expDetail">
                                    <span class="icon bicycle"></span>
                                    <h4 data-i18n="alaska.one.name"></h4>
                                    <span class="location"><span data-i18n="alaska.surname"></span> / <span data-i18n="alaska.location"></span></span>
                                </div>
                                <div class="illu">
                                    <div class="figure" style="background: url(img/teaser_activities/alaska/slider1.jpg)"></div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="kmDetail">
                                    <span data-i18n="[html]profile.journey.experience1.0.totalOfKmWon"></span>
                                    <p data-i18n="modalProfile.counterCollect"></p>

                                </div>
                                <div class="btnContainer">
                                    <button class="btnToExpand">
                                        <span class="openDetails" data-i18n="[html]profile.btn.open"></span>
                                        <span class="closeDetails" data-i18n="[html]profile.btn.close"></span>
                                    </button>
                                </div>

                            </div>
                        </div>
                        <div class="details">
                            <p>
                                <span data-i18n="[html]profile.journey.details.activityUnlocked"></span>
                                <a href="activities.html" data-i18n="[html]profile.journey.experience1.0.nameOfActivityUnlocked"></a>
                                <span class="kmWon" data-i18n="[html]profile.journey.experience1.0.kmWonAfterUnlocked"></span>
                            </p>

                            <p>
                                <span data-i18n="[html]profile.journey.details.acitivityChoices"></span>
                                <a href="activities.html" data-i18n="[html]profile.journey.experience1.0.nameOfAcitivityChoices"></a>
                                <span class="kmWon" data-i18n="[html]profile.journey.experience1.0.kmWonAfterChoiced"></span>
                            </p>

                            <p>
                                <span data-i18n="[html]profile.journey.details.gamePlayed"></span>
                                <a href="activities.html" data-i18n="[html]profile.journey.experience1.0.numberofGamePlayed"></a>
                                <span class="kmWon" data-i18n="[html]profile.journey.experience1.0.kmWonAfterplayed"></span>
                            </p>
                        </div>
                    </div>

                    <div class="checked">
                        <div class="summary">
                            <div class="col-6">
                                <div class="expDetail">
                                    <span class="icon bicycle"></span>
                                    <h4 data-i18n="alaska.one.name"></h4>
                                    <span class="location"><span data-i18n="alaska.surname"></span> / <span data-i18n="alaska.location"></span></span>
                                </div>
                                <div class="illu">
                                    <div class="figure" style="background: url(img/teaser_activities/alaska/slider1.jpg)"></div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="kmDetail">
                                    <span data-i18n="[html]profile.journey.experience1.0.totalOfKmWon"></span>
                                    <p data-i18n="modalProfile.counterCollect"></p>

                                </div>
                                <div class="btnContainer">
                                    <button class="btnToExpand">
                                        <span class="openDetails" data-i18n="[html]profile.btn.open"></span>
                                        <span class="closeDetails" data-i18n="[html]profile.btn.close"></span>
                                    </button>
                                </div>

                            </div>
                        </div>
                        <div class="details">
                            <p>
                                <span data-i18n="[html]profile.journey.details.activityUnlocked"></span>
                                <a href="activities.html" data-i18n="[html]profile.journey.experience1.0.nameOfActivityUnlocked"></a>
                                <span class="kmWon" data-i18n="[html]profile.journey.experience1.0.kmWonAfterUnlocked"></span>
                            </p>

                            <p>
                                <span data-i18n="[html]profile.journey.details.acitivityChoices"></span>
                                <a href="activities.html" data-i18n="[html]profile.journey.experience1.0.nameOfAcitivityChoices"></a>
                                <span class="kmWon" data-i18n="[html]profile.journey.experience1.0.kmWonAfterChoiced"></span>
                            </p>

                            <p>
                                <span data-i18n="[html]profile.journey.details.gamePlayed"></span>
                                <a href="activities.html" data-i18n="[html]profile.journey.experience1.0.numberofGamePlayed"></a>
                                <span class="kmWon" data-i18n="[html]profile.journey.experience1.0.kmWonAfterplayed"></span>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="btnCentered">
                    <a href="gifts.html" data-i18n="profile.prizes.linkToGiftCatalog" class="btn"></a>
                </div>

            </div>
            <div class="col-sm-12 col-md-4 col-md-offset-1 sideBar col-xs-12 profile">
                <div class="col-sm-6  col-md-12">
                    <div class="inputAddCode">
                        <form action="">
                            <div class="inputContainer">
                                <input type="text" name="personalCode" placeholder="Your code here" value="Your code here" maxlength="8">
                                <button type="submit" disabled></button>
                            </div>

                            <div class="notifMsg">
                                <div class="congrats">
                                    <span data-i18n="modalProfile.validCode"></span>
                                    <strong data-i18n="modalProfile.validCodeStrong"></strong>
                                </div>
                                <div class="error">
                                    <span data-i18n="modalProfile.wrongCode"></span>
                                    <strong data-i18n="modalProfile.wrongCodeStrong"></strong>
                                </div>
                            </div>
                        </form>

                        <a href="#" data-video="trailer">
                            <span class="ico_tooltip question"></span><span data-i18n="[html]modalProfile.helpWhereFindCode"></span>
                        </a>

                    </div>
                    <!-- end .inputAddCode -->

                    <div class="listLink">
                        <ul>
                            <li>
                                <a href="invite-friend.html">
                                    <span class="ico inviteFriend"></span>
                                    <span data-i18n="[html]profile.sideBar.links.inviteFriend"></span>
                                </a>
                            </li>
                            <li>
                                <a href="events.html">
                                    <span class="ico agenda"></span>
                                    <span data-i18n="[html]profile.sideBar.links.calendar"></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <!-- end .listLink-->
                </div>

                <div class="col-sm-6 col-md-12">
                    <div class="history">
                        <h2 data-i18n="[html]profile.sideBar.history.title"></h2>
                        <ul>
                            <li><span data-i18n="[html]profile.sideBar.history.packCode"></span> <b data-i18n="dataUser.packCode"></b>
                            </li>
                            <li><span data-i18n="[html]profile.sideBar.history.checkIn"></span> <b data-i18n="dataUser.checkIn"></b>
                            </li>
                            <li><span data-i18n="[html]profile.sideBar.history.event"></span> <b data-i18n="dataUser.event"></b>
                            </li>
                            <li><span data-i18n="[html]profile.sideBar.history.shared"></span> <b data-i18n="dataUser.shared"></b>
                            </li>
                        </ul>
                    </div>
                    <!-- end .history-->
                </div>
            </div>

            <!-- end .mainProfileContent-->
        </div>
    </div>
</div>
<!-- end .fullProfile -->