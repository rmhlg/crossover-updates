<section class="section fullscreen active">
    <div class="howToPlay trianglesArrows">
        <h2><span data-i18n="howTo.title"></span></h2>
        <div class="pannel events">
            <div class="element"></div>
            <div class="infos">
                <div class="row">
                    <div class="col-sm-5 col-sm-offset-1 col-md-6 col-md-offset-0 title">
                        <span data-i18n="howTo.slide1.category"></span>
                        <strong data-i18n="[html]howTo.slide1.headline"></strong>
                    </div>
                    <div class="col-sm-5 col-md-5 detail" data-i18n="[html]howTo.slide1.description"></div>
                </div>
            </div>
        </div>
        <div class="pannel packCode">
            <div class="element"></div>
            <div class="infos">
                <div class="row">
                    <div class="col-sm-5 col-sm-offset-1 col-md-6 col-md-offset-0 title">
                        <span data-i18n="howTo.slide2.category"></span>
                        <strong data-i18n="[html]howTo.slide2.headline"></strong>
                    </div>
                    <div class="col-sm-5 col-md-5 detail" data-i18n="[html]howTo.slide2.description"></div>
                </div>
            </div>
        </div>
        <div class="pannel events">
            <div class="element"></div>
            <div class="infos">
                <div class="row">
                    <div class="col-sm-5 col-sm-offset-1 col-md-6 col-md-offset-0 title">
                        <span data-i18n="howTo.slide1.category"></span>
                        <strong data-i18n="[html]howTo.slide1.headline"></strong>
                    </div>
                    <div class="col-sm-5 col-md-5 detail" data-i18n="[html]howTo.slide1.description"></div>
                </div>
            </div>
        </div>
        <div class="pannel events">
            <div class="element"></div>
            <div class="infos">
                <div class="row">
                    <div class="col-sm-5 col-sm-offset-1 col-md-6 col-md-offset-0 title">
                        <span data-i18n="howTo.slide2.category"></span>
                        <strong data-i18n="[html]howTo.slide2.headline"></strong>
                    </div>
                    <div class="col-sm-5 col-md-5 detail" data-i18n="[html]howTo.slide2.description"></div>
                </div>
            </div>
        </div>
        <div class="pannel events">
            <div class="element"></div>
            <div class="infos">
                <div class="row">
                    <div class="col-sm-5 col-sm-offset-1 col-md-6 col-md-offset-0 title">
                        <span data-i18n="howTo.slide1.category"></span>
                        <strong data-i18n="[html]howTo.slide1.headline"></strong>
                    </div>
                    <div class="col-sm-5 col-md-5 detail" data-i18n="[html]howTo.slide1.description"></div>
                </div>
            </div>
        </div>
    </div>
</section>