<!-- CONTENT -->
    <div class="fullProfile">
        <div class="topBanner">
            <div class="inner">
                <div class="userData">
                    <div class="collectedKm">
                        <span><strong><?= $this->session->userdata('total_km_points') ?> </strong>km</span>
                        <p>collected killometers</p>
                    </div>

                    <div class="profileImg" data-toggle="modal" data-target="#uploadPhoto">

                        <img src="<?= $this->session->userdata('profile_picture') ?>" alt"user name" />
                        <!--    At the beginning, the user hasn't a profile image, so
                                replace the image by these <span>
                            <span class="withOutProfileImage"></span>
                         -->
                    </div>

                    <div class="creditKm">
                        <span><strong><?= $registrants->credit_km_points ?></strong></span>
                        <p>credit kilometers</p>
                    </div>
                </div>
                <div class="hidden-xs userName">
                    <p class="name"><?= $registrants->first_name." ".$registrants->third_name ?></p>
                    <p class="place">Place</p>
                </div>
            </div>
            <div class="profileNav">
                <ul>
                    <li>
                        <a href="<?php SITE_URL ?>profile" class="active">My Journey</a>
                    </li>
                    <li>
                        <a href="profilePrizes">Claimed Prizes</a>
                    </li>
                    <!--<li>
        <a href="#" data-i18n="profile.nav.notifications"></a>
        <span class="profileNotifications">11</span>
    </li>
    <li class="btnRoadTrip"><a href="roadtrip.html" data-i18n="[html]profile.nav.roadtrip"></a></li>-->
                </ul>
            </div>

        </div>
        <!-- end .topBanner -->

        <div class="container">
            <div class="row">

                <!-- end .sideBar -->


                <div class="col-md-7 mainProfileContent">
                    <h1 data-i18n="[html]profile.journey.title"></h1>
                    <p data-i18n="[html]profile.journey.subtitle"></p>

                    <div class="expandedContent">
                        <?php foreach($experiences as $exp) {
                            $class = "locked";
                            if($exp->category_id==1){
                                $region = "california";
                                if($registrants->total_km_points>=10 and $registrants->total_km_points<=ALASKA_START_POINTS){
                                    $class = "checked";
                                }
                            }else if($exp->category_id==2){
                                $region = "alaska";
                                if($registrants->total_km_points>=ALASKA_START_POINTS and $registrants->total_km_points<=CANADA_START_POINTS){
                                    $class = "checked";
                                }
                            }else if($exp->category_id==3){
                                $region = "canada";
                                if($registrants->total_km_points>CANADA_START_POINTS){
                                    $class = "checked";
                                }
                            }
                        ?>
                        <div class="<?= $class ?>">
                            <div class="summary">
                                <div class="col-6">
                                    <div class="expDetail">
                                        <span class="icon bicycle"></span>
                                        <h4><?= $exp->title ?></h4>
                                        <span class="location"><!-- <span data-i18n="alaska.surname"></span> / --> <span><?= $region ?></span></span>
                                    </div>
                                    <div class="illu">
                                        <div class="figure" style="background: url(<?= $exp->thumbnail  ?>)"></div>
                                    </div>
                                </div>
                                <div class="col-6"> 
                                    <div class="kmDetail">
                                        <span><?= $exp->km_points ?></span>
                                        <p data-i18n="modalProfile.counterCollect"></p>

                                    </div>
                                    <div class="btnContainer">
                                        <?php if($class=="locked"){
                                        ?>
                                        <button class="btnToExpand" style="border: 1px solid #b3b3b3">
                                            <span style="color: #b3b3b3 !important;">Locked</span>  
                                        </button>
                                        <?php
                                        } else { ?>
                                        <button class="btnToExpand" onClick="window.location='<?= BASE_URL."activities/".$exp->experience_id ?>'">
                                            <span class="openDetails" data-i18n="[html]profile.btn.open"></span>
                                            <span class="closeDetails" data-i18n="[html]profile.btn.close"></span>
                                        </button>
                                        <?php } ?>
                                    </div>

                                </div>
                            </div>
                            <div class="details">
                                <p>
                                    <span data-i18n="[html]profile.journey.details.activityUnlocked"></span>
                                    <a href="activities.html" data-i18n="[html]profile.journey.experience1.0.nameOfActivityUnlocked"></a>
                                    <span class="kmWon" data-i18n="[html]profile.journey.experience1.0.kmWonAfterUnlocked"></span>
                                </p>

                                <p>
                                    <span data-i18n="[html]profile.journey.details.acitivityChoices"></span>
                                    <a href="activities.html" data-i18n="[html]profile.journey.experience1.0.nameOfAcitivityChoices"></a>
                                    <span class="kmWon" data-i18n="[html]profile.journey.experience1.0.kmWonAfterChoiced"></span>
                                </p>

                                <p>
                                    <span data-i18n="[html]profile.journey.details.gamePlayed"></span>
                                    <a href="activities.html" data-i18n="[html]profile.journey.experience1.0.numberofGamePlayed"></a>
                                    <span class="kmWon" data-i18n="[html]profile.journey.experience1.0.kmWonAfterplayed"></span>
                                </p>
                            </div>
                        </div>
                        <?php } ?>
                        <!-- end expanded content -->
                    </div>
                </div>
                <div class="col-sm-12 col-md-4 col-md-offset-1 col-xs-12 sideBar profile">
                    <div class="col-sm-6  col-md-12">
                        <div class="inputAddCode">
                            <form method="POST" action="#" onsubmit="return validateCodeProfile()" id="form-profile">

                                <div class="inputContainer">                        
                                    <input type="text" name="personalCode" placeholder="Your code here" value="Your code here" maxlength="8" >              
                                    <button type="submit" disabled></button>        
                                </div>

                                <div class="notifMsg" style="display: none">
                                <!-- the code is correct -->
                                <div class="congrats" style="display: none">
                                    <span></span> 
                                    <strong></strong>
                                </div>
                                <!-- the code is wrong -->
                                <div class="error" style="display: none">
                                    <span></span> 
                                    <strong></strong>
                                </div>
                            </div>              
                                                        
                                <!-- <div class="notifMsg">
                                    <div class="congrats">
                                        <span data-i18n="modalProfile.validCode"></span> 
                                        <strong data-i18n="modalProfile.validCodeStrong"></strong>
                                    </div>
                                    <div class="error">
                                        <span data-i18n="modalProfile.wrongCode"></span> 
                                        <strong data-i18n="modalProfile.wrongCodeStrong"></strong>
                                    </div>
                                </div> -->
                            </form>

                            <a href="#" data-video="trailer">
                                <span class="ico_tooltip question"></span><span data-i18n="[html]modalProfile.helpWhereFindCode"></span>
                            </a>

                        </div>
                        <!-- end .inputAddCode -->

                        <div class="listLink">
                            <ul>
                                <li>
                                    <a href="invite-friend.html">
                                        <span class="ico inviteFriend"></span>
                                        <span data-i18n="[html]profile.sideBar.links.inviteFriend"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="events">
                                        <span class="ico agenda"></span>
                                        <span data-i18n="[html]profile.sideBar.links.calendar"></span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <!-- end .listLink-->
                    </div>

                    <div class="col-sm-6 col-md-12">
                        <div class="history">
                            <h2 data-i18n="[html]profile.sideBar.history.title"></h2>
                            <ul>
                                <li><span data-i18n="[html]profile.sideBar.history.packCode"></span> <b><?= $packCode ?></b>
                                </li>
                                <li><span data-i18n="[html]profile.sideBar.history.checkIn"></span> <b><?= $checkIn ?></b>
                                </li>
                                <li><span data-i18n="[html]profile.sideBar.history.event"></span> <b><?= $event ?></b>
                                </li>
                                <li><span data-i18n="[html]profile.sideBar.history.shared"></span> <b><?= $shared ?></b>
                                </li>
                            </ul>
                        </div>
                        <!-- end .history-->
                    </div>
                </div>
                <!-- end .mainProfileContent -->

            </div>
        </div>
    </div>
    <!-- end .fullProfile -->s