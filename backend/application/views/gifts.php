 <!-- lytebox gift -->

<div class="lytebox-wrap">
    <div class="lytebox-content-holder">
        <div class="lytebox-wrapped-content" id="giftmodal">
            <div id="preLoader" style="
                position: absolute;
                background: white;
                width: 100%;
                height: 100%;
                top: 0px;
                left: 0px;
                z-index: 9999;
            "><img src="http://localhost/marlboro/marlboro-crossover/backend/assets/images/preloader.gif" style="
                position: absolute;
                top: 150px;
                left: 47%;
            "><br> &nbsp; &nbsp; &nbsp; Loading...</div>
            <div class="content txtcenter">
                <button type="button" class="popup-close">&times;</button>


                <!-- Picture of the Gift -->
                <div class="col-sm-6 picture">
                    <img src="" id="popUpImg" alt="">
                </div>

                <div class="col-sm-6 contents ">
                    <div class="row gift-popup-slider">
                        <!-- STEP 1 : More infos about the gifts -->
                        <div class="col-xs-4 moreInfos">
                            <h4 class="modal-title">
                            <span id="popUpGiftName"></span>
                            / <strong id="popUpKm"></strong><i>km</i>
                        </h4>

                            <p class="subtitle" id="popUpDescription"></p>
                            <button class="btn" id="iwantthis" type="button" style="padding: 12px 30px 28px 30px; top: 105px;">I want this</button>

                        </div>
                        <!-- end step1 -->

                        <!-- STEP 2 : Confirm you adress -->
                        <div class="col-xs-4 confirm">
                            <h4 class="modal-title"><span>Confirm</span> Your adress</h4>
                            <form class="confirmAdress" action="">
                                <div class="line">
                                    <label for="street">street</label>
                                    <input type="text" name="street" id="street">
                                </div>

                                <div class="line">
                                    <label for="number">Barangay:</label>
                                    <input type="text" name="brgy" id="brgy">
                                </div>

                                <div class="line">
                                    <label for="city">City:</label>
                                    <input type="text" name="city" id="city">
                                </div>

                                <div class="line">
                                    <label for="zip">Province:</label>
                                    <input type="text" name="province" id="province">
                                </div>

                                <div class="line">
                                    <label for="zip">Postal Code:</label>
                                    <input type="text" name="postal" id="postal" />
                                </div>

                                <br />
                                <br />
                                <button class="btn" onClick="insertIWantThis($(this))" type="button" id="giftconfirm">Confirm</button>
                            </form>
                        </div>
                        <!-- end step2 -->

                        <!-- STEP 3 : statut file -->
                        <div class="col-xs-4 statut">
                            <div class="thanks">
                                <h4 class="modal-title"><span>thank</span> <strong>you</strong></h4>
                                <p class="subtitle"><span>LOREM IPSUM</span> AMAT QUID.</p>
                                <p>Fusce id est luctus, vulputate justo in, estut tempus arcu lorem ipsum.</p>
                                <button aria-label="Close" data-dismiss="modal" class="btn closeLyte" type="button" style="padding: 12px 30px 28px 30px; top: 105px;">Back to list</button>
                            </div>
                            <!-- end valid -->

                            <!-- Error -->
                            <div style="display: none" class="serverError">
                                <h4><span>Error</span></h4>
                                <p>Fusce id est luctus, vulputate justo in, estut tempus arcu lorem ipsum.</p>
                                <button aria-label="Close" data-dismiss="modal" class="btn" type="button"></button>
                            </div>
                            <!-- end Error -->
                        </div>
                    </div>
                    <!-- end modal-body -->
                </div>
                <!-- end col-sm-6 -->
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<section class="section giftsCatalogue">
    <div class="banner">
        <h2>
        <span>
            <strong data-i18n="gifts.title"></strong>
            <span>
                <span data-i18n="gifts.subtitle"></span>
                <span data-i18n="catalogue.youHave"></span>
                <span><strong id="creditKm"><?= $registrant->credit_km_points ?></strong></span>
                <span data-i18n="catalogue.available"></span>
            </span>
        </span>
    </h2>
        <?php
        
            $fill = "";
            $knob = "";
            $fillNum = 0;
            $knobNum = 0;
            $california = "";
            $canada = "";
            $alaska = "";
            if($registrant->total_km_points>=600 and $registrant->total_km_points<=1399){
                $california = "active";
                $canada = "locked";
                $alaska = "locked";
                if($registrant->total_km_points==600){
                    $fillNum = 17;
                    $knobNum = 16.5;
                }else if($registrant->total_km_points>=601 and $registrant->total_km_points<=700){
                    $fillNum = 18;
                    $knobNum = 17.5;
                }else if($registrant->total_km_points>=701 and $registrant->total_km_points<=800){
                    $fillNum = 20;
                    $knobNum = 19.5;
                }else if($registrant->total_km_points>=801 and $registrant->total_km_points<=900){
                    $fillNum = 25;
                    $knobNum = 24.5;
                }else if($registrant->total_km_points>=901 and $registrant->total_km_points<=1000){
                    $fillNum = 30;
                    $knobNum = 29.5;
                }else if($registrant->total_km_points>=1001 and $registrant->total_km_points<=1100){
                    $fillNum = 35;
                    $knobNum = 34.5;
                }else if($registrant->total_km_points>=1101 and $registrant->total_km_points<=1200){
                    $fillNum = 40;
                    $knobNum = 39.5;
                }else if($registrant->total_km_points>=1201 and $registrant->total_km_points<=1300){
                    $fillNum = 45;
                    $knobNum = 44.5;
                }else if($registrant->total_km_points>=1301 and $registrant->total_km_points<1400){
                    $fillNum = 48;
                    $knobNum = 4.5;
                }
            }else if($registrant->total_km_points>=1400 and $registrant->total_km_points<=3999){
                $alaska = "active";
                $california = "active";
                $canada = "locked";
                if($registrant->total_km_points==1400){
                    $fillNum = 50;
                    $knobNum = 49.5;
                }else if($registrant->total_km_points>=1401 and $registrant->total_km_points<=1700){
                    $fillNum = 51;
                    $knobNum = 50.5;
                }else if($registrant->total_km_points>=1701 and $registrant->total_km_points<=2000){
                    $fillNum = 53;
                    $knobNum = 52;
                }else if($registrant->total_km_points>=2001 and $registrant->total_km_points<=2300){
                    $fillNum = 55;
                    $knobNum = 54;
                }else if($registrant->total_km_points>=2301 and $registrant->total_km_points<=2600){
                    $fillNum = 60;
                    $knobNum = 59;
                }else if($registrant->total_km_points>=2601 and $registrant->total_km_points<=2900){
                    $fillNum = 65;
                    $knobNum = 64;
                }else if($registrant->total_km_points>=2901 and $registrant->total_km_points<=3200){
                    $fillNum = 70;
                    $knobNum = 69;
                }else if($registrant->total_km_points>=3201 and $registrant->total_km_points<=3500){
                    $fillNum = 75;
                    $knobNum = 74;
                }else if($registrant->total_km_points>=3501 and $registrant->total_km_points<3999){
                    $fillNum = 80;
                    $knobNum = 79;
                }else if($registrant->total_km_points==3999){
                    $fillNum = 82;
                    $knobNum = 81;
                }
            }else if($registrant->total_km_points>=3999){
                $canada = "active";
                $alaska = "active";
                $california = "active";
                if($registrant->total_km_points==3999){
                    $fillNum = 84;
                    $knobNum = 83;
                }else if($registrant->total_km_points>=4000 and $registrant->total_km_points<4100){
                    $fillNum = $fillNum + 88;
                    $knobNum = $knobNum + 87;
                }else if($registrant->total_km_points>=4101 and $registrant->total_km_points<4300){
                    $fillNum = $fillNum + 92;
                    $knobNum = $knobNum + 91;
                }else if($registrant->total_km_points>=4301){
                    $fillNum = $fillNum + 100;
                    $knobNum = $knobNum + 99;
                }
            }else{
                $california = "active";
                $alaska = "locked";
                $canada = "locked";
                $fillNum = $fillNum + 1;
                $knobNum = $knobNum + 1;
            }
            $fill = $fillNum."%";
            $knob = $knobNum."%";
        ?>
        <div class="level">
            <div class="segments">
                <div onClick="filterGift(1)" id="region1" data-region-id="1" class="<?= $california ?>">
                    <span>California</span>
                    <span>
                        <img src="<?= ASSETS_URL ?>img/gifts/hat.png" alt="">
                    </span>
                    <span>prize</span>
                </div>
                <div onClick="filterGift(2)" id="region2" data-region-id="2" class="<?= $alaska ?>">
                    <span>Alaska</span>
                    <span><img src="<?= ASSETS_URL ?>img/gifts/hat.png" alt=""></span>
                    <span>prize</span>
                </div>
                <div onClick="filterGift(3)" id="region3" data-region-id="3" class="<?= $canada ?>">
                    <span>Canada</span>
                    <span><img src="<?= ASSETS_URL ?>img/gifts/hat.png" alt=""></span>
                    <span>prize</span>
                </div>
            </div>
            <div class="range">
                <span class="rail"></span>
                <span class="fill" style="width: <?= $fill ?>;"></span>
                <span class="knob" style="left: <?= $knob ?>;"></span>
            </div>
        </div>
    </div>
    <!--/ .banner -->

    <div class="container">

        <div class="catalogue" style="width: 100%">
            <?php
            $ctr = 0;
            $count = 0;
            foreach($gifts as $gift){
                $class = "";
                $clickableButton = true;
                $buttonTxt = "I want this";

                if($gift->stock==0){
                    $class = "outStock";
                    $clickableButton = false;
                    $buttonTxt = "OUT OF STOCK";
                }else if(in_array($gift->gift_id, $claimed)){
                    $class = "claimed";
                    $clickableButton = false;
                    $buttonTxt = "CLAIMED";
                }else if($registrant->credit_km_points<$gift->km_points or $registrant->credit_km_points<$gift->minimum_km_points_required){
                    $class = "locked";
                    $clickableButton = false;
                    $buttonTxt = "LOCKED";
                }else if($gift->type==2){
                    $class = "premium";
                }

                $count++;
                if($ctr==0){?>
                    <div class="row">
                        <div id="popUpWrap<?= $gift->gift_id ?>" class="gift col-md-4 <?= $class ?>">
                            <div class="picture">
                                <img src="<?= UPLOADS_URL ?><?= $gift->image ?>" alt="<?= $gift->name ?>" />
                            </div>
                            <div class="infos">
                                <p class="name"><span><?= $gift->name ?></span><span class="km">/ <span><?= $gift->km_points ?></span> km</span>
                                </p>
                                <p class="description"><?= substr($gift->description, 0, 120)."..." ?></p>
                                <?php if($clickableButton) { ?>
                                    <a href="#" id="popUpButton<?= $gift->gift_id ?>" onclick="loadGiftInfo(<?= $gift->gift_id ?>)" <?php if($clickableButton) echo 'class="iWantButton"' ?> data-giftId="<?= $gift->gift_id ?>"><?= $buttonTxt ?></a>
                                <?php } else { ?>
                                    <p class="cantClick"><?= $buttonTxt ?></p>
                                <?php } ?>
                            </div>
                        </div>
                <?php 
                    if($count==count($gifts)){
                        echo "</div>";
                    }
                    $ctr=1;
                }else if($ctr==1){ ?>
                    <div id="popUpWrap<?= $gift->gift_id ?>" class="gift col-md-4 <?= $class ?>">
                        <div class="picture">
                            <img src="<?= UPLOADS_URL ?><?= $gift->image ?>" alt="<?= $gift->name ?>" />
                        </div>
                        <div class="infos">
                            <p class="name"><span><?= $gift->name ?></span><span class="km">/ <span><?= $gift->km_points ?></span> km</span>
                            </p>
                            <p class="description"><?= substr($gift->description, 0, 120)."..." ?></p>
                            <?php if($clickableButton) { ?>
                                <a href="#" id="popUpButton<?= $gift->gift_id ?>" onclick="loadGiftInfo(<?= $gift->gift_id ?>)" <?php if($clickableButton) echo 'class="iWantButton"' ?> data-giftId="<?= $gift->gift_id ?>"><?= $buttonTxt ?></a>
                            <?php } else { ?>
                                    <p class="cantClick"><?= $buttonTxt ?></p>
                            <?php } ?>
                         </div>
                    </div>
                <?php 
                    if($count==count($gifts)){
                        echo "</div>";
                    }
                    $ctr=2;
                }else { ?>
                    <div id="popUpWrap<?= $gift->gift_id ?>" class="gift col-md-4 <?= $class ?>">
                        <div class="picture">
                            <img src="<?= UPLOADS_URL ?><?= $gift->image ?>" alt="<?= $gift->name ?>" />
                        </div>
                        <div class="infos">
                            <p class="name"><span><?= $gift->name ?></span><span class="km">/ <span><?= $gift->km_points ?></span> km</span>
                            </p>
                            <p class="description"><?= substr($gift->description, 0, 120)."..." ?></p>
                            <?php if($clickableButton) { ?>
                                <a href="#" id="popUpButton<?= $gift->gift_id ?>" onclick="loadGiftInfo(<?= $gift->gift_id ?>)" <?php if($clickableButton) echo 'class="iWantButton"' ?> data-giftId="<?= $gift->gift_id ?>"><?= $buttonTxt ?></a>
                            <?php } else { ?>
                                    <p class="cantClick"><?= $buttonTxt ?></p>
                            <?php } ?>
                         </div>
                    </div>
                </div>
                <?php $ctr=0;
                }
            }
            ?>
        </div>
        <!--/ .catalogue -->
    </div>
    <!--/ .container -->
</section>