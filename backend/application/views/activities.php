        <!-- CONTENT -->
        <!-- PRESENTATION OF THE ACTIVITY -->
        <section class="section splash fullscreen">
            <div class="background" style="background-image:url(<?= $row->media ?>)"></div>

            <div class="title">
                <div class="nav">
                    <span class="control prev"></span>
                    <span class="icon bicycle"></span>
                    <span class="control next"></span>
                </div>
                <div class="subtitle">
                    <div class="stroked">EXPERIENCE</div>
                </div>
                <h1><span><?= $row->experience_title ?></span></h1>
            </div>

            <div class="weather">
                <b>
                    <?php if(unserialize(REGIONS_ALL)[$row->category_id] == 'California'): ?>
                        KACHEMAK BAY
                    <?php elseif(unserialize(REGIONS_ALL)[$row->category_id] == 'Alaska'): ?>
                        KACHEMAK BAY
                    <?php elseif(unserialize(REGIONS_ALL)[$row->category_id] == 'Canada'): ?>
                        KACHEMAK BAY
                    <?php endif; ?>
                </b>
                <span><?= unserialize(REGIONS_ALL)[$row->category_id] ?></span> / <span data-i18n="alaska.country"></span>
                <p><?= $row->experience_description ?></p>
            </div>

            <div class="pageName next">
                <a href="#!" data-scrollTo="act_choices">
                    <span data-i18n="activities.pageNameNextLeft"></span>
                    <span data-i18n="activities.pageNameNextRight"></span>
                    <div class="icon"></div>
                </a>
            </div>

        </section>

        <!-- DISCOVER THE 2 PROPOSITIONS -->
        <section class="section choose fullscreen" id="act_choices">

            <div class="propositions">
                <!-- Proposition n°1 -->
                <div class="col-sm-6 bgImg" data-choice="kayak">
                    <div class="background" style="background-image:url(<?= $row->left_image ?>)"></div>
                    <div class="infos">
                        <span class="icon bicycle"></span>
                        <div class="subtitle">
                            <span class="stroked">decision</span>
                        </div>
                        <h3><span><?= $row->decision_left_title ?></span></h3>
                        <div class="choiceDetails">
                            <button data-seeMore="#kayakDetails"><span>More Infos</span>
                            </button>

                            <div class="moreInfos" id="kayakDetails">
                                <p><?= $row->decision_left_description ?></p>
                            </div>
                        </div>

                    </div>
                </div>

                <!-- Proposition n°2 -->
                <div class="col-sm-6 bgImg" data-choice="paddle">
                    <div class="background" style="background-image:url(<?= $row->right_image ?>)"></div>
                    <div class="infos">
                        <span class="icon bicycle"></span>
                        <div class="subtitle">
                            <span class="stroked">decision</span>
                        </div>
                        <h3><span><?= $row->decision_right_title ?></span></h3>
                        <div class="choiceDetails">
                            <button data-seeMore="#PaddleDetails"><span>More Infos</span>
                            </button>

                            <div class="moreInfos" id="PaddleDetails">
                                <p><?= $row->decision_right_description ?></p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </section>

        <!-- MAKE A DECISION -->
        <section class="makeChoice" id="makeYourChoice">

            <div class="inner">
                <p>HOW FAR WILL YOU GO</p>
                <div class="row">
                    <!-- STEP 1: offer 2 choices to the user -->
                    <div class="question">
                        <button data-choice="<?= $row->decision_left_title ?>" onclick="earnPoints('<?= encrypt_points(ACTIVITY_READ) ?>', '<?= encrypt_points($activity_id) ?>', '<?= encrypt_points(DECISION_LEFT) ?>')"><?= $row->decision_left_title ?></button>

                        <span>OR <br>CROSSOVER</span>

                        <button data-choice="<?= $row->decision_right_title ?>" onclick="earnPoints('<?= encrypt_points(ACTIVITY_READ) ?>', '<?= encrypt_points($activity_id) ?>', '<?= encrypt_points(DECISION_RIGHT) ?>')"><?= $row->decision_right_title ?></button>

                        <p data-i18n="activities.choice.youDecide"></p>
                    </div>

                    <!-- STEP 2: summary with the user's response -->
                    <div class="reponse">
                        <p>
                            <i data-i18n="activities.choice.decideActivity2"></i>
                            <b data-i18n="activities.choice.activity2"></b>
                        </p>
                        <br />
                        <a href="" class="change" data-i18n="activities.modify"></a>
                    </div>

                </div>
            </div>

            <!-- display an error message if the user didn't have enought km -->
            <div class="inner noAcces" style="display:none">
                <div class="stroked" data-i18n="activities.choice.noAccesSubtitle"></div>
                <p data-i18n="activities.choice.noAccesTitle"></p>
                <a href="#" class="btn" data-i18n="activities.choice.noAccesLink"></a>
            </div>

        </section>

        <!-- the content will be loaded here, when the user make his choice -->

    <!-- MODAL VIDEO -->
    <div class="modal fade modal_video" id="modalVideo">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body video">

                    <img src="" alt="" class="ratio poster">

                    <div id="jp_container_1" class="jp-video" role="application" aria-label="media player">
                        <div class="jp-type-single">
                            <div id="js-video" class="jp-jplayer"></div>
                            <div class="jp-gui">
                                <div class="jp-video-play">
                                    <button class="jp-video-play-icon" role="button" tabindex="0">play</button>
                                </div>
                                <div class="jp-interface">
                                    <div class="jp-progress">
                                        <div class="jp-seek-bar">
                                            <div class="jp-play-bar"></div>
                                        </div>
                                    </div>
                                    <div class="jp-current-time" role="timer" aria-label="time">&nbsp;</div>
                                    <div class="jp-duration" role="timer" aria-label="duration">&nbsp;</div>
                                    <div class="jp-controls-holder">
                                        <div class="jp-controls">
                                            <button class="jp-play" role="button" tabindex="0">play</button>
                                            <button class="jp-stop" role="button" tabindex="0">stop</button>
                                        </div>
                                        <div class="jp-volume-controls">
                                            <button class="jp-mute" role="button" tabindex="0">mute</button>
                                            <button class="jp-volume-max" role="button" tabindex="0">max volume</button>
                                            <div class="jp-volume-bar">
                                                <div class="jp-volume-bar-value"></div>
                                            </div>
                                        </div>
                                        <div class="jp-toggles">
                                            <button class="jp-repeat" role="button" tabindex="0">repeat</button>
                                            <button class="jp-full-screen" role="button" tabindex="0">full screen</button>
                                        </div>
                                    </div>
                                    <div class="jp-details">
                                        <div class="jp-title" aria-label="title">&nbsp;</div>
                                    </div>
                                </div>
                            </div>
                            <div class="jp-no-solution">
                                <span>Update Required</span> To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->