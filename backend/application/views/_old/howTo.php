		
			<!-- CONTENT slider How to Play-->
			<section class="section fullscreen" id="home_howToPlay">
				
				<div class="howToPlay trianglesArrows">
					<h2><span data-i18n="howTo.title"></span></h2>
					<div class="pannel events">
						<div class="element"></div>
						<div class="infos">
							<div class="row">
								<div class="col-xs-6 title">
									<span data-i18n="howTo.slide1.category"></span>
									<strong data-i18n="[html]howTo.slide1.headline"></strong>
								</div>
								<div class="col-xs-6 detail" data-i18n="[html]howTo.slide1.description"></div>
							</div>
						</div>
					</div>
					<div class="pannel packCode">
						<div class="element"></div>
						<div class="infos">
							<div class="row">
								<div class="col-xs-6 title">
									<span data-i18n="howTo.slide2.category"></span>
									<strong data-i18n="[html]howTo.slide2.headline"></strong>
								</div>
								<div class="col-xs-6 detail" data-i18n="[html]howTo.slide2.description"></div>
							</div>
						</div>
					</div>
					<div class="pannel events">
						<div class="element"></div>
						<div class="infos">
							<div class="row">
								<div class="col-xs-6 title">
									<span data-i18n="howTo.slide3.category"></span>
									<strong data-i18n="[html]howTo.slide3.headline"></strong>
								</div>
								<div class="col-xs-6 detail" data-i18n="[html]howTo.slide3.description"></div>
							</div>
						</div>
					</div>
					<div class="pannel events">
						<div class="element"></div>
						<div class="infos">
							<div class="row">
								<div class="col-xs-6 title">
									<span data-i18n="howTo.slide4.category"></span>
									<strong data-i18n="[html]howTo.slide4.headline"></strong>
								</div>
								<div class="col-xs-6 detail" data-i18n="[html]howTo.slide4.description"></div>
							</div>
						</div>
					</div>
					<div class="pannel events">
						<div class="element"></div>
						<div class="infos">
							<div class="row">
								<div class="col-xs-6 title">
									<span data-i18n="howTo.slide5.category"></span>
									<strong data-i18n="[html]howTo.slide5.headline"></strong>
								</div>
								<div class="col-xs-6 detail" data-i18n="[html]howTo.slide5.description"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="pageName next" data-i18n="[html]howTo.pageName"></div>
			</section>


			<!-- FOOTER -->
			<footer>

				<div class="container-fluid health_warning">
			        <div class="row">
			            <div class="col-md-12">
			                <p>
			                    <img src="img/footer/health_warning.jpg" alt="Rauchen ist tödlich. Fumer tue. Il fumo uccide.">
			                </p>
			            </div>
			        </div>
			    </div>
				
			</footer>	

		</div><!-- end .mainWrapper -->

		<script>
			var siteUrl = '<?=site_url()?>';
			var baseUrl = '<?=base_url()?>';
			var assetsUrl = '<?=ASSETS_URL?>';
			var segments = '<?=$this->uri->segment(2)?>';
		</script>
		<script src="<?=ASSETS_URL?>js/scripts.min.js"></script>
		<script src="<?=ASSETS_URL?>js/main.js"></script>
	</body>
</html>