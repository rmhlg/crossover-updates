
			
			<!-- CONTENT -->
			<!-- PRESENTATION OF THE ACTIVITY -->
			<section class="section splash fullscreen">
				<div class="background" style="background-image:url(<?=ASSETS_URL?>img/alaska/activities.jpg)"></div>

				<div class="title">
					<div class="nav">
						<span class="control prev"></span>
						<span class="icon bicycle"></span>
						<span class="control next"></span>
					</div>
					<div class="subtitle">
						<div class="stroked" data-i18n="activities.subtitle"></div>
					</div>		
					<h1><span data-i18n="alaska.one.name"></span></h1>
				</div>
				
				<div class="pageName next">
					<a href="#!" data-scrollTo="act_choices">
						<span data-i18n="activities.pageNameNextLeft"></span>
						<span data-i18n="activities.pageNameNextRight"></span>
						<div class="icon"></div>
					</a>
				</div>

			</section>

			<!-- DISCOVER THE 2 PROPOSITIONS -->
			<section class="section choose fullscreen" id="act_choices">

				<div class="propositions">
					<!-- Proposition n°1 -->
					<div class="col-sm-6 bgImg" data-choice="kayak">
						<div class="background" style="background-image:url(<?=ASSETS_URL?>img/alaska/bg_img_kayak.jpg)"></div>
						<div class="infos">
							<span class="icon bicycle"></span>
							<div class="subtitle">
								<span class="stroked" data-i18n="activities.choice.decide"></span>
							</div> 
							<h3><span data-i18n="activities.choice.activity1"></span></h3>

							<div class="choiceDetails">
								<div class="moreInfos" id="kayakDetails">
									<p data-i18n="[html]activities.choice.activity1_detail"></p>
								</div>
							</div>
							
						</div>
					</div>

					<!-- Proposition n°2 -->
					<div class="col-sm-6 bgImg" data-choice="paddle">
						<div class="background" style="background-image:url(<?=ASSETS_URL?>img/alaska/bg_img_paddle.jpg)"></div>
						<div class="infos">
							<span class="icon bicycle"></span>
							<div class="subtitle">
								<span class="stroked" data-i18n="activities.choice.decide"></span>
							</div> 
							<h3><span data-i18n="activities.choice.activity2"></span></h3>

							<div class="choiceDetails">								
								<div class="moreInfos" id="PaddleDetails">
									<p data-i18n="[html]activities.choice.activity1_detail"></p>
								</div>
							</div>
						</div>
					</div>

				</div>				

			</section>

			<!-- MAKE A DECISION -->
			<section class="makeChoice" id="makeYourChoice">
			
				<div class="inner">
					<p data-i18n="activities.choice.makeChoiceTitle"></p>
					<div class="row">
						<!-- STEP 1: offer 2 choices to the user -->
						<div class="question">
							<button data-choice="kayak" data-i18n="activities.choice.activity1"></button>

							<button data-choice="paddle" data-i18n="activities.choice.activity2"></button>

							<p data-i18n="activities.choice.youDecide"></p>
						</div>

						<!-- STEP 2: summary with the user's response -->
						<div class="reponse">
							<p>
								<i data-i18n="activities.choice.decideActivity2"></i>
								<b data-i18n="activities.choice.activity2"></b>
							</p><br />
							<a href="" class="change" data-i18n="activities.modify"></a>
						</div>

					</div>			
				</div>
				
				<!-- display an error message if the user didn't have enought km -->
				<div class="inner noAcces" style="display:none">
					<div class="stroked"  data-i18n="activities.choice.noAccesSubtitle"></div>
					<p data-i18n="activities.choice.noAccesTitle"></p>
					<a href="#" class="btn" data-i18n="activities.choice.noAccesLink"></a>
				</div>
			</section>

			<!-- the content will be loaded here, when the user make his choice -->
			
			<!-- FOOTER -->
			<footer>

				<div class="container-fluid health_warning">
			        <div class="row">
			            <div class="col-md-12">
			                <p>
			                    <img src="<?=ASSETS_URL?>img/footer/health_warning.jpg" alt="Rauchen ist tödlich. Fumer tue. Il fumo uccide.">
			                </p>
			            </div>
			        </div>
			    </div>
				
			</footer>

		</div><!-- /.mainWrapper -->

		<script>
			var siteUrl = '<?=site_url()?>';
			var baseUrl = '<?=base_url()?>';
			var assetsUrl = '<?=ASSETS_URL?>';
			var segments = '<?=$this->uri->segment(2)?>';
			var region = '<?=$region?>';
		</script>
		<script src="<?=ASSETS_URL?>js/scripts.min.js"></script>
		<script src="<?=ASSETS_URL?>js/main.js"></script>
	</body>
</html>