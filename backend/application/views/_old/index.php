			
			<!-- CONTENT -->
			<div class="homePage">
				<div class="section" id="home_banner">

					<h1 data-i18n="title"></h1>

					<!-- Introduction -->
					<div class="intro">
						<h2>
							<span data-i18n="home.subtitle"></span>
							<strong data-i18n="home.headline"></strong>
						</h2>
						<a href="activities.html" data-i18n="home.start"></a>						
					</div>

				</div>
			</div><!-- end .homePage -->

			<!-- FOOTER -->
			<footer>

				<div class="container-fluid health_warning">
			        <div class="row">
			            <div class="col-md-12">
			                <p>
			                    <img src="img/footer/health_warning.jpg" alt="Rauchen ist tödlich. Fumer tue. Il fumo uccide.">
			                </p>
			            </div>
			        </div>
			    </div>
				
			</footer>	

		</div><!-- end .mainWrapper -->

		<script>
			var siteUrl = '<?=site_url()?>';
			var baseUrl = '<?=base_url()?>';
			var assetsUrl = '<?=ASSETS_URL?>';
			var segments = '<?=$this->uri->segment(2)?>';
		</script>
		<script src="<?=ASSETS_URL?>js/scripts.min.js"></script>
		<script src="<?=ASSETS_URL?>js/main.js"></script>

		<?php if($this->session->userdata('xover_first_login') == 1) { ?>
			<script type="text/javascript">
				$(window).load(function(){
					$('#isFirstLogin').modal('show');
				});
				
				function unsetXover(){
					$.post('<?=site_url()?>home/unsetXoverFirstLogin', function(){});
				}

			</script>
		<?php } ?>

		<!-- TEMPORARY -->
		<script type="text/javascript">

			$(function(){

				console.log('User Session Key: <?=$session_key?>');
				console.log('xover:  <?=$this->session->userdata("xover_first_login")?>');
				
				<?php if($birthday_offer){  ?>
				 		alert('BIRTHDAY OFFER');
				 <?php } ?> 
			});

		</script>

		<!-- OFFERS POPUP // DO NOT DELETE
		<script type="text/javascript">

			$(function(){

				console.log('User Session Key: <?=$session_key?>');

				<?php if($referral_offer){  ?>
				 		popup.open({url:"flash_offer/get_flash_offer?prize_id=<?=$referral_offer?>&rand=<?=uniqid()?>&type=referral"});
				 <?php } ?> 

				 <?php if(!$referral_offer && $birthday_offer && $birthdate != '0000-00-00'){ ?>
				 		popup.open({url:"birthday_offer/get_birthday_offer?prize_id=<?=$birthday_offer?>&flash_offer=<?=$flash_offer?>&rand=<?=uniqid()?>",
				 					onClose:function(){		
										<?php if($flash_offer && !$flash_offer_confirmed) { ?>
											 	 popup.open({url:"flash_offer/get_flash_offer?prize_id=<?=$flash_offer?>&rand=<?=uniqid()?>&type=flash"});
										 <?php } ?> 		
				 					}
				 				});
				 <?php } ?>

				 <?php if(!$referral_offer && !$birthday_offer && $flash_offer && !$flash_offer_confirmed) { ?>
				 			popup.open({url:"flash_offer/get_flash_offer?prize_id=<?=$flash_offer?>&rand=<?=uniqid()?>&type=flash"});
				 <?php } ?> 

				 <?php if(!$referral_offer && !$birthday_offer && !$flash_offer && $bids){ ?>
				 		popup.open({url:"perks/bid/confirm_address"});
				 <?php } ?> 
			});

		</script>
		-->
	</body>
</html>
