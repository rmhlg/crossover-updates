	
	<!-- CONTENT -->
	<div class="homePage">
		<div class="section" id="home_banner" style="background-image:url(<?= ASSETS_URL ?>img/california/home_banner1024.jpg)">

			<h1 data-i18n="title"></h1>

			<!-- Introduction -->
			<div class="intro">
				<h2>
					<span data-i18n="home.subtitle"></span>
					<strong data-i18n="home.headline"></strong>
				</h2>
				<div class="toTrailer"data-video="trailer" onclick="earnPoints('<?= encrypt_points(HOME) ?>', '<?= encrypt_points(WATCH) ?>', '<?= encrypt_points(0) ?>')">watch <span>Play</span> trailer</div>

				<a href="<?= isset($row) ? SITE_URL.'activities/'.$row->experience_id : SITE_URL ?>" data-i18n="home.start"></a>						
			</div>

			<!-- Links to bottom -->
			<div class="linkTo row">
				<a href="<?= SITE_URL ?>events" class="iWantButton">
					<b data-i18n="[html]home.link3.title"></b>
					<span class="subtitle" data-i18n="home.link3.subtitle"></span>
				</a>
				<a href="<?= SITE_URL ?>gifts">
					<b data-i18n="[html]home.link4.title"></b>
					<span class="subtitle" data-i18n="home.link4.subtitle"></span>
				</a>
			</div>

			<div class="imgLegend">
				<span class="lineTop"></span>
				<b data-i18n="california.surname" data-random="imgLegend"></b>
				<span data-i18n="california.location" data-random="imgLegend"></span> 
				/ <span data-i18n="california.country" data-random="imgLegend"></span>
			</div>

		</div>
	</div>

	<!-- MODAL NAV PROFILE -->
	<div class="modal fade profile" id="myProfileInfo">
		<div class="modal-dialog">
		    <div class="modal-content">
			    <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			    </div>
			    <div class="modal-body">
			      	<div class="inputAddCode">
			      		<form action="">
			      			<!-- input text to add code -->
			      			<div class="inputContainer">		      			
				      			<input type="text" name="personalCode" placeholder="Your code here" value="Your code here" maxlength="8" >		      	
				      			<button type="submit" disabled></button>		
				      		</div>	      		
			
							<!-- message to display if ...  -->
							<div class="notifMsg" style="display: none;">
								<!-- the code is correct -->
								<div class="congrats">
									<span data-i18n="modalProfile.validCode"></span> 
								<strong data-i18n="modalProfile.validCodeStrong"></strong>
								</div>
								<!-- the code is wrong -->
								<div class="error">
									<span data-i18n="modalProfile.wrongCode"></span> 
									<strong data-i18n="modalProfile.wrongCodeStrong"></strong>
								</div>
							</div>
			      		</form>	
			      	</div><!-- end .inputAddCode-->

			      	<!-- data user (KM) -->
			        <div class="dataKm">
			        	<div class="col-md-6 col-sm-6">
			        		<span data-i18n="[html]dataUser.collected"></span>
			        		<p data-i18n="modalProfile.counterCollect"></p>
			        	</div>
			        	<div class="col-md-6 col-sm-6">
			        		<span data-i18n="[html]dataUser.credit"></span>
			        		<p data-i18n="modalProfile.counterCredit"></p>
			        	</div>
			        	<div class="clearfix"></div>
			        </div>
					
					<!-- list Link -->
			        <ul class="listLink">
			        	<li><a href="" data-i18n="[html]modalProfile.history"></a></li>
			        	<li><a href="" data-i18n="[html]modalProfile.gifts"></a></li>
			        </ul>

			    </div>
			    <div class="modal-footer">
			      	<a href="profile.htlm" class="btn arrowed" data-i18n="modalProfile.goProfile"></a>
			    </div>
		    </div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

	<!-- FOOTER -->
	<footer>

		<div class="navFooter">                

	        <!-- BOTTOMBAR -->
	        <nav class="bottombar clearfix">

	            <ul class="user">
	                <li><a href="<?= CROSSOVER_STAGING_LOGOUT ?>" data-i18n="footer.logout"></a></li>
	            </ul>

	            <ul class="nav">
	                <li><a href="#" data-i18n="footer.account"></a></li>
	                <li><a href="legal.html" target="_blank" data-i18n="footer.terms"></a></li>
	                <li><a href="#" data-i18n="footer.contact"></a></li>
	                <li><a href="#" data-i18n="footer.faq"></a></li>
	                <li><a href="#" data-i18n="footer.health"></a></li>
	                <li class="invite_friend"><a href="#" data-i18n="footer.invite"></a></li>
	            </ul>                

	        </nav>
	        <!-- /BOTTOMBAR -->
	    </div>

		<div class="container-fluid health_warning">
	        <div class="row">
	            <div class="col-md-12">
	                <p>
	                    <img src="<?= ASSETS_URL ?>img/footer/health_warning.jpg" alt="Rauchen ist tödlich. Fumer tue. Il fumo uccide.">
	                </p>
	            </div>
	        </div>
	    </div>
		
	</footer>	

	<!-- MODAL VIDEO -->
	<div class="modal fade modal_video" id="modalVideo">
		<div class="modal-dialog modal-lg">
		    <div class="modal-content">
			    <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close" <?= $row_first_visit === FALSE ? 'data-toggle="modal" data-target="#infoGift" onclick="userFirstVisit()" id="user-first-visit"' : '' ?>><span aria-hidden="true">&times;</span></button>		       
			    </div>
		      	<div class="modal-body video">

					<img src="" alt="" class="ratio poster">

					<div id="jp_container_1" class="jp-video" role="application" aria-label="media player">
						<div class="jp-type-single">
							<div id="js-video" class="jp-jplayer" ></div>
							<div class="jp-gui">
								<div class="jp-video-play">
									<button class="jp-video-play-icon" role="button" tabindex="0">play</button>
								</div>
								<div class="jp-interface">
									<div class="jp-progress">
										<div class="jp-seek-bar">
											<div class="jp-play-bar"></div>
										</div>
									</div>
									<div class="jp-current-time" role="timer" aria-label="time">&nbsp;</div>
									<div class="jp-duration" role="timer" aria-label="duration">&nbsp;</div>
									<div class="jp-controls-holder">
										<div class="jp-controls">
											<button class="jp-play" role="button" tabindex="0">play</button>
											<button class="jp-stop" role="button" tabindex="0">stop</button>
										</div>
										<div class="jp-volume-controls">
											<button class="jp-mute" role="button" tabindex="0">mute</button>
											<button class="jp-volume-max" role="button" tabindex="0">max volume</button>
											<div class="jp-volume-bar">
												<div class="jp-volume-bar-value"></div>
											</div>
										</div>
										<div class="jp-toggles">
											<button class="jp-repeat" role="button" tabindex="0">repeat</button>
											<button class="jp-full-screen" role="button" tabindex="0">full screen</button>
										</div>
									</div>
									<div class="jp-details">
										<div class="jp-title" aria-label="title">&nbsp;</div>
									</div>
								</div>
							</div>
							<div class="jp-no-solution">
								<span>Update Required</span>
								To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
							</div>
						</div>
					</div>
				</div>	      
		    </div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->


	<!-- CUSTOM MODAL -->
	<div class="modal fade default" id="infoGift">
	    <div class="modal-dialog modal-sm">
			<div class="modal-content">
		            <div class="modal-header">
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		            </div>

		            <div class="contents col-sm-6">
		                <div class="row modal-body">
		                    <!-- STEP 1 : More infos about the gifts -->
		                    <div class="col-sm-4 moreInfos">
		                        
		                        <p class="subtitle">You just completed your first activity, you get 50KM. Visit more pages to get more kilometers</p>

		                        <div class="modal-footer">
		                            <button type="button" class="btn" data-dismiss="modal" aria-label="Close">Close</button>
		                        </div>
		                    </div><!-- end step1 -->


		                    <!-- STEP 2 : Confirm you adress -->
		                    <div class="col-sm-4 confirm">
		                        <h4 class="modal-title" data-i18n="[html]catalogue.modal.0.confirmTitle"></h4>
		                        <form action="" class="confirmAdress">
		                            <div class="line">
		                                <label for="street" data-i18n="catalogue.labelsForm.0.street"></label>
		                                <input type="text" name="street"/>
		                            </div>
		                            
		                            <div class="line">
		                                <label for="number" data-i18n="catalogue.labelsForm.0.number"></label>
		                                <input type="text" name="number"/>
		                            </div>

		                            <div class="line">
		                                <label for="city" data-i18n="catalogue.labelsForm.0.city"></label>

		                                <!-- add a div.correct (that containing a empty <span>) around the input : valid -->
		                                <div class="correct">                                    
		                                    <input type="text" name="city" class="correct" value="valid" />
		                                    <span></span>
		                                </div>
		                               
		                            </div>
		                            
		                            <!-- add this class" .error " on a line containing a wrong value -->
		                            <div class="line error">
		                                <label for="zip" data-i18n="catalogue.labelsForm.0.zip"></label>
		                                <input type="text" name="zip" value="not valid"/>
		                            </div>
		                            
		                            <!-- display this message if the form is not valid -->
		                            <div class="errorMsg" data-i18n="catalogue.labelsForm.0.error"></div>
		                            
		                            <button type="submit" class="btn" data-i18n="catalogue.links.0.confirm"></button>
		                        </form>
		                    </div><!-- end step2 -->

		                    <!-- STEP 3 : statut file -->
		                    <div class="col-sm-4 statut">
		                        <div class="thanks">
		                            <h4 class="modal-title" data-i18n="[html]catalogue.modal.0.thanksTitle"></h4>
		                            <p class="subtitle" data-i18n="[html]catalogue.modal.0.thanksSubtitle">
		                            <p data-i18n="[html]catalogue.modal.0.thanksContent"></p>
		                            <button type="button" class="btn" data-dismiss="modal" aria-label="Close" data-i18n="catalogue.modal.0.btnBakcToList"></button>
		                        </div><!-- end valid -->

		                        <!-- Error -->
		                        <div class="serverError" style="display: none">
		                            <h4 data-i18n="[html]catalogue.modal.0.errorTitle"></h4>
		                            <p data-i18n="[html]catalogue.modal.0.errorContent"></p>
		                            <button type="button" class="btn" data-dismiss="modal" aria-label="Close" data-i18n="catalogue.modal.0.btnBakcToForm"></button>
		                        </div><!-- end Error -->
		                    </div>
		                    

		                </div><!-- end modal-body -->
		            </div>   <!-- end col-sm-6 -->
	            <div class="clearfix"></div>  
      		</div><!-- /.modal-content -->      
	    </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	<!-- CUSTOM MODAL -->

	<div class="modal fade default" id="isFirstLogin">
	    <div class="modal-dialog modal-sm">
			<div class="modal-content">
		            <div class="modal-header">
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="unsetXover();"><span aria-hidden="true">&times;</span></button>
		            </div>

		            <div class="contents col-sm-6">
						<div class="row modal-body">
		                    <!-- STEP 1 : More infos about the gifts -->
		                    <div class="col-sm-4 moreInfos">
		                        
		                        <p class="subtitle">Welcome to the CROSSOVER community! Here’s 20 points to get you started.</p>

		                        <div class="modal-footer">
		                            <button type="button" class="btn" data-dismiss="modal" aria-label="Close">Close</button>
		                        </div>
		                    </div><!-- end step1 -->


		                    <!-- STEP 2 : Confirm you adress -->
		                    <div class="col-sm-4 confirm">
		                        <h4 class="modal-title" data-i18n="[html]catalogue.modal.0.confirmTitle"></h4>
		                        <form action="" class="confirmAdress">
		                            <div class="line">
		                                <label for="street" data-i18n="catalogue.labelsForm.0.street"></label>
		                                <input type="text" name="street"/>
		                            </div>
		                            
		                            <div class="line">
		                                <label for="number" data-i18n="catalogue.labelsForm.0.number"></label>
		                                <input type="text" name="number"/>
		                            </div>

		                            <div class="line">
		                                <label for="city" data-i18n="catalogue.labelsForm.0.city"></label>

		                                <!-- add a div.correct (that containing a empty <span>) around the input : valid -->
		                                <div class="correct">                                    
		                                    <input type="text" name="city" class="correct" value="valid" />
		                                    <span></span>
		                                </div>
		                               
		                            </div>
		                            
		                            <!-- add this class" .error " on a line containing a wrong value -->
		                            <div class="line error">
		                                <label for="zip" data-i18n="catalogue.labelsForm.0.zip"></label>
		                                <input type="text" name="zip" value="not valid"/>
		                            </div>
		                            
		                            <!-- display this message if the form is not valid -->
		                            <div class="errorMsg" data-i18n="catalogue.labelsForm.0.error"></div>
		                            
		                            <button type="submit" class="btn" data-i18n="catalogue.links.0.confirm"></button>
		                        </form>
		                    </div><!-- end step2 -->

		                    <!-- STEP 3 : statut file -->
		                    <div class="col-sm-4 statut">
		                        <div class="thanks">
		                            <h4 class="modal-title" data-i18n="[html]catalogue.modal.0.thanksTitle"></h4>
		                            <p class="subtitle" data-i18n="[html]catalogue.modal.0.thanksSubtitle">
		                            <p data-i18n="[html]catalogue.modal.0.thanksContent"></p>
		                            <button type="button" class="btn" data-dismiss="modal" aria-label="Close" data-i18n="catalogue.modal.0.btnBakcToList"></button>
		                        </div><!-- end valid -->

		                        <!-- Error -->
		                        <div class="serverError" style="display: none">
		                            <h4 data-i18n="[html]catalogue.modal.0.errorTitle"></h4>
		                            <p data-i18n="[html]catalogue.modal.0.errorContent"></p>
		                            <button type="button" class="btn" data-dismiss="modal" aria-label="Close" data-i18n="catalogue.modal.0.btnBakcToForm"></button>
		                        </div><!-- end Error -->
		                    </div>
		                    

		                </div><!-- end modal-body -->

		            </div>   <!-- end col-sm-6 -->
	            <div class="clearfix"></div>  
      		</div><!-- /.modal-content -->      
	    </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	<!-- CUSTOM MODAL -->

</div>


