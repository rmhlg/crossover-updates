<div class="mainWrapper" style="top: 0px">

	<!-- HEADER -->
	<header>
	<!-- MODAL Gifts -->
	<!-- Modal -->

		<div class="container">

			<!-- Main navigation -->
			<ul class="nav">
				<li class="home">
					<a href="index.html">
						<img src="<?= ASSETS_URL ?>img/crossover.png" alt="Malboro - Cross|Over" />
					</a>
				</li>
				<li>
					<a href="index.html" data-i18n="header.links.home"></a>
				</li>
				<li>
					<a href="map.html" data-i18n="header.links.map"></a>
				</li>
				<li>
					<a href="gifts.html" data-i18n="header.links.gifts"></a>
				</li>
				<li class="active">
					<a href="events.html" data-i18n="header.links.events"></a>
				</li>
				<li>
					<a href="howTo.html" data-i18n="header.links.howTo"></a>
				</li>
				<!-- activities links call a new panel of menu: Cfr below sub-menu .activities  -->
				<li class="sliderActivities">
					<a href="#" data-i18n="header.links.activities"></a>
				</li>

				<!-- data user + Call modal Profile-->
				<li class="navProfile">
					<ul>
						<!-- open modal Profile + add focus on input addCode -->
						<li class="addCode">
							<p data-i18n="[html]dataUser.collected"></p>
							<p><a href="#" class="addCodeTo" data-i18n="header.addCode"></a></p>
						</li>
						<!-- open modal Profile  -->
						<li class="profileBtn">
							<a href="#" data-toggle="modal" data-target="#myProfileInfo">
								<span class="border">
									<img src="<?= ASSETS_URL ?>img/user.png" alt"user name" />
								</span>
								<span class="arrowDown"></span>
							</a>
						</li>
					</ul>
				</li>
			</ul>

		</div>

		<!-- sub-menu: display a slider with all the activities -->
		<div class="activities">
			<div class="slider">
				<div class="checked">
					<a href="activities.html">
						<div class="illu">
							<img data-i18n="[src]california.one.sliderImg" />
						</div>
						<div>
							<span class="icon bicycle"></span>
							<h4 data-i18n="california.one.name"></h4>
							<span class="location"><span data-i18n="california.surname"></span> / <span data-i18n="california.location"></span></span>
						</div>
					</a>
				</div>
				<div class="checked">
					<a href="activities.html">
						<div class="illu">
							<img data-i18n="[src]california.two.sliderImg" />
						</div>
						<div>
							<span class="icon bicycle"></span>
							<h4 data-i18n="california.two.name"></h4>
							<span class="location"><span data-i18n="california.surname"></span> / <span data-i18n="california.location"></span></span>
						</div>
					</a>
				</div>
				<div>
					<a href="activities.html">
						<div class="illu">
							<img data-i18n="[src]california.three.sliderImg" />
						</div>
						<div>
							<span class="icon bicycle"></span>
							<h4 data-i18n="california.three.name"></h4>
							<span class="location"><span data-i18n="california.surname"></span> / <span data-i18n="california.location"></span></span>
						</div>
					</a>
				</div>
				<div class="locked">
					<a href="activities.html">
						<div class="illu">
							<img data-i18n="[src]alaska.one.sliderImg" />
						</div>
						<div>
							<span class="icon bicycle"></span>
							<h4 data-i18n="canada.one.name"></h4>
							<span class="location"><span data-i18n="canada.surname"></span> / <span data-i18n="canada.location"></span></span>
						</div>
					</a>
				</div>
				<div class="locked">
					<a href="activities.html">
						<div class="illu">
							<img data-i18n="[src]alaska.two.sliderImg" />
						</div>
						<div>
							<span class="icon bicycle"></span>
							<h4 data-i18n="canada.two.name"></h4>
							<span class="location"><span data-i18n="canada.surname"></span> / <span data-i18n="canada.location"></span></span>
						</div>
					</a>
				</div>
				<div class="locked">
					<a href="activities.html">
						<div class="illu">
							<img data-i18n="[src]alaska.three.sliderImg" />
						</div>
						<div>
							<span class="icon bicycle"></span>
							<h4 data-i18n="canada.three.name"></h4>
							<span class="location"><span data-i18n="canada.surname"></span> / <span data-i18n="canada.location"></span></span>
						</div>
					</a>
				</div>
				<div class="locked">
					<a href="activities.html">
						<div class="illu">
							<img data-i18n="[src]alaska.one.sliderImg" />
						</div>
						<div>
							<span class="icon bicycle"></span>
							<h4 data-i18n="alaska.one.name"></h4>
							<span class="location"><span data-i18n="alaska.surname"></span> / <span data-i18n="alaska.location"></span></span>
						</div>
					</a>
				</div>
				<div class="locked">
					<a href="activities.html">
						<div class="illu">
							<img data-i18n="[src]alaska.two.sliderImg" />
						</div>
						<div>
							<span class="icon bicycle"></span>
							<h4 data-i18n="alaska.two.name"></h4>
							<span class="location"><span data-i18n="alaska.surname"></span> / <span data-i18n="alaska.location"></span></span>
						</div>
					</a>
				</div>
				<div class="locked">
					<a href="activities.html">
						<div class="illu">
							<img data-i18n="[src]alaska.three.sliderImg" />
						</div>
						<div>
							<span class="icon bicycle"></span>
							<h4 data-i18n="alaska.three.name"></h4>
							<span class="location"><span data-i18n="alaska.surname"></span> / <span data-i18n="alaska.location"></span></span>
						</div>
					</a>
				</div>
			</div>
		</div>

	</header>

	<!-- CONTENT -->
	<section class="events">
		<div class="filter-menu">
			<ul>
				<li>
					<select id="venue" class="select-l" onchange="filterEvent($('#venue').val(), $('#region').val(),$('#event_type').val(),$('#startdate').val(), $('#enddate').val())">
						<option value="">Select Venue</option>
						<?php
						foreach($venues as $venue){
							echo "<option value='".$venue->name."'>".$venue->name."</option>";
						}
						?>
					</select>
				</li>
				<li>
					<select id="region" class="select-l" onchange="filterEvent($('#venue').val(), $('#region').val(),$('#event_type').val(),$('#startdate').val(), $('#enddate').val())">
						<option value="">Select Region</option>
						<?php
						foreach($regions as $region){
							echo "<option value='".$region->region_id."'>".$region->name."</option>";
						}
						?>
					</select>
				</li>
				<li>
					<select id="event_type" class="select-l" onchange="filterEvent($('#venue').val(), $('#region').val(),$('#event_type').val(),$('#startdate').val(), $('#enddate').val())">
						<option value="">Select Event Type</option>
						<?php
						foreach($eventTypes as $eventType){
						?>
						<option value="<?= $eventType->name ?>"><?= $eventType->name ?></option>
						<?php
						}
						?>
					</select>
				</li>
			</ul>
			<ul style="margin-top: 10px;">
				<li>
					<input type="text" placeholder="Start date" onmouseover="setDatepicker($(this))" class="filterGiftTxt dateField"  id="startdate"onchange="filterEvent($('#venue').val(), $('#region').val(),$('#event_type').val(),$('#startdate').val(), $('#enddate').val())" />
				</li>
				<li>
					<input type="text" placeholder="End date" onmouseover="setDatepicker($(this))" class="filterGiftTxt dateField" id="enddate" onchange="filterEvent($('#venue').val(), $('#region').val(),$('#event_type').val(),$('#startdate').val(), $('#enddate').val())" />
				</li>
			</ul>

		</div>
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="padding-top: 134px;">
		  <div class="modal-dialog">
		    <div class="modal-content" style="padding: 10px;">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="myModalLabel">Thank you!</h4>
		      </div>
		      <div class="modal-body">
		        You have confirmed your attendance to <strong><span class='eventData' id="eventTitle"></span></strong>. <br /><br />
		        See you at <strong><span class='eventData' id="eventVenue"></span></strong> on  <strong><span class='eventData' id="eventDate"></span>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		      </div>
		    </div>
		  </div>
		</div>
		<div class="events-list">
			<?php
			$ctr = 0;
			$att_arr = array();
			foreach($attendance as $att){
				$att_arr[] = $att->event_id;
			}

			foreach($rows as $row){
				if($ctr==0){
					
					?>
					<div class="item">
						<div class="thumbnails">
							<img src="<?= UPLOADS_URL."backstage/events/331_176_".$row->image ?>" alt="thumbnail"/>
						</div>
						<div class="content">
							<h3><?= $row->title ?></h3>
							<h6><?= $row->venue ?> <br/> <?= date('F d, Y ', strtotime($row->start_date)) ?> TO <?= date('F d, Y ', strtotime($row->end_date)) ?></h6><br/>
							<p>
								<?= $row->description ?>
							</p>
							<?php
							if(in_array($row->backstage_event_id, $att_arr)){
							?>
							<button disabled="disabled" onclick="imGoing($(this), <?= $row->backstage_event_id ?>, 1)" class="lockedButton"><i></i>GOING</button>
							<?php
							}else{

							?>		

							<button data-toggle="modal" data-target="#myModal" data-date="<?= date('F d, Y ', strtotime($row->start_date)) ?> to <?= date('F d, Y ', strtotime($row->end_date)) ?>" data-description="<?= $row->description ?>" data-venue="<?= $row->venue ?>" data-title="<?= $row->title ?>" onclick="imGoing($(this), <?= $row->backstage_event_id ?>, 1)"><i></i>I'M GOING</button>
							<?php
							}
							?>
						</div>

					</div>
					<?php
					$ctr=1;
				}else{
					?>
					<div class="item">
						<div class="thumbnails">
							<img src="<?= UPLOADS_URL."backstage/events/331_176_".$row->image ?>" alt="thumbnail"/>
						</div>
						<div class="content">
							<h3><?= $row->title ?></h3>
							<h6><?= $row->venue ?> <br/> <?= date('F d, Y ', strtotime($row->start_date)) ?> TO <?= date('F d, Y ', strtotime($row->end_date)) ?></h6><br/>
							<p>
								<?= $row->description ?>
							</p>
							<?php
							if(in_array($row->backstage_event_id, $att_arr)){
							?>
							<button disabled="disabled" onclick="imGoing($(this), <?= $row->backstage_event_id ?>, 1)" class="lockedButton"><i></i>GOING</button>
							<?php
							}else{
							?>	
							<button data-toggle="modal" data-target="#myModal" data-date="<?= date('F d, Y ', strtotime($row->start_date)) ?> to <?= date('F d, Y ', strtotime($row->end_date)) ?>" data-description="<?= $row->description ?>" data-venue="<?= $row->venue ?>" data-title="<?= $row->title ?>" onclick="imGoing($(this), <?= $row->backstage_event_id ?>, 1)"><i></i>I'M GOING</button>
							<?php
							}
							?>
						</div>

					</div>
					<br class="clear">
					<?php
					$ctr=0;
				}
			}
			?>		
		</div>
	</section>
	<!-- MODAL NAV PROFILE -->
	<div class="modal fade profile" id="myProfileInfo">
		<div class="modal-dialog">
		    <div class="modal-content">
			    <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			    </div>
			    <div class="modal-body">
			      	<div class="inputAddCode">
			      		<form action="">
			      			<!-- input text to add code -->
			      			<div class="inputContainer">
				      			<input type="text" name="personalCode" placeholder="Your code here" value="Your code here" maxlength="8" >
				      			<button type="submit" disabled></button>
				      		</div>

							<!-- message to display if ...  -->
							<div class="notifMsg" style="display: none;">
								<!-- the code is correct -->
								<div class="congrats">
									<span data-i18n="modalProfile.validCode"></span>
								<strong data-i18n="modalProfile.validCodeStrong"></strong>
								</div>
								<!-- the code is wrong -->
								<div class="error">
									<span data-i18n="modalProfile.wrongCode"></span>
									<strong data-i18n="modalProfile.wrongCodeStrong"></strong>
								</div>
							</div>
			      		</form>
			      	</div><!-- end .inputAddCode-->

			      	<!-- data user (KM) -->
			        <div class="dataKm">
			        	<div class="col-md-6 col-sm-6">
			        		<span data-i18n="[html]dataUser.collected"></span>
			        		<p data-i18n="modalProfile.counterCollect"></p>
			        	</div>
			        	<div class="col-md-6 col-sm-6">
			        		<span data-i18n="[html]dataUser.credit"></span>
			        		<p data-i18n="modalProfile.counterCredit"></p>
			        	</div>
			        	<div class="clearfix"></div>
			        </div>

					<!-- list Link -->
			        <ul class="listLink">
			        	<li><a href="" data-i18n="[html]modalProfile.history"></a></li>
			        	<li><a href="" data-i18n="[html]modalProfile.gifts"></a></li>
			        </ul>

			    </div>
			    <div class="modal-footer">
			      	<a href="profile.htlm" class="btn arrowed" data-i18n="modalProfile.goProfile"></a>
			    </div>
		    </div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

	<!-- FOOTER -->
	<footer>

		<div class="navFooter">

	        <!-- BOTTOMBAR -->
	        <nav class="bottombar clearfix">

	            <ul class="user">
	                <li><a href="<?= CROSSOVER_STAGING_LOGOUT ?>" data-i18n="footer.logout"></a></li>
	            </ul>

	            <ul class="nav">
	                <li><a href="#" data-i18n="footer.account"></a></li>
	                <li><a href="legal.html" target="_blank" data-i18n="footer.terms"></a></li>
	                <li><a href="#" data-i18n="footer.contact"></a></li>
	                <li><a href="#" data-i18n="footer.faq"></a></li>
	                <li><a href="#" data-i18n="footer.health"></a></li>
	                <li class="invite_friend"><a href="#" data-i18n="footer.invite"></a></li>
	            </ul>

	        </nav>
	        <!-- /BOTTOMBAR -->
	    </div>
	   <div class="container-fluid health_warning">
	        <div class="row">
	            <div class="col-md-12">
	               <h1>GOVERNMENT WARNING: CIGARETTE SMOKING IS DANGEROUS TO YOUR HEALTH</h1>
	            </div>
	        </div>
	    </div>

	</footer>

	<!-- MODAL VIDEO -->
	<div class="modal fade modal_video" id="modalVideo" >
		<div class="modal-dialog modal-lg">
		    <div class="modal-content">
			    <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			    </div>
		      	<div class="modal-body video">

					<img src="" alt="" class="ratio poster">

					<div id="jp_container_1" class="jp-video" role="application" aria-label="media player">
						<div class="jp-type-single">
							<div id="js-video" class="jp-jplayer" ></div>
							<div class="jp-gui">
								<div class="jp-video-play">
									<button class="jp-video-play-icon" role="button" tabindex="0">play</button>
								</div>
								<div class="jp-interface">
									<div class="jp-progress">
										<div class="jp-seek-bar">
											<div class="jp-play-bar"></div>
										</div>
									</div>
									<div class="jp-current-time" role="timer" aria-label="time">&nbsp;</div>
									<div class="jp-duration" role="timer" aria-label="duration">&nbsp;</div>
									<div class="jp-controls-holder">
										<div class="jp-controls">
											<button class="jp-play" role="button" tabindex="0">play</button>
											<button class="jp-stop" role="button" tabindex="0">stop</button>
										</div>
										<div class="jp-volume-controls">
											<button class="jp-mute" role="button" tabindex="0">mute</button>
											<button class="jp-volume-max" role="button" tabindex="0">max volume</button>
											<div class="jp-volume-bar">
												<div class="jp-volume-bar-value"></div>
											</div>
										</div>
										<div class="jp-toggles">
											<button class="jp-repeat" role="button" tabindex="0">repeat</button>
											<button class="jp-full-screen" role="button" tabindex="0">full screen</button>
										</div>
									</div>
									<div class="jp-details">
										<div class="jp-title" aria-label="title">&nbsp;</div>
									</div>
								</div>
							</div>
							<div class="jp-no-solution">
								<span>Update Required</span>
								To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
							</div>
						</div>
					</div>
				</div>
		    </div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

</div>
<script>