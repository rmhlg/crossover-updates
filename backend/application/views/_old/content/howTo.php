
	<!-- CONTENT -->
	<section class="section fullscreen active">
		<div class="howToPlay trianglesArrows">
			<h2><span data-i18n="howTo.title"></span></h2>
			<div class="pannel events">
				<div class="element"></div>
				<div class="infos">
					<div class="row">
						<div class="col-sm-5 col-sm-offset-1 col-md-6 col-md-offset-0 title">
							<span data-i18n="howTo.slide1.category"></span>
							<strong data-i18n="[html]howTo.slide1.headline"></strong>
						</div>
						<div class="col-sm-5 col-md-5 detail" data-i18n="[html]howTo.slide1.description"></div>
					</div>
				</div>
			</div>
			<div class="pannel packCode">
				<div class="element"></div>
				<div class="infos">
					<div class="row">
						<div class="col-sm-5 col-sm-offset-1 col-md-6 col-md-offset-0 title">
							<span data-i18n="howTo.slide2.category"></span>
							<strong data-i18n="[html]howTo.slide2.headline"></strong>
						</div>
						<div class="col-sm-5 col-md-5 detail" data-i18n="[html]howTo.slide2.description"></div>
					</div>
				</div>
			</div>
			<div class="pannel events">
				<div class="element"></div>
				<div class="infos">
					<div class="row">
						<div class="col-sm-5 col-sm-offset-1 col-md-6 col-md-offset-0 title">
							<span data-i18n="howTo.slide1.category"></span>
							<strong data-i18n="[html]howTo.slide1.headline"></strong>
						</div>
						<div class="col-sm-5 col-md-5 detail" data-i18n="[html]howTo.slide1.description"></div>
					</div>
				</div>
			</div>
			<div class="pannel events">
				<div class="element"></div>
				<div class="infos">
					<div class="row">
						<div class="col-sm-5 col-sm-offset-1 col-md-6 col-md-offset-0 title">
							<span data-i18n="howTo.slide2.category"></span>
							<strong data-i18n="[html]howTo.slide2.headline"></strong>
						</div>
						<div class="col-sm-5 col-md-5 detail" data-i18n="[html]howTo.slide2.description"></div>
					</div>
				</div>
			</div>
			<div class="pannel events">
				<div class="element"></div>
				<div class="infos">
					<div class="row">
						<div class="col-sm-5 col-sm-offset-1 col-md-6 col-md-offset-0 title">
							<span data-i18n="howTo.slide1.category"></span>
							<strong data-i18n="[html]howTo.slide1.headline"></strong>
						</div>
						<div class="col-sm-5 col-md-5 detail" data-i18n="[html]howTo.slide1.description"></div>
					</div>
				</div>
			</div>
		</div>				
	</section>


	<!-- MODAL NAV PROFILE -->
	<div class="modal fade profile" id="myProfileInfo">
		<div class="modal-dialog">
		    <div class="modal-content">
			    <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			    </div>
			    <div class="modal-body">
			      	<div class="inputAddCode">
			      		<form action="">
			      			<!-- input text to add code -->
			      			<div class="inputContainer">		      			
				      			<input type="text" name="personalCode" placeholder="Your code here" value="Your code here" maxlength="8" >		      	
				      			<button type="submit" disabled></button>		
				      		</div>	      		
			
							<!-- message to display if ...  -->
							<div class="notifMsg" style="display: none;">
								<!-- the code is correct -->
								<div class="congrats">
									<span data-i18n="modalProfile.validCode"></span> 
								<strong data-i18n="modalProfile.validCodeStrong"></strong>
								</div>
								<!-- the code is wrong -->
								<div class="error">
									<span data-i18n="modalProfile.wrongCode"></span> 
									<strong data-i18n="modalProfile.wrongCodeStrong"></strong>
								</div>
							</div>
			      		</form>	
			      	</div><!-- end .inputAddCode-->

			      	<!-- data user (KM) -->
			        <div class="dataKm">
			        	<div class="col-md-6 col-sm-6">
			        		<span data-i18n="[html]dataUser.collected"></span>
			        		<p data-i18n="modalProfile.counterCollect"></p>
			        	</div>
			        	<div class="col-md-6 col-sm-6">
			        		<span data-i18n="[html]dataUser.credit"></span>
			        		<p data-i18n="modalProfile.counterCredit"></p>
			        	</div>
			        	<div class="clearfix"></div>
			        </div>
					
					<!-- list Link -->
			        <ul class="listLink">
			        	<li><a href="" data-i18n="[html]modalProfile.history"></a></li>
			        	<li><a href="" data-i18n="[html]modalProfile.gifts"></a></li>
			        </ul>

			    </div>
			    <div class="modal-footer">
			      	<a href="profile.htlm" class="btn arrowed" data-i18n="modalProfile.goProfile"></a>
			    </div>
		    </div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

	<!-- FOOTER -->
	<footer>

		<div class="navFooter">                

	        <!-- BOTTOMBAR -->
	        <nav class="bottombar clearfix">

	            <ul class="user">
	                <li><a href="<?= CROSSOVER_STAGING_LOGOUT ?>" data-i18n="footer.logout"></a></li>
	            </ul>

	            <ul class="nav">
	                <li><a href="#" data-i18n="footer.account"></a></li>
	                <li><a href="legal.html" target="_blank" data-i18n="footer.terms"></a></li>
	                <li><a href="#" data-i18n="footer.contact"></a></li>
	                <li><a href="#" data-i18n="footer.faq"></a></li>
	                <li><a href="#" data-i18n="footer.health"></a></li>
	                <li class="invite_friend"><a href="#" data-i18n="footer.invite"></a></li>
	            </ul>                

	        </nav>
	        <!-- /BOTTOMBAR -->
	    </div>

		<div class="container-fluid health_warning">
	        <div class="row">
	            <div class="col-md-12">
	                <p>
	                    <img src="<?= ASSETS_URL ?>img/footer/health_warning.jpg" alt="Rauchen ist tödlich. Fumer tue. Il fumo uccide.">
	                </p>
	            </div>
	        </div>
	    </div>
		
	</footer>	
</div>
