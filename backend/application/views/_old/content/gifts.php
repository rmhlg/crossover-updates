<div class="mainWrapper" style="top: 0px">

	<section class="section giftsCatalogue">
		<div class="banner">
			<h2>
				<span>
					<strong data-i18n="gifts.title"></strong>
					<span>
						<span data-i18n="gifts.subtitle"></span>
						<span data-i18n="catalogue.youHave"></span>
						<span data-i18n="[html]dataUser.collected"></span>
						<span data-i18n="catalogue.available"></span>
					</span>
				</span>
			</h2>
			<?php
			
				$fill = "";
				$knob = "";
				$california = "";
				$canada = "";
				$alaska = "";
				
				if($registrant->total_km_points>=600 and $registrant->total_km_points<=1399){
					$california = "active";
					$canada = "locked";
					$alaska = "locked";
					$fill = "150px";
					$knob = "146px";
				}else if($registrant->total_km_points>=1400 and $registrant->total_km_points<=3999){
					$alaska = "active";
					$california = "active";
					$canada = "locked";
					$fill="420px";
					$knob = "418px";
				}else if($registrant->total_km_points>=4000){
					$canada = "active";
					$alaska = "active";
					$california = "active";
					$fill="690px";
					$knob = "688px";
				}else{
					$canada = "locked";
					$alaska = "locked";
					$california = "locked";
				}
			?>
			<div class="level">
				<div class="segments">
					<div data-region-id="1" id="region1" onClick="filterGift(1)" class="<?= $california ?>">
						<span>California</span>
						<span>
							<?php 
							if(!empty($california) and $california=="active"){
							?>
							<img src="<?= ASSETS_URL ?>img/gifts/hat.png" alt="">
							<?php
							}
							?>
						</span>
						<span>prize</span>
					</div>
					<div onClick="filterGift(2)"  id="region2" data-region-id="2" class="<?= $alaska ?>">
						<span>Alaska</span>
						<span>
							<?php 
							if(!empty($alaska) and $alaska=="active"){
							?>
							<img src="<?= ASSETS_URL ?>img/gifts/hat.png" alt="">
							<?php
							}
							?>
						</span>
						<span>prize</span>
					</div>
					<div onClick="filterGift(3)" id="region3" data-region-id="3" class="<?= $canada ?>">
						<span>Canada</span>
						<span>
							<?php 
							if(!empty($canada) and $canada=="active"){
							?>
							<img src="<?= ASSETS_URL ?>img/gifts/hat.png" alt="">
							<?php
							}
							?>
						</span>
						<span>prize</span>
					</div>
				</div>
				<div class="range">
					<span class="rail"></span>
					<span class="fill" style="width:<?= $fill ?>"></span>
					<span class="knob" style="left:<?= $knob ?>"></span>
				</div>
			</div>
		</div><!--/ .banner -->

		<div class="container">

			<div class="catalogue" style="width:100%">
					<?php
					$ctr = 0;
					foreach($rows as $row){
						$class = "";
						$clickableButton = true;
						$buttonTxt = "I want this";

						if($row->stock==0){
							$class = "outStock";
							$clickableButton = false;
							$buttonTxt = "OUT OF STOCK";
						}else if(in_array($row->gift_id, $claimed)){
							$class = "claimed";
							$clickableButton = false;
							$buttonTxt = "CLAIMED";
						}else if($registrant->credit_km_points<$row->km_points){
							$class = "locked";
							$clickableButton = false;
							$buttonTxt = "LOCKED";
						}
						// else if($registrant->credit_km_points<$row->minimum_km_points_required){
						// 	$class = "locked";
						// 	$clickableButton = false;
						// }

						if($ctr==0){
						?>
						<div class="row">
							<div id="popUpWrap<?= $row->gift_id ?>" class="gift col-md-4 <?= $class ?>">
								<div class="picture">
									<img src="<?= UPLOADS_URL."backstage/".$row->image ?>" />
								</div>
								<div class="infos">
									<p class="name"><span><?= $row->name ?></span><span class="km">/ <span><?= $row->km_points ?></span> km</span></p>
									<p class="description"><?= $row->description ?></p>											
									<a href="#" id="popUpButton<?= $row->gift_id ?>" onclick="loadGiftInfo(<?= $row->gift_id ?>)" <?php if($clickableButton) echo 'class="iWantButton" data-toggle="modal" data-target="#infoGift"' ?> data-giftId="<?= $row->gift_id ?>"><?= $buttonTxt ?></a>
								</div>
							</div>
						<?php
							$ctr = 1;
						}else if($ctr == 1) {
						?>
							<div id="popUpWrap<?= $row->gift_id ?>" class="gift col-md-4 <?= $class ?>">
								<div class="picture">
									<img src="<?= UPLOADS_URL."backstage/".$row->image ?>" />
								</div>
								<div class="infos">
									<p class="name"><span><?= $row->name ?></span><span class="km">/ <span><?= $row->km_points ?></span> km</span></p>
									<p class="description"><?= $row->description ?></p>											
									<a href="#" id="popUpButton<?= $row->gift_id ?>" onclick="loadGiftInfo(<?= $row->gift_id ?>)" <?php if($clickableButton) echo 'class="iWantButton" data-toggle="modal" data-target="#infoGift"' ?> data-giftId="<?= $row->gift_id ?>"><?= $buttonTxt ?></a>
								
								</div>	
							</div>	
						<?php
							$ctr = 2;
						}else {
						?>
							<div id="popUpWrap<?= $row->gift_id ?>" class="gift col-md-4 <?= $class ?>">
								<div class="picture">
									<img src="<?= UPLOADS_URL."backstage/".$row->image ?>" />
								</div>
								<div class="infos">
									<p class="name"><span><?= $row->name ?></span><span class="km">/ <span><?= $row->km_points ?></span> km</span></p>
									<p class="description"><?= $row->description ?></p>											
									<a href="#" id="popUpButton<?= $row->gift_id ?>" onclick="loadGiftInfo(<?= $row->gift_id ?>)" <?php if($clickableButton) echo 'class="iWantButton" data-toggle="modal" data-target="#infoGift"' ?> data-giftId="<?= $row->gift_id ?>"><?= $buttonTxt ?></a>
									
								</div>
							</div>
							</div>
							<?php
							$ctr = 0;	
						}
					}
					?>				
			</div><!--/ .catalogue -->
			
		</div><!--/ .container -->
		
	</section>
	<!-- FOOTER -->
	<footer>

		<div class="navFooter">                

	        <!-- BOTTOMBAR -->
	        <nav class="bottombar clearfix">

	            <ul class="user">
	                <li><a href="<?= CROSSOVER_STAGING_LOGOUT ?>" data-i18n="footer.logout"></a></li>
	            </ul>

	            <ul class="nav">
	                <li><a href="#" data-i18n="footer.account"></a></li>
	                <li><a href="legal.html" target="_blank" data-i18n="footer.terms"></a></li>
	                <li><a href="#" data-i18n="footer.contact"></a></li>
	                <li><a href="#" data-i18n="footer.faq"></a></li>
	                <li><a href="#" data-i18n="footer.health"></a></li>
	                <li class="invite_friend"><a href="#" data-i18n="footer.invite"></a></li>
	            </ul>                

	        </nav>
	        <!-- /BOTTOMBAR -->
	    </div>

		<div class="container-fluid health_warning">
	        <div class="row">
	            <div class="col-md-12">
	                <p>
	                    <img src="<?= ASSETS_URL ?>img/footer/health_warning.jpg" alt="Rauchen ist tödlich. Fumer tue. Il fumo uccide.">
	                </p>
	            </div>
	        </div>
	    </div>
		
	</footer>

	<!-- MODAL NAV PROFILE -->
	<div class="modal fade profile" id="myProfileInfo">
		<div class="modal-dialog">
		    <div class="modal-content">
			    <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			    </div>
			    <div class="modal-body">
			      	<div class="inputAddCode">
			      		<form action="">
			      			<!-- input text to add code -->
			      			<div class="inputContainer">		      			
				      			<input type="text" name="personalCode" placeholder="Your code here" value="Your code here" maxlength="8" >		      	
				      			<button type="submit" disabled></button>		
				      		</div>	      		
			
							<!-- message to display if ...  -->
							<div class="notifMsg" style="display: none;">
								<!-- the code is correct -->
								<div class="congrats">
									<span data-i18n="modalProfile.validCode"></span> 
								<strong data-i18n="modalProfile.validCodeStrong"></strong>
								</div>
								<!-- the code is wrong -->
								<div class="error">
									<span data-i18n="modalProfile.wrongCode"></span> 
									<strong data-i18n="modalProfile.wrongCodeStrong"></strong>
								</div>
							</div>
			      		</form>	
			      	</div><!-- end .inputAddCode-->

			      	<!-- data user (KM) -->
			        <div class="dataKm">
			        	<div class="col-md-6 col-sm-6">
			        		<span data-i18n="[html]dataUser.collected"></span>
			        		<p data-i18n="modalProfile.counterCollect"></p>
			        	</div>
			        	<div class="col-md-6 col-sm-6">
			        		<span data-i18n="[html]dataUser.credit"></span>
			        		<p data-i18n="modalProfile.counterCredit"></p>
			        	</div>
			        	<div class="clearfix"></div>
			        </div>
					
					<!-- list Link -->
			        <ul class="listLink">
			        	<li><a href="" data-i18n="[html]modalProfile.history"></a></li>
			        	<li><a href="" data-i18n="[html]modalProfile.gifts"></a></li>
			        </ul>

			    </div>
			    <div class="modal-footer">
			      	<a href="profile.htlm" class="btn arrowed" data-i18n="modalProfile.goProfile"></a>
			    </div>
		    </div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

	<!-- MODAL Gifts -->
	<!-- MODAL Gifts -->
	<div class="modal fade default" id="infoGift">
	    <div class="modal-dialog modal-sm">
	      <div class="modal-content">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	            </div>
	            <!-- LOAD HERE -->
	              <!-- Picture of the Gift -->
            <div class="col-sm-6 picture">
                <div class="inner">
                    <img id="popUpImg" />
                </div>              
            </div>

            <div class="contents col-sm-6">
                <div class="row modal-body">
                    <!-- STEP 1 : More infos about the gifts -->
                    <div class="col-sm-4 moreInfos">
                        <h4 class="modal-title">
                            <span id="popUpGiftName"></span>
                            / <i id="popUpKm"></i></span>
                        </h4>
                        
                        <p class="subtitle" id="popUpDescription"></p>
                        
                        <div class="modal-footer">
                            <button type="submit" class="btn" data-i18n="catalogue.links.0.wantThis"></button>
                        </div>
                    </div><!-- end step1 -->

                    <!-- STEP 2 : Confirm you adress -->
                    <div class="col-sm-4 confirm">
                        <h4 class="modal-title" data-i18n="[html]catalogue.modal.0.confirmTitle"></h4>
                        <form action="" class="confirmAdress">
                            <div class="line">
                                <label for="street" data-i18n="catalogue.labelsForm.0.street"></label>
                                <input type="text" name="street" id="street" />
                            </div>
                            
                            <div class="line">
                                <label for="brgy">Barangay:</label>
                                <input type="text" name="brgy" id="brgy" />
                            </div>

                            <div class="line">
                                <label for="city" data-i18n="catalogue.labelsForm.0.city"></label>

                                <!-- add a div.correct (that containing a empty <span>) around the input : valid -->
                                <!-- <div class="correct">                                    
                                    <input type="text" name="city" class="correct" value="valid" />
                                    <span></span>
                                </div> -->
                                <input type="text" name="city" id="city" />
                               
                            </div>
                            
                            <!-- add this class" .error " on a line containing a wrong value -->
                            <div class="line">
                                <label for="province">Province:</label>
                                <input type="text" name="province" id="province" />
                            </div>

                             <div class="line">
                                <label for="postal">Postal Code:</label>
                                <input type="text" name="postal" id="postal" />
                            </div>
                            
                            <!-- display this message if the form is not valid -->
                            <!-- <div class="errorMsg" data-i18n="catalogue.labelsForm.0.error"></div> -->
                            
                            <button type="submit" class="btn" onClick="insertIWantThis($(this))" id="popUpId">Confirm</button>
                        </form>
                    </div><!-- end step2 -->

                    <!-- STEP 3 : statut file -->
                    <div class="col-sm-4 statut">
                        <div class="thanks">
                            <h4 class="modal-title" data-i18n="[html]catalogue.modal.0.thanksTitle"></h4>
                            <p class="subtitle" data-i18n="[html]catalogue.modal.0.thanksSubtitle">
                            <p data-i18n="[html]catalogue.modal.0.thanksContent"></p>
                            <button type="button" class="btn" data-dismiss="modal" aria-label="Close" data-i18n="catalogue.modal.0.btnBakcToList"></button>
                        </div><!-- end valid -->

                        <!-- Error -->
                        <div class="serverError" style="display: none">
                            <h4 data-i18n="[html]catalogue.modal.0.errorTitle"></h4>
                            <p data-i18n="[html]catalogue.modal.0.errorContent"></p>
                            <button type="button" class="btn" data-dismiss="modal" aria-label="Close" data-i18n="catalogue.modal.0.btnBakcToForm"></button>
                        </div><!-- end Error -->
                    </div>
                </div><!-- end modal-body -->
            </div>   <!-- end col-sm-6 -->
            <div class="clearfix"></div>  
	            <!-- LOAD IT HERE -->
	      </div><!-- /.modal-content -->      
	    </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

</div><!-- /.mainWrapper -->