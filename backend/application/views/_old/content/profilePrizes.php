<div class="mainWrapper">		
	<!-- CONTENT -->
	<div class="fullProfile">
		
		<div class="topBanner">
			<div class="inner">
				<div class="userData">
				
					<div class="collectedKm">
						<span data-i18n="[html]dataUser.collected"></span>
					    <p data-i18n="modalProfile.counterCollect"></p>
					</div>

					<div class="profileImg" data-toggle="modal" data-target="#uploadPhoto">
						
							<img src="<?= ASSETS_URL ?>img/user_rounded.png" />
							<!--  	At the beginning, the user hasn't a profile image, so 
									replace the image by these <span>
								<span class="withOutProfileImage"></span>
							 -->
					</div>

					<div class="creditKm">
						<span data-i18n="[html]dataUser.credit"></span>
					    <p data-i18n="modalProfile.counterCredit"></p>
					</div>
				</div>
				<div class="userName">
					<p class="name" data-i18n="dataUser.name"></p>
					<p class="place" data-i18n="dataUser.place"></p>
				</div>
			</div>
			<div class="profileNav">
				<ul>
					<li><a href="profile">My Journey</a></li>
					<li><a href="profilePrizes" class="active">Prizes Claim</a></li>
					<!--<li>
						<a href="#" data-i18n="profile.nav.notifications"></a>
						<span class="profileNotifications">11</span>
					</li>
					<li class="btnRoadTrip"><a href="roadtrip.html" data-i18n="[html]profile.nav.roadtrip"></a></li>-->
				</ul>
			</div>
				
		</div><!-- end .topBanner -->

		<div class="container">
			<div class="row">
				
				<div class="col-sm-12 col-md-4 col-md-offset-1 sideBar profile">
					<div class="col-sm-6  col-md-12">
						<div class="inputAddCode">
					  		<form action="">
					  			<div class="inputContainer">		      			
					      			<input type="text" name="personalCode" placeholder="Your code here" value="Your code here" maxlength="8" >		      	
					      			<button type="submit" disabled></button>		
					      		</div>	      		
					      					      		
					  			<div class="notifMsg">
									<div class="congrats">
										<span data-i18n="modalProfile.validCode"></span> 
										<strong data-i18n="modalProfile.validCodeStrong"></strong>
									</div>
									<div class="error">
										<span data-i18n="modalProfile.wrongCode"></span> 
										<strong data-i18n="modalProfile.wrongCodeStrong"></strong>
									</div>
								</div>  
					  		</form>

					  		<a href="#" data-video="trailer">
					  			<span class="ico_tooltip question"></span><span data-i18n="[html]modalProfile.helpWhereFindCode"></span>
					  		</a>
					  		
					    </div> <!-- end .inputAddCode -->

					    <div class="listLink">
					    	<ul>
						    	<li><a href="invite-friend.html">
						    		<span class="ico inviteFriend"></span>
						    		<span>Invite a <strong>Friend</strong></span>
						    	</a></li>
						    	<li><a href="events">
						    		<span class="ico agenda"></span>
						    		<span>View <strong>Calendar</strong></span>
						    	</a></li>
						    </ul>
					    </div><!-- end .listLink-->
					</div>

					<div class="col-sm-6 col-md-12">
						<div class="history">
					    	<h2>My History</h2>
							<ul>
								<li><strong>Pack Code</strong> KM:<b><?= $packCode ?></b></li>
								<li><strong>Check In</strong> KM:<b><?= $checkIn ?></b></li>
								<li><strong>Event</strong> KM:<b><?= $event ?></b></li>
								<li><strong>Shared</strong> KM:<b><?= $shared ?></b></li>
							</ul>
					    </div><!-- end .history-->
					</div>
				</div><!-- end .sideBar -->

				
				<div class="col-md-7 mainProfileContent">
					<h1>Prizes <strong>Claimed</strong></h1>
					<p>
						Fusce nec odio et 
						<b>mi ornare hendrerit quis non libero</b>
						<br />
						Aliquam eget eros non quam volutpat porta ut in felis.
					</p>

					<div class="expandedContent">
						<?php
						for($i=0; $i<count($gifts); $i++){
						?>
							<div class="checked">
								<div class="summary">
									<div class="col-6">
										<div class="illu">
											<img src='<?= $gifts[$i]['image'] ?>' />
										</div>
										<div class="expDetail">
											<span class="icon bicycle"></span>
											<h4><?= $gifts[$i]['name'] ?></h4>
											<span class="location"><span data-i18n="alaska.surname"></span> / <span data-i18n="alaska.location"></span></span>
										</div>
									</div>
									<div class="col-6">
										<div class="kmDetail">
							        		<span><?= $gifts[$i]['km_points'] ?></span>
							        		<p data-i18n="modalProfile.counterCollect"></p>
							        	
										</div>
										<div class="btnContainer">
											<button class="btnToExpand">
											<span class="openDetails" data-i18n="[html]profile.btn.open">details</span>
											<span class="closeDetails" data-i18n="[html]profile.btn.close">Close</span>
										</button>
										</div>
									
									</div>	
								</div>
								<div class="details">
									<p>
										<?= $gifts[$i]['description'] ?>
									</p>
								</div>				
							</div>
						<?php
						}
						?>
					</div>
				</div><!-- end .mainProfileContent -->

			</div>
		</div>
	</div><!-- end .fullProfile -->

	<!-- MODAL NAV PROFILE -->
	<div class="modal fade profile" id="myProfileInfo">
		<div class="modal-dialog">
		    <div class="modal-content">
			    <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			    </div>
			    <div class="modal-body">
			      	<div class="inputAddCode">
			      		<form action="">
			      			<!-- input text to add code -->
			      			<div class="inputContainer">		      			
				      			<input type="text" name="personalCode" placeholder="Your code here" value="Your code here" maxlength="8" >		      	
				      			<button type="submit" disabled></button>		
				      		</div>	      		
			
							<!-- message to display if ...  -->
							<div class="notifMsg">
								<!-- the code is correct -->
								<div class="congrats">
									<span data-i18n="modalProfile.validCode"></span> 
								<strong data-i18n="modalProfile.validCodeStrong"></strong>
								</div>
								<!-- the code is wrong -->
								<div class="error">
									<span data-i18n="modalProfile.wrongCode"></span> 
									<strong data-i18n="modalProfile.wrongCodeStrong"></strong>
								</div>
							</div>
			      		</form>	
			      	</div><!-- end .inputAddCode-->

			      	<!-- data user (KM) -->
			        <div class="dataKm">
			        	<div class="col-md-6 col-sm-6">
			        		<span data-i18n="[html]dataUser.collected"></span>
			        		<p data-i18n="modalProfile.counterCollect"></p>
			        	</div>
			        	<div class="col-md-6 col-sm-6">
			        		<span data-i18n="[html]dataUser.credit"></span>
			        		<p data-i18n="modalProfile.counterCredit"></p>
			        	</div>
			        	<div class="clearfix"></div>
			        </div>
					
					<!-- list Link -->
			        <ul class="listLink">
			        	<li><a href="" data-i18n="[html]modalProfile.history"></a></li>
			        	<li><a href="" data-i18n="[html]modalProfile.gifts"></a></li>
			        </ul>

			    </div>
			    <div class="modal-footer">
			      	<a href="profile.html" class="btn arrowed" data-i18n="modalProfile.goProfile"></a>
			    </div>
		    </div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

	<!-- FOOTER -->
	<footer>

		<div class="navFooter">                

	        <!-- BOTTOMBAR -->
	        <nav class="bottombar clearfix">

	            <ul class="user">
	                <li><a href="#" data-i18n="footer.logout"></a></li>
	            </ul>

	            <ul class="nav">
	                <li><a href="#" data-i18n="footer.account"></a></li>
	                <li><a href="legal.html" target="_blank" data-i18n="footer.terms"></a></li>
	                <li><a href="#" data-i18n="footer.contact"></a></li>
	                <li><a href="#" data-i18n="footer.faq"></a></li>
	                <li><a href="#" data-i18n="footer.health"></a></li>
	                <li class="invite_friend"><a href="#" data-i18n="footer.invite"></a></li>
	            </ul>                

	        </nav>
	        <!-- /BOTTOMBAR -->
	    </div>

		<div class="container-fluid health_warning">
	        <div class="row">
	            <div class="col-md-12">
	                <p>
	                    <img src="img/footer/health_warning.jpg" alt="Rauchen ist tödlich. Fumer tue. Il fumo uccide.">
	                </p>
	            </div>
	        </div>
	    </div>
		
	</footer>	

	<!-- MODAL help : where is my code  -->
	<div class="modal fade default" id="whereFindCode">
	    <div class="modal-dialog modal-sm">
	      <div class="modal-content">
	          <div class="modal-header">
	              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	          </div>         
	          <div class="modal-body">
	            <div class="inner">
	                <h4 class="modal-title">
	                  <span data-i18n="profile.sideBar.modalHelp.howTo"></span>
	                  <Strong data-i18n="profile.sideBar.modalHelp.find"></Strong>
	                </h4>
	                <p class="subtitle" data-i18n="profile.sideBar.modalHelp.subtitle"></p>  
	                <p data-i18n="[html]profile.sideBar.modalHelp.explanation"></p>           
	            </div>
	                
	          </div>
	      </div><!-- /.modal-content -->      
	    </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
		
	<!-- MODAL PROFILE : upload photo  -->
	<div class="modal fade default" id="uploadPhoto">
	    <div class="modal-dialog modal-sm">
	      <div class="modal-content">
	          <div class="modal-header">
	              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	          </div>  
	          <div class="innerModalUpload">
	            <div class="col-sm-6">
	              <div class="placeholderProfilePhoto">
	                <!-- when the user has upload his photo, 
	                     add a class "updatedPhoto" on  .placeholderProfilePhoto (ex: <div class="placeholderProfilePhoto updatedPhoto">)
	                     and add the image here :


	                     <img data-i18n="[src]dataUser.profileImage;[alt]dataUser.name" />
	                 -->  
	                 <span class="withOutProfileImage"></span>      
	              </div>            
	            </div><!-- end .col-sm-6 -->

	            <div class="col-sm-6">
	              <div class="modal-body">
	                    <h4 class="modal-title">
	                      <span data-i18n="profile.modalUpload.upload"></span>
	                      <Strong data-i18n="profile.modalUpload.photo"></Strong>
	                    </h4>
	                    <p data-i18n="[html]profile.modalUpload.explanation"></p>  

	                    <div class="fileUpload">
	                        <span data-i18n="[html]profile.modalUpload.button"></span>
	                        <input type="file" class="upload" />
	                    </div>   
	              </div>
	                    
	            </div><!-- end .col-sm-6 -->
	            <div class="clearfix"></div>  

	          </div>   <!-- end .innerModalUpload -->  

	      </div><!-- /.modal-content -->      
	    </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

<!-- MODAL VIDEO -->
	<div class="modal fade modal_video" id="modalVideo" >
		<div class="modal-dialog modal-lg">
		    <div class="modal-content">
			    <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>		       
			    </div>
		      	<div class="modal-body video">

					<img src="" alt="" class="ratio poster">

					<div id="jp_container_1" class="jp-video" role="application" aria-label="media player">
						<div class="jp-type-single">
							<div id="js-video" class="jp-jplayer" ></div>
							<div class="jp-gui">
								<div class="jp-video-play">
									<button class="jp-video-play-icon" role="button" tabindex="0">play</button>
								</div>
								<div class="jp-interface">
									<div class="jp-progress">
										<div class="jp-seek-bar">
											<div class="jp-play-bar"></div>
										</div>
									</div>
									<div class="jp-current-time" role="timer" aria-label="time">&nbsp;</div>
									<div class="jp-duration" role="timer" aria-label="duration">&nbsp;</div>
									<div class="jp-controls-holder">
										<div class="jp-controls">
											<button class="jp-play" role="button" tabindex="0">play</button>
											<button class="jp-stop" role="button" tabindex="0">stop</button>
										</div>
										<div class="jp-volume-controls">
											<button class="jp-mute" role="button" tabindex="0">mute</button>
											<button class="jp-volume-max" role="button" tabindex="0">max volume</button>
											<div class="jp-volume-bar">
												<div class="jp-volume-bar-value"></div>
											</div>
										</div>
										<div class="jp-toggles">
											<button class="jp-repeat" role="button" tabindex="0">repeat</button>
											<button class="jp-full-screen" role="button" tabindex="0">full screen</button>
										</div>
									</div>
									<div class="jp-details">
										<div class="jp-title" aria-label="title">&nbsp;</div>
									</div>
								</div>
							</div>
							<div class="jp-no-solution">
								<span>Update Required</span>
								To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
							</div>
						</div>
					</div>
				</div>	      
		    </div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
</div>