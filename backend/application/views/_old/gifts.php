
			<section class="section giftsCatalogue">
				<div class="banner">
					<h2>
						<strong data-i18n="gifts.title"></strong>
					</h2>
					<p>
						<span>
							<span data-i18n="gifts.subtitle"></span>
							<span data-i18n="catalogue.youHave"></span>
							<span data-i18n="[html]dataUser.collected"></span>
							<span data-i18n="catalogue.available"></span>
						</span>
					</p>
				</div><!--/ .banner -->

				<div class="container">

					<div class="row catalogue">
							<div class="gift">
								<div class="picture">
									<img data-i18n="[src]gifts.hat.url;[alt]gifts.hat.name"/>
								</div>
								<div class="infos">
									<p class="name"><span data-i18n="gifts.hat.name"></span><span class="km">/ <span data-i18n="gifts.hat.price"></span> <i data-i18n="dataUser.km"></i></span></p>
									<p class="description" data-i18n="gifts.hat.shortDescription"></p>											
									<a href="#" data-i18n="catalogue.links.0.wantThis" data-toggle="modal" data-target="#infoGiftMobile"></a>
								</div>
							</div>
							<div class="gift claimed">
								<div class="picture">
									<img data-i18n="[src]gifts.bag.url;[alt]gifts.bag.name"/>
								</div>
								<div class="infos">
									<p class="name"><span data-i18n="gifts.bag.name"></span><span class="km">/ <span data-i18n="gifts.bag.price"></span> <i data-i18n="dataUser.km"></i></span></p>
									<p class="description" data-i18n="gifts.bag.shortDescription"></p>
									<a href="#" data-i18n="catalogue.links.0.claimed" data-toggle="modal" data-target="#infoGiftMobile"></a>
								</div>
							</div>
							<div class="gift ">
								<div class="picture">
									<img data-i18n="[src]gifts.gloves.url;[alt]gifts.gloves.name" />
								</div>
								<div class="infos">
									<p class="name"><span data-i18n="gifts.gloves.name"></span><span class="km">/ <span data-i18n="gifts.gloves.price"></span> <i data-i18n="dataUser.km"></i></span></p>
									<p class="description" data-i18n="gifts.gloves.shortDescription"></p>
									<a href="#" data-i18n="catalogue.links.0.wantThis" data-toggle="modal" data-target="#infoGiftMobile"></a>
								</div>
							</div>
							
							<div class="gift">
								<div class="picture">
									<img data-i18n="[src]gifts.goPro.url;[alt]gifts.goPro.name" />
								</div>
								<div class="infos">
									<p class="name"><span data-i18n="gifts.goPro.name"></span><span class="km">/ <span data-i18n="gifts.goPro.price"></span> <i data-i18n="dataUser.km"></i></span></p>
									<p class="description" data-i18n="gifts.goPro.shortDescription"></p>
									<a href="#" data-i18n="catalogue.links.0.wantThis" data-toggle="modal" data-target="#infoGiftMobile"></a>
								</div>
							</div>
							<div class="gift outStock">
								<div class="picture">
									<img data-i18n="[src]gifts.sunglasses.url;[alt]gifts.sunglasses.name"/>
								</div>
								<div class="infos">
									<p class="name"><span data-i18n="gifts.sunglasses.name"></span><span class="km">/ <span data-i18n="gifts.sunglasses.price"></span> <i data-i18n="dataUser.km"></i></span></p>
									<p class="description" data-i18n="gifts.sunglasses.shortDescription"></p>
									<a href="#" data-i18n="catalogue.links.0.outOfStock"></a>
								</div>
							</div>
							<div class="gift">
								<div class="picture">
									<img data-i18n="[src]gifts.watches.url;[alt]gifts.watches.name" />
								</div>
								<div class="infos">
									<p class="name"><span data-i18n="gifts.watches.name"></span><span class="km">/ <span data-i18n="gifts.watches.price"></span> <i data-i18n="dataUser.km"></i></span></p>
									<p class="description" data-i18n="gifts.watches.shortDescription"></p>
									<a href="#" data-i18n="catalogue.links.0.wantThis" data-toggle="modal" data-target="#infoGiftMobile"></a>
								</div>
							</div>
							<div class="gift locked">
								<div class="picture">
									<img data-i18n="[src]gifts.vtt.url;[alt]gifts.vtt.name"/>
								</div>
								<div class="infos">
									<p class="name"><span data-i18n="gifts.vtt.name"></span><span class="km">/ <span data-i18n="gifts.vtt.price"></span> <i data-i18n="dataUser.km"></i></span></p>
									<p class="description" data-i18n="gifts.vtt.shortDescription"></p>
									<a href="#" data-i18n="catalogue.links.0.locked" data-toggle="modal" data-target="#infoGiftMobile"></a>
								</div>
							</div>
							<div class="gift locked">
								<div class="picture">
									<img data-i18n="[src]gifts.iphone.url;[alt]gifts.iphone.name" />
								</div>
								<div class="infos">
									<p class="name"><span data-i18n="gifts.iphone.name"></span><span class="km">/ <span data-i18n="gifts.iphone.price"></span> <i data-i18n="dataUser.km"></i></span></p>
									<p class="description" data-i18n="gifts.iphone.shortDescription"></p>
									<a href="#" data-i18n="catalogue.links.0.locked" data-toggle="modal" data-target="#infoGiftMobile"></a>
								</div>
							</div>
					</div><!--/ .catalogue -->

				</div><!--/ .container -->
				
			</section>



			<!-- FOOTER -->
			<footer>

				<div class="container-fluid health_warning">
			        <div class="row">
			            <div class="col-md-12">
			                <p>
			                    <img src="img/footer/health_warning.jpg" alt="Rauchen ist tödlich. Fumer tue. Il fumo uccide.">
			                </p>
			            </div>
			        </div>
			    </div>
				
			</footer><!-- End footer -->
		</div><!-- end .mainWrapper -->

		<!-- MODAL Gifts -->
		<div class="modal fade default" id="infoGiftMobile">
		    <div class="modal-dialog modal-sm">
		      <div class="modal-content">
		            <div class="modal-header">
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		            </div>
		            
		            <!-- Picture of the Gift -->
		            <div class="col-sm-6 picture">
		                <div class="inner">
		                    <img data-i18n="[src]gifts.goPro.url" alt="Go Pro" src="img/gifts/goPro.png">
		                </div>              
		            </div>

		            <div class="contents col-sm-6">
		                <div class="row modal-body">
		                    <!-- STEP 1 : More infos about the gifts -->
		                    <div class="col-xs-4 moreInfos">
		                        <h4 class="modal-title">
		                            <span data-i18n="gifts.goPro.name"></span>
		                            / <strong data-i18n="gifts.goPro.price"></strong> km</span>
		                        </h4>
		                        
		                        <p class="subtitle"  data-i18n="gifts.goPro.introduction"></p>
		                        <ul>
		                            <li data-i18n="gifts.goPro.listElement1"></li>
		                            <li data-i18n="gifts.goPro.listElement2"></li>
		                            <li data-i18n="gifts.goPro.listElement3"></li>
		                            <li data-i18n="gifts.goPro.listElement4"></li>
		                            <li data-i18n="gifts.goPro.listElement5"></li>
		                        </ul>
		                        <div class="modal-footer">

		                            <!-- if the gift is "LOCKED" don't display this button -->
		                            <button type="submit" class="btn" data-i18n="catalogue.links.0.wantThis"></button>
		                            
		                        </div>
		                    </div><!-- end step1 -->

		                    <!-- STEP 2 : Confirm you adress -->
		                    <div class="col-xs-4 confirm">
		                        <h4 class="modal-title" data-i18n="[html]catalogue.modal.0.confirmTitle"></h4>
		                        <form action="" class="confirmAdress">
		                            <div class="line">
		                                <label for="street" data-i18n="catalogue.labelsForm.0.street"></label>
		                                <input type="text" name="street"/>
		                            </div>
		                            
		                            <div class="line">
		                                <label for="number" data-i18n="catalogue.labelsForm.0.number"></label>
		                                <input type="text" name="number"/>
		                            </div>

		                            <!-- add this class" .error " on a line containing a wrong value -->
		                            <div class="line error">
		                                <label for="zip" data-i18n="catalogue.labelsForm.0.zip"></label>
		                                <input type="text" name="zip" value="not valid"/>
		                            </div>
		                            
		                            <!-- display this message if the form is not valid -->
		                            <div class="errorMsg" data-i18n="catalogue.labelsForm.0.error"></div>
		                            
		                            <button type="submit" class="btn" data-i18n="catalogue.links.0.confirm"></button>
		                        </form>
		                    </div><!-- end step2 -->

		                    <!-- STEP 3 : statut file -->
		                    <div class="col-xs-4">
		                        <div class="thanks">
		                            <h4 class="modal-title" data-i18n="[html]catalogue.modal.0.thanksTitle"></h4>
		                            <p class="subtitle" data-i18n="[html]catalogue.modal.0.thanksSubtitle">
		                            <p data-i18n="[html]catalogue.modal.0.thanksContent"></p>
		                            <button type="button" class="btn" data-dismiss="modal" aria-label="Close" data-i18n="catalogue.modal.0.btnBakcToList"></button>
		                        </div><!-- end valid -->

		                        <!-- Error -->
		                        <div class="serverError" style="display: none">
		                            <h4 data-i18n="[html]catalogue.modal.0.errorTitle"></h4>
		                            <p data-i18n="[html]catalogue.modal.0.errorContent"></p>
		                            <button type="button" class="btn" data-dismiss="modal" aria-label="Close" data-i18n="catalogue.modal.0.btnBakcToForm"></button>
		                        </div><!-- end Error -->
		                    </div>
		                    

		                </div><!-- end modal-body -->
		            </div>   <!-- end col-sm-6 -->
		            <div class="clearfix"></div>  
		      </div><!-- /.modal-content -->      
		    </div><!-- /.modal-dialog -->
		</div><!-- /.modal -->

		<script>
			var siteUrl = '<?=site_url()?>';
			var baseUrl = '<?=base_url()?>';
			var assetsUrl = '<?=ASSETS_URL?>';
			var segments = '<?=$this->uri->segment(2)?>';
		</script>
		<script src="<?=ASSETS_URL?>js/scripts.min.js"></script>
		<script src="<?=ASSETS_URL?>js/main.js"></script>
	</body>
</html>