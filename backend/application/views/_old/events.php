	<!-- CONTENT -->
	<section class="events">
		<!--div class="filter-menu">
			<ul>
				<li>
					<select class="select-l">
						<option value="#">Select Venue</option>
					</select>
				</li>
				<li>
					<select class="select-s">
						<option value="#">Start Date</option>
					</select>
				</li>
				<li>
					<select class="select-s">
						<option value="#">End Date</option>
					</select>
				</li>
			</ul>

		</div>
		<div class="events-list">
			<div class="item">
				<div class="thumbnails">
					<img src="../img/events-thumb.png" alt="thumbnail"/>
				</div>
				<div class="content">
					<h3>Spade Superclub</h3>
					<h6>Pasig City, National Capital Region (NCR) <br/> March 12 to June 25, 2015</h6><br/>
					<p>
						Bring your group of adult smoker friends and don't miss out on the exciting activities that Marlboro has prepared for you!
					</p>
				</div>
				<br class="clear">
			</div>
			<div class="item">
				<div class="thumbnails">
					<img src="../img/events-thumb.png" alt="thumbnail"/>
				</div>
				<div class="content">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore tempora quos deserunt obcaecati debitis assumenda, aliquam facere corporis voluptate veritatis, accusantium, adipisci amet sed, corrupti nesciunt doloremque optio ea possimus.
				</div>
				<br class="clear">
			</div>
			<br class="clear">
			<div class="item">
				<div class="thumbnails">
					<img src="../img/events-thumb.png" alt="thumbnail"/>
				</div>
				<div class="content">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore tempora quos deserunt obcaecati debitis assumenda, aliquam facere corporis voluptate veritatis, accusantium, adipisci amet sed, corrupti nesciunt doloremque optio ea possimus.
				</div>
				<br class="clear">
			</div>
			<div class="item">
				<div class="thumbnails">
					<img src="../img/events-thumb.png" alt="thumbnail"/>
				</div>
				<div class="content">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore tempora quos deserunt obcaecati debitis assumenda, aliquam facere corporis voluptate veritatis, accusantium, adipisci amet sed, corrupti nesciunt doloremque optio ea possimus.
				</div>
				<br class="clear">
			</div>
			<br class="clear">
			<div class="item">
				<div class="thumbnails">
					<img src="../img/events-thumb.png" alt="thumbnail"/>
				</div>
				<div class="content">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore tempora quos deserunt obcaecati debitis assumenda, aliquam facere corporis voluptate veritatis, accusantium, adipisci amet sed, corrupti nesciunt doloremque optio ea possimus.
				</div>
				<br class="clear">
			</div>
			<div class="item">
				<div class="thumbnails">
					<img src="../img/events-thumb.png" alt="thumbnail"/>
				</div>
				<div class="content">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore tempora quos deserunt obcaecati debitis assumenda, aliquam facere corporis voluptate veritatis, accusantium, adipisci amet sed, corrupti nesciunt doloremque optio ea possimus.
				</div>
				<br class="clear">
			</div>
			<br class="clear">
		</div-->
	</section>

	<!-- MODAL NAV PROFILE -->
	<div class="modal fade profile" id="myProfileInfo">
		<div class="modal-dialog">
		    <div class="modal-content">
			    <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			    </div>
			    <div class="modal-body">
			      	<div class="inputAddCode">
			      		<form action="">
			      			<!-- input text to add code -->
			      			<div class="inputContainer">
				      			<input type="text" name="personalCode" placeholder="Your code here" value="Your code here" maxlength="8" >
				      			<button type="submit" disabled></button>
				      		</div>

							<!-- message to display if ...  -->
							<div class="notifMsg" style="display: none;">
								<!-- the code is correct -->
								<div class="congrats">
									<span data-i18n="modalProfile.validCode"></span>
								<strong data-i18n="modalProfile.validCodeStrong"></strong>
								</div>
								<!-- the code is wrong -->
								<div class="error">
									<span data-i18n="modalProfile.wrongCode"></span>
									<strong data-i18n="modalProfile.wrongCodeStrong"></strong>
								</div>
							</div>
			      		</form>
			      	</div><!-- end .inputAddCode-->

			      	<!-- data user (KM) -->
			        <div class="dataKm">
			        	<div class="col-md-6 col-sm-6">
			        		<span data-i18n="[html]dataUser.collected"></span>
			        		<p data-i18n="modalProfile.counterCollect"></p>
			        	</div>
			        	<div class="col-md-6 col-sm-6">
			        		<span data-i18n="[html]dataUser.credit"></span>
			        		<p data-i18n="modalProfile.counterCredit"></p>
			        	</div>
			        	<div class="clearfix"></div>
			        </div>

					<!-- list Link -->
			        <ul class="listLink">
			        	<li><a href="" data-i18n="[html]modalProfile.history"></a></li>
			        	<li><a href="" data-i18n="[html]modalProfile.gifts"></a></li>
			        </ul>

			    </div>
			    <div class="modal-footer">
			      	<a href="profile.htlm" class="btn arrowed" data-i18n="modalProfile.goProfile"></a>
			    </div>
		    </div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

	<!-- FOOTER -->
	<footer>

		<div class="navFooter">

	        <!-- BOTTOMBAR -->
	        <nav class="bottombar clearfix">

	            <ul class="user">
	                <li><a href="#" data-i18n="footer.logout"></a></li>
	            </ul>

	            <ul class="nav">
	                <li><a href="#" data-i18n="footer.account"></a></li>
	                <li><a href="legal.html" target="_blank" data-i18n="footer.terms"></a></li>
	                <li><a href="#" data-i18n="footer.contact"></a></li>
	                <li><a href="#" data-i18n="footer.faq"></a></li>
	                <li><a href="#" data-i18n="footer.health"></a></li>
	                <li class="invite_friend"><a href="#" data-i18n="footer.invite"></a></li>
	            </ul>

	        </nav>
	        <!-- /BOTTOMBAR -->
	    </div>

		<div class="container-fluid health_warning">
	        <div class="row">
	            <div class="col-md-12">
	                <p>
	                    <img src="img/footer/health_warning.jpg" alt="Rauchen ist tödlich. Fumer tue. Il fumo uccide.">
	                </p>
	            </div>
	        </div>
	    </div>

	</footer>

	<!-- MODAL VIDEO -->
	<div class="modal fade modal_video" id="modalVideo" >
		<div class="modal-dialog modal-lg">
		    <div class="modal-content">
			    <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			    </div>
		      	<div class="modal-body video">

					<img src="" alt="" class="ratio poster">

					<div id="jp_container_1" class="jp-video" role="application" aria-label="media player">
						<div class="jp-type-single">
							<div id="js-video" class="jp-jplayer" ></div>
							<div class="jp-gui">
								<div class="jp-video-play">
									<button class="jp-video-play-icon" role="button" tabindex="0">play</button>
								</div>
								<div class="jp-interface">
									<div class="jp-progress">
										<div class="jp-seek-bar">
											<div class="jp-play-bar"></div>
										</div>
									</div>
									<div class="jp-current-time" role="timer" aria-label="time">&nbsp;</div>
									<div class="jp-duration" role="timer" aria-label="duration">&nbsp;</div>
									<div class="jp-controls-holder">
										<div class="jp-controls">
											<button class="jp-play" role="button" tabindex="0">play</button>
											<button class="jp-stop" role="button" tabindex="0">stop</button>
										</div>
										<div class="jp-volume-controls">
											<button class="jp-mute" role="button" tabindex="0">mute</button>
											<button class="jp-volume-max" role="button" tabindex="0">max volume</button>
											<div class="jp-volume-bar">
												<div class="jp-volume-bar-value"></div>
											</div>
										</div>
										<div class="jp-toggles">
											<button class="jp-repeat" role="button" tabindex="0">repeat</button>
											<button class="jp-full-screen" role="button" tabindex="0">full screen</button>
										</div>
									</div>
									<div class="jp-details">
										<div class="jp-title" aria-label="title">&nbsp;</div>
									</div>
								</div>
							</div>
							<div class="jp-no-solution">
								<span>Update Required</span>
								To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
							</div>
						</div>
					</div>
				</div>
		    </div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

</div>
		<script>
			var siteUrl = '<?=site_url()?>';
			var baseUrl = '<?=base_url()?>';
			var assetsUrl = '<?=ASSETS_URL?>';
			var segments = '<?=$this->uri->segment(2)?>';
		</script>

		<script src="<?= ASSETS_URL ?>js/scripts.min.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"></script> 	
		<script src="<?= ASSETS_URL ?>js/main.js"></script>
		<script type="text/javascript">
		//$("#myModal").modal();
		</script>
	</body>
</html>
