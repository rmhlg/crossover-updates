<div class="mainWrapper scrollable">		
	
	<!-- HEADER -->
	<header>
		<div class="container">
			
			<!-- Main navigation -->
			<ul class="nav">
				<li class="home">
					<a href="<?= SITE_URL ?>home">
						<img src="<?=ASSETS_URL?>img/crossover.png" alt="Malboro - Cross|Over" />
					</a>
				</li>
				<?php /* <li class="<?= $this->router->class == 'home' ? 'active' : '' ?>">
					<a href="<?= SITE_URL ?>home" data-i18n="header.links.home"></a>
				</li> */ ?>
				<li class="<?= $this->router->class == 'maps' ? 'active' : '' ?>">
					<a href="<?= SITE_URL ?>maps" data-i18n="header.links.map"></a>
				</li>
				<li class="<?= $this->router->class == 'gifts' ? 'active' : '' ?>">
					<a href="<?= SITE_URL ?>gifts" data-i18n="header.links.gifts"></a>
				</li>
				<li class="<?= $this->router->class == 'events' ? 'active' : '' ?>">
					<a href="<?= SITE_URL ?>events" data-i18n="header.links.events"></a>
				</li>
				<li class="<?= $this->router->class == 'how_to' ? 'active' : '' ?>">
					<a href="<?= SITE_URL ?>how-to" data-i18n="header.links.howTo"></a>
				</li>
				<!-- activities links call a new panel of menu: Cfr below sub-menu .activities  -->
				<li class="sliderActivities <?= $this->router->class == 'activities' ? 'active' : '' ?>">
					<a href="#" data-i18n="header.links.activities"></a>
				</li>
				<li class="<?= $this->router->class == 'network' ? 'active' : '' ?>">
					<a href="<?= SITE_URL ?>network">Forum</a>
				</li>

				<!-- data user + Call modal Profile-->
				<li class="navProfile">
					<ul>
						<!-- open modal Profile + add focus on input addCode -->
						<li class="addCode">
							<p><strong class="collected_km_points"><?= @$this->session->userdata('total_km_points') ?></strong>km</p>
							<p><a href="#" class="addCodeTo" data-i18n="header.addCode"></a></p>				
						</li>
						<!-- open modal Profile  -->
						<li class="profileBtn">
							<a href="javascript:void(0)" data-toggle="modal" data-target="#myProfileInfo">
								<span class="border">
									<img src="<?= $this->session->userdata('profile_picture') ?>" alt"user name" />
								</span>
								<span class="arrowDown"></span>
							</a>
						</li>
					</ul>
				</li>
				
			</ul>	
			
		</div>

		<!-- sub-menu: display a slider with all the activities -->
		<div class="activities">
			<div class="slider">
				<?php if($experience): foreach($experience as $k => $v): ?>
					<?php $points = (int)$this->session->userdata('total_km_points'); ?>
					<?php $accessible = FALSE; ?>
					<!-- Condition if region is already unlocked -->
						<?php if($v->category_id == REGION_CALIFORNIA): ?>
							<?php if($points > CALIFORNIA_START_POINTS || $points == CALIFORNIA_START_POINTS): ?>
								<?php $accessible = TRUE; ?>
							<?php endif; ?>
						<?php elseif($v->category_id == REGION_ALASKA): ?>
							<?php if($points >= ALASKA_START_POINTS): ?>
								<?php $accessible = TRUE; ?>
							<?php endif; ?>
						<?php elseif($v->category_id == REGION_CANADA): ?>
							<?php if($points >= CANADA_START_POINTS): ?>
								<?php $accessible = TRUE; ?>
							<?php endif; ?>
						<?php endif; ?>
						<div class="<?= $accessible === TRUE ? 'checked' : 'locked' ?>">
							<?php if($accessible === TRUE): ?>
								<a href="<?= SITE_URL ?>activities/<?= $v->experience_id ?>/<?= url_title($v->title) ?>" <?php /*onclick="earnPoints('<?= encrypt_points(ACTIVITY_SELECT) ?>', '<?= encrypt_points($v->experience_id) ?>', '<?= encrypt_points(0) ?>')"*/ ?>>
							<?php else: ?>
								<a href="javascript:void(0);">
							<?php endif; ?>
								<div class="illu">
									<img src="<?=$v->thumbnail?>">
								</div>
								<div>
									<span class="icon bicycle"></span>
									<h4><?=$v->title?></h4>
									<span class="location"><?= unserialize(REGIONS_ALL)[$v->category_id]; ?><span>
								</div>
							</a>
						</div>
					<!-- Condition if region is already unlocked -->
				<?php endforeach; endif; ?>	
			</div>
		</div>
		
	</header>

<!-- MODAL NAV PROFILE -->
	<div class="modal fade profile" id="myProfileInfo">
		<div class="modal-dialog">
		    <div class="modal-content">
			    <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label"=Close"><span aria-hidden="true">&times;</span></button>
			    </div>
			    <div class="modal-body">
			      	<div class="inputAddCode">
			      		<form method="POST" action="#" onsubmit="return validateCode()" id="form-header">
			      			<!-- input text to add code -->
			      			<div class="inputContainer">		      			
				      			<input type="text" name="personalCodes" placeholder="Your code here" value="Your code here" maxlength="8" >		      	
				      			<button type="submit" disabled></button>		
				      		</div>	      		
			
							<!-- message to display if ...  -->
							<div class="notifMsg" style="display: none">
								<!-- the code is correct -->
								<div class="congrats" style="display: none">
									<span></span> 
									<strong></strong>
								</div>
								<!-- the code is wrong -->
								<div class="error" style="display: none">
									<span></span> 
									<strong></strong>
								</div>
							</div>
			      		</form>	
			      	</div><!-- end .inputAddCode-->

			      	<!-- data user (KM) -->
			        <div class="dataKm">
			        	<div class="col-md-6 col-sm-6">
			        		<span><strong class="collected_km_points"><?= $total_km_points ?></strong>&nbsp;km</span>
			        		<p data-i18n="modalProfile.counterCollect"></p>
			        	</div>
			        	<div class="col-md-6 col-sm-6">
			        		<span><strong class="credit_km_points"><?= $credit_km_points ?></strong>km</span>
			        		<p data-i18n="modalProfile.counterCredit"></p>
			        	</div>
			        	<div class="clearfix"></div>
			        </div>
					
					<!-- list Link -->
			        <ul class="listLink">
			        	<li><a href="" data-i18n="[html]modalProfile.history"></a></li>
			        	<li><a href="" data-i18n="[html]modalProfile.gifts"></a></li>
			        </ul>

			    </div>
			    <div class="modal-footer">
			      	<a href="<?= SITE_URL ?>profile" class="btn arrowed" data-i18n="modalProfile.goProfile"></a>
			    </div>
		    </div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->