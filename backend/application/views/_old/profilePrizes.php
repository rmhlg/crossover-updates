
			<div class="trailer mobileVideo" id="trailer">
				<video controls>
					<source src="videos/trailer/trailer.mp4" type="video/mp4" />
					<source src="videos/trailer/trailer.ogv" type="video/ogg" />
					<source src="videos/trailer/trailer.webm" type="video/webm" />
				</video>
				<button type="button" class="close">
					<span>×</span>
				</button>
			</div>
			
			<!-- CONTENT -->

			<div class="fullProfile">
				<div class="topBanner">
					<div class="inner">
						<div class="userData">
						
							<div class="collectedKm">
								<span data-i18n="[html]dataUser.collected"></span>
							    <p data-i18n="mobile.profile.nav.counterCollect"></p>
							</div>

							<div class="profileImg" data-toggle="modal" data-target="#uploadPhoto">
								
									<img data-i18n="[src]dataUser.profileImage;[alt]dataUser.name" />
									<!--  	At the beginning, the user hasn't a profile image, so 
											replace the image by these <span>
										<span class="withOutProfileImage"></span>
									 -->
							</div>

							<div class="creditKm">
								<span data-i18n="[html]dataUser.credit"></span>
							    <p data-i18n="mobile.profile.nav.counterCredit"></p>
							</div>
						</div>
					</div>
					<div class="profileNav">
						<ul>
							<li><a href="profile.html" data-i18n="profile.nav.journey" class="active"></a></li>
							<li><a href="profile-prizes.html" data-i18n="profile.nav.prizes"></a></li>
							<!--<li>
								<a href="#" data-i18n="profile.nav.notifications"></a>
								<span class="profileNotifications">11</span>
							</li>
							<li class="btnRoadTrip"><a href="roadtrip.html" data-i18n="[html]profile.nav.roadtrip"></a></li>-->
						</ul>
					</div>						
				</div><!-- end .topBanner -->

				<div class="container">
					<div class="row">
						<div class="col-md-7 mainProfileContent">

							<div class="expandedContent">

								<div class="checked">
									<div class="summary">
										<div class="col-6">
											<div class="illu">
												<img data-i18n="[src]mobile.profile.journey.experience1.0.experienceImg" />
											</div>
											
										</div>
										<div class="col-6">
											<div class="expDetail">
												<span class="icon bicycle"></span>
												<h4 data-i18n="alaska.one.name"></h4>
												<span class="location"><span data-i18n="alaska.surname"></span> / <span data-i18n="alaska.location"></span></span>
											</div>
											<div class="kmDetail">
								        		<span data-i18n="[html]profile.journey.experience1.0.totalOfKmWon"></span>
								        		<p data-i18n="modalProfile.counterCollect"></p>
								        	
											</div>
											<div class="btnContainer">
												<a href="#" class="btnToExpand">details</a>
											</div>
										
										</div>	
									</div>
									<div class="details">
										<p>
											<span data-i18n="[html]profile.journey.details.activityUnlocked"></span>
											<a href="activities.html" data-i18n="[html]profile.journey.experience1.0.nameOfActivityUnlocked"></a>
											<span class="kmWon" data-i18n="[html]profile.journey.experience1.0.kmWonAfterUnlocked"></span>
										</p>

										<p>
											<span data-i18n="[html]profile.journey.details.acitivityChoices"></span>
											<a href="activities.html" data-i18n="[html]profile.journey.experience1.0.nameOfAcitivityChoices"></a>
											<span class="kmWon" data-i18n="[html]profile.journey.experience1.0.kmWonAfterChoiced"></span>
										</p>

										<p>
											<span data-i18n="[html]profile.journey.details.gamePlayed"></span>
											<a href="activities.html" data-i18n="[html]profile.journey.experience1.0.numberofGamePlayed"></a>
											<span class="kmWon" data-i18n="[html]profile.journey.experience1.0.kmWonAfterplayed"></span>
										</p>
									</div>				
								</div>

								<div class="locked">
									<div class="summary">
										<div class="col-6">
											<div class="illu">
												<img data-i18n="[src]mobile.profile.journey.experience1.0.experienceImg" />
											</div>
											
										</div>
										<div class="col-6">
											<div class="expDetail">
												<span class="icon bicycle"></span>
												<h4 data-i18n="alaska.one.name"></h4>
												<span class="location"><span data-i18n="alaska.surname"></span> / <span data-i18n="alaska.location"></span></span>
											</div>
											<div class="kmDetail">
								        		<span data-i18n="[html]profile.journey.experience1.0.totalOfKmWon"></span>
								        		<p data-i18n="modalProfile.counterCollect"></p>
								        	
											</div>
											<div class="btnContainer">
												<a href="activities.html" data-i18n="[html]profile.btn.discover"></a>
											</div>
										
										</div>	
									</div>
									<!-- if the experience is locked, you can remove this content
									<div class="details">
										<p>
											<span data-i18n="[html]profile.journey.details.activityUnlocked"></span>
											<a href="activities.html" data-i18n="[html]profile.journey.experience1.0.nameOfActivityUnlocked"></a>
											<span class="kmWon" data-i18n="[html]profile.journey.experience1.0.kmWonAfterUnlocked"></span>
										</p>

										<p>
											<span data-i18n="[html]profile.journey.details.acitivityChoices"></span>
											<a href="activities.html" data-i18n="[html]profile.journey.experience1.0.nameOfAcitivityChoices"></a>
											<span class="kmWon" data-i18n="[html]profile.journey.experience1.0.kmWonAfterChoiced"></span>
										</p>

										<p>
											<span data-i18n="[html]profile.journey.details.gamePlayed"></span>
											<a href="activities.html" data-i18n="[html]profile.journey.experience1.0.numberofGamePlayed"></a>
											<span class="kmWon" data-i18n="[html]profile.journey.experience1.0.kmWonAfterplayed"></span>
										</p>
									</div>	-->			
								</div>
								<div class="locked">
									<div class="summary">
										<div class="col-6">
											<div class="illu">
												<img data-i18n="[src]mobile.profile.journey.experience1.0.experienceImg" />
											</div>
											
										</div>
										<div class="col-6">
											<div class="expDetail">
												<span class="icon bicycle"></span>
												<h4 data-i18n="alaska.one.name"></h4>
												<span class="location"><span data-i18n="alaska.surname"></span> / <span data-i18n="alaska.location"></span></span>
											</div>
											<div class="kmDetail">
								        		<span data-i18n="[html]profile.journey.experience1.0.totalOfKmWon"></span>
								        		<p data-i18n="modalProfile.counterCollect"></p>
								        	
											</div>
											<div class="btnContainer">
												<a href="activities.html" data-i18n="[html]profile.btn.discover"></a>
											</div>
										
										</div>	
									</div>
									<!-- if the experience is locked, you can remove this content
									<div class="details">
										<p>
											<span data-i18n="[html]profile.journey.details.activityUnlocked"></span>
											<a href="activities.html" data-i18n="[html]profile.journey.experience1.0.nameOfActivityUnlocked"></a>
											<span class="kmWon" data-i18n="[html]profile.journey.experience1.0.kmWonAfterUnlocked"></span>
										</p>

										<p>
											<span data-i18n="[html]profile.journey.details.acitivityChoices"></span>
											<a href="activities.html" data-i18n="[html]profile.journey.experience1.0.nameOfAcitivityChoices"></a>
											<span class="kmWon" data-i18n="[html]profile.journey.experience1.0.kmWonAfterChoiced"></span>
										</p>

										<p>
											<span data-i18n="[html]profile.journey.details.gamePlayed"></span>
											<a href="activities.html" data-i18n="[html]profile.journey.experience1.0.numberofGamePlayed"></a>
											<span class="kmWon" data-i18n="[html]profile.journey.experience1.0.kmWonAfterplayed"></span>
										</p>
									</div>	-->			
								</div>

							</div>
						</div>			<!-- end .mainProfileContent-->
					</div>
				</div>
			</div><!-- end .fullProfile -->

			<!-- FOOTER -->
			<footer>

				<div class="container-fluid health_warning">
			        <div class="row">
			            <div class="col-md-12">
			                <p>
			                    <img src="img/footer/health_warning.jpg" alt="Rauchen ist tödlich. Fumer tue. Il fumo uccide.">
			                </p>
			            </div>
			        </div>
			    </div>
				
			</footer>	

		</div><!-- end .mainWrapper -->

		<!-- MODAL PROFILE : upload photo  -->
		<div class="modal fade default" id="uploadPhoto">
		    <div class="modal-dialog modal-sm">
		      <div class="modal-content">
		          <div class="modal-header">
		              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		          </div>  
		          <div class="innerModalUpload">
		            <div class="col-sm-6">
		              <div class="placeholderProfilePhoto">
		                <!-- when the user has upload his photo, 
		                     add a class "updatedPhoto" on  .placeholderProfilePhoto (ex: <div class="placeholderProfilePhoto updatedPhoto">)
		                     and add the image here :


		                     <img data-i18n="[src]dataUser.profileImage;[alt]dataUser.name" />
		                 -->  
		                 <span class="withOutProfileImage"></span>      
		              </div>            
		            </div><!-- end .col-sm-6 -->

		            <div class="col-sm-6">
		              <div class="modal-body">
		                    <h4 class="modal-title">
		                      <span data-i18n="profile.modalUpload.upload"></span>
		                      <Strong data-i18n="profile.modalUpload.photo"></Strong>
		                    </h4>
		                    <p data-i18n="[html]profile.modalUpload.explanation"></p>  

		                    <div class="fileUpload">
		                        <span data-i18n="[html]profile.modalUpload.button"></span>
		                        <input type="file" class="upload" />
		                    </div>   
		              </div>
		                    
		            </div><!-- end .col-sm-6 -->
		            <div class="clearfix"></div>  

		          </div>   <!-- end .innerModalUpload -->  

		      </div><!-- /.modal-content -->      
		    </div><!-- /.modal-dialog -->
		</div><!-- /.modal -->

		<script>
			var siteUrl = '<?=site_url()?>';
			var baseUrl = '<?=base_url()?>';
			var assetsUrl = '<?=ASSETS_URL?>';
			var segments = '<?=$this->uri->segment(2)?>';
		</script>
		<script src="<?=ASSETS_URL?>js/scripts.min.js"></script>
	</body>
</html>