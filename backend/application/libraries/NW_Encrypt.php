<?php

class NW_Encrypt extends CI_Encrypt
{
	public function password($password, $salt)
	{
		return parent::sha1($password.'{'.$salt.'}');
	}
}