<?php

class Header_Model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		define('SITE_URL', site_url());
		define('ASSETS_URL', base_url().'assets/');
		define('UPLOADS_URL', base_url().'uploads/');
		define('ENCRYPTION_KEY', $this->config->item('encryption_key'));
	}

	public function get_header()
	{
		$data['data_content'] = $this->router->class;
		$data['total_km_points'] = $this->session->userdata('total_km_points');
		$data['credit_km_points'] = $this->session->userdata('credit_km_points');
		$data['experience'] = $this->db->select()->from('tbl_experience')->get()->result();
		$view = $this->load->view('headers/main-header', $data);

		return $view;
	}

	public function get_experience_header()
	{
		$data['total_km_points'] = $this->session->userdata('total_km_points');
		$data['credit_km_points'] = $this->session->userdata('credit_km_points');

		$data['experience'] = array_merge(
			$this->db->select()->from('tbl_experience')->where('category_id', REGION_CALIFORNIA)->limit(EXPERIENCE_HEADER_LIMIT)->order_by('order', 'ASC')->get()->result(),
			$this->db->select()->from('tbl_experience')->where('category_id', REGION_ALASKA)->limit(EXPERIENCE_HEADER_LIMIT)->order_by('order', 'ASC')->get()->result(),
			$this->db->select()->from('tbl_experience')->where('category_id', REGION_CANADA)->limit(EXPERIENCE_HEADER_LIMIT)->order_by('order', 'ASC')->get()->result()
		);

		$view = $this->load->view('headers/experience-header', $data);

		return $view;
	}

	public function get_region($experience_id)
	{
		if($experience_id) {
			$data = $this->db->select('*, tbl_experience.title as experience_title')->from('tbl_experience')->where('tbl_experience.experience_id', $experience_id)->get()->row();
			if($data) {
				return $data->category_id;
			} else {
				return NULL;
			}
		} else {
			return NULL;
		}
	}

}