<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

class Km_Points_Model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}

	public function earn(array $details)
	{
		if($details) {
			$this->db->set('total_km_points', 'total_km_points + '.$details['points'], FALSE);
			$this->db->set('credit_km_points', 'credit_km_points + '.$details['points'], FALSE);
			$this->db->where('user_id', $this->session->userdata('user_id'));
			$this->db->update('tbl_registrants');

			$this->db = $this->load->database('marlboro_local', TRUE);
			$this->db->set('total_km_points', 'total_km_points + '.$details['points'], FALSE);
			$this->db->set('credit_km_points', 'credit_km_points + '.$details['points'], FALSE);
			$this->db->where('registrant_id', $this->session->userdata('user_id'));
			$this->db->update('tbl_registrants');
			$this->db->close();
			$this->db = $this->load->database('localhost', TRUE);

			$row = $this->db->select()->from('tbl_registrants')->where('user_id', SESSION_USER_ID)->get()->row();
			if($row) {
				$this->session->set_userdata('total_km_points', $row->total_km_points);
			}
		}
	}
	
	public function earn_history(array $details)
	{
		if($details) {
			$this->db->set('user_id', $this->session->userdata('user_id'));
			$this->db->set('origin_id', $details['origin_id']);
			$this->db->set('sub_origin_id', $details['sub_origin_id']);
			$this->db->set('main_sub_origin_id', $details['main_sub_origin_id']);
			$this->db->set('points_earned', $details['points']);
			$this->db->insert('tbl_points');
		}
	}

	public function points_history(array $details)
	{
		if($details) {
			$this->db->set('registrant_id', $details['registrant_id']);
			$this->db->set('points_earned', $details['points_earned']);
			$this->db->set('module', $details['module']);
			$this->db->set('description', $details['description']);
			$this->db->insert('tbl_registrant_points_history');
		}
	}
}