<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Referral_Model extends CI_Model {
	

	public function __construct() {
		parent::__construct();
	}

	public function save($email,$referer, $code, $name) {
		//$registered = $this->db->where('email_address',$email)->get('tbl_registrants')->num_rows();
		$registered = false;
		$this->db = $this->db->load('marlboro_local', TRUE);
		$pending = $this->db->where('email',$email)->get('tbl_referrals')->num_rows();
		
		if($registered){
			$result = 'registered';
		}elseif ($pending) {
			$result = 'pending';
		}else{
			$this->db->insert(
				'tbl_referrals', 
				array('email' => $email, 
				  	'referer' => $referer,
				      'code' => $code,
				      'name' => $name,
					)
				);
			$result = 'success';
		}

		return $result;
	}
}