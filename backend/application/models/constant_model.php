<?php

class Constant_Model extends CI_Model {
	
	function __construct()
	{
		/* Origin IDs */
			define('ACTIVITY', 1);
			define('ACTIVITY_READ', 2);
			define('HOME', 3);
			define('CODE', 4);

		/* Main Sub-Origin IDs */
			define('READ', 1);
			define('WATCH', 2);
			define('DECISION_LEFT', 3);
			define('DECISION_RIGHT', 4);

		/* Site Url */
			define('SITE_URL', site_url());

		/* Assets Url */
			define('ASSETS_URL', base_url().'assets/');

		/* Uploads Url */
			define('UPLOADS_URL', base_url().'uploads/');

		/* Gift Types */
			define('GIFT_TYPE_ACTIVITY', 1);
			define('GIFT_TYPE_SPECIAL', 2);
		/* Redemption Types */
			define('GIFT_REDEMPTION_DELIVERY', 1);
			define('GIFT_REDEMPTION_REDEMPTION', 2);
		/* Regions */
			define('REGION_ALASKA', 1);
			define('REGION_CALIFORNIA', 2);
			define('REGION_CANADA', 3);

		/* Code Limits */
			define('PACK_CODE_LIMIT', 30);
			define('LAMP_DRIVE_LIMIT', 24);
			define('ELD_LIMIT', 12);
			define('LEAVE_BEHIND_LIMIT', 1);
			define('KA_LIMIT', 1);
		/* Code Types */
			define('PACK_CODE', 'Pack Codes');
			define('LAMP_DRIVE', 'LAMP Drive');
			define('ELD', 'ELD');
			define('LEAVE_BEHIND', 'Leave Behind');
			define('KA', 'KA Codes');

		/* Points */
			/* Region Unlocking Points */
				define('CALIFORNIA_START_POINTS', 0);
				define('ALASKA_START_POINTS', 1399);
				define('CANADA_START_POINTS', 3999);

			/* Activities */
				define('ACTIVITIES_EARN_POINTS', 40);
				define('HOMEPAGE_EARN_POINTS', 50);

				define('READ_CONTENT', 1);
				define('WATCH_VIDEO', 2);

	}
	
}