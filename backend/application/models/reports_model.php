<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

class Reports_Model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}

	public function user_activity(array $details)
	{
		if($details) {
			$params['user_id'] = $details['user_id'];
			$params['activity'] = $details['activity'];
			$params['km_points'] = $details['km_points'];
			if($details['activity'] == UA_WATCH) {
				if($details['page'] == HOME_PAGE) {
					$params['activity_details'] = 'Home Page';
					$this->points_history(array('registrant_id'=>$this->session->userdata('user_id'), 'points_earned'=>HOMEPAGE_EARN_POINTS, 'module'=>UA_WATCH, 'description'=>'Watched video in home page'));
				} else if($details['page'] == ACTIVITY_PAGE) {
					$params['activity_details'] = 'Activity Page';
					$this->points_history(array('registrant_id'=>$this->session->userdata('user_id'), 'points_earned'=>ACTIVITY_SELECT_POINTS, 'module'=>UA_SELECTED_ACTIVITIES, 'description'=>'Watched video in activities page'));
				}
			} else if($details['activity'] == UA_ENTERED_CODES) {
				$params['activity_details'] = $details['category'];
			} else if($details['activity'] == UA_VIEW_CONTENT) {
				$row = $this->db->select('*, tbl_experience.title as experience_title')->from('tbl_activity')->where('tbl_activity.activity_id', $details['activity_id'])->join('tbl_experience', 'tbl_experience.experience_id = tbl_activity.experience_id')->get()->row();
				if($row) {
					if($details['decision'] == DECISION_RIGHT) {
						$decision = $row->decision_right_title;
					} else if($details['decision'] == DECISION_LEFT) {
						$decision = $row->decision_left_title;
					}
					$params['activity_details'] = $row->experience_title.' - '.$decision;
					$this->points_history(array('registrant_id'=>$this->session->userdata('user_id'), 'points_earned'=>$details['km_points'], 'module'=>UA_VIEW_CONTENT, 'description'=>'View contents of '.$decision. ' decision'));
				}
			} else if($details['activity'] == UA_SELECTED_ACTIVITIES) {
				$row = $this->db->select()->from('tbl_experience')->where('experience_id', $details['activity_id'])->get()->row();
				if($row) {
					$params['activity_details'] = $row->title;
					$this->points_history(array('registrant_id'=>$this->session->userdata('user_id'), 'points_earned'=>$details['km_points'], 'module'=>UA_SELECTED_ACTIVITIES, 'description'=>'Selected '.$row->title.' activity'));
				}
			} else if($details['activity'] == PLAY_GAME) {
				$params['activity_details'] = $details['details'];
			}
			$this->db->insert('tbl_reports_user_activity', $params);
		}
	}

	private function points_history(array $details)
	{
		if($details) {
			$this->db->set('registrant_id', $details['registrant_id']);
			$this->db->set('points_earned', $details['points_earned']);
			$this->db->set('module', $details['module']);
			$this->db->set('description', $details['description']);
			$this->db->insert('tbl_registrant_points_history');
		}
	}

}