<?php

class Check_Login_Model extends CI_Model {
	public function __construct()
	{
		parent::__construct();
		ini_set('error_reporting', 0);
		define('SESSION_USER_ID', $this->session->userdata('user_id'));
		define('ASSETS_URL', base_url().'assets/');
		define('SITE_URL', site_url());
		define('UPLOADS_URL', base_url().'uploads/');
		define('ENCRYPTION_KEY', $this->config->item('encryption_key'));

		// $staging_url = "http://marlboro-stage2.yellowhub.com/spice/user/login";
		// $production_url = "https://marlboro.ph/user/login";
		// if($this->uri->segment(1) != 'map') {
		// 	if( ! $this->session->userdata('session_id') && ! $this->session->userdata('user_id')) {
		// 		redirect($staging_url);
		// 	}
		// }
		
		// if( in_array( $_SERVER['REMOTE_ADDR'], array( '127.0.0.1', '::1' ) ) ) 
		// 		{ //check if localhost or in staging / prod server
		// 				$this->db = $this->load->database('marlboro_local', TRUE);
		// 		}else
		// 		{
		// 				$this->db = $this->load->database('marlboro_staging', TRUE);
		// 		}

	
		// $row = $this->db->select()->from('tbl_registrants')->where('registrant_id', $this->session->userdata('user_id'))->get()->row();
		// if($row) {
		// 	$row_total_km_points = $row->total_km_points;
		// 	$row_credit_km_points = $row->credit_km_points;
		// 	$this->session->set_userdata('total_km_points', $row_total_km_points);
		// 	$this->session->set_userdata('credit_km_points', $row_credit_km_points);
		// 	$this->db->close();
		// 	if( in_array( $_SERVER['REMOTE_ADDR'], array( '127.0.0.1', '::1' ) ) ) 
		// 		{ //check if localhost or in staging / prod server
		// 			$this->db = $this->load->database('localhost', TRUE);
		// 		}else
		// 		{
		// 			$this->db = $this->load->database('staging', TRUE);
		// 		}

		// 	$row_user = $this->db->select()->from('tbl_registrants')->where('user_id', $this->session->userdata('user_id'))->get()->row();
		// 	if($row_user) {
		// 		$new_data = array(
		// 			'total_km_points'=>$row_total_km_points,
		// 			'credit_km_points'=>$row_credit_km_points
		// 		);
		// 		$this->db->update('tbl_registrants', $new_data, array('user_id'=>$this->session->userdata('user_id')));
		// 	} else {
		// 		$new_data = array(
		// 			'first_name'=>$this->session->userdata('user')['first_name'],
		// 			'third_name'=>$this->session->userdata('user')['last_name'],
		// 			'middle_initial'=>$this->session->userdata('user')['middle_initial'],
		// 			'nick_name'=>$this->session->userdata('user')['nick_name'],
		// 			'person_id'=>$this->session->userdata('_spice_admin_person_id'),
		// 			'user_id'=>$this->session->userdata('user_id'),
		// 			'total_km_points'=>$row_total_km_points,
		// 			'credit_km_points'=>$row_credit_km_points
		// 		);
				
		// 		$this->db->insert('tbl_registrants', $new_data);
		// 	}	
		// }else
		// {

		// 		if( in_array( $_SERVER['REMOTE_ADDR'], array( '127.0.0.1', '::1' ) ) ) 
		// 		{ //check if localhost or in staging / prod server
		// 			$this->db = $this->load->database('localhost', TRUE);
		// 		}else
		// 		{
		// 			$this->db = $this->load->database('staging', TRUE);
		// 		}


			
		// }
		
		
		// if($this->uri->segment(1) != 'map') {
		// 	if( ! $this->session->userdata('session_id') && ! $this->session->userdata('user_id')) {
		// 		redirect($staging_url);
		// 		// redirect('http://localhost/marlboro/user/login');
		// 	}
		// 	if($this->session->userdata('total_km_points') && $this->session->userdata('credit_km_points')) {
		// 		$row = $this->db->select()->from('tbl_registrants')->where('user_id', $this->session->userdata('user_id'))->get()->row();
		// 		if($row) {
		// 			$this->db->set('person_id', $this->session->userdata('_spice_member_person_id'));
		// 			$this->db->where('user_id', $this->session->userdata('user_id'));
		// 			$this->db->update('tbl_registrants');
		// 		}
		// 	} else {
		// 		$row = $this->db->select()->from('tbl_registrants')->where('user_id', $this->session->userdata('user_id'))->get()->row();
		// 		if( ! $row) {
		// 			$this->db->set('person_id', $this->session->userdata('_spice_member_person_id'));
		// 			$this->db->set('user_id', $this->session->userdata('user_id'));
		// 			$this->db->set('date_created', date('Y-m-d H:i:s'));
		// 			$this->db->insert('tbl_registrants');

		// 			$this->session->set_userdata('total_km_points', 0);
		// 			$this->session->set_userdata('credit_km_points', 0);
		// 		} else {
		// 			$this->session->set_userdata('total_km_points', $row->total_km_points);
		// 			$this->session->set_userdata('credit_km_points', $row->credit_km_points);
		// 		}				
		// 	}
		// }
		}

	public function get_experiences()
	{
		$data['experience'] = array_merge(
			$this->db->select()->from('tbl_experience')->where('category_id', REGION_CALIFORNIA)->limit(EXPERIENCE_HEADER_LIMIT)->order_by('order', 'ASC')->get()->result(),
			$this->db->select()->from('tbl_experience')->where('category_id', REGION_ALASKA)->limit(EXPERIENCE_HEADER_LIMIT)->order_by('order', 'ASC')->get()->result(),
			$this->db->select()->from('tbl_experience')->where('category_id', REGION_CANADA)->limit(EXPERIENCE_HEADER_LIMIT)->order_by('order', 'ASC')->get()->result()
			);
		return $data['experience'];
	}			
}