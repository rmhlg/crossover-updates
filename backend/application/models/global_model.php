<?php

class Global_model extends CI_Model {
	

	function get_rows($param)
	{
  		array_filter($param);

 
		
		if(isset($param['fields'])){
			$this->db->select($param['fields']);
		}
		
		if(isset($param['order_by']) && $param['order_by']){
			$order_by = $param['order_by'];
 			$this->db->order_by($order_by['field'], $order_by['order']);
		}

		if(isset($param['group_by'])){
			$this->db->group_by($param['group_by']);
		}
		
		if(isset($param['like']) && $param['like']){
			
			$like = $param['like'];
			foreach($like as $key=>$v){
				if(strpos($key, 'or_') !== false)
					$this->db->or_like(str_replace('or_', '', $key),$v);
				else
					$this->db->like($key, $v); 
				
			}
			
		}

		if(isset($param['join']) && $param['join'])
		{
			$joins = (array)$param['join'];

			if(isset( $joins[0] ) && is_array( $joins[0] )){
				foreach($joins as $v){
					$this->db->join($v['table'],$v['on'],@$v['type']);
				}	
			}else{
				$join = (array)$param['join'];
 				$this->db->join($join['table'],$join['on'],@$join['type']);
			}
			
		}

		$this->db->from($param['table']);
		
		if(isset($param['where']) && $param['where'])
		{

			$where = $param['where'];	
			if(isset($where['where_in'])){ 
		    
				$field = $where['where_in']['field'];
				$arr = $where['where_in']['arr'];
				
				if($field && count($arr) > 0)
					$this->db->where_in($field,$arr); 
					unset($where['where_in']);
 			}

 			$this->db->where($where);
		 
		}

		if(isset($param['limit']) && isset($param['offset']) && is_numeric($param['limit'])){
			$this->db->limit((int)$param['limit'],(int)$param['offset']);
		}else if(isset($param['limit']) && is_numeric($param['limit']) ){
			$this->db->limit((int)$param['limit']);
		}
			
 		return $this->db->get();
		
 	}
	
	
	function get_row($param)
	{
		$table = @$param['table'];
		$where = @$param['where'];

		if(isset($param['fields'])){
			$this->db->select($param['fields']);
		}
		
		if(isset($param['limit']))
			$this->db->limit($param['limit']);
			
 		
		if($where)
 	   		$record = $this->db->get_where($table,$where);
		else
		    $record = $this->db->get($table);
			
	   return $record->row();
	   
 	}
	
	
	function get_total_rows($param)
	{
		
  		if(isset($param['where']) && $param['where'])
	  		$this->db->where($param['where']);
			
		if(isset($param['like']) && $param['like']){
			$like = $param['like'];
			foreach($like as $key=>$v){
				
				$this->db->like($key, $v); 
				
			}
			
		}
			
		$this->db->from($param['table']);
		return $this->db->count_all_results();
 	
	}
 
	function insert($table,$data)
	{

		if(isset($data['set']))
		{
			$dates = (array)$data['set'];

			foreach($dates as $key=>$v){
				$this->db->set($key,$v,FALSE);
			}
			
		}  

  		$this->db->insert($table,$this->filter_data_fields($table,$data));		
		return $this->db->insert_id();
		
	}

	function insert_batch($table,$data){


  		$this->db->insert_batch($table,$data);		
		return $this->db->insert_id();

	}

	function filter_data_fields($table,$data)
	{
		
		$fields =  $this->db->list_fields($table);
 
		$filtered_data = array();
   		foreach($data as $key=>$v){

			if(in_array($key, $fields))
				$filtered_data[$key] = $v;

		}
 
		return $filtered_data;
	}
	
	function update($table,$data,$where)
	{
	 
		if(isset($data['set']))
		{
			$dates = (array)$data['set'];

			foreach($dates as $key=>$v){
				$this->db->set($key,$v,FALSE);
			}
			
		}
				
		$this->db->where($where);
		return $this->db->update($table,$this->filter_data_fields($table,$data));		
		
	}
	
	function delete($table,$where)
	{
		
		return $this->db->delete($table,$where);
			
	}
	
	function get_last_id($table,$id)
	{
		
		$this->db->order_by($id,'desc');
		$res = $this->db->get($table,1,0);
		$row = $res->row();
		if($row)
			return $row->$id;
		else
			return 0;
		
	}
	
	function get_sum($table,$field,$where)
	{
		
		$this->db->select_sum($field);
		if($where)
			$query = $this->db->get_where($table,$where);
 		else
			$query = $this->db->get($table);
		
		$row = $query->row();
		return $row->$field;
	
	}
	
	
	function custom_query($sql)
	{
		return $this->db->query($sql);
	}

	

	
}