<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
 	}

 	public function index()
 	{
 		$reg_id = $this->session->userdata('user_id');
 		//$reg_id = 117333;
 		$data = array();	
 		$data['experiences'] = $this->db->select()->from('tbl_experience')->order_by('category_id asc')->get()->result();
 		$data['registrants'] = $this->db->select()->from('tbl_registrants')->where('user_id', $reg_id)->get()->row();
 		$data['packCode'] = 100;
 		$data['checkIn'] = 120;
 		$data['event'] = 140;
 		$data['shared'] = 150;

 		$data['content'] = $this->load->view('profile', $data, TRUE);
 		$this->load->view('main-template', $data);
 	}
 	
}