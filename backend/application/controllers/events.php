<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

class Events extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
 	}

 	public function index()
 	{
 		$data['experience'] = $this->check_login_model->get_experiences();
 		$data['events'] = $this->db->select()->from('tbl_backstage_events')->where('is_deleted', 0)->order_by('backstage_event_id desc')->get()->result();
 		$data['attendance'] = $this->db->select()->from('tbl_backstage_attendance')->get()->result();
 		$data['venues'] = $this->db->select()->from('tbl_venues')->get()->result();
 		$data['eventTypes'] = $this->db->select()->from('tbl_backstage_event_types')->where('is_deleted', 0)->get()->result();
 		$data['regions'] = $this->db->select()->from('tbl_regions')->get()->result();

 		$data['content'] = $this->load->view('events', $data, TRUE);
 		$this->load->view('main-template', $data);
 	}

 	public function content()
 	{	
 		$data = array();
 		$data['venues'] = $this->db->select()->from('tbl_venues')->get()->result();
 		$data['events'] = $this->db->select()->from('tbl_backstage_events')->where('is_deleted', 0)->order_by('backstage_event_id desc')->get()->result();
 		$data['attendance'] = $this->db->select()->from('tbl_backstage_attendance')->get()->result();
 		$data['eventTypes'] = $this->db->select()->from('tbl_backstage_event_types')->where('is_deleted', 0)->get()->result();
 		$data['regions'] = $this->db->select()->from('tbl_regions')->get()->result();
 		$this->header_model->get_experience_header();
 		$this->load->view('events', $data);
 	}

 	public function filterEvent(){
 		// $data = array();
 		// $data['venues'] = $this->db->select()->from('tbl_venues')->get()->result();
 		// $data['rows'] = $this->db->select()->from('tbl_event')->get()->result();
 		// $this->load->view('content/events', $data);
 		$venue = $this->input->post('venue');
 		$region = $this->input->post('region');
 		$eventType = $this->input->post('eventType');
 		$startdate = $this->input->post('startdate');
 		$enddate = $this->input->post('enddate');
 		
 		$data = array();
 		$qstring = "";

 		if(!empty($venue) and strlen($qstring)>0){
			$qstring .= " and venue='".$venue."'";
 		}else if(!empty($venue)){
 			$qstring .= "venue='".$venue."'";
 		}

 		if(!empty($region) and strlen($qstring)>0){
			$qstring .= " and region_id=".$region;
 		}elseif(!empty($region)){
			$qstring .= "region_id=".$region;
 		}

 		if(!empty($eventType) and strlen($qstring)>0){
			$qstring .= " and event_type='".$eventType."'";
 		}else if(!empty($eventType)){
			$qstring .= "event_type='".$eventType."'";
 		}

 		if(!empty($startdate) and strlen($qstring)>0){
			$qstring .= " and start_date >= '".$startdate."'";
 		}else if(!empty($startdate)) {
			$qstring .= "start_date >= '".$startdate."'";
 		}

 		if(!empty($enddate) and strlen($qstring)>0){
			$qstring .= " and end_date >= '".$enddate."'";
 		}else if(!empty($enddate)){
			$qstring .= "end_date >= '".$enddate."'";
 		}

 		if($venue == "" and $region == "" and $eventType == "" and $startdate == "" and $enddate == ""){
 			$rows = $this->db->query("SELECT * FROM `tbl_backstage_events` where is_deleted=0 order by backstage_event_id desc")->result();
 		}else{
 			$rows = $this->db->query("SELECT * FROM `tbl_backstage_events` where ".$qstring)->result();
 		}

 		$events = array();
 		
 		$attendance = $this->db->select()->from('tbl_backstage_attendance')->get()->result();
 		$att_arr = array();
 		foreach($attendance as $att){
 			$att_arr[] = $att->event_id;
 		}
 		foreach($rows as $row){
 			if(in_array($row->backstage_event_id, $att_arr)) {
	 			$events[] = array('event_id'=>$row->backstage_event_id, 'name'=>$row->title, 'address'=>$row->venue, 'start_date'=>date_format(date_create($row->start_date), "F d, Y"),'end_date'=>date_format(date_create($row->end_date), "F d, Y"), 'description'=>$row->description, 'image'=>$row->image, 'disabled'=>true);
 			}else{
 				$events[] = array('event_id'=>$row->backstage_event_id, 'name'=>$row->title, 'address'=>$row->venue, 'start_date'=>date_format(date_create($row->start_date), "F d, Y"),'end_date'=>date_format(date_create($row->end_date), "F d, Y"), 'description'=>$row->description, 'image'=>$row->image, 'disabled'=>false);
 			}
 		}
	//	$this->load->view('content/events', $data);
 		echo json_encode($events);
 	}
 	public function imGoing(){
 		$event_id = $this->input->post('event_id');
 		$registrant_id = $this->input->post('registrant_id');

 		$data = array(
 				'attendance_id' => '',
 				'event_id' => $event_id,
 				'registrant_id' => $registrant_id,
 				'status' => 1,
 				'date_status_changed' => date("Y-m-d H:i:s"),
 				'date_added' => date("Y-m-d H:i:s")
 				);

 		//FOR SPICE TRACK ACTION - EL //
 	// 	$sec_settings = $this->profile_model->get_row(array('table' => 'tbl_section_settings', 'where' => 'name = "BACKSTAGE_PASS"'));
		// $cell_id = $sec_settings->setting_section_id;

		// $act_settings = $this->profile_model->get_row(array('table' => 'tbl_activity_settings', 'where' => 'name = "BACKSTAGE_PASS"'));
		// $act_id = $act_settings->setting_activity_id;

 	// 	$param = array('CellId' => $cell_id, 'ActionList' =>array(array('ActivityId'   => $act_id, 'ActionValue' => $event->title.'|'.date("Y-m-d H:i:s"))));
		// $response = $this->spice->trackAction($param);
		// $response_json = $this->spice->parseJSON($response);

		// if($response_json->MessageResponseHeader->TransactionStatus != 0){
		// 		$data['message'] = '<h3>'.$response_json->MessageResponseHeader->TransactionStatusMessage.'</h3>';

		// 		$data_error = array('origin_id'=>BACKSTAGE_PHOTOS,
		// 				'method'=>'trackAction',
		// 				'transaction'=>$this->router->method,
		// 				'input'=> $this->session->userdata('spice_input'),
		// 				'response'=>$response,
		// 				'response_message'=>$response_json->MessageResponseHeader->TransactionStatusMessage
		// 			);
		// 		$this->spice->saveError($data_error);
		// }

 		$this->db->insert('tbl_backstage_attendance', $data);
 	}
}