<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

class Popups extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
 	}

 	public function terms()
 	{
 		$this->load->view('popups/terms');
 	}

 	public function contactus()
 	{
 		$this->load->view('popups/contactus');
 	}

 	public function privacy()
 	{
 		$this->load->view('popups/privacy');
 	}

 	public function mechanics()
 	{
 		$this->load->view('popups/mechanics');
 	}

}