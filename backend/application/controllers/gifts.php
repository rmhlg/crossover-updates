<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

class Gifts extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
 	}

 	public function index(){
 		//$this->session->set_userdata('user_id', 117333);
 		$reg_id = $this->session->userdata('user_id');
 		//$reg_id = 117333;
 		
 		$registrant = $this->db->select()->from('tbl_registrants')->where('user_id', $reg_id)->get()->row();
 		$data['registrant'] = $registrant;
 		
 		
 		
 		if($registrant->total_km_points>=600 and $registrant->total_km_points<=ALASKA_START_POINTS){
			$region = 1;
		}else if($registrant->total_km_points>=1400 and $registrant->total_km_points<=CANADA_START_POINTS){
			$region = 2;
		}else if($registrant->total_km_points>=4000) {
			$region = 3;
		}else {
			$region = 1;
		}

 		$data['gifts'] = $this->db->select()->from('tbl_gift')->where('region', $region)->or_where('type', 2)->order_by('gift_id desc')->get()->result();
 	
 		$ids = $this->db->select()->from('tbl_registrant_gift')->where('registrant_id', $reg_id)->get()->result();
 			
 		$data['claimed']= array();
 		foreach($ids as $id){
 			$data['claimed'][] = $id->gift_id;
 		}

 		$data['content'] = $this->load->view('gifts', $data, TRUE);
 		$this->load->view('main-template', $data);
 	}

 	public function content(){
 		$reg_id = $this->session->userdata('user_id');
 		//$reg_id = 117333;
 		
 		$registrant = $this->db->select()->from('tbl_registrants')->where('user_id', $reg_id)->get()->row();
 		$data['registrant'] = $registrant;
 		
 		if($registrant->total_km_points>=600 and $registrant->total_km_points<=ALASKA_START_POINTS){
			$region = 1;
		}else if($registrant->total_km_points>=1400 and $registrant->total_km_points<=CANADA_START_POINTS){
			$region = 2;
		}else if($registrant->total_km_points>=4000) {
			$region = 3;
		}else {
			$region = 1;
		}

 		$data['rows'] = $this->db->select()->from('tbl_gift')->where('region', $region)->or_where('type', 2)->order_by('gift_id desc')->get()->result();
 	
 		$ids = $this->db->select()->from('tbl_registrant_gift')->where('registrant_id', $reg_id)->get()->result();
 			
 		$this->header_model->get_experience_header();
 		$data['claimed']= array();
 		foreach($ids as $id){
 			$data['claimed'][] = $id->gift_id;
 		}
 		$this->load->view('content/gifts', $data);
 	}
 	public function filterGift(){
 		$reg_id = $this->session->userdata('user_id');
 		//$reg_id = 117333;
 		$region_id = $this->input->post('region_id');
 		
 		$gifts = $this->db->select()->from('tbl_gift')->where('region', $region_id)->or_where('type', 2)->order_by('gift_id desc')->get()->result();
 		$giftsArr = array();

 		$registrant = $this->db->select()->from('tbl_registrants')->where('user_id',$reg_id)->get()->row();

 		foreach($gifts as $gift){

 			$claimed = $this->db->select()->from('tbl_registrant_gift')->where('gift_id', $gift->gift_id)->where('registrant_id', $reg_id)->get()->row();
 			if($claimed){
 				$claimed = true;
 			}else{
 				$claimed = false;
 			}
 			if($registrant->credit_km_points < $gift->minimum_km_points_required or $registrant->credit_km_points < $gift->km_points){
 				$locked = true;
 			}else{
 				$locked = false;
 			}
 			if($gift->type==2){
 				$premium = true;
 			}else{
 				$premium = false;
 			}
 			$giftsArr[] = array(
 					"gift_id"=>$gift->gift_id,
 					"image"=>$gift->image,
 					"name"=>$gift->name,
 					"description"=>substr($gift->description, 0, 120)."...",
 					"km_points"=>$gift->km_points,
 					"minimum_km_points_required"=>$gift->minimum_km_points_required,
 					"stock"=>$gift->stock,
 					"date_created"=>$gift->date_created,
 					"claimed"=>$claimed,
 					"locked"=>$locked,
 					"premium"=>$premium
 			);
 		}
 		echo json_encode($giftsArr);
 	}
 	public function loadGiftInfo() {
 		$this->load->library('Spice');
 		//SPICE ADDRESS
 		$spice_userinfo = $this->spice->GetPerson(array('PersonId' => $this->spice->getMemberPersonId()));
		$spice_userinfo = $this->spice->parseJSON($spice_userinfo);
		$spice_userinfo = $spice_userinfo->ConsumerProfiles;
		//end

 		$gift_id = $this->input->post('gift_id');
 		
 		$gift = $this->db->select()->from('tbl_gift')->where('gift_id',$gift_id)->get()->row();
 		
 		$gift_arr = array();
		$gift_arr = array(
					"gift_id"=>$gift->gift_id,
					"experience_id"=>$gift->experience_id,
					"region"=>$gift->region,
					"type"=>$gift->type,
					'order'=>$gift->order,
					'image'=>$gift->image,
					'name'=>$gift->name,
					'description'=> substr($gift->description, 0, 120)."...",
					'km_points'=>$gift->km_points,
					'minimum_km_points_required'=>$gift->minimum_km_points_required,
					'stock'=>$gift->stock,
					'redemption_type'=>$gift->redemption_type,
					'street' => $spice_userinfo->Addresses[0]->AddressLine1,
					'brgy' => $spice_userinfo->Addresses[0]->Area,
					'city' => $spice_userinfo->Addresses[0]->City,
					'postal' => $spice_userinfo->Addresses[0]->PostalCode,
					'province' => $spice_userinfo->Addresses[0]->Locality
					// 'street' => "123",
					// 'brgy' => "brgy",
					// 'city' => "city",
					// 'postal' => 'postal',
					// 'province' => 'local'
				);
 		echo json_encode($gift_arr);
 	}
 	public function insertIWantThis(){
 		$gift_id = $this->input->post("gift_id");
 		$reg_id = $this->session->userdata('user_id');
 		//$reg_id = 117333;

 		$data = array(
 			"registrant_gift_id" => "",
 			"registrant_id" => $reg_id,
 			"gift_id" => $gift_id,
 			"date_created" => date("Y-m-d")
 		);
 		$this->updateAddress($this->input->post('city'), $this->input->post('province'), $this->input->post('street'), $this->input->post('brgy'), $this->input->post('postal'));
 		$this->updateGiftStocks($gift_id);
 		$this->updateUserKM($reg_id, $gift_id);
 		$this->db->insert('tbl_registrant_gift', $data); 
 	}
 	public function updateUserKM($reg_id, $gift_id){
 		$gift =  $this->db->select("*")->from("tbl_gift")->where("gift_id", $gift_id)->get()->row();
 		$registrant =  $this->db->select("*")->from("tbl_registrants")->where("user_id", $reg_id)->get()->row();

 		if($registrant){
 			$data = array(
	               'credit_km_points' => $registrant->credit_km_points-$gift->km_points
	        	);
 			$this->db->where('user_id', $reg_id);
			$this->db->update('tbl_registrants', $data); 
 		}
 	}
 	public function getUserCreditKm(){
 		$reg_id = $this->session->userdata('user_id');
 		//$reg_id = 117333;

 		$registrant =  $this->db->select("*")->from("tbl_registrants")->where("user_id", $reg_id)->get()->row();

 		echo $registrant->credit_km_points;
 	}
 	public function checkClaiming(){
 		$gift_id = $this->input->post("gift_id");
 		$reg_id = $this->session->userdata('user_id');
 		//$reg_id = 117333;

 		$gift =  $this->db->select("*")->from("tbl_gift")->where("gift_id", $gift_id)->get()->row();
 		$registrant =  $this->db->select("*")->from("tbl_registrants")->where("user_id", $reg_id)->get()->row();

 		if($registrant->credit_km_points > $gift->km_points){
 			echo "a";
 		}else {
 			// echo $registrant->credit_km_points."-".$gift->minimum_km_points_required."asd";
 			echo "b";
 		}
 	}
 	public function updateGiftStocks($gift_id){
 		$gift =  $this->db->select("*")->from("tbl_gift")->where("gift_id", $gift_id)->get()->row();

 		if($gift){
	 		$data = array(
	               'stock' => $gift->stock-1
	        	);

			$this->db->where('gift_id', $gift_id);
			$this->db->update('tbl_gift', $data); 
 		}
		// Produces:
		// UPDATE mytable 
		// SET title = '{$title}', name = '{$name}', date = '{$date}'
		// WHERE id = $id
 	}
 	public function updateAddress($cityA="", $provinceA="", $street_name="", $brgy="",$postal=""){
 		$this->load->library('Spice');

 		$user_id = $this->session->userdata('user_id');
		$user = $this->session->userdata('user');

		$response = $this->spice->GetPerson(array('PersonId' => $this->spice->getMemberPersonId()));
		$spice_userinfo = $this->spice->parseJSON($response);

		if($spice_userinfo->MessageResponseHeader->TransactionStatus == 0){
			$spice_userinfo = $spice_userinfo->ConsumerProfiles;

			$city = $this->db->select()
						->from('tbl_cities')
						->where(array('city_id'=>$cityA))
						->limit(1)
						->get()
						->row();

			$province = $this->db->select()
						->from('tbl_provinces')
						->where(array('province_id'=>$provinceA))
						->limit(1)
						->get()
						->row();
			$city = $city ? $city->city : '';
			$province = $province ? $province->province : '';

			$params = array('person_id'=> $this->spice->getMemberPersonId(),
	                		'updates'=> array(
	                				'AddressType' => array(
	                					array(
	                						'AddressLine1' => $street_name,
											'Area' => $brgy,
											'Locality' => $province,
									        'City' => $city, 
									        'Country' => 'PH',
									        'Premise'=>'PH',
									        'PostalCode' => $postal
	                					)
	                				)
	                        )
	                );

    		$response = $this->spice->updatePerson($params);
    		$response_json = $this->spice->parseJSON($response);
		}
 	}	
}