<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

class Activities extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
 	}

 	public function index()
 	{
 		$data['experience'] = $this->check_login_model->get_experiences();
 		$row = $this->db->select()->from('tbl_experience')->where('experience_id', $this->uri->segment(2))->get()->num_rows();
 		$fallback_row = $this->db->select()->from('tbl_experience')->where('category_id', REGION_CALIFORNIA)->where('order', 1)->get()->row();
 		if($row) {
 			// revised
 			$valid_row = $this->db->select()->from('tbl_experience')->where('experience_id', $this->uri->segment(2))->get()->row();
 			if($this->uri->segment(2)) {
 				$redirect_row = $this->db->select()->from('tbl_experience')->where('category_id', REGION_CALIFORNIA)->where('order', 1)->get()->row();
 				if($valid_row->category_id == REGION_ALASKA && $this->session->userdata('total_km_points') < ALASKA_START_POINTS) {
 					redirect('activities/'.$redirect_row->experience_id.'/'.url_title($redirect_row->title), 'refresh');
 				}
 				if($valid_row->category_id == REGION_CANADA && $this->session->userdata('total_km_points') < CANADA_START_POINTS) {
 					redirect('activities/'.$redirect_row->experience_id.'/'.url_title($redirect_row->title), 'refresh');
 				}
 				$row = $this->db->select('*, tbl_experience.title as experience_title, tbl_experience.description as experience_description')->from('tbl_experience')->where('tbl_experience.experience_id', $this->uri->segment(2))->join('tbl_activity', 'tbl_activity.experience_id = tbl_experience.experience_id')->get()->row();
 				if($row) {
 					$data['row'] = $row;
		 			$data['region'] = $data['row']->category_id;
					$data['activity_id'] = $data['row']->activity_id;
 				} else {
 					redirect('activities/'.$redirect_row->experience_id.'/'.url_title($redirect_row->title), 'refresh');
 				}
 			} else {
 				exit();
 			}

	 		$data['region'] = $this->get_region($this->uri->segment(2));

	 		$data['content'] = $this->load->view('activities', $data, TRUE);
	 		$this->load->view('main-template', $data);
 		} else {
 			redirect('activities/'.$fallback_row->experience_id.'/'.url_title($fallback_row->title));
 		} 
 	}

 	public function content()
 	{
 		$experience_id = $this->uri->segment(3);
 		$valid_row = $this->db->select()->from('tbl_experience')->where('experience_id', $experience_id)->get()->row();
 		if($experience_id) {
 			$row = $this->db->select('*, tbl_experience.title as experience_title, tbl_experience.description as experience_description')->from('tbl_experience')->where('tbl_experience.experience_id', $experience_id)->join('tbl_activity', 'tbl_activity.experience_id = tbl_experience.experience_id')->get()->row();
 			if($row) {
 				$data['row'] = $row;
	 			$data['region'] = $data['row']->category_id;
	 			$data['activity_id'] = $data['row']->activity_id;	
 			} else {
 				$row = $this->db->select()->from('tbl_experience')->where('category_id', REGION_CANADA)->where('order', 1)->get()->row();
 				redirect('activities/'.$row->experience_id.'/'.url_title($row->title), 'refresh');
 			}
 		} else {
 			exit();
 		}

 		$this->header_model->get_experience_header();
 		// $this->load->view('content/activities', $data);
 		return $this->load->view('content/activities', $data);
 	}

 	public function info()
 	{
 		if($this->input->is_ajax_request()) {
 			$data['rows'] = $this->db->select()->from('tbl_activity_media')->where('decision_title', $this->input->get('choice'))->get()->result();
 			$data['activity_id'] = $data['rows'][0]->activity_id;
 			$data['choice'] = $this->input->get('choice');

 			$this->load->view('templates/activities_infos', $data);	
 		} 		
 	}

 	public function gifts()
 	{
 		if($this->input->is_ajax_request()) {
 			$data['rows'] = $this->db->select()->from('tbl_gift')->where('region', $_GET['region'])->order_by('order', 'ASC')->get()->result();
	 		$data['next'] = $this->db->select()->from('tbl_experience')->limit(1)->where('experience_id >', $this->input->get('id'))->get()->row();
	 		
	 		$this->load->view('templates/activities_gifts', $data);	
 		} 		
 	}

 	public function game()
 	{
 		$row = $this->db->select()->from('tbl_experience')->where('experience_id', $this->input->get('id'))->get()->row();
 		$data = array();
 		if($row) {
 			$data['region'] = $row->category_id;
 			$data['order'] = $row->order;
 		}
 		$this->load->view('templates/activities_game', $data);
 	}

 	public function _remap($method)
 	{
 		if(is_numeric($method)) {
 			$this->index();
 		} else if(method_exists(__CLASS__, $method)) {
 			$this->$method();
 		} else {
 			$row = $this->db->select()->from('tbl_experience')->where('category_id', REGION_CALIFORNIA)->where('order', 1)->get()->row();
 			redirect('activities/'.$row->experience_id.'/'.url_title($row->title));
 		}
 	}

 	private function get_region($experience_id)
	{
		if($experience_id) {
			$data = $this->db->select('*, tbl_experience.title as experience_title')->from('tbl_experience')->where('tbl_experience.experience_id', $experience_id)->get()->row();
			if($data) {
				return $data->category_id;
			} else {
				return NULL;
			}
		} else {
			return NULL;
		}
	}

	public function load_game()
	{
		if($_GET) {
			$data = array();
			// california
			if($this->input->get('r_id') == 1) {
				if($this->input->get('o_id') == 1) {
					$data = array(
						'game_title'=>'zipline',
						'data_game'=>'zipline',
						'div_id'=>'zipline-game',
						'image'=>'_canada/exp8/',
						'title'=>'zip lining practice',
						'game_id'=>ZIPLINE
					);
				} else if($this->input->get('o_id') == 2) {
					$data = array(
						'game_title'=>'landsail',
						'data_game'=>'landsail',
						'div_id'=>'phaser-car',
						'image'=>'_california/exp2/',
						'title'=>'land sailing challenge',
						'game_id'=>LANDSAIL
					);
				} else if($this->input->get('o_id') == 3) {
					$data = array(
						'game_title'=>'grab',
						'data_game'=>'grab',
						'div_id'=>'grab-game',
						'image'=>'_california/exp3/',
						'title'=>'grabbing challenge',
						'game_id'=>GRAB
					);
				}
			}
			$this->load->view('templates/games-container', $data);
		}
	}

 	// public function _remap($method)
 	// {
 	// 	if(is_numeric($method)) {
 	// 		$this->index();
 	// 	} else {
 	// 		$this->$method();
 	// 	}
 	// }

}