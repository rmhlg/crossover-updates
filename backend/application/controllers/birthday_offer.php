<?php if(!defined("BASEPATH")){ exit("No direct script access allowed!"); }

class Birthday_offer extends CI_Controller {
    
    public function __construct()
    {
        parent::__construct();
    }

    public function get_birthday_offer()
    {
        $prize_id = $this->input->get('prize_id');
        $user = $this->session->userdata('user');
        $data['flash_offer'] = $this->input->get('flash_offer');

        //SPICE
        $spice_userinfo = $this->spice->GetPerson(array('PersonId' =>$this->spice->getMemberPersonId()));
        $spice_userinfo = $this->spice->parseJSON($spice_userinfo);

        $date_time = $spice_userinfo && (int)$spice_userinfo->MessageResponseHeader->TransactionStatus == 0 ?  $spice_userinfo->ConsumerProfiles->PersonDetails->DateOfBirth : '';
        $match = preg_match('/\/Date\((\d+)([-+])(\d+)\)\//', $date_time, $date);

        $timestamp = $date[1]/1000;
        $operator = $date[2];
        $hours = $date[3]*36; // Get the seconds

        $datetime = new DateTime();
        $datetime->setTimestamp($timestamp);
        $datetime->modify($operator . $hours . ' seconds');

        $data['birthday_offer'] = $this->db->select()
                                          ->from('tbl_birthday_offers')
                                          ->where("month = '".date('m',strtotime($datetime->format('Y-m-d')))."' AND month ='".date('m')."'", null)
                                          ->where('stock >', 0)
                                          ->where('status', 1)
                                          ->where('is_deleted', 0)
                                          ->where('prize_id', $prize_id)
                                          ->get()->row();

        // print_r($data['birthday_offer']); die();
        $this->load->view('birthday_offer/birthday-offer',$data);
    }

    public function confirm_offer()
    {
      if($this->input->post()){

          if($this->validate_offer_confirmation()){

              $user_id = $this->session->userdata('user_id');
              $user = $this->session->userdata('user');
              
              $birthday_offer = $this->db->select()
                                        ->from('tbl_birthday_offers')
                                        ->where('month', date('m'))
                                        ->where('stock >', 0)
                                        ->where('is_deleted', 0)
                                        ->get()->row();

              $new_stock = (int)$birthday_offer->stock - 1;

              // SPICE
              $response = $this->spice->GetPerson(array('PersonId' => $this->spice->getMemberPersonId()));
              $spice_userinfo = $this->spice->parseJSON($response);
              
              if($spice_userinfo->MessageResponseHeader->TransactionStatus == 0)
              {
                  $spice_userinfo = $spice_userinfo->ConsumerProfiles;

                  $city = $this->db->select()
                      ->from('tbl_cities')
                      ->where(array('city_id'=>$this->input->post('city')))
                      ->limit(1)
                      ->get()
                      ->row();

                  $province = $this->db->select()
                      ->from('tbl_provinces')
                      ->where(array('province_id'=>$this->input->post('province')))
                      ->limit(1)
                      ->get()
                      ->row();

                  $city = $city ? $city->city : '';
                  $province = $province ? $province->province : '';


                  $sec_settings = $this->db->select()
                                          ->from('tbl_section_settings')
                                          ->where('name', 'BIRTHDAY_OFFERS')
                                          ->get()->row();

                  $act_settings = $this->db->select()
                                          ->from('tbl_activity_settings')
                                          ->where('name', 'BIRTHDAY_OFFERS')
                                          ->get()->row();

                  $cell_id = $sec_settings->setting_section_id;
                  $act_id = $act_settings->setting_activity_id;

                  $param = array('CellId' => $cell_id, 'ActionList' => array(array('ActivityId' => $act_id, 'ActionValue' => $birthday_offer->prize_name)));
                  
                  
                  $response = $this->spice->trackAction($param);
                  $response_json = $this->spice->parseJSON($response);

                  if($response_json->MessageResponseHeader->TransactionStatus != 0){
                      $data_error = array('origin_id'=>BIRTHDAY_OFFERS,
                              'method'=>'trackAction',
                              'transaction'=>$this->router->method,
                              'input'=> $this->session->userdata('spice_input'),
                              'response'=>$response,
                              'response_message'=>$response_json->MessageResponseHeader->TransactionStatusMessage
                          );
                      $this->spice->saveError($data_error);
                  }else{
                      if(!$this->input->post('confirm_redemption')){
                          $params = array('person_id'=> $this->spice->getMemberPersonId(),
                                  'updates'=> array(
                                          'AddressType' => array(
                                              array(
                                                  'AddressLine1' => $this->input->post('street_name'),
                                                  'Area' => $this->input->post('barangay'),
                                                  'Locality' => $province,
                                                  'City' => $city, 
                                                  'Country' => 'PH',
                                                  'Premise'=>'PH',
                                                  'PostalCode' => $this->input->post('zip_code')
                                              )
                                          )
                                  )
                          );

                          $response = $this->spice->updatePerson($params);
                          $response_json = $this->spice->parseJSON($response);

                          if($response_json->MessageResponseHeader->TransactionStatus != 5){
                              $data_error = array('origin_id'=>BIRTHDAY_OFFERS,
                                      'method'=>'ManagePerson',
                                      'transaction'=>$this->router->method,
                                      'input'=> $this->session->userdata('spice_input'),
                                      'response'=>$response,
                                      'response_message'=>$response_json->MessageResponseHeader->TransactionStatusMessage
                                  );
                              $this->spice->saveError($data_error);
                          }else{
                              $data = array('registrant_id'=>$user_id,
                                    'registrant_name'=>$user['first_name'].' '.$user['last_name'],
                                    'registrant_birthdate'=>$user['birthdate'],
                                    'prize_id'=>$birthday_offer->prize_id,                              
                                    'prize_name'=>$birthday_offer->prize_name,
                                    'prize_description'=>$birthday_offer->description,
                                    'prize_month'=>$birthday_offer->month,
                                    'prize_image'=>$birthday_offer->prize_image,
                                    'send_type'=>$birthday_offer->send_type,
                                    'redemption_address'=>$birthday_offer->redemption_address,
                                    'redemption_instruction'=>$birthday_offer->redemption_instruction
                                    );

                              $id = $this->db->insert('tbl_user_birthday_prizes',$data);
                              $this->db->update('tbl_birthday_offers', array('stock' => $new_stock), array('prize_id' => $birthday_offer->prize_id));

                              $this->load->model('notification_model');
                              if($birthday_offer->send_type=='Delivery'){
                                  $message = 'Your birthday gift item from Marlboro will be delivered to your mailing address within the next month.';
                              }else{
                                  $message = 'Please follow the instruction below to redeem your birthday gift item from Marlboro:<br/>'.$birthday_offer->redemption_instruction;
                              }

                              $param = array('message'=>$message,'suborigin'=>$id);
                              $this->notification_model->notify($user_id,BIRTHDAY_OFFERS,$param);

                              $data = array('msg'=>'You have successfully confirmed the birthday offer!<br><small>(Expect to receive your gift within the next month.)</small>',
                                'error_holder'=>'',
                                'error'=>false,
                                'btn'=>'',
                                'btn_text'=>'');
                          }
                      }else{
                          $data = array('registrant_id'=>$user_id,
                                    'registrant_name'=>$user['first_name'].' '.$user['last_name'],
                                    'registrant_birthdate'=>$user['birthdate'],
                                    'prize_id'=>$birthday_offer->prize_id,                              
                                    'prize_name'=>$birthday_offer->prize_name,
                                    'prize_description'=>$birthday_offer->description,
                                    'prize_month'=>$birthday_offer->month,
                                    'prize_image'=>$birthday_offer->prize_image,
                                    'send_type'=>$birthday_offer->send_type,
                                    'redemption_address'=>$birthday_offer->redemption_address,
                                    'redemption_instruction'=>$birthday_offer->redemption_instruction
                                    );

                          $id = $this->db->insert('tbl_user_birthday_prizes',$data);
                          $this->db->update('tbl_birthday_offers', array('stock' => $new_stock), array('prize_id' => $birthday_offer->prize_id));

                          $this->load->model('notification_model');
                          if($birthday_offer->send_type=='Delivery'){
                              $message = 'Your birthday gift item from Marlboro will be delivered to your mailing address within the next month.';
                          }else{
                              $message = 'Please follow the instruction below to redeem your birthday gift item from Marlboro:<br/>'.$birthday_offer->redemption_instruction;
                          }

                          $param = array('message'=>$message,'suborigin'=>$id);
                          $this->notification_model->notify($user_id,BIRTHDAY_OFFERS,$param);

                          $data = array('msg'=>'You have successfully confirmed the birthday offer!<br><small>(Expect to receive your gift within the next month.)</small>',
                                'error_holder'=>'',
                                'error'=>false,
                                'btn'=>'',
                                'btn_text'=>'');
                      }
                  }
              }else{
                  $data = array('msg'=> '<h3>'.$spice_userinfo->MessageResponseHeader->TransactionStatusMessage.'</h3>',
                              'error_holder'=>'',
                              'error'=>false,
                              'btn'=>'',
                              'btn_text'=>'');
                  $data_error = array('origin_id'=>BIRTHDAY_OFFERS,
                          'method'=>'GetPerson',
                          'transaction'=>$this->router->method,
                          'input'=> $this->session->userdata('spice_input'),
                          'response'=>$response,
                          'response_message'=>$spice_userinfo->MessageResponseHeader->TransactionStatusMessage
                      );
                  $this->spice->saveError($data_error);
              }
          }else{

              $data = array('msg'=>trim(validation_errors()),
                            'error_holder'=>'birthday-offer-error',
                            'error'=>true,
                            'btn'=>'birthday-offer-btn',
                            'btn_text'=>'<i>Submit Confirmation</i>');
          }
          $this->load->view('profile/submit-response',$data);

      }else{

          $spice_userinfo = $this->spice->GetPerson(array('PersonId' => $this->spice->getMemberPersonId()));
          $spice_userinfo = $this->spice->parseJSON($spice_userinfo);
          $spice_userinfo = $spice_userinfo->ConsumerProfiles;

          $province = $this->db->select()->from('tbl_provinces')->where('LOWER(province)',strtolower($spice_userinfo->Addresses[0]->Locality))->get()->row();
          $province = $province ? $province->province_id : '';
          
          $cities = $this->db->select()->from('tbl_cities')->where(array('province_id '=> $province))->get();
          $provinces = $this->db->select()->from('tbl_provinces')->get();

          $data['spice_userinfos'] = $spice_userinfo;
          $data['cities'] = $cities;
          $data['provinces'] = $provinces;

          $this->load->view('birthday_offer/confirm-birthday-offer',$data);

      }    
    }

    private function validate_offer_confirmation()
    {

        $this->load->library('form_validation');
        $rules = array(array(
                             'field'   => 'prize_id',
                             'label'   => 'Birthday Offer',
                             'rules'   => 'callback_check_birthday_offer'
                          )
                    );

        if(!$this->input->post('confirm_redemption')){

            $rules[] = array(array(
                             'field'   => 'street_name',
                             'label'   => 'Street Name',
                             'rules'   => 'trim|required'
                          ),
                       array(
                             'field'   => 'barangay',
                             'label'   => 'Barangay',
                             'rules'   => 'trim|required'
                          ),
                       array(
                             'field'   => 'zip_code',
                             'label'   => 'Zip Code',
                             'rules'   => 'trim|required'
                          ),
                       array(
                             'field'   => 'city',
                             'label'   => 'City',
                             'rules'   => 'trim|required'
                          ),
                       array(
                             'field'   => 'province',
                             'label'   => 'Province',
                             'rules'   => 'trim|required'
                          ),
                       array(
                             'field'   => 'prize_id',
                             'label'   => 'Birthday Offer',
                             'rules'   => 'callback_check_birthday_offer'
                          )
                    );

        }

        $this->form_validation->set_rules($rules);      
        return $this->form_validation->run();
    }

    public function check_birthday_offer()
    {

        $user = $this->session->userdata('user');

        $spice_userinfo = $this->spice->GetPerson(array('PersonId' =>$this->spice->getMemberPersonId()));
        $spice_userinfo = $this->spice->parseJSON($spice_userinfo);
        $spice_userinfo = $spice_userinfo->ConsumerProfiles;

        $date_time = $spice_userinfo->PersonDetails->DateOfBirth;


        $match = preg_match('/\/Date\((\d+)([-+])(\d+)\)\//', $date_time, $date);

        $timestamp = $date[1]/1000;
        $operator = $date[2];
        $hours = $date[3]*36; // Get the seconds

        $datetime = new DateTime();

        $datetime->setTimestamp($timestamp);
        $datetime->modify($operator . $hours . ' seconds');

        $birthday_offer =  $this->db->select('prize_id')
                                   ->from('tbl_birthday_offers')
                                   ->where('month', date('m',strtotime($datetime->format('Y-m-d'))))
                                   ->where('stock >', 0)
                                   ->get()
                                   ->row();
      
        
        if($birthday_offer && (int)$this->session->userdata('user_id')){

            

            $user_birthday_prize = $this->db->select('user_birthday_prize_id')
                                   ->from('tbl_user_birthday_prizes')
                                   ->where('prize_id', $birthday_offer->prize_id)
                                   ->where('registrant_id', $this->session->userdata('user_id'))
                                   ->get()
                                   ->row();

            if($user_birthday_prize){
                $this->form_validation->set_message('check_birthday_offer','You have already confirmed the birthday offer to your account.');
                return false;
            }else{              
                return true;
            }
        }else{
            $this->form_validation->set_message('check_birthday_offer','No available birthday offer at this period of time.');
            return false;
        }

        return true;
    }

    public function get_birthday_offer_edm()
    {
        $user = $this->session->userdata('user');
        
        $spice_userinfo = $this->spice->GetPerson(array('PersonId' =>$this->spice->getMemberPersonId()));
        $spice_userinfo = $this->spice->parseJSON($spice_userinfo);
        $spice_userinfo = $spice_userinfo->ConsumerProfiles;

        $date_time = $spice_userinfo->PersonDetails->DateOfBirth;


        $match = preg_match('/\/Date\((\d+)([-+])(\d+)\)\//', $date_time, $date);

        $timestamp = $date[1]/1000;
        $operator = $date[2];
        $hours = $date[3]*36; // Get the seconds

        $datetime = new DateTime();

        $datetime->setTimestamp($timestamp);
        $datetime->modify($operator . $hours . ' seconds');
        
        $birthday_offer = $this->profile_model->get_row(array('table'=>'tbl_birthday_offers',
                                                               'where'=>array("month = '".date('m',strtotime($datetime->format('Y-m-d')))."' AND month ='".date('m')."'" =>null,
                                                                              'stock >'=>0,
                                                                              'status'=>1,
                                                                              'is_deleted'=>0
                                                                              ),
                                                               'fields'=>'prize_id'
                                                        )
                                                    );
        if($birthday_offer){
            $user_birthday_prize = $this->profile_model->get_row(array('table'=>'tbl_user_birthday_prizes',
                                                                        'where'=>array('prize_id' =>$birthday_offer->prize_id,'registrant_id' =>$this->session->userdata('user_id')),
                                                                        'fields'=>'user_birthday_prize_id'
                                                                        )
                                                                );
            $birthday_offer = $user_birthday_prize ? '' : $birthday_offer->prize_id;
        }

        $prize_id = $birthday_offer;
        $user = $this->session->userdata('user');
        $data['flash_offer'] = $this->input->get('flash_offer');
        $data['birthday_offer'] = $this->profile_model->get_row(array('table'=>'tbl_birthday_offers','where'=>array('prize_id' =>$prize_id)) );
        $this->load->view('birthday_offer/birthday-offer',$data);
    }

    public function get_cities()
    {
        $rows = $this->db->select()
                         ->from('tbl_cities')
                         ->where('province_id', $this->input->get('province'))
                         ->order_by('city', 'ASC') 
                         ->group_by('city')
                         ->get();

        $data['data'] = $rows->result_array();
        $this->load->view('profile/json_format',$data);

    }

}