<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

class Code_Validation extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		if( ! $this->input->is_ajax_request()) {
			exit();
		}
		if( ! $_SERVER['REQUEST_METHOD'] == 'POST') {
			exit();
		}
		if( ! $this->session->userdata('user_id')) {
			exit();
		}
		$this->load->model('reports_model');
	}

	private function check_if_code_is_valid($code)
	{
		$row = $this->db->select()->from('tbl_pack_code')->where("BINARY code='". $code ."'", NULL, FALSE)->get()->num_rows();
		if($row) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	private function check_if_code_is_expired($code)
	{
		$row = $this->db->select()->from('tbl_pack_code')->where("BINARY code='". $code ."'", NULL, FALSE)->where('end_date >=', date('Y-m-d H:i:s'))->get()->num_rows();
		if($row) {
			return FALSE;
		} else {
			return TRUE;
		}
	}

	private function check_if_max_usage_reached($code)
	{
		$row = $this->db->select()->from('tbl_pack_code')->where("BINARY code='". $code ."'", NULL, FALSE)->get()->row();
		if($row) {
			$row_user = $this->db->select()->from('tbl_registrant_pack_code')->where('user_id', SESSION_USER_ID)->where("pack_code='". $code ."'", NULL, FALSE)->get()->num_rows();
			if( ! $row_user) {
				if($row_user < $row->stock) {
					return TRUE;
				} else {
					return FALSE;
				}
			}
		}
	}

	private function check_if_max_cap_reached($code)
	{
		$row = $this->db->select()->from('tbl_pack_code')->where("BINARY code='". $code ."'", NULL, FALSE)->get();
		if($row->num_rows()) {
			$row_code = $this->db->select()->from('tbl_registrant_pack_code')->where('user_id', SESSION_USER_ID)->where('category', $row->row()->category)->get()->num_rows();
			if($row->row()->category == PACK_CODE) {
				if($row_code < PACK_CODE_LIMIT) {
					return TRUE;
				} else {
					return FALSE;
				}
			} else if($row->row()->category == LAMP_DRIVE) {
				if($row_code < LAMP_DRIVE_LIMIT) {
					return TRUE;
				} else {
					return FALSE;
				}
			} else if($row->row()->category == LEAVE_BEHIND) {
				if($row_code < LEAVE_BEHIND_LIMIT) {
					return TRUE;
				} else {
					return FALSE;
				}
			} else if($row->row()->category == KA || $row->row()->category == ELD) {
				return TRUE;
			}
		}
	}

	private function check_if_code_is_already_used($code)
	{
		$row = $this->db->select()->from('tbl_registrant_pack_code')->where('user_id', SESSION_USER_ID)->where("BINARY pack_code='". $code ."'", NULL, FALSE)->get();
		if($row->num_rows()) {
			return FALSE;
		} else {
			return TRUE;
			// if($row->row()->category == KA || $row->row()->category == ELD) {
			// 	return FALSE;
			// } else {
			// 	return TRUE;
			// }
		}
		// if($row) {
		// 	return FALSE;
		// } else {
		// 	return TRUE;
		// }
	}

	public function index()
	{
		if($_POST && isset($_POST['code'])) {
			$row = $this->db->select()->from('tbl_pack_code')->where("BINARY code='". $this->input->post('code') ."'", NULL, FALSE)->get();
			if($this->check_if_code_is_valid($this->input->post('code'))) {
				if( ! $this->check_if_code_is_expired($this->input->post('code'))) {
					if($this->check_if_max_usage_reached($this->input->post('code'))) {
						if($this->check_if_max_cap_reached($this->input->post('code'))) {
							if($this->check_if_code_is_already_used($this->input->post('code'))) {
								$category = 0;
								if($row->row()->category == PACK_CODE) {
									$category = CODE_PACK_CODE;
									$limit = PACK_CODE_LIMIT;
								} else if($row->row()->category == LAMP_DRIVE) {
									$category = CODE_LAMP_DRIVE;
									$limit = LAMP_DRIVE_LIMIT;
								} else if($row->row()->category == ELD) {
									$category = CODE_ELD;
									$limit = ELD_LIMIT;
								} else if($row->row()->category == LEAVE_BEHIND) {
									$category = CODE_LEAVE_BEHIND;
									$limit = LEAVE_BEHIND_LIMIT;
								} else if($row->row()->category == KA) {
									$category = CODE_KA;
									$limit = FALSE;
								}
								$this->load->model('km_points_model');
								$this->km_points_model->earn(array('points'=>$row->row()->km_points));
								$this->km_points_model->earn_history(array('origin_id'=>CODE, 'sub_origin_id'=>$category, 'main_sub_origin_id'=>0, 'points'=>$row->row()->km_points));
								$this->km_points_model->points_history(array('registrant_id'=>$this->session->userdata('user_id'), 'points_earned'=>$row->row()->km_points, 'module'=>UA_ENTERED_CODES, 'description'=>'User input code '.$this->input->post('code')));
								$this->reports_model->user_activity(array('user_id'=>$this->session->userdata('user_id'), 'activity'=>UA_ENTERED_CODES, 'km_points'=>$row->row()->km_points, 'category'=>$row->row()->category));
								$this->db->insert('tbl_registrant_pack_code', array('pack_code'=>$this->input->post('code'), 'user_id'=>$this->session->userdata('user_id'), 'category'=>$row->row()->category, 'earned_km_points'=>$row->row()->km_points));
								$response['status'] = 'Success';
								$response['points'] = $row->row()->km_points;

								$row_user = $this->db->select()->from('tbl_registrant_pack_code')->where('user_id', SESSION_USER_ID)->where('category', $row->row()->category)->get();
								if($limit) {
									$response['message'] = 'Code entered successfully. '.$row->row()->km_points.'KM is added to your account. You now have '.$this->session->userdata('total_km_points').' out of 5,500KM. '.$row_user->num_rows().'/'.$limit.' '.$row->row()->category.' has been entered.';
								} else {
									$response['message'] = 'Code entered successfully. '.$row->row()->km_points.'KM is added to your account. You now have '.$this->session->userdata('total_km_points').' out of 5,500KM. ';
								}
							} else {
								$response['status'] = 'Sorry, this code has already been entered';
							}
						} else {
							$response['status'] = 'Sorry, you have entered the maximum number of '.$row->row()->category;
						}
					} else {
						$response['status'] = 'Sorry, this code has already been entered';
					}
				} else {
					$response['status'] = 'This code has already expired';
				}
			} else {
				$response['status'] = 'Sorry, this code is invalid';
			}
			echo json_encode($response);
		}
	}

	public function _index()
	{
		if($_POST && isset($_POST['code'])) {
			$row = $this->db->select()->from('tbl_pack_code')->where("BINARY code='".$this->input->post('code')."'", NULL, FALSE)->get();
			if($this->is_valid($this->input->post('code'))) {
				if( ! $this->is_expired($this->input->post('code'))) {
					if($this->max_usage($this->input->post('code'))) {
						if($this->max_limit($this->input->post('code'))) {
							$category = 0;
							if($row->row()->category == PACK_CODE) {
								$category = CODE_PACK_CODE;
								$limit = PACK_CODE_LIMIT;
							} else if($row->row()->category == LAMP_DRIVE) {
								$category = CODE_LAMP_DRIVE;
								$limit = LAMP_DRIVE_LIMIT;
							} else if($row->row()->category == ELD) {
								$category = CODE_ELD;
								$limit = ELD_LIMIT;
							} else if($row->row()->category == LEAVE_BEHIND) {
								$category = CODE_LEAVE_BEHIND;
								$limit = LEAVE_BEHIND_LIMIT;
							} else if($row->row()->category == KA) {
								$category = CODE_KA;
								$limit = FALSE;
							}
							$this->load->model('km_points_model');
							$this->km_points_model->earn(array('points'=>$row->row()->km_points));
							$this->km_points_model->earn_history(array('origin_id'=>CODE, 'sub_origin_id'=>$category, 'main_sub_origin_id'=>0, 'points'=>$row->row()->km_points));
							$this->km_points_model->points_history(array('registrant_id'=>$this->session->userdata('user_id'), 'points_earned'=>$row->row()->km_points, 'module'=>UA_ENTERED_CODES, 'description'=>'User input code '.$this->input->post('code')));
							$this->reports_model->user_activity(array('user_id'=>$this->session->userdata('user_id'), 'activity'=>UA_ENTERED_CODES, 'km_points'=>$row->row()->km_points, 'category'=>$row->row()->category));
							$this->db->insert('tbl_registrant_pack_code', array('pack_code'=>$this->input->post('code'), 'user_id'=>$this->session->userdata('user_id'), 'category'=>$row->row()->category, 'earned_km_points'=>$row->row()->km_points));
							$response['status'] = 'Success';
							$response['points'] = $row->row()->km_points;

							$row_user = $this->db->select()->from('tbl_registrant_pack_code')->where('user_id', SESSION_USER_ID)->where('category', $row->row()->category)->get();
							if($limit) {
								$response['message'] = 'Code entered successfully. '.$row->row()->km_points.'KM is added to your account. You now have '.$this->session->userdata('total_km_points').' out of 5,500KM. '.$row_user->num_rows().'/'.$limit.' '.$row->row()->category.' has been entered.';
							} else {
								$response['message'] = 'Code entered successfully. '.$row->row()->km_points.'KM is added to your account. You now have '.$this->session->userdata('total_km_points').' out of 5,500KM. ';
							}
						} else {
							$response['status'] = 'Sorry, you have entered the maximum number of '.$row->row()->category;
						}
					} else {
						$response['status'] = 'Sorry, this code has already been entered';
					}
				} else {
					$response['status'] = 'This code has already expired';
				}
			} else {
				$response['status'] = 'Sorry, this code is invalid';
			}
			echo json_encode($response);
		}
	}

	private function max_usage($code)
	{
		$row = $this->db->select()->from('tbl_pack_code')->where("BINARY code='$code'", NULL, FALSE)->get();
		if($row->num_rows()) {
			$rows = $this->db->select()->from('tbl_registrant_pack_code')->where('pack_code', $code)->where('category', $row->row()->category)->get();
			if($row->row()->category == KA) {
				$row_user = $this->db->select()->from('tbl_registrant_pack_code')->where('user_id', SESSION_USER_ID)->where('pack_code', $code)->get();
				if( ! $row_user->num_rows()) {
					if($rows->num_rows() < NO_LIMIT) {
						return TRUE;
					} else {
						return FALSE;
					}
				} else {
					return FALSE;
				}
			} else {
				$row_user = $this->db->select()->from('tbl_registrant_pack_code')->where('user_id', SESSION_USER_ID)->where('pack_code', $code)->get();
				if( ! $row_user->num_rows()) {
					if($rows->num_rows() < $row->row()->stock) {
						return TRUE;
					} else {
						return FALSE;
					}
				}					
			}
		}
	}

	private function is_expired($code)
	{
		$row = $this->db->select()->from('tbl_pack_code')->where("BINARY code='$code'", NULL, FALSE)->where('end_date >=', date('Y-m-d H:i:s'))->get();
		if($row->num_rows()) {
			return FALSE;
		} else {
			return TRUE;
		}
	}

	private function is_valid($code)
	{
		$row = $this->db->select()->from('tbl_pack_code')->where("BINARY code='$code'", NULL, FALSE)->get();
		if($row->num_rows()) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	private function max_limit($code)
	{
		$row = $this->db->select()->from('tbl_pack_code')->where("BINARY code='$code'", NULL, FALSE)->get();
		if($row->num_rows()) {
			$this->db->start_cache();
			$this->db->select()->from('tbl_registrant_pack_code')->where('user_id', SESSION_USER_ID)->where('pack_code', $code);
			if($row->row()->category == PACK_CODE) {
				$row = $this->db->where('category', $row->row()->category)->get()->num_rows();
				$this->db->stop_cache();
				$this->db->flush_cache();
				if($row < PACK_CODE_LIMIT) {
					return TRUE;
				} else {
					return FALSE;
				}
			} else if($row->row()->category == LAMP_DRIVE) {
				$row = $this->db->where('category', $row->row()->category)->get()->num_rows();
				$this->db->stop_cache();
				$this->db->flush_cache();
				if($row < LAMP_DRIVE_LIMIT) {
					return TRUE;
				} else {
					return FALSE;
				}
			} else if($row->row()->category == ELD) {
				$row = $this->db->where('category', $row->row()->category)->get()->num_rows();
				$this->db->stop_cache();
				$this->db->flush_cache();
				if($row < ELD_LIMIT) {
					return TRUE;
				} else {
					return FALSE;
				}
			} else if($row->row()->category == LEAVE_BEHIND) {
				$row = $this->db->where('category', $row->row()->category)->get()->num_rows();
				$this->db->stop_cache();
				$this->db->flush_cache();
				if($row < LEAVE_BEHIND_LIMIT) {
					return TRUE;
				} else {
					return FALSE;
				}
			} else if($row->row()->category == KA) {
				$row = $this->db->where('category', $row->row()->category)->get()->num_rows();
				$this->db->stop_cache();
				$this->db->flush_cache();
				if($row < NO_LIMIT) {
					return TRUE;
				} else {
					return FALSE;
				}
			}
		}
	}
	
}