<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

class ProfilePrizes extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
 	}

 	public function index()
 	{
 		$reg_id = $this->session->userdata('user_id');
 		$data = array();
 		$claimed_ids = $this->db->select()->from('tbl_registrant_gift')->where('registrant_id', $reg_id)->order_by('registrant_gift_id desc')->get()->result();
 		$gifts = array();
 		foreach($claimed_ids as $claimed_id){
 			$gift = $this->db->select()->from('tbl_gift')->where('gift_id', $claimed_id->registrant_gift_id)->get()->row();
 			if($gift){
 				$gifts[] = array(
 					'gift_id' => $gift->gift_id,
 					'region' => $gift->region,
 					'type' => $gift->type,
 					'image' => $gift->image,
 					'name' => $gift->name,
 					'description' => $gift->description,
 					'km_points' => $gift->km_points
 				);	
 			}
 		}
 		$data['gifts'] = $gifts;
 		$data['experience'] = $this->db->select()->from('tbl_experience')->get()->result();
 		$data['packCode'] = 100;
 		$data['checkIn'] = 120;
 		$data['event'] = 140;
 		$data['shared'] = 150;

 		$data['content'] = $this->load->view('profilePrizes', $data, TRUE);
 		$this->load->view('main-template', $data);
 	}	
}