<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
 	}

 	public function index()
 	{
        //GET OFFER
        $user = $this->session->userdata('user');
        $birthday_offer = '';
        $flash_offer = '';
        $referral_offer = '';
        $bids = '';
        $user_flash_prize = '';
        $birthdate = '';
        
        //SPICE
        $spice_userinfo = $this->spice->GetPerson(array('PersonId' =>$this->spice->getMemberPersonId()));
        $spice_userinfo = $this->spice->parseJSON($spice_userinfo);
        $spice_userinfo = $spice_userinfo->ConsumerProfiles;

        $date_time = $spice_userinfo->PersonDetails->DateOfBirth;

        $match = preg_match('/\/Date\((\d+)([-+])(\d+)\)\//', $date_time, $date);

        $timestamp = $date[1]/1000;
        $operator = $date[2];
        $hours = $date[3]*36; // Get the seconds

        $datetime = new DateTime();

        $datetime->setTimestamp($timestamp);
        $datetime->modify($operator . $hours . ' seconds');

        //BIRTHDAY OFFER
        $birthday_offer = $this->db->select('prize_id')
                ->from('tbl_birthday_offers')
                ->where("month = '".date('m',strtotime($datetime->format('Y-m-d')))."' AND month ='".date('m')."'" , null)
                ->where('stock >', 0)
                ->where('status', 1)
                ->where('is_deleted', 0)
                ->get()->row(); 

        if($birthday_offer){
            $user_birthday_prize = $this->db->select('user_birthday_prize_id')
                                            ->from('tbl_user_birthday_prizes')
                                            ->where('prize_id', $birthday_offer->prize_id)
                                            ->where('registrant_id', $this->session->userdata('user_id'))
                                            ->get()->row();

            $birthday_offer = $user_birthday_prize ? '' : $birthday_offer->prize_id;

            $birthdate = $datetime->format('Y-m-d');
        }else{

            $birthday_offer = '';
        }
        
        //FLASH OFFER
        if(!$birthday_offer){
            $flash_offer = $this->db->select('prize_id,display_number,display_province,start_date,end_date')
                                    ->from('tbl_flash_offers')
                                    ->where('status', 1)
                                    ->where('CURDATE() >= DATE(start_date)', null)
                                    ->where('CURDATE() <= DATE(end_date)', null)
                                    ->where('stock >', 0)
                                    ->where('is_deleted', 0)
                                    ->where('category', 1)
                                    ->get()->row();

            $flash_offer_prize_id = ($flash_offer) ? $flash_offer->prize_id : '';

            $user_flash_prize = $this->db->select('user_flash_prize_id')
                                        ->from('tbl_user_flash_prizes')
                                        ->where('prize_id', $flash_offer_prize_id)
                                        ->where('registrant_id', $this->session->userdata('user_id'))
                                        ->get()->row();
  
            $flash_offer_provinces = $flash_offer->display_province ? explode(',', $flash_offer->display_province) : array();
            $province_name = ($spice_userinfo->Addresses[0]->Locality) ? $spice_userinfo->Addresses[0]->Locality : '';
            $province = $this->db->select()
                                ->from('tbl_provinces')
                                ->where('province', $province_name)
                                ->get()->row();

            $registrant_province_id = ($province) ? $province->province_id : '';

            if(in_array($registrant_province_id, $flash_offer_provinces))
                $reg = $this->db->select()
                                ->from('tbl_registrants')
                                ->where('registrant_id', $this->session->userdata('user_id'))
                                ->get()->row();
            else 
                $reg = $this->db->select()
                                ->from('tbl_registrants')
                                ->where('registrant_id', $this->session->userdata('user_id'))
                                ->get()->row();
            
            $display_number = $flash_offer->display_number;
            //$user_flash_prize = 1;

            $login_count =  $this->db->select('login_id')
                                    ->from('tbl_login')
                                    ->where('DATE(date_login) >= "' . $flash_offer->start_date . '"', null)
                                    ->where('DATE(date_login) <= "' . $flash_offer->end_date . '"', null)
                                    ->where('registrant_id', $this->session->userdata('user_id'))
                                    ->get()->num_rows();
                    
            if($reg){
                // $n = $this->session->userdata('flash_offer_count') ? (int)$this->session->userdata('flash_offer_count') - 1 : (int)$display_number - 1;
                // $this->session->set_userdata('flash_offer_count',$n);
                if($login_count <= $display_number && $this->session->userdata('flash_offer_sess')) {
                    $user_flash_prize = $this->db->select('user_flash_prize_id')
                                                ->from('tbl_user_flash_prizes')
                                                ->where('prize_id', $flash_offer->prize_id)
                                                ->where('registrant_id', $this->session->userdata('user_id'))
                                                ->get()->row();

                    $this->session->unset_userdata('flash_offer_sess');
                } 
            }               
            
            $flash_offer = !isset($user_flash_prize) ? '' : $flash_offer->prize_id;
        }else{
            $flash_offer = '';
        }       

        /*start referral */
        if($this->session->userdata('flash_referral_offer_sess'))
        {       
            $referral_offer = $this->db->select('prize_id,display_number,display_province,start_date,end_date')
                                    ->from('tbl_flash_offers')
                                    ->where('status', 1)
                                    ->where('CURDATE() >= DATE(start_date)', null)
                                    ->where('CURDATE() <= DATE(end_date)', null)
                                    ->where('stock >', 0)
                                    ->where('is_deleted', 0)
                                    ->where('category', 2)
                                    ->get()->row();

            if($referral_offer){
                
                $referral_offer_provinces = $referral_offer->display_province ? explode(',', $referral_offer->display_province) : array();
              
                
                $province_name = ($spice_userinfo->Addresses[0]->Locality) ? $spice_userinfo->Addresses[0]->Locality : '';
                $province = $this->db->select()
                                ->from('tbl_provinces')
                                ->where('province', $province_name)
                                ->get()->row();

                $registrant_province_id = ($province) ? $province->province_id : '';

                if(in_array($registrant_province_id, $referral_offer_provinces))
                    $ref = $this->db->select()
                                ->from('tbl_registrants')
                                ->where('registrant_id', $this->session->userdata('user_id'))
                                ->get()->row();
                else 
                    $ref = $this->db->select()
                                ->from('tbl_registrants')
                                ->where('registrant_id', $this->session->userdata('user_id'))
                                ->get()->row();
                    
                $display_number = $referral_offer->display_number;
                        
                if($ref){
                    $login_count = $this->db->select('login_id')
                                            ->from('tbl_login')
                                            ->where('registrant_id', $this->session->userdata('user_id'))
                                            ->get()->num_rows();

                    if($login_count==1) {
                        $user_flash_prize = $this->db->select('user_flash_prize_id')
                                                    ->from('tbl_user_flash_prizes')
                                                    ->where('prize_id', $referral_offer->prize_id)
                                                    ->where('registrant_id', $this->session->userdata('user_id'))
                                                    ->get()->row();
                    } 
                }               
                
                $referral_offer = !isset($user_flash_prize) ? '' : $referral_offer->prize_id;

            }else{
                $referral_offer = '';    
            }   
        }
        /*end referral*/

        //$bids = $this->Perks_model->get_user_confirmed_bid();
        $data['birthday_offer'] = $birthday_offer;
        $data['flash_offer'] = $flash_offer;
        $data['referral_offer'] = $referral_offer;
        $data['birthdate'] = $birthdate;
        // $data['bids'] = $bids;
        $data['flash_offer_confirmed'] = $user_flash_prize;
        $data['session_key'] = $this->session->userdata('_spice_member_session_key');

        $data['experience'] = $this->check_login_model->get_experiences();
        $row = $this->db->select('experience_id')->from('tbl_experience')->get();
        $row_first_visit = $this->db->select()->from('tbl_points')->where('user_id', $this->session->userdata('user_id'))->where('origin_id', HOME)->where('sub_origin_id', WATCH)->get();
        $data['row_first_visit'] = FALSE;
        if($row_first_visit->num_rows()) {
            $data['row_first_visit'] = TRUE;
        }
        if($row->num_rows()) {
            $data['row'] = $row->row();
        }
        $data['content'] = $this->load->view('home', $data, TRUE);

        $this->load->view('main-template', $data);
 	}

 	public function content()
 	{
 		// $this->header_model->get_experience_header();
 		// $row = $this->db->select('experience_id')->from('tbl_experience')->get();
 		// $row_first_visit = $this->db->select()->from('tbl_points')->where('user_id', $this->session->userdata('user_id'))->where('origin_id', HOME)->where('sub_origin_id', WATCH)->get();
 		// $data['row_first_visit'] = FALSE;
 		// if($row_first_visit->num_rows()) {
 		// 	$data['row_first_visit'] = TRUE;
 		// }
 		// if($row->num_rows()) {
 		// 	$data['row'] = $row->row();
 		// }

 		// $this->load->view('content/index', $data);
 	}

  public function first_login()
  {
      $this->session->unset_userdata('xover_first_login');
  }

}