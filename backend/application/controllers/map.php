<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

class Map extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
 	}

 	public function index()
 	{
 		$data['experience'] = $this->check_login_model->get_experiences();
 		$data['content'] = $this->load->view('map', NULL, TRUE);
 		$this->load->view('main-template', $data);
 	}

 	public function getAllDetails()
 	{
 		$california = $this->db->select('tbl_experience.experience_id, tbl_experience.title, tbl_experience.description, tbl_experience.media as image, tbl_experience.order')->from('tbl_experience')->where('tbl_experience.category_id', REGION_CALIFORNIA)->limit(3)->order_by('tbl_experience.order', 'ASC')->join('tbl_activity', 'tbl_activity.experience_id = tbl_experience.experience_id')->get()->result_array();
 		$canada = $this->db->select('tbl_experience.experience_id, tbl_experience.title, tbl_experience.description, tbl_experience.media as image, tbl_experience.order')->from('tbl_experience')->where('tbl_experience.category_id', REGION_CANADA)->limit(3)->order_by('tbl_experience.order', 'ASC')->join('tbl_activity', 'tbl_activity.experience_id = tbl_experience.experience_id')->get()->result_array();
 		$alaska = $this->db->select('tbl_experience.experience_id, tbl_experience.title, tbl_experience.description, tbl_experience.media as image, tbl_experience.order')->from('tbl_experience')->where('tbl_experience.category_id', REGION_ALASKA)->limit(3)->order_by('tbl_experience.order', 'ASC')->join('tbl_activity', 'tbl_activity.experience_id = tbl_experience.experience_id')->get()->result_array();
 		$user_points = $this->db->select('total_km_points')->from('tbl_registrants')->where('user_id', $this->session->userdata('user_id'))->get()->result_array();
 		// for local
 		// $user_points = array(array('total_km_points'=>1234));

 		foreach($california as $k => $v) {
 			$california[$k] = $v;
 			$california[$k]['url'] = site_url().'activities/'.$v['experience_id'].'/'.url_title($v['title']);
 		}
 		foreach($canada as $k => $v) {
 			$canada[$k] = $v;
 			$canada[$k]['url'] = site_url().'activities/'.$v['experience_id'].'/'.url_title($v['title']);;
 		}
 		foreach($alaska as $k => $v) {
 			$alaska[$k] = $v;
 			$alaska[$k]['url'] = site_url().'activities/'.$v['experience_id'].'/'.url_title($v['title']);;
 		}

 		$rows = array_merge($california, $alaska, $canada, $user_points);

 		$response['status'] = 1;
 		$response['data'] = $rows;
 		$response['points'] = $user_points;

 		header('Content-Type: application/json');
 		echo json_encode($rows);

 		// $row = $this->db->select('tbl_activity.title, tbl_activity.description, tbl_experience.media as image, tbl_experience.category_id as region, tbl_experience.order')->from('tbl_experience')->join('tbl_activity', 'tbl_activity.experience_id = tbl_experience.experience_id')->get();
 		// if($row->num_rows()) {
 		// 	$response['status'] = 1;
 		// 	$response['data'] = $row->result_array();
 		// } else {
 		// 	$response['status'] = 0;
 		// 	$response['data'] = NULL;
 		// }
 		// header('Content-Type: application/json');
 		// echo json_encode($response);
 	}

}