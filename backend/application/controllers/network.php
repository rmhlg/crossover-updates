<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

class Network extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
 	}

 	public function index()
 	{
 		$reg_id = $this->session->userdata('user_id');
 		//$reg_id = 117333;

 		$registrant = $this->db->select()->from('tbl_registrants')->where('user_id', $reg_id)->get()->row();
 		$data['registrant'] = $registrant;
 		
 		$_SESSION['user_id'] = $reg_id;
 		
 		if($registrant->is_cm == 1){
 			$_SESSION['is_cm'] = true;
 		}else{	
 			$_SESSION['is_cm'] = false;
 		}

 		if($registrant->total_km_points<=ALASKA_START_POINTS){
			$region = 1;
		}else if($registrant->total_km_points<=CANADA_START_POINTS){
			$region = 2;
		}else {
			$region = 3;
		}

 		$data['rows'] = $this->db->select()->from('tbl_gift')->where('region', $region)->or_where('type', 2)->get()->result();
 	
 		$ids = $this->db->select()->from('tbl_registrant_gift')->where('registrant_id', $reg_id)->get()->result();
 			
 		foreach($ids as $id){
 			$data['claimed'][] = $id->gift_id;
 		}
 		$data['content'] = $this->load->view('network', $data, TRUE);
 		$this->load->view('main-template', $data);
 	}
}