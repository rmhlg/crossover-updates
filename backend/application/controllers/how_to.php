<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

class How_To extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
 	}

 	public function index()
 	{
 		$data['experience'] = $this->check_login_model->get_experiences();
 		$data['content'] = $this->load->view('howTo', NULL, TRUE);
 		$this->load->view('main-template', $data);
 	}
 	
}