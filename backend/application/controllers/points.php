<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

class Points extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		if( ! $this->input->is_ajax_request()) {
			exit();
		}
		$this->load->model('km_points_model');
		$this->load->model('reports_model');
 	}

 	public function earn_user_points()
 	{
 		$origin_id = decrypt_points($this->input->post('origin_id'));
 		$sub_origin_id = decrypt_points($this->input->post('sub_origin_id'));
 		$main_sub_origin_id = decrypt_points($this->input->post('main_sub_origin_id'));

 		if($origin_id == 0 || strlen($origin_id) > 2 || strlen($sub_origin_id) > 2 || strlen($main_sub_origin_id) > 2) {
 			exit('Invalid parameters');
 		}

 		/* Begin points for Homepage */
 		if($origin_id == HOME) {
 			if($sub_origin_id == WATCH) {
 				$row = $this->db->select()->from('tbl_points')->where('user_id', $this->session->userdata('user_id'))->where('origin_id', $origin_id)->where('sub_origin_id', $sub_origin_id)->get();
 				if( ! $row->num_rows()) {
 					$this->km_points_model->earn(array('points'=>HOMEPAGE_EARN_POINTS));
 					$this->km_points_model->earn_history(array('origin_id'=>$origin_id, 'sub_origin_id'=>$sub_origin_id, 'main_sub_origin_id'=>$main_sub_origin_id, 'points'=>HOMEPAGE_EARN_POINTS));
 					$this->reports_model->user_activity(array('user_id'=>$this->session->userdata('user_id'), 'activity'=>UA_WATCH, 'km_points'=>HOMEPAGE_EARN_POINTS, 'page'=>HOME_PAGE));
 				}
 			}
 		}
 		/* End points for Homepage */

 		/* Begin points for Activity */
 		if($origin_id == ACTIVITY_SELECT) {
 			$row = $this->db->select()->from('tbl_points')->where('user_id', $this->session->userdata('user_id'))->where('origin_id', $origin_id)->where('sub_origin_id', $sub_origin_id)->where('main_sub_origin_id', $main_sub_origin_id)->get();
 			if( ! $row->num_rows()) {
 				$this->km_points_model->earn(array('points'=>ACTIVITY_SELECT_POINTS));
 				$this->km_points_model->earn_history(array('origin_id'=>$origin_id, 'sub_origin_id'=>$sub_origin_id, 'main_sub_origin_id'=>$main_sub_origin_id, 'points'=>ACTIVITY_SELECT_POINTS));
 				$this->reports_model->user_activity(array('user_id'=>$this->session->userdata('user_id'), 'activity'=>UA_SELECTED_ACTIVITIES, 'km_points'=>ACTIVITY_SELECT_POINTS, 'page'=>ACTIVITY_PAGE, 'activity_id'=>$sub_origin_id));
 			}
 		}
 		if($origin_id == ACTIVITY) {
 			if($main_sub_origin_id == WATCH) {
 				$row = $this->db->select()->from('tbl_points')->where('user_id', $this->session->userdata('user_id'))->where('origin_id', $origin_id)->where('sub_origin_id', $sub_origin_id)->where('main_sub_origin_id', $main_sub_origin_id)->get();
 				if( ! $row->num_rows()) {
 					$this->km_points_model->earn(array('points'=>ACTIVITIES_EARN_POINTS));
 					$this->km_points_model->earn_history(array('origin_id'=>$origin_id, 'sub_origin_id'=>$sub_origin_id, 'main_sub_origin_id'=>$main_sub_origin_id, 'points'=>ACTIVITIES_EARN_POINTS));
 					$this->reports_model->user_activity(array('user_id'=>$this->session->userdata('user_id'), 'activity'=>UA_WATCH, 'km_points'=>ACTIVITIES_EARN_POINTS, 'page'=>ACTIVITY_PAGE));
 				}
 			}
 		} else if($origin_id == ACTIVITY_READ) {
 			if($main_sub_origin_id == DECISION_LEFT || $main_sub_origin_id == DECISION_RIGHT) {
 				$row = $this->db->select()->from('tbl_points')->where('user_id', $this->session->userdata('user_id'))->where('origin_id', $origin_id)->where('sub_origin_id', $sub_origin_id)->where('main_sub_origin_id', $main_sub_origin_id)->get();
	 			if( ! $row->num_rows()) {
	 					$this->km_points_model->earn(array('points'=>ACTIVITIES_EARN_POINTS));
	 					$this->km_points_model->earn_history(array('origin_id'=>$origin_id, 'sub_origin_id'=>$sub_origin_id, 'main_sub_origin_id'=>$main_sub_origin_id, 'points'=>ACTIVITIES_EARN_POINTS));
	 					$this->reports_model->user_activity(array('user_id'=>$this->session->userdata('user_id'), 'activity'=>UA_VIEW_CONTENT, 'km_points'=>ACTIVITIES_EARN_POINTS, 'page'=>ACTIVITY_PAGE, 'activity_id'=>$sub_origin_id, 'decision'=>$main_sub_origin_id));
	 			}
 			} 			
 		}
 		/* End points for Activity */
 	}

 	public function user_points()
 	{
 		if($this->input->is_ajax_request()) {
 			$row = $this->db->select('credit_km_points, total_km_points')->from('tbl_registrants')->where('user_id', $this->session->userdata('user_id'))->get()->result_array();
 			if($row) {
 				foreach($row as $k => $v) {
 					$row[$k] = $v;
 					$row[$k]['total_km_points_padded'] = str_pad($row[0]['total_km_points'], 5, '0', STR_PAD_LEFT);
 				}
 				$this->session->set_userdata('total_km_points', $row[0]['total_km_points']);
				$this->session->set_userdata('credit_km_points', $row[0]['credit_km_points']);
 			} else {
 				$row = array(array('credit_km_points'=>0, 'total_km_points'=>0, 'total_km_points_padded'=>str_pad(0, 5, '0', STR_PAD_LEFT)));
 			}
 			echo json_encode($row);
 		}
	}

	/* public function earn_points_activities()
	{
		if($this->input->is_ajax_request()) {
			if($this->input->post('type') == WATCH_VIDEO) {
				$row = $this->db->select()->from('tbl_registrant_activities')->where('registrant_id', $this->session->userdata('registrant_id'))->where('activity_id', $this->input->post('activity_id'))->get();
				if( ! $row->num_rows()) {
					$this->db->insert('tbl_registrant_points_history', array('registrant_id'=>$this->session->userdata('registrant_id'),'points_earned'=>ACTIVITIES_EARN_POINTS,'module'=>'Activities decision','description'=>'Earned '.ACTIVITIES_EARN_POINTS.' for watching in activities page'));

					$this->db->set('total_km_points', 'total_km_points + '.ACTIVITIES_EARN_POINTS, FALSE);
					$this->db->set('credit_km_points', 'credit_km_points + '.ACTIVITIES_EARN_POINTS, FALSE);
					$this->db->where('registrant_id', $this->session->userdata('registrant_id'));
					$this->db->update('tbl_registrants');

					$this->db->set('registrant_id', $this->session->userdata('registrant_id'));
					$this->db->set('activity_id', $this->input->post('activity_id'));
					$this->db->set('activity_type', $this->input->post('type'));
					$this->db->set('points_earned', ACTIVITIES_EARN_POINTS);
					$this->db->set('decision_name', $this->input->post('decision'));
					$this->db->insert('tbl_registrant_activities');
				}
			} else if($this->input->post('type') == READ_CONTENT) {
				$row = $this->db->select()->from('tbl_registrant_activities')->where('registrant_id', $this->session->userdata('registrant_id'))->where('decision_name', $this->input->post('decision'))->get();
				$row_validate = $this->db->select()->from('tbl_registrant_activities')->where('registrant_id', $this->session->userdata('registrant_id'))->where('activity_type', READ_CONTENT)->get();
				if( ! $row->num_rows() && $row_validate < 3) {
					$this->db->insert('tbl_registrant_points_history', array('registrant_id'=>$this->session->userdata('registrant_id'),'points_earned'=>ACTIVITIES_EARN_POINTS,'module'=>'Activities decision','description'=>'Earned '.ACTIVITIES_EARN_POINTS.' for clicking '.$this->input->post('decision').' decision.'));

					$this->db->set('total_km_points', 'total_km_points + '.ACTIVITIES_EARN_POINTS, FALSE);
					$this->db->set('credit_km_points', 'credit_km_points + '.ACTIVITIES_EARN_POINTS, FALSE);
					$this->db->where('registrant_id', $this->session->userdata('registrant_id'));
					$this->db->update('tbl_registrants');

					$this->db->set('registrant_id', $this->session->userdata('registrant_id'));
					$this->db->set('activity_id', $this->input->post('activity_id'));
					$this->db->set('activity_type', $this->input->post('type'));
					$this->db->set('points_earned', ACTIVITIES_EARN_POINTS);
					$this->db->set('decision_name', $this->input->post('decision'));
					$this->db->insert('tbl_registrant_activities');
				}
			}
		}
	} */

	/* public function earn_points_homepage()
	{
		if($this->input->is_ajax_request()) {
			if($this->input->post('type') == WATCH_VIDEO) {
				$row = $this->db->select()->from('tbl_registrant_home_page')->where('registrant_id', $this->session->userdata('registrant_id'))->get();
				if( ! $row->num_rows()) {
					$this->db->insert('tbl_registrant_points_history', array('registrant_id'=>$this->session->userdata('registrant_id'),'points_earned'=>HOMEPAGE_EARN_POINTS,'module'=>'Activities decision','description'=>'Earned '.HOMEPAGE_EARN_POINTS));

					$this->db->set('total_km_points', 'total_km_points + '.HOMEPAGE_EARN_POINTS, FALSE);
					$this->db->set('credit_km_points', 'credit_km_points + '.HOMEPAGE_EARN_POINTS, FALSE);
					$this->db->where('registrant_id', $this->session->userdata('registrant_id'));
					$this->db->update('tbl_registrants');

					$this->db->set('registrant_id', $this->session->userdata('registrant_id'));
					$this->db->set('points_earned', HOMEPAGE_EARN_POINTS);
					$this->db->insert('tbl_registrant_home_page');
				}
			}
		}
	} */

}