<?php

/**
 * 
 * @author anthony
 * 
 */
class NW_Input extends CI_Input
{
	/**
	* Returns HTML escaped variable
	*
	* @access	private
	* @param	mixed
	* @return	mixed
	*/
	private function _html_clean($var)
	{
		if (is_array($var)) {
			return array_map(array($this, '_html_clean'), $var);
		}
		return htmlspecialchars($var, HTML_ESCAPE_CONFIG, config_item('charset'));
	}

	/**
	* Fetch an item from the GET array
	*
	* @access	public
	* @param	string
	* @param	bool
	* @return	string
	*/
	public function get($index = NULL, $xss_clean = FALSE)
	{
		return $this->_html_clean(parent::get($index, $xss_clean));
	}

	/**
	* Fetch an item from the POST array
	*
	* @access	public
	* @param	string
	* @param	bool
	* @return	string
	*/
	public function post($index = NULL, $xss_clean = FALSE)
	{
		return $this->_html_clean(parent::post($index, $xss_clean));
	}
}