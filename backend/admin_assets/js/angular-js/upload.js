angular.module('fileUpload', [ 'angularFileUpload' ]);

var MyCtrl = [ '$scope', '$http', '$timeout', '$upload',  function($scope, $http, $timeout, $upload) {
	$scope.fileReaderSupported = window.FileReader != null;
	$scope.uploadRightAway = true; 

	$scope.hasUploader = function(index) {
		return $scope.upload[index] != null;
	};
	$scope.abort = function(index) {
		$scope.upload[index].abort(); 
		$scope.upload[index] = null;
	};
	$scope.onFileSelect = function($files) {
		$scope.selectedFiles = [];
		$scope.progress = [];
		$scope.uploadError = [];
 		$scope.removeParam = [];
		if ($scope.upload && $scope.upload.length > 0) {
			for (var i = 0; i < $scope.upload.length; i++) {
				if ($scope.upload[i] != null) {
					$scope.upload[i].abort();
				}
			}
		}
		$scope.upload = [];
		$scope.selectedFiles = $files;
		$scope.dataUrls = [];
		for ( var i = 0; i < $files.length; i++) {
			var $file = $files[i];
			if (window.FileReader && $file.type.indexOf('image') > -1) {
				var fileReader = new FileReader();
				fileReader.readAsDataURL($files[i]);
				var loadFile = function(fileReader, index) {
					fileReader.onload = function(e) {
						$timeout(function() {
							$scope.dataUrls[index] = e.target.result;
						});
					}
				}(fileReader, i);
			}
			$scope.progress[i] = -1;
			$scope.start(i);
		}
	}

	
	$scope.start = function(index) {

		$scope.progress[index] = 0;
		$scope.upload[index] = $upload.upload({
			url : $scope.uploadURL,
			method: 'POST',
			headers: {'my-header': 'my-header-value'},
			data : {
				myModel : $scope.myModel
			},
			file: $scope.selectedFiles[index],
			fileFormDataName: 'myFile'
		}).then(function(response) {
			var response = response.data;

			if(response.video_thumbnail){
				$scope.dataUrls[index] = response.video_thumbnail;
			}

			if(response.uploadError){
				$scope.uploadError[index] =  response.uploadError;
				$scope.progress[index] = -1;
			}

				$scope.removeParam[index] = response.removeParam;

		}, null, function(evt) {
			$scope.progress[index] = parseInt(100.0 * evt.loaded / evt.total);
		}).xhr(function(xhr){
			xhr.upload.addEventListener('abort', function(){console.log('aborted complete')}, false);
		});

	}

	$scope.remove = function(index) {

  		 
		if($scope.removeParam[index]){

			$http({url: $scope.removeURL+$scope.removeParam[index],method:'GET'})
				.success(function(response){
					console.log(response);
				});
		}

		$scope.selectedFiles.splice(index, 1);
		$scope.dataUrls.splice(index, 1);
		$scope.progress.splice(index, 1);
		$scope.uploadError.splice(index, 1);
		$scope.removeParam.splice(index,1);

	}
} ];
