// JavaScript Document
var empty = /^\s*$/;
var url = new RegExp("^(http|https|ftp)\://[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(:[a-zA-Z0-9]*)?/?([a-zA-Z0-9\-\._\?\,\'/\\\+&amp;%\$#\=~])*$");
var email = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
function validateForm(elem) {
	elem = $(elem);
	error = false;
	elem.find('.required').each(function() {
		
		id = $(this).attr('id');
		val = $(this).val();
		field = $(this).data('name');
		if(empty.test(val)) {
			error = 'Sorry, you need to input ' + field + ' to proceed';
			$(this).focus();
			return false;
		}
		
		if(id == 'url' && !url.test(val)) {
			error = 'Sorry, you need to a valid url to proceed';
			$(this).focus();
			return false;
		}	
		
		if(id == 'email' && !email.test(val)) {
			error = 'Sorry, you need to a valid email address to proceed';
			$(this).focus();
			return false;	
		}

		if($(this).hasClass('number')) {
			if(val < 1 && $('input[name="status"]').val() == '1') {
				error = 'Sorry, you need enter at least 1 ' + field + ' to proceed';
				$(this).focus();
				return false;		
			}
		}
		
		
	});
	
	if(!error) {
		if($('#password1').val() != null && $('#password2').val() != null && $('#password2').val().length > 0 && $('#password1').val() != $('#password2').val()) {
			error = 'Sorry, passwords did not match';
			$('#password2').focus();
		}
		
		if($('#password1').val() != null) {
			//checkPasswordComponents($('#password1').val(), 'test');
			if($('#password1').val().length > 0) {
				if(!validatePassword($('#password1').val())) {
					error = 'Sorry, passwords must be at least eight(8) characters with any three(3) of the following: <ul><li>lower case letters</li><li> upper case letters</li><li> numbers</li><li> special characters</li></ul>';
					$('#password1').focus();
				} 
				
				if(checkPasswordComponents($('#password1').val(), $('#name').val())) {
					error = 'Sorry, passwords must not contain the following: <ul><li>account name</li><li>user</li><li>admin</li><li> guest</li><li>sys</li><li>test</li><li>pass</li><li>super</li></ul>';
					$('#password1').focus();	
				}
			}	
		}
	}
	if(error)
		return error;
	else
		return false;
}

function showPreloader(elem) {
	$(elem).hide();
	$(elem).parent().append('<img src="images/admin/loading.gif" alt="Loading..." />');
}

function hidePreloader(elem) {
	$(elem).show();
	$(elem).parent().find('img').remove();
}

function confirmDeletion(elem, type) {
	elem = $(elem);
	type = type ? type : '';
	bootbox.confirm('Are you sure you want to delete this ' + type + '?', function(result) {
		if(result == 1) {
			window.location = elem.attr('href');
			return true;
		}
	});
	return false
}

function confirmAction(elem, type, entry) {
	elem = $(elem);
	type = type ? type : '';
	entry = entry ? entry : 'entry';
	bootbox.confirm('Are you sure you want to ' + type + ' this ' + entry +'?', function(result) {
		if(result == 1) {
			window.location = elem.attr('href');
			return true;
		}
	});
	return false
}

function submitFrm() {
	$('.alert').hide();
	theForm = $('form');
	error = validateForm(theForm);
	data = false;
	if(error) {
		$('.alert').show().html(error);
		$(document).scrollTop(0);
		return false;
	} else {
		return true;
	}
	return false;
}

function checkPasswordStrength(elem) {
	password = $(elem).val();
	var hasUpperCase = /[A-Z]/.test(password);
	var hasLowerCase = /[a-z]/.test(password);
	var hasNumbers = /\d/.test(password);
	var hasNonalphas = /\W/.test(password);
	if (password.length < 8)
		return 0;
	else {
		return 10 + (hasUpperCase * 15) + (hasLowerCase * 10) + (hasNumbers * 25) + (hasNonalphas * 40);
	}
	///if (hasUpperCase + hasLowerCase + hasNumbers + hasNonalphas < 3)
}

function validatePassword(password) {
	var hasUpperCase = /[A-Z]/.test(password);
	var hasLowerCase = /[a-z]/.test(password);
	var hasNumbers = /\d/.test(password);
	var hasNonalphas = /\W/.test(password);
	if (password.length < 8)
		return 0;
	else {
		complexity = hasUpperCase + hasLowerCase + hasNumbers + hasNonalphas;
		return complexity >= 3;
	}
	///if (hasUpperCase + hasLowerCase + hasNumbers + hasNonalphas < 3)
}

function checkPasswordComponents(password, name) {
	var excludedWords = new RegExp("/(\\b" + name + "\\b)|user|guest|admin|sys|test|pass|super|/i");
	return excludedWords.test(password);

}

function loadCities(province, city) {
	params = {'province': province};
	opts = '';
	$('.provinces option[value="' + province + '"]').attr("selected", true);
	$('.cities').html('<option>Loading...</option>');
	$.getJSON(siteUrl + '/ajax/load_cities', params, function(data) {
		for(i in data) {
			selected = city == data[i].city_id ? 'selected' : '';
			opts += '<option value="' + data[i].city_id + '" ' + selected + '>' + data[i].city  + '</option>';
		}
		$('.cities').html(opts);
	});
}

function previewImage(img, preview, callback, e) {
	var oFReader = new FileReader();
	oFReader.readAsDataURL(document.getElementById(img).files[0]);
	oFReader.onload = function (oFREvent) {
		image = new Image();
		image.src =  oFREvent.target.result;
		image.onload = function() {
			width = this.width;
			height = this.height;
			if(width > 500) {
				percentageWidth = 500 / width;
				width = percentageWidth * this.width;
				height = percentageWidth * this.height;		
			}
			$('.jcrop-holder').find('img').css({width:width + 'px', height: height + 'px'})
			$('.' + preview + ', .jcrop-tracker, .jcrop-holder').css({width:width + 'px', height: height + 'px'})
		}
		$('.jcrop-holder').find('img').prop('src', oFREvent.target.result);
		$('.' + preview).prop('src', oFREvent.target.result);
	};
	callback(e);
};

function checkDigit(evt) {
	var charCode = (evt.which) ? evt.which : event.keyCode;
	var BACKSPACE = 8;
	var DELETE = 46;
	var LEFT = 37;
	var RIGHT = 39;
	if (charCode != 0)  {
		if((charCode < 48 || 57 < charCode) && charCode != BACKSPACE && charCode != DELETE && charCode != LEFT && charCode != RIGHT) {
		  evt.preventDefault();		  
		}
		else {
			return true;
		}
	}
}

function showRegistrantDetails(id) {
	$.post(siteUrl + '/registrant/get_registrant_details/' + id, function(data) {
		bootbox.dialog({'message': data,
						'title': 'Registrant Details'});	
	})
}


function showReferrals(id,status) {
	$.post(siteUrl + '/user_referrals/referrals/'+id+'/'+status, function(data) {
		bootbox.dialog({'message': data,
						'title': 'Referrals'});	
	})
}

function buildUrl(url, limit){
	url = url.replace(/&limit=\d*/g, '');
	url = url.replace(/\?limit=\d*/g, '?');
	urlSplit = url.split('?');
	if(urlSplit.length > 1)
		return url + '&limit=' + limit;
	else 
		return url + '?limit=' + limit;
}


$(function() {
	 $('.from').datepicker({
	  changeMonth: true,
	  numberOfMonths: 1,
	  dateFormat:'yy-mm-dd',
	  onClose: function( selectedDate ) {
		$( 'input[name="to"]').datepicker( "option", "minDate", selectedDate );
	  }
	});
	$('.to').datepicker({
	  defaultDate: "+1w",
	  changeMonth: true,
	  numberOfMonths: 1,
	  dateFormat:'yy-mm-dd',
	  onClose: function( selectedDate ) {
		$('input[name="from"]').datepicker( "option", "maxDate", selectedDate );
	  }
	});

	//$('.modal').modal();
	$('.modal-pop').click(function() {
		//$(this).parent().find('div').modal('show');
		bootbox.dialog({'message': $(this).parent().find('div').html(),
						'title': $(this).data('title')});
	});
	
	$(document).on('click', '.close-bootbox', function() {
		bootbox.hideAll();
	});

	$('.pagination div select').change(function() {
		window.location = buildUrl(window.location.toString(), $(this).val());
	});

});

