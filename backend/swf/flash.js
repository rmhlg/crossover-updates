function onImageUpload() {
   
}

function onImageError() {
   $('#take-photo-btn').html('<i>TAKE PHOTO</i>');
}
function onSizeError() {
   $('#take-photo-btn').html('<i>TAKE PHOTO</i>');
}

function onUploadStart() {
    

}

function onUploadComplete()
{

}

function onRedirect() {
    // do something
}

function enableUpload() {
    

}

function getFlash(id) {
    if (window.document[id]) {
        return window.document[id];
    }
    if (navigator.appName.indexOf("Microsoft Internet") == -1) {
        if (document.embeds && document.embeds[id]) {
            return document.embeds[id];
        }
    } else {
        return document.getElementById(id);
    }
    return null;

}

function onSaveBA(image)
{
    $('input[name="imageString"]').val(image);
}

function initFlash() {
    var so = new SWFObject("swf/app.swf?debug=true&forceWebcam=true", "flash", "100%", "100%", "10", "#ffffff");
	so.addParam("quality", "high");
	so.addParam("wmode", "transparent");
	so.addParam("salign", "tl");
	so.addParam("scaleMode", "scale");
	so.addParam("allowscriptaccess","always");
	so.useExpressInstall("swf/expressInstall.swf");
    so.write("flashcontent"); 
	
	// check if flash is not embeded
    var pat = /expressInstall/;
    if(!pat.test(jQuery('#flashcontent embed, object').attr("src"))) {
        
        if(jQuery("#flashcontent").html() == "") {
            jQuery('.preloader').hide();
            jQuery("#flashcontent").html('<div style="padding:250px 0 0; text-align:center;"><strong>Please download the latest flash player</strong><br /><a href="http://www.adobe.com/go/getflashplayer" target="_top"><img src="https://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" /></a></div>');
        }
    }
     
}
