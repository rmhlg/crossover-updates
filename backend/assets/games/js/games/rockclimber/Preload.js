(function () {

    var Preloader = function () {
        this.titleText = null;
        this.ready = false;
    };

    Preloader.prototype = {

        preload: function () {
            $(".loader").addClass("visible");
            
            this.load.onLoadComplete.addOnce(this.onLoadComplete, this);
            this.load.image('letsgo', 'img/games/CTA.png');
            this.game.load.spritesheet('points', 'img/games/rockclimber/points.png', 76, 107);
            this.load.image('block', 'img/games/rockclimber/big_rock_01.png');
            this.load.image('block2', 'img/games/rockclimber/big_rock_02.png');
            this.load.image('pointsback', 'img/games/rockclimber/pointsback.png');
            this.load.spritesheet('dude', 'img/games/rockclimber/Climbing_24frames_spritesheet.png', 171, 215);
            this.load.image('starfield', 'img/games/rockclimber/starfield.jpg');
        },

        create: function () {
        },

        update: function () {
            if (this.ready === false) {
                this.ready = true;
                this.state.start('GameTitle');
            }
        },

        onLoadComplete: function () {
            $(".loader").removeClass("visible");
        }
    };

    window['rockclimber'] = window['rockclimber'] || {};
    window['rockclimber'].Preloader = Preloader;

})();