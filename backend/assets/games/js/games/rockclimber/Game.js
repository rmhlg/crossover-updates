(function () {

    var Game = function () {

    };

    Game.prototype = {

        create: function () {
            this.game.time.advancedTiming = true;
            this.rocks = [];
            this.gameover = false;
            this.increment = 2000;
            this.dudepos = 3;
            this.distance = 0;
            this.lastaddedblock = 0;
            this.addscore = 0;
            this.world.setBounds(0, 0, 1024, 540);
            this.stage.backgroundColor = 0x000000;
            this.movingDown = false;

            this.dudepositions = [150, 250, 350, 450, 550, 650, 750, 850];
            this.blockpositions = [100, 200, 300, 400, 500, 600, 700, 800];
            this.pointpositions = [130, 170, 260, 350, 550, 580, 740, 770];
            
            this.physics.startSystem(Phaser.Physics.ARCADE);

            this.tilesprite = this.add.tileSprite(0, 0, this.game.width, this.world.height, 'starfield');

            this.graphics = this.add.graphics(0, 0);

            this.cursors = this.input.keyboard.createCursorKeys();

            this.initListeners();
            
            this.createBlocks();
            this.createPlayer();
            this.createText();
            this.createPoints();

        },
        pointOverlapHandler: function(){
            this.resetPoint();
            this.plusscore.alpha = 1;
            this.plusscore.y = 300;
            var plustween = this.game.add.tween(this.plusscore);
                plustween.to({
                    y: 250,
                    alpha:0
                }, 2000,Phaser.Easing.Circular.Out, true, 0, 0);
            
            this.addscore += 5;
           // this.increment+= 750;
            

        },
        
        update: function () {
            if (!this.gameover) {
                var pos = this.increment / 2000;
                var sp = Math.round((pos * 3) + 16);
                if (sp > 60) {
                    sp = 60;
                }
                this.dude.animations.getAnimation('climb').speed = sp;

                this.blocks.forEach(function (blockitem) {
                    blockitem.y += pos;
                    this.physics.arcade.overlap(this.dude, blockitem, this.overlapHandler, null, this);
                }, this);

                this.tilesprite.tilePosition.y += pos;
                this.point.y += pos;
                this.increaseSpeed();

                this.checkBlocks();


                this.distance = Math.floor(this.increment / 150) - 13;
                this.checkPoint();
                this.game.physics.arcade.overlap(this.dude,this.point, this.pointOverlapHandler, null, this);
                
                this.updateCounter();
                
            }

        },
        checkPoint : function(){
           if(this.point.y > 600){
            this.resetPoint();
           }
        },

        updateCounter: function () {
            this.timeText.setText(this.distance + this.addscore);
            this.timeText.x = 70 + this.timeText.width;

           
        },

        createText: function () {
            var style = {
                font: '60px bebas-neue',
                fill: '#FFFFFF',
                align: 'center'
            };

            

            this.overmessage = this.add.text(this.game.width / 2, this.game.height / 2, '', style);
            this.overmessage.anchor.set(0.5);
            this.overmessage.fixedToCamera = true;
            
            this.plusscore = this.game.add.text(this.game.world.centerX - 20, 100, '+5', {
                font: '40px bebas-neue',
                fill: '#ffffff',
                align: 'center'
            });
            
            var pb = this.add.sprite(-80, 15, 'pointsback');
            pb.fixedToCamera = true;
            var style2 = {
                font: '45px bebas-neue',
                fill: '#000',
                width:'100px',
                align: 'right'
            };
            
            var style3 = {
                font: '25px bebas-neue',
                fill: '#000',
                align: 'right'
            };

            this.timeText = this.add.text(50, 53, this.hightScore + '', style2);
            this.timeText.anchor.set(0.5);
            this.timeText.fixedToCamera = true;

            
            this.scoreText2 = this.add.text(50, 50,'M', style3);
            this.scoreText2.anchor.set(0.5);
            this.scoreText2.x = 85;
            this.scoreText2.y = 60;
            this.scoreText2.fixedToCamera = true;

        
            this.plusscore.alpha = 0;
        },
        createPoints : function(){
            var pos = Math.floor(Math.random() * this.pointpositions.length);
            this.point = this.add.sprite(this.pointpositions[pos], -150, 'points');
            this.point.alpha = 1;
            this.point.animations.add('turn', [0,1,2,3,4,5,6,7,8], 15, true);
            this.point.animations.play("turn");
            this.game.physics.enable(this.point, Phaser.Physics.ARCADE);
            this.point.body.setSize(50, 100, 10, 0);
        },

        createPlayer: function () {
            this.dude = this.add.sprite(this.dudepositions[this.dudepos], this.world.height - 210, 'dude');

            this.dude.animations.add('climb', [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22], 5, true);
            this.dude.animations.add('right', [23, 24, 25, 26, 27, 28, 29, 30, 31], 15, true);
            this.dude.animations.add('left', [31, 32, 33, 34, 35, 36, 37], 15, true);
            this.physics.enable(this.dude, Phaser.Physics.ARCADE);
            this.dude.animations.play('climb', 5, true);
            this.dude.body.setSize(40, 30, 65, 50);

        },

        createBlocks: function () {
            this.blocks = this.add.group();
            this.enableBody = true;
            this.physicsBodyType = Phaser.Physics.ARCADE;

            var e = 0;
            for (; e < 12; e++) {

                var b = (e % 2 === 0 ? 'block' : 'block2');

                var time = 3000 * e;
                if (e < 3) {
                    time = 500 * e;
                }

                if (e > 1) {
                    this.game.time.events.add(time, this.spawnBlock, this, b, e);
                } else {
                    var newy = 250 - Math.random() * 80;
                    var pos = 0;
                    if (e === 0) {
                        pos = this.blockpositions.length - 1;
                    }
                    var block = this.blocks.create(this.blockpositions[pos], newy, b);
                    block.name = 'block' + e.toString();
                    block.checkWorldBounds = true;
                    // block.events.onOutOfBounds.add(this.blockOut, this);

                    this.physics.enable(block, Phaser.Physics.ARCADE);
                    block.body.setSize(120, 35, 60, 80);
                    this.lastaddedblock = newy;
                }
            }
        },

        spawnBlock: function (b, e) {
            var newy = Math.round(-120 - (Math.random() * 200));
            var pos = Math.floor(Math.random() * this.blockpositions.length);

            var block = this.blocks.create(this.blockpositions[pos], newy, b);
            block.name = 'block' + e.toString();
            block.checkWorldBounds = true;
            // block.events.onOutOfBounds.add(this.blockOut, this);

            this.physics.enable(block, Phaser.Physics.ARCADE);
            block.body.setSize(120, 35, 60, 80);
            this.lastaddedblock = newy;
        },

        initListeners: function () {
            var ga = this;
            this.input.keyboard.onDownCallback = function (e) {
                if (e.keyCode === Phaser.Keyboard.RIGHT) {
                    ga.move(1);
                } else if (e.keyCode === Phaser.Keyboard.LEFT) {
                    ga.move(-1);
                }
            };

            this.input.onTap.add(function (e) {
                if (e.x < 512) {
                    ga.move(-1);
                } else {
                    ga.move(1);
                }
            });

        },

        move: function (direction) {
            if (!this.gameover) {
                if (!this.movingDown && this.distance > 0) {
                    this.dudepos += direction;
                    var animpos = true;
                    if (this.dudepos < 0) {
                        this.dudepos = 0;
                        animpos = false;
                    } else if (this.dudepos > this.dudepositions.length - 1) {
                        this.dudepos = this.dudepositions.length - 1;
                        animpos = false;
                    }

                    if (animpos) {
                        if (direction === -1) {
                            this.dude.animations.play('left');
                        } else {
                            this.dude.animations.play('right');
                        }
                        this.moveWorldDown();

                    }

                    var dud = this.dude;
                    this.game.time.events.add(400, function () {
                        dud.animations.play('climb');
                        this.movingDown = false;
                    }, this);

                    var s = this.add.tween(this.dude.body);
                    s.to({
                        x: this.dudepositions[this.dudepos]
                    }, 400, Phaser.Easing.Linear.None);

                    s.start();

                    if (this.increment > 1050) {
                        this.increment -= 110;
                    }
                }
            }

        },
        resetPoint : function(){
            this.point.destroy();
        this.createPoints();
            

        },
        moveWorldDown: function () {
            var amount = 25;
            this.blocks.forEach(function (blockitem) {
                var newy = blockitem.y - amount;
                var tw = this.add.tween(blockitem);
                tw.to({
                    y: newy
                }, 400, Phaser.Easing.Linear.None);
                tw.start();
            }, this);

            var newy = this.tilesprite.tilePosition.y - amount;
            var tw = this.add.tween(this.tilesprite.tilePosition);
            tw.to({
                y: newy
            }, 400, Phaser.Easing.Linear.None);
            tw.start();
            
            var newy = this.point.y - amount;
            var twp = this.add.tween(this.point);
            twp.to({
                y: newy
            }, 400, Phaser.Easing.Linear.None);
            twp.start();
           
            this.movingDown = true;

        },

        increaseSpeed: function () {
            this.increment+=3;
        },

        overlapHandler: function () {
            this.dude.animations.stop();
            this.gameover = true;
            
            var grade = 'none';
            var count  = this.distance + this.addscore;
            if (count > gold) {
                grade = 'gold';
            } else if (count > silver) {
                grade = 'silver';
            } else if (count > bronze) {
                grade = 'bronze';
            }

           gameOver(grade, count);

        },

        quitGame: function () {
            this.reset();
            this.state.start('StartMenu');
        },

        checkBlocks: function () {
            this.blocks.forEach(function (blockitem) {
                if (blockitem.y > 600) {
                    var randy = Math.round(-600 - (Math.random() * 200));
                    var pos = Math.floor(Math.random() * this.blockpositions.length);
                    blockitem.reset(this.blockpositions[pos], randy);
                }
            }, this);
        },
        render: function () {
       
              //  this.game.debug.body(this.dude);
            //    this.game.debug.body(this.point);
                /*for (var i = 0; i < this.rocksOnScreen.length; i++) {
                    this.game.debug.body(this.rocksOnScreen[i]);
                }*/
            
        },

        reset: function () {
            this.gameover = false;
            this.worldspeed = 200;
            this.increment = 25;
            this.lastblockOut = 1;
            this.game.time.advancedTiming = true;
            this.increment = 2000;
            this.dudepos = 3;
            this.distance = 0;
            this.lastaddedblock = 0;
            this.addscore = 0;
            this.movingDown = false;
            //ring this.lastRingOut = 1;
        }
    };

    window['rockclimber'] = window['rockclimber'] || {};
    window['rockclimber'].Game = Game;

})();