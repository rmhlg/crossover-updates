(function () {
    'use strict';
    
    
    function GameTitle() {
        this.startPrompt = null;
        this.ding = null;
    }

    GameTitle.prototype = {

        create: function () {

            var startBG = this.add.image(360, 220, 'letsgo');
            startBG.inputEnabled = true;
            startBG.input.useHandCursor = true;
            startBG.events.onInputDown.addOnce(this.startGame, this);
            
        },

        startGame: function () {

        $.ajax({
            url: gamesStart_url+"games/start_game",
            data: {'game_name' : 'grab'},
            type: 'post',
            success: function(response) {
                gameToken = response;
            }
        });

        this.game.state.start('game');
        }
    };

    window['grabgame'] = window['grabgame'] || {};
    window['grabgame'].GameTitle = GameTitle;

})();