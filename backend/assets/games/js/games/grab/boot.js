(function () {
    'use strict';

    function Boot() {}

    Boot.prototype = {

        create: function () {
            this.input.maxPointers = 1;

            if (this.game.device.desktop) {
                this.game.scale.pageAlignHorizontally = true;
            } else {
                this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
                this.game.scale.minWidth = 480;
                this.game.scale.minHeight = 320;
                this.game.scale.maxWidth = 1024;
                this.game.scale.maxHeight = 540;
                this.game.scale.forceOrientation(false);
                this.game.scale.compatibility.scrollTo = false;
            }

            this.state.start('preloader');
        }
    };

    window['grabgame'] = window['grabgame'] || {};
    window['grabgame'].Boot = Boot;

}());