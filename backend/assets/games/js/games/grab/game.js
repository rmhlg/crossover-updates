(function () {

    'use strict';

    function Game() {
        this.hero = null;
        this.tilesprite = null;
        this.tilesprite2 = null;
        this.start = null;
        this.overmessage = null;
        this.linesprite = null;
        this.endgame = null;
    }

    Game.prototype = {

        create: function () {
            
            this.world.setBounds( 0, 0, this.game.width, this.game.height );
            if(this.cursor != null){
                this.cursor = null;
                this.hero.destroy();
                this.hero = null;
                this.platforms.destroy();
                this.platforms = null;  
                this.ground.destroy();
                this.ground = null;
            }
            
            // background
            //this.stage.backgroundColor = '#6bf';
            this.tilesprite = this.add.sprite(0, -636 + this.game.height, 'rock1');
            this.tilesprite2 = this.add.sprite(0, -1492 + this.game.height, 'rock2');
            this.tilesprite3 = this.add.sprite(0, -2348 + this.game.height, 'rock2');
            this.tilesprite4 = this.add.sprite(0, -3204 + this.game.height, 'rock2');
            this.tilesprite5 = this.add.sprite(0, -4060 + this.game.height, 'rock2');
            this.tilesprite6 = this.add.sprite(0, -4916 + this.game.height, 'rock2');
            this.tilesprite7 = this.add.sprite(0, -5772 + this.game.height, 'rock2');
            this.linesprite = this.add.tileSprite(0, 0, 10, 10, 'rope');
            
            
            // physics
            this.physics.startSystem(Phaser.Physics.ARCADE);

            // initialize variables
            this.platformYMin = 99999;
            this.addStarDistance = 50;
            this.moveSpeed = 400;
            this.pulldistance = this.game.height * 0.8;
            this.hightScore = 0;
            this.start = true;
            this.endgame = false;
            
            
           

            /*  if (localStorage.HighScore) {
                  this.highScore = localStorage.HighScore;
              }*/


            this.paused = false;
            

            // Create all sprites

            this.createGround();

            this.createPlatforms();

            this.createHero();

            this.createInterface();
        },

        createHero: function () {
            // basic hero setup
            this.hero = this.add.sprite(this.world.centerX, this.world.height - 115, 'hero');
            this.hero.animations.add('start', [0, 1, 2, 3, 4, 5], 10, true);
            this.hero.animations.add('grab', [6, 7, 8, 9, 10, 11, 12], 10, true);
            this.hero.animations.add('fall', [13, 14, 15, 16], 5, false);
            this.hero.anchor.set(0.5);

            // track where the hero started and how much the distance has changed from that point
            this.hero.yOrig = this.hero.y;
            this.hero.yChange = 0;

            // hero collision setup
            // disable all collisions except for down
            this.physics.arcade.enable(this.hero);
            if(Phaser.Device.desktop){
                this.hero.body.setSize(50, 150, 10, 0);
            }
            else{
                this.hero.body.setSize(70, 180, 0, 0);
            }
            this.hero.body.collideWorldBounds = true;
            this.hero.body.bounce.setTo(0.5, 0.1);
            this.hero.alpha = 1;
            this.hero.body.gravity.y = 500;
        },

        createGround: function () {
            this.ground = this.add.group();
            this.ground.enableBody = true;
            this.ground.opacity = 0;
            var asset = this.ground.create(300, this.world.height - 40, 'ground');
            asset.body.immovable = true;

        },

        createPlatforms: function () {
            // platform basic setup
            this.platforms = this.add.group();
            this.cracks = this.add.group();
            this.platforms.enableBody = true;

            for (var i = 0; i < 20; ++i) {
                var starnum = i % 6;
                var randx = Math.floor(Math.random() * (770 - 350 + 1) + 350);
                if (i === 0) {
                    randx = this.world.width / 2;
                }

                var randy = this.world.height - 400 - this.addStarDistance * i;
                if (randy > -31 && randy < 31) {
                    randy -= 60;
                }
                
                var crack = this.cracks.create(randx - 86, randy-86, 'cracks');
                crack.animations.add('cracking', [0,1,2,3], 3, false);
                crack.frame = 0;
                crack.id = i;
               // crack.opacity = 0;
                
                var star = this.platforms.create(randx, randy, 'fixations');
                star.frame = starnum;

                star.anchor.set(0.5);
                star.body.setSize(30, 30, 0, 0);
                if(!Phaser.Device.desktop){
                     star.body.setSize(50, 50, 0, 0);
                }
                star.inputEnabled = true;
                star.events.onInputDown.add(this.clickStar, this);
                star.mass = 10;
                star.id = i;
                
                
            }
            this.input.onUp.add(this.releaseStar, this);
        },

        createInterface: function () {
            
            var pb = this.add.sprite(-80, 15, 'pointsback');
            pb.fixedToCamera = true;
            var style2 = {
                font: '45px bebas-neue',
                fill: '#000',
                width:'100px',
                align: 'right'
            };
            
            var style3 = {
                font: '25px bebas-neue',
                fill: '#000',
                align: 'right'
            };

            this.scoreText = this.add.text(50, 53, this.hightScore + '', style2);
            this.scoreText.anchor.set(0.5);
            

            this.scoreText.fixedToCamera = true;
            
            this.scoreText2 = this.add.text(50, 50,'M', style3);
            this.scoreText2.anchor.set(0.5);
            this.scoreText2.x = 85;
            this.scoreText2.y = 60;
            this.scoreText2.fixedToCamera = true;


             var style = {
                font: '40px bebas-neue',
                fill: '#FFFFFF',
                align: 'center'
            };
            this.overmessage = this.add.text(this.game.width / 2, this.game.height / 2, '', style);
            this.overmessage.anchor.set(0.5);
            this.overmessage.fixedToCamera = true;

        },

        update: function () {
            
            if(!this.endgame){
                this.updateCamera();
                
                this.updateHero();

                this.updatePlatforms();

                this.updateInterface();

                this.updateDifficulty();
            }


        },

        updateCamera: function () {
            // the y offset and the height of the world are adjusted
            // to match the highest point the hero has reached
            this.world.setBounds(0, -this.hero.yChange, this.world.width, this.game.height + this.hero.yChange);

            // custom camera will only move up
            this.camera.y = -this.hero.yChange;
        },

        overlapHandler: function () {
            this.hero.kill();
            this.onGameOver();
        },

        updateHero: function () {
            // track the maximum amount that the hero has travelled
            this.hero.yChange = Math.max(this.hero.yChange, Math.floor(this.hero.yOrig - this.hero.y));
           
            // if the hero falls below the camera view, gameover
            if (this.hero.y > this.camera.y + this.game.height && this.hero.alive) {
                this.onGameOver();

            }

            if (this.camera.y > -300) {
                this.physics.arcade.collide(this.hero, this.ground);
              
            }

            if (this.drawLine) {
                this.physics.arcade.overlap(this.hero, this.activeStar, this.overlapHandler, null, this);

                this.hero.animations.play('grab');


                var dist = this.physics.arcade.distanceBetween(this.hero, this.activeStar);


                if (dist < this.pulldistance) {
                    this.start = false;
                    var angle = this.physics.arcade.angleBetween(this.hero, this.activeStar);
                    var herospeed = this.lerp(50, this.moveSpeed, 1 - (dist / this.pulldistance));
                    this.physics.arcade.velocityFromRotation(angle, herospeed, this.hero.body.velocity);
                }
                
                    
               
                if(dist<300){
                   this.cracks.forEach(function (elem) {
                        if(elem.id == this.activeStar.id){
                            elem.opacity = 1;
                            elem.play('cracking');
                        }
                    }, this)
                   /* this.cracks.animations.add('cracking', [0, 1, 2, 3], 4, false);
                    this.cracks.x = this.activeStar.x - 86;
                    this.cracks.y = this.activeStar.y - 86;
                    this.cracks.animations.play('cracking');
                */}
               

                // Move star towards player
                //                    var speed = this.lerp(0, 300, 1 - (this.game.height - dist) / this.game.height);
                //                    this.physics.arcade.moveToObject(this.activeStar, this.hero, speed);

            } else {
                
               /* this.cracks.x = -200
                this.cracks.frame = 0;
                */
                if (this.hero.body.velocity.y > 0 && this.hero.y < this.world.height - 150) {
                    this.hero.animations.play('fall');
                } else {
                    this.hero.animations.stop();
                    
                    if (this.start) {
                        this.hero.animations.play('start');
                        this.hero.frame = 0;

                    } else {
                       
                        this.hero.frame = 6;
                        if(this.hero.body.velocity.y<0 && this.hero.body.velocity.y>-1){
                            this.hero.frame = 0;
                        }
                        
                    }

                }
            }
            this.updateLine();
        },

        updateLine: function () {
            if (this.drawLine) {
                this.linesprite.width = this.physics.arcade.distanceBetween(this.hero, this.activeStar);
                this.linesprite.x = this.hero.x;
                this.linesprite.y = this.hero.y;
                var rot = Math.atan2(this.hero.y - this.activeStar.y, this.hero.x - this.activeStar.x);
                this.linesprite.rotation = rot - Math.PI;


            } else {
                this.linesprite.width = 0;
            }
        },

        updateDifficulty: function () {
            if (this.hero.yChange > 2500) {
                if (this.hero.yChange > 10000) {
                    this.moveSpeed = 600;
                    this.addStarDistance = 250;
                } else if (this.hero.yChange > 7500) {
                    this.moveSpeed = 550;
                    this.addStarDistance = 200;
                } else if (this.hero.yChange > 5000) {
                    this.moveSpeed = 500;
                    this.addStarDistance = 150;
                } else {
                    this.moveSpeed = 450;
                    this.addStarDistance = 100;
                }
            }
        },

        updatePlatforms: function () {
            // for each plat form, find out which is the highest
            // if one goes below the camera view, then create a new one at a distance from the highest one
            // these are pooled so they are very performant
            this.platforms.forEach(function (elem) {
                // this.game.debug.body(elem);
                this.platformYMin = Math.min(this.platformYMin, elem.y);
                if (elem.y > this.camera.y + this.game.height) {
                    this.resetPlatform(elem);
                }
            }, this);


        },

        updateInterface: function () {
            if (this.hero.yChange > 4750) {
                this.endgame = true;
                this.overmessage.setText('Congrats!\n\n You reached the top');
               this.hero.kill();
            }
            
            this.scoreText.x = 70 + this.scoreText.width;

            this.scoreText.text = Math.floor(this.hero.yChange/15);
        },

        clickStar: function (star) {
            this.activeStar = star;
            this.drawLine = true;
        },

        releaseStar: function () {
            if(this.activeStar != undefined){
                this.activeStar.body.velocity.setTo(0, 0);
                this.drawLine = false;
            }
        }, 

        randomX: function () {
            return this.rnd.integerInRange(25, this.world.width - 50);
        },

        lerp: function (v0, v1, t) {
            return (1 - t) * v0 + t * v1;
        },

        resetPlatform: function (platform) {
            var y = this.platformYMin - this.addStarDistance;
            var x = this.randomX();

            if( y > -4450){
                platform.reset(x, y);
                this.cracks.forEach(function (elem) {
                if(elem.id == platform.id){
                    elem.reset(x-86,y-86);
                    elem.frame = 0;
                }
            })
            }
            
        },

        reset: function () {
            // reset everything, or the world will be messed up
            this.world.setBounds(0, 0, this.game.width, this.game.height);

            this.camera.y = 0;
            this.platformYMin = 99999;
            this.moveSpeed = 400;
            this.addStarDistance = 50;

            var i = 0;
            this.platforms.forEach(function (platform) {
                var randx = Math.floor(Math.random() * (770 - 350 + 1) + 350);
                if (i === 0) {
                    randx = this.world.width / 2;
                }

                var randy = this.world.height - 500 - this.addStarDistance * i;
                if (randy > -31 && randy < 31) {
                    randy -= 60;
                }
                platform.x = randx;
                platform.y = randy;
                i++;
            }, this);

            this.start = true;
            this.hero.frame = 0;
            this.hero.reset(this.world.centerX, this.world.height - 115);
            this.hero.body.velocity.x = 0;
            this.hero.body.velocity.y = 0;
            this.hero.yChange = 0;
            this.hero.revive();

            this.drawLine = false;

            this.paused = false;
        },

        onGameOver: function () {
            if (this.paused === false) {
                this.paused = true;

              
                this.endgame = true;
                
                var count = Math.floor(this.hero.yChange/15)
                var grade = "none";
                if (count > gold) {
                    grade = "gold";
                } else if (count > silver) {
                    grade = "silver";
                } else if (count > bronze) {
                    grade = "bronze";
                }

                this.hero.alpha = 0;

                this.drawLine = false;
                gameOver(grade,Math.floor(this.hero.yChange/15),gameToken);
            }
        }
    };

    window['grabgame'] = window['grabgame'] || {};
    window['grabgame'].Game = Game;

})();