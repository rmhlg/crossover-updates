(function () {
    'use strict';

    function Preloader() {
        this.ready = false;
    }

    Preloader.prototype = {

        preload: function () {
            this.load.onLoadComplete.addOnce(this.onLoadComplete, this);
            this.loadResources();

            $(".loader").addClass("visible");
        },

        loadResources: function () {

            this.load.spritesheet('character', gamesURL+'img/games/zipline/character.png', 480, 400, 17);
            this.load.spritesheet('point', gamesURL+'img/games/zipline/points_spritesheet.png', 100, 142);
            this.load.image('sky', gamesURL+'img/games/zipline/Sky.jpg');
            this.load.image('cloud', gamesURL+'img/games/zipline/Cloud.png');

            this.load.spritesheet('rope', gamesURL+'img/games/zipline/rope.png', 10, 654);
            this.load.image('boom1', gamesURL+'img/games/zipline/boom1.png');
            this.load.image('boom2', gamesURL+'img/games/zipline/boom2.png');
            this.load.image('arrival', gamesURL+'img/games/zipline/arrival.png');

            this.load.image('landscape1', gamesURL+'img/games/zipline/1_landscape.png');
            this.load.image('landscape3', gamesURL+'img/games/zipline/3_landscape.png');
            this.load.image('landscape5', gamesURL+'img/games/zipline/5_landscape.png');

            this.load.image('scorebg', gamesURL+'img/games/zipline/pointsback.png');

            this.load.image('letsgo', gamesURL+'img/games/CTA.png');
        },

        create: function () {},

        update: function () {
            if (!!this.ready) {
                this.game.state.start('GameTitle');
            }
        },

        onLoadComplete: function () {
            this.ready = true;

            $(".loader").removeClass("visible");
        }
    };

    window['zipline'] = window['zipline'] || {};
    window['zipline'].Preloader = Preloader;

}());