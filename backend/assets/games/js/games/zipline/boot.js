(function () {
    'use strict';

    function Boot() {}

    Boot.prototype = {
        create: function () {
            this.game.input.maxPointers = 1;
            this.time.desiredFps = 30;
            
            if (this.game.device.desktop) {
                this.game.scale.pageAlignHorizontally = true;
            }

            this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
            this.scale.minWidth = 480;
            this.scale.minHeight = 260;
            this.scale.maxWidth = 1024;
            this.scale.maxHeight = 540;

            this.game.scale.forceOrientation(false);
            this.game.scale.compatibility.scrollTo = false;
            this.scale.pageAlignHorizontally = true;

            this.game.state.start('preloader');
        }
    };

    window['zipline'] = window['zipline'] || {};
    window['zipline'].Boot = Boot;

}());