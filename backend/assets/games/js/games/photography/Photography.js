function Photography() {
							
	this.refSpeed = gParams.speed;
	
	this.ended = false;
	this.score = 0;
	this.maxObjectValue = 0;
	
	// Set max object value //
	for (var v in gParams.objects) {
		this.maxObjectValue = Math.max(this.maxObjectValue, gParams.objects[v].value);		
	}
	
	// Set Pictures properties //
	this.pictures = new Array();
	this.picturesBG = new Array();
	this.max_pictures = gParams.nb_pictures;
	this.thumbMargin = 10;
	//this.thumbWidth = (gScene.width-this.thumbMargin-(this.thumbMargin*this.max_pictures))/this.max_pictures;
	//this.thumbHeight = this.thumbWidth*(280/400);
	this.thumbWidth = 86;
	this.thumbHeight = 56;
	
	// Set Backgrounds //
	this.backgrounds = new Array();		
	for (var b=0;b<gParams.backgrounds.length;b++) {
		var tBG = gParams.backgrounds[b];
		this.backgrounds.push(new Background(tBG));
	}
		
	// Create objects manager //
	this.myObjectsManager = new ObjectsManager(gParams.objects);
	
	// Create Camera //
	this.camera = mySpritesManager.addSprite('viewfinder', document.getElementById('viewfinder'), gScene.width/2, gScene.height/2, gParams.camera.width, gParams.camera.height, 70);
	
	// Create texts manager //
	this.myTextsManager = new TextsManager();
	
	// Create score //	
	this.scoreRect = mySpritesManager.addSprite('scorerect', document.getElementById('scorerect'), 50, 60, 199, 66, 70);
	this.scoreTxt = this.myTextsManager.addText(80, 78, '0', 'text-align: right; font-size: 46px; font-family: bebas-neue; color:#000000;');
		
	// Create FPS //
	if (gParams.showFPS==true) {
		this.fps = this.myTextsManager.addText(gScene.width-10, gScene.height-10, 'fps', 'text-align: right; font-size: 46px; font-family: bebas-neue; color:#000000; text-stroke:3px #ffffff;');
	}
	
	// Create temporary canvas to capture image //
	this.tempCanvas = document.createElement("canvas");
	this.tempCanvas.width = this.camera.width;
	this.tempCanvas.height = this.camera.height;
			
	// Manage click event //	
	gScene.removeEventListener('click', gRestartFunction);
	gScene.addEventListener('click', gGameMouseupFunction);
	
	gRegisteredForAnimation.push(this);
				
	this.destroy = destroy;
	function destroy() {
		
		gRegisteredForAnimation.splice(indexOfObj(gRegisteredForAnimation, this), 1);
		
		this.myObjectsManager.destroy();
		this.myObjectsManager = null;
		this.myTextsManager.destroy();
		this.myTextsManager = null;
		
		mySpritesManager.removeSprite(this.scoreRect.layer);
		this.scoreRect = null;
		this.scoreTxt = null;
				
		this.fps = null;		
		
		for (var b in this.backgrounds) {
			if(typeof this.backgrounds[b].destroy == "function"){
				this.backgrounds[b].destroy();
				this.backgrounds[b] = null;
			}
		}
		this.backgrounds = null;
		
		for (var p in this.pictures) {
			if(document.getElementById('pict_'+p)){
				document.getElementById('assets').removeChild(document.getElementById('pict_'+p));
			}
			mySpritesManager.removeSprite(this.pictures[p].layer);
			mySpritesManager.removeSprite(this.picturesBG[p].layer);
			this.pictures[p] = null;
			this.picturesBG[p] = null;
		}
		this.pictures = null;
		
		mySpritesManager.removeSprite(this.camera.layer);	
		this.camera = null;		
		
	}
				
	this.animate = animate;
	function animate() {
				
		// Move camera //
		if (gMousePos.left===undefined) {
			gMousePos = {'left':gScene.width/2, 'top':gScene.height/2}
		}
		this.camera.x = Math.max(this.camera.width/2, Math.min(gMousePos.left, gScene.width-(this.camera.width/2)));
		this.camera.y = Math.max(this.camera.height/2, Math.min(gMousePos.top, gScene.height-(this.camera.height/2)));
				
	}
			
	this.mouseUp = mouseUp;
	function mouseUp(e) {
						
		if (this.ended==false) {
		
			gPause = true;
			
			// Update camera pos //
			updateMousePos(e);
			this.animate();
		
			// Check captured layers for score //
			var tCameraRect = this.camera.getRect();		
			/*
			var d = new Date();
			var n = d.getMilliseconds();
			*/	
				
			var tIntersect = Math.round(this.myObjectsManager.checkIntersect(tCameraRect));		
			if (tIntersect>0) {
				//alert(tIntersect+' '+tObjectRect);
				this.score += tIntersect;
				this.scoreTxt.DOM.innerHTML = ''+this.score;						
				this.myTextsManager.addText(gMousePos.left, gMousePos.top, '+'+tIntersect, 'font-size: 46px; font-family: bebas-neue; color:#000000;', 2000, -2);	

				if (tIntersect>this.maxObjectValue) {
					this.myTextsManager.addText(gMousePos.left-50, gMousePos.top-50, 'Nice shot !', 'font-size: 46px; font-family: bebas-neue; color:#000000;', 2000, -2);	
				}
				
			} else {
				this.myTextsManager.addText(gMousePos.left, gMousePos.top, gParams.miss, 'font-size: 46px; font-family: bebas-neue; color:#ff0000;', 2000, -2);	
			}
			/*
			var d2 = new Date();
			var n2 = d2.getMilliseconds();
			*/
			//alert(n2-n);
			
			
			// Hide camera & pictures & texts //
			this.camera.opacity=0.0;
			for (var p in this.pictures) {
				this.pictures[p].opacity = 0.0;
			}
			this.myTextsManager.hide();
			
			mySpritesManager.draw();
			
			// Capture image on temp canvas //
			var tCtx = this.tempCanvas.getContext("2d");
			tCtx.drawImage(gScene, -(this.camera.x-(this.camera.width/2)), -(this.camera.y-(this.camera.height/2)), gScene.width, gScene.height);				
			var tImg = this.tempCanvas.toDataURL("image/png");
						
			// Show camera & pictures //
			this.camera.opacity=1.0;		
			for (var p in this.pictures) {
				this.pictures[p].opacity = 1.0;
			}
			this.myTextsManager.show();
			
			mySpritesManager.draw();
						
			// Add captured image to assets //
			var tImgDom = document.createElement('img');
			tImgDom.src = tImg;
			tImgDom.id = 'pict_'+this.pictures.length;
			tImgDom.width = this.thumbWidth;
			tImgDom.height = this.thumbHeight;
			tImgDom.onload = function() { myGame.addLastPicture(); }
			document.getElementById('assets').appendChild(tImgDom);
										
			// Do not try to catch missing frames => avoid jumping forward //
			var d = new Date();
			var n = d.getTime();
			//console.log((n-gTime)+' '+(gFrameTime));
			gTime = n-gFrameTime;
			
			gPause = false;
			
		}
	}
	
	this.addLastPicture = addLastPicture;
	function addLastPicture() {
	
		// Add thumb to scene //
		//var tPosX = gScene.width-(this.thumbWidth/2)-(this.pictures.length*(this.thumbWidth+this.thumbMargin))-this.thumbMargin;
		//var tPosY = /*gScene.height-*/((this.thumbHeight/2)+this.thumbMargin);	
		
		var tPosX = 175-((this.pictures.length/gParams.nb_pictures)*25);
		var tPosY = 75-((this.pictures.length/gParams.nb_pictures)*15);
		var tRotation = 10-(Math.random()*20);
		var tSp;	
		tSp = mySpritesManager.addSprite('picture-bg_'+this.pictures.length, document.getElementById('picture-bg'), tPosX, tPosY, 98, 68, 80, 0, 1, false, tRotation);
		this.picturesBG.push(tSp);
		tSp = mySpritesManager.addSprite('thumb_'+this.pictures.length, document.getElementById('pict_'+this.pictures.length), tPosX, tPosY, this.thumbWidth, this.thumbHeight, 80, 0, 1, false, tRotation);
		//tSp.border = true;
		this.pictures.push(tSp);
		
		if (this.pictures.length>=this.max_pictures) {				
			// End of game //
			endGame();
		}
	}
	
}