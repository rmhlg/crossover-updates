(function () {
  var boot = function () {
    console.log('%c Booting game ', 'color: white; background:red;');
  };

  boot.prototype = {
    preload: function () {

    },
    create: function () {
      if (this.game.device.desktop) {
        this.game.scale.pageAlignHorizontally = true;
      } else {

        this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        this.game.scale.compatibility.scrollTo = false;

        this.game.scale.forceOrientation(false, false);
      }

      this.game.state.start('Preload');
    }
  };

  window['landsail'] = window['landsail'] || {};
  window['landsail'].boot = boot;

})();