(function () {
    'use strict';

    var GameTitle = function () {
        this.startPrompt = null;
        this.ding = null;
    };

    GameTitle.prototype = {

        create: function () {

            this.camera.y = 0;
            this.camera.x = 0;
            this.world.setBounds(0, 0, this.game.width, this.game.height);
            var startBG = this.add.image(360, 220, 'letsgo');
            startBG.inputEnabled = true;
            startBG.input.useHandCursor = true;
            startBG.events.onInputDown.add(this.startGame, this);

            this.stage.backgroundColor = '#000000';

        },

        startGame: function () {
            console.log("CLICK");
            this.state.start('game', true, false);
        }
    };

    window['snowscooter'] = window['snowscooter'] || {};
    window['snowscooter'].GameTitle = GameTitle;
})();