//Function called the first time the user click on Play
var game;
var initGame = function(){

    //Codes from the game to init it
    var ns = window['snowscooter'];

    game = new Phaser.Game(1024, 540, Phaser.AUTO, 'snowscooter-game');
    game.state.add('boot', ns.Boot);
    game.state.add('preloader', ns.Preloader);
    game.state.add('GameTitle', ns.GameTitle);
    game.state.add('game', ns.Game);

    game.state.start('boot');

    //Reset the game so it is visible
    resetGame();
    
};

//Function called to reset the game
var resetGame = function(){

    //If we are on a mobile
    if(isMobile){
        $(".canvasGame").show();//Show the canvas
    }
    else{
        $(".teaserGame").removeClass("showOutro howToOn").addClass("gameOn");
    }

};


//Function called whenever the player lose the game
var gameOver = function(grade, score){

    //Change the copy on the outro pannel
    //Three var needs to be passed
    //grade = "gold", "silver", "bronze" or "none"
    //score = score of the player convert in correct unit
    pages.activities.showScores(grade, score);

    $(".teaserGame").removeClass("gameOn").addClass("showOutro");

    if(isMobile){
        $(".canvasGame").hide();//Hide the canvas
        $(window).scrollTop($(".teaserGame").offset().top);//Scroll to the outro
    }

    game.state.start('GameTitle');
};