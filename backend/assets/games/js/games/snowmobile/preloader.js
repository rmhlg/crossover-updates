(function () {
    'use strict';

    function Preloader() {
        this.ready = false;
    }

    Preloader.prototype = {

        preload: function () {
            this.load.onLoadComplete.addOnce(this.onLoadComplete, this);
            this.loadResources();

            $(".loader").addClass("visible");
        },

        loadResources: function () {

            // Load backgrounds
            this.load.image('sky', 'img/games/snowmobile/pattern_sky.png');
            this.load.image('mountains', 'img/games/snowmobile/mountain.png');
            this.load.image('snowBackground', 'img/games/snowmobile/snow.png');

            // Load snowmobile
            this.load.image('player', 'img/games/snowmobile/snowmobile/snowmobile_body.png');
            this.load.spritesheet('pilot', 'img/games/snowmobile/snowmobile/snowmobile_pilot.png', 59, 148);
            this.load.image('front', 'img/games/snowmobile/snowmobile/snowmobile_front.png');
            this.load.spritesheet('back', 'img/games/snowmobile/snowmobile/snowmobile_back.png', 119, 42);

            // Load particles
            this.load.spritesheet('snow', 'img/games/snowmobile/asset_snow.png', 258, 175);

            // Load extras
            this.load.spritesheet('bonus', 'img/games/snowmobile/points_spritesheet.png', 62, 88);
            this.load.image('tree1', 'img/games/snowmobile/tree1.png');
            this.load.image('tree3', 'img/games/snowmobile/tree3.png');
            this.load.spritesheet('flag', 'img/games/snowmobile/flag.png', 188, 452);

            // Load terrain
            this.load.tilemap('map', 'img/games/snowmobile/map.txt', null, Phaser.Tilemap.TILED_JSON);
            // this.load.spritesheet('tiles', 'img/games/snowmobile/tiles.png', 950, 405, 20);

            this.load.image('terrain0', 'img/games/snowmobile/terrain/1_bg.png');
            this.load.image('terrain1', 'img/games/snowmobile/terrain/2_bg.png');
            this.load.image('terrain2', 'img/games/snowmobile/terrain/3_bg.png');
            this.load.image('terrain3', 'img/games/snowmobile/terrain/4_bg.png');
            this.load.image('terrain4', 'img/games/snowmobile/terrain/5_bg.png');
            this.load.image('terrain5', 'img/games/snowmobile/terrain/6_bg.png');
            this.load.image('terrain6', 'img/games/snowmobile/terrain/7_bg.png');
            this.load.image('terrain7', 'img/games/snowmobile/terrain/8_bg.png');
            this.load.image('terrain8', 'img/games/snowmobile/terrain/9_bg.png');
            this.load.image('terrain9', 'img/games/snowmobile/terrain/10_bg.png');
            this.load.image('terrain10', 'img/games/snowmobile/terrain/11_bg.png');
            this.load.image('terrain11', 'img/games/snowmobile/terrain/12_bg.png');
            this.load.image('terrain12', 'img/games/snowmobile/terrain/13_bg.png');
            this.load.image('terrain13', 'img/games/snowmobile/terrain/14_bg.png');
            this.load.image('terrain14', 'img/games/snowmobile/terrain/15_bg.png');
            this.load.image('terrain15', 'img/games/snowmobile/terrain/16_bg.png');
            this.load.image('terrain16', 'img/games/snowmobile/terrain/17_bg.png');
            this.load.image('terrain17', 'img/games/snowmobile/terrain/18_bg.png');
            this.load.image('terrain18', 'img/games/snowmobile/terrain/19_bg.png');
            this.load.image('terrain19', 'img/games/snowmobile/terrain/20_bg.png');

            this.load.image('scorebg', 'img/games/snowmobile/pointsback.png');

            this.load.image('letsgo', 'img/games/CTA.png');
        },

        create: function () {},

        update: function () {
            if (!!this.ready) {
                this.game.state.start('GameTitle');
            }
        },

        onLoadComplete: function () {
            this.ready = true;

            $(".loader").removeClass("visible");
        }
    };

    window['snowscooter'] = window['snowscooter'] || {};
    window['snowscooter'].Preloader = Preloader;

}());