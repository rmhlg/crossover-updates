(function () {
  'use strict';

  var game = function () {
    //    this.paddling;
    //    this.rockingTween;
    //    this.swayingTween;
    //    this.clouds;
    //    this.sky;
    //    this.distance;
    //    this.rockGroup;
    //    this.loopTimer;
    //    this.debug;

    this.currentSpeed = 0;
    this.maxSpeed = 8;
    this.roadWidth = 1024;
    this.carFrameCount = 7;
    this.sailFrameCount = 21;
    this.secondsPerLevel = 20;
    this.ticksPerSecond = 4;
    this.timePlayed = 0;
    this.ticks = 0;
    this.level = 0;
    this.rocktimers = [3, 2, 1.5, 1, 0.5, 0.25];
    this.rocksOnScreen = [];
    this.rocksPassed = 0;
    this.rocks = [];
    this.pointerLeft = false;
    this.pointerRight = false;
    this.roadSpeed = 25;
    this.rockXCoords = []
    this.rockback = null;
    this.plusscore = null;
    this.distance = null;
  };

  game.prototype = {
    create: function () {
      if(this.rockGroup != null){
        this.rockGroup.destroy();
      }

      this.clouds = null;
      this.trees = null;
      this.gameover = false;
      this.plusscore = null;
      this.rockGroup = null;
      this.roadSpeed = 25;
      this.currentSpeed = 0;
      this.maxSpeed = 8;
      this.distance = 0;

      this.rocks = [];
      this.ticks = 0;
      this.level = 0;

      this.debug = this.getQueryVariable('debug');

      this.initStage();
      this.createControls();
      this.createSky();
      this.createRoad();
      this.createRocks();
      //this.createEmitters();
      this.createPoints();
      this.createSail();

      this.rocktimer = this.rocktimers[this.level];
      this.loopTimer = this.game.time.events.loop(Phaser.Timer.SECOND / this.ticksPerSecond, this.timer, this);

      this.createUI();
    },
    initStage: function () {
      this.game.physics.startSystem(Phaser.Physics.ARCADE);
      //this.game.stage.backgroundColor = '#c6e5f1';
    },
    createControls: function () {
      if (this.game.device.desktop) {
        this.cursors = this.game.input.keyboard.createCursorKeys();
      } else {
        this.game.input.onDown.add(this.handlePointerDown, this);
        this.game.input.onUp.add(this.handlePointerUp, this);
      }
    },
    createSky: function () {
      this.sky = this.game.add.sprite(this.game.world.centerX, 133, 'sky');
      this.sky.anchor.set(0.5, 0.8);
      this.sky.scale.set(0.5, 0.5);

      this.game.add.tween(this.sky.scale).to({
          x: 1,
          y: 1
      }, 200000, Phaser.Easing.Linear.None, true, 0, 0, false);

      var trees = this.game.add.sprite(this.game.world.centerX-75, 170, 'trees');
      trees.anchor.set(0.5, 1);
      trees.scale.set(0.6, 0.6);

      this.game.add.tween(trees.scale).to({
          x: 1,
          y: 1
      }, 150000, Phaser.Easing.Linear.None, true, 0, 0, false);

      this.clouds = this.game.add.tileSprite(0, 0, 1024, 200, 'cloud');
    },
    createRoad: function () {
      this.road = this.game.add.sprite(0, 153, 'road');
      if(this.game.isWinTablet || this.game.isIOS7) {
        this.road.scale.set(1.5, 1.5);
      }
      
      this.roadSegmentWidth = Math.round(this.roadWidth / this.sailFrameCount);
      this.roadAnim = this.road.animations.add('moving', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17], this.roadSpeed, true);
      this.road.play('moving');
      
      
      this.rockback = this.game.add.sprite(0, -5, 'rocks');
      if(this.game.isWinTablet || this.game.isIOS7) {
        this.rockback.scale.set(2, 2);
      }
      this.rockback.animations.add('move', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34], this.roadSpeed, true);
      this.rockback.play('move');
    },
    createSail: function () {
      this.sail = this.game.add.sprite(this.game.world.width / 2, 350, 'sail');
      this.sail.anchor.set(0.5);
      this.game.physics.enable(this.sail, Phaser.Physics.ARCADE);
      this.sail.body.setSize(180, 30, 0, 50);

      this.sail.body.collideWorldBounds = true;
      this.sail.body.bounce.set(1);
      this.sail.body.immovable = true;

      this.zPoint = (this.game.world.width / 2) - (this.roadWidth / 2);
      this.sail.angle = -6;

      this.rockingTween = this.game.add.tween(this.sail).to({
        y: 375
      }, 750, Phaser.Easing.Linear.EaseOut, true, 0, -1, true);
      this.swayingTween = this.game.add.tween(this.sail).to({
        angle: 6
      }, 1250, Phaser.Easing.Linear.EaseOut, true, 0, -1, true);

      this.paddling = this.sail.animations.add('paddling', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15], 20, true);
      this.sail.play('paddling');
    },
    createUI: function () {
      this.plusscore = this.game.add.text(this.game.world.centerX - 20, 100, '+10', {
        font: '40px bebas-neue',
        fill: '#ffffff',
        align: 'center'
      });
      this.plusscore.alpha = 0;

      this.add.sprite(-80, 13, 'scorebg');

      this.distance = this.add.text(72, 50, '0', {
        font: '48px bebas-neue',
        fill: '#000000',
        align: 'right'
      });
      this.distance.anchor.setTo(1, 0.5);

      this.add.text(78, 45, 'm', {
        font: '24px bebas-neue',
        fill: '#000000',
        align: 'center'
      });
     },
    timer: function () {
      this.ticks++;
      if (this.ticks % this.ticksPerSecond === 0) {
        this.timePlayed++;
      }

      if (this.distance) {
        this.distance.setText(Math.floor(this.ticks / this.ticksPerSecond));
      }

      if (this.ticks % (this.secondsPerLevel * this.ticksPerSecond) === 0) {
        this.level++;
        if(this.level > 5){
          this.level = 5;
        }
        if (this.level == this.rocktimers.length-1) {
          this.rocktimer = this.rocktimers[this.rocktimers.length - 1];
          this.level = 5;
        } else {
          this.rocktimer = this.rocktimers[this.level];
        }
      }
    },
    createPoints: function () {
      this.pointsGroup = this.game.add.group();

      for (var i = 0; i < 40; ++i) {
          var point = this.pointsGroup.create(-2000, -2000, 'points');
          point.animations.add('turn');
          point.animations.play('turn', 15, true);
          this.game.physics.enable(point, Phaser.Physics.ARCADE);
          point.body.setSize(100, 200, 0, 40);
          point.kill();
      }

      this.spawnPoint();
    },
    createRocks: function () {
      this.rocktimer = this.rocktimers[this.level];

      this.rocks[0] = 'rocksheet1';
      this.rocks[1] = 'rocksheet2';
      this.rocks[2] = 'rocksheet3';
      this.rocks[3] = 'rocksheet4';

      this.rockXCoords[0] = this.game.world.width / 2 + 200;
      this.rockXCoords[1] = 1600;
      this.rockXCoords[2] = -500;
      this.rockXCoords[3] = this.game.world.width / 2 - 200;
      this.rockXCoords[4] = 1200;
      this.rockXCoords[5] = 0;
      this.rockXCoords[6] = 650;
      this.rockXCoords[7] = 955;
      this.rockXCoords[8] = 300;
      this.rockXCoords[9] = this.game.world.width / 2;
      this.rockXCoords[10] = -500;
      this.rockXCoords[11] = 1600;
      this.rockXCoords[12] = 450;

      this.rockGroup = this.game.add.group();

      this.game.time.events.add(Phaser.Timer.SECOND * this.rocktimer, this.spawnRock, this).autoDestroy = true;
    },
    spawnRock: function () {
      if (!this.gameover) {
        var rndTimer = 500 + Math.floor(Math.random() * this.rocktimer) * Phaser.Timer.SECOND;
        this.game.time.events.add(rndTimer, this.spawnRock, this).autoDestroy = true;

        var rnd = Math.floor(Math.random() * (this.rocks.length));
        var rndPos = Math.floor(Math.random() * 4096) - 2048;

        var rock = this.rockGroup.getFirstDead();
        if (rock === null) {
          rock = this.rockGroup.create(-2000, -2000, this.rocks[rnd]);
          this.game.physics.enable(rock, Phaser.Physics.ARCADE);
          rock.body.setSize(200, 45, 0,-50);
        }
        rock.revive();

        //rock.x = this.game.world.centerX -25 + (Math.random()*25);
        rock.x = this.game.world.centerX + (rndPos / 40);
        rock.y = 155;

        rock.anchor.setTo(0.5);
        rock.scale.set(0.02, 0.02);

        rock.animations.add('ripple', [0, 1, 2, 3, 4, 5], 8, true);
        rock.play('ripple');

        var rockScaleTween = this.game.add.tween(rock.scale);
        rockScaleTween.to({
          x: 3,
          y: 3
        }, 16000, this.ultratween, true, 0, 0);
        var rockYTween = this.game.add.tween(rock);
        rockYTween.to({
          //x:this.rockXCoords[rnd],
          x: this.game.world.centerX + rndPos,
          y: 1500
        }, 16000, this.ultratween, true, 0, 0);

        rockScaleTween.onComplete.add(function () {
          this.destroyRock(rock);
        }, this);

        this.rocksOnScreen.push(rock);

        this.rockGroup.sort('y', Phaser.Group.SORT_ASCENDING);

        this.game.world.bringToTop(this.sail);
      }
    },
    destroyRock: function (rock) {
      this.rocksOnScreen.splice(0, 1);
      rock.kill();
    },
    update: function () {
      this.clouds.tilePosition.x -= 0.1;

      if (!this.gameover) {
        if (this.game.device.desktop) {
          if (this.cursors.left.isDown) {
            this.currentSpeed = -this.maxSpeed;
          } else if (this.cursors.right.isDown) {
            this.currentSpeed = this.maxSpeed;
          } else {
            this.currentSpeed = this.currentSpeed / 1.05;
          }
        }

        if (this.pointerLeft) {
          this.currentSpeed = -this.maxSpeed;
        } else if (this.pointerRight) {
          this.currentSpeed = this.maxSpeed;
        } else {
          this.currentSpeed = this.currentSpeed / 1.05;
        }

        var rX;
        var frame;

        this.sail.x += this.currentSpeed;
        rX = this.sail.x - this.zPoint;
        frame = Math.floor(rX / this.roadSegmentWidth);

        for (var i = 0; i < this.rocksOnScreen.length; i++) {
          this.game.physics.arcade.overlap(this.sail, this.rocksOnScreen[i], this.overlapHandler, null, this);
        }

        this.pointsGroup.forEachAlive(function (point) {
          this.game.physics.arcade.overlap(this.sail, point, this.pointOverlapHandler, null, this);
        }, this);
      }
    },
    spawnPoint: function () {
      if (!this.gameover) {
        var rndTimer = 250 + Math.floor(Math.random() * 15) * Phaser.Timer.SECOND;
        this.game.time.events.add(rndTimer, this.spawnPoint, this).autoDestroy = true;

        var rndDoublePoint = Math.floor(Math.random() * 3);

        var rndPos = Math.floor(Math.random() * 4096) - 2048;

        var point = this.pointsGroup.getFirstDead();
        if (point) {

          //var rnd = Math.floor(Math.random() * 12);

          //var px = this.game.world.centerX + (((this.rockXCoords[rnd] / 2) - 700) / 110) ;
          point.x = this.game.world.centerX + (rndPos / 40);;
          point.alpha = 1;
          point.y = 150;

          point.anchor.setTo(0.5);
          point.scale.set(0.01, 0.01);

          var pointScaleTween = this.game.add.tween(point.scale);
          pointScaleTween.to({
            x: 3,
            y: 3
          }, 16000, this.ultratween, true, 0, 0);
          var pointYTween = this.game.add.tween(point);

          pointYTween.to({
            x: this.game.world.centerX + rndPos,
            y: 1500
          }, 16000, this.ultratween, true, 0, 0);

          pointScaleTween.onComplete.add(function () {
            if(point.alive) {
              point.kill();
            }
          }, this);
          point.revive();
        }

        if(rndDoublePoint === 0) {
          //console.log("spawn second point");
          var point2 = this.pointsGroup.getFirstDead();
          if (point2) {

            //var rnd = Math.floor(Math.random() * 12);

            if(rndPos < 0) {
              rndPos += 3000;
            } else {
              rndPos -= 3000;
            }

            //var px = this.game.world.centerX + (((this.rockXCoords[rnd] / 2) - 700) / 110) ;
            point2.x = this.game.world.centerX + (rndPos / 40);;
            point2.alpha = 1;
            point2.y = 150;

            point2.anchor.setTo(0.5);
            point2.scale.set(0.01, 0.01);

            var point2ScaleTween = this.game.add.tween(point2.scale);
            point2ScaleTween.to({
              x: 3,
              y: 3
            }, 16000, this.ultratween, true, 0, 0);
            var point2YTween = this.game.add.tween(point2);

            point2YTween.to({
              x: this.game.world.centerX + rndPos,
              y: 1500
            }, 16000, this.ultratween, true, 0, 0);

            point2ScaleTween.onComplete.add(function () {
              if(point2.alive) {
                point2.kill();
              }
            }, this);
            point2.revive();
          }
        }

        /*var legame = this.game;
        var ctr = 0;
        this.pointsGroup.forEachAlive(function (point) {
          ctr++;
        });

        console.log("points alive: " + ctr);*/
      }
    },
    pointOverlapHandler: function (sail, point) {
      point.kill();

      this.plusscore.alpha = 1;
      this.plusscore.y = 300;
      var plustween = this.game.add.tween(this.plusscore);
      plustween.to({
        y: 250,
        alpha: 0
      }, 2000, Phaser.Easing.Circular.Out, true, 0, 0);

      this.ticks += (10 * this.ticksPerSecond); // ticks are being divided by 4 for the score
    },
    overlapHandler: function () {
      this.game.time.events.remove(this.loopTimer);
      this.gameover = true;
      this.road.animations.stop(null, true);

      this.game.tweens.pauseAll();
      this.rockback.animations.stop();
      this.sail.animations.stop();
          //this.crashTween.start();
          
      for (var i = 0; i < this.rocksOnScreen.length; i++) {
        this.rocksOnScreen[i].animations.stop();
      }
      this.setMedalScore();
     },
     setMedalScore: function(){

      var text = 'No medal';
      var grade = "none";
      if (this.ticks > (gold * this.ticksPerSecond)) {
        text = 'Gold medal';
          grade= "gold";
      } else if (this.ticks > (silver * this.ticksPerSecond)) {
        text = 'Silver medal';
          grade = "silver";
      } else if (this.ticks > (bronze * this.ticksPerSecond)) {
        text = 'Bronze medal';
          grade="bronze";
      }

      var endScore = this.game.add.text(this.game.world.centerX, 50, text, {
        font: '40px bebas-neue',
        fill: '#FFF',
        align: 'center'
      });
      endScore.anchor.set(0.5);
         
        gameOver(grade,Math.floor(this.ticks / this.ticksPerSecond), "m" );
         
    },
    render: function () {
      if (this.debug === '1') {
        this.game.debug.body(this.sail);
        for (var i = 0; i < this.rocksOnScreen.length; i++) {
          this.game.debug.body(this.rocksOnScreen[i]);
        }

        var legame = this.game;

        this.pointsGroup.forEachAlive(function (point) {
          legame.debug.body(point);
        });
      }
    },
    handlePointerDown: function (pointer) {
      if (pointer.x < this.game.width * 0.5) {
        this.pointerLeft = true;
        this.pointerRight = false;
      } else {
        this.pointerLeft = false;
        this.pointerRight = true;
      }
    },
    handlePointerUp: function () {
      this.pointerLeft = false;
      this.pointerRight = false;
    },

    ultratween: function (k) {
      return Math.pow(k, 8);
    },

    getQueryVariable: function (variable) {
      var query = window.location.search.substring(1);
      var vars = query.split('&');
      for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
        if (pair[0] === variable) {
          return pair[1];
        }
      }
      return (false);
    }
  };

  window['rafting'] = window['rafting'] || {};
  window['rafting'].game = game;
})();