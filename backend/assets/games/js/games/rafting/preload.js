(function () {
  'use strict';
  var preload = function () {
    this.ready = false;
  };

  preload.prototype = {
    preload: function () {
      console.log('%c Preloading assets ', 'color: white; background:red;');

      $(".loader").addClass("visible");
      this.load.onLoadComplete.addOnce(this.onLoadComplete, this);
      this.loadResources();
    },
    loadResources: function () {
      if(this.game.isWinTablet  || this.game.isIOS7) {
        this.game.load.spritesheet('road', 'img/games/rafting/water_spritesheet_retexture.jpg', 682, 258);
        this.game.load.spritesheet('rocks', 'img/games/rafting/rocks_retexture.png', 512, 135);
      } else {
        this.game.load.spritesheet('road', 'img/games/rafting/water_spritesheet.jpg', 1024, 387);
        this.game.load.spritesheet('rocks', 'img/games/rafting/rocks.png', 1024, 270);
      }

      this.game.load.spritesheet('sail', 'img/games/rafting/rafter_spritesheet.png', 470, 240);
      this.game.load.spritesheet('points', 'img/games/rafting/points.png', 76, 107);
      this.game.load.image('sky', 'img/games/rafting/sky.jpg');
      this.game.load.image('trees', 'img/games/rafting/trees.png');
      this.game.load.image('cloud', 'img/games/rafting/cloud.png');
      this.game.load.spritesheet('rocksheet1', 'img/games/rafting/rock1_sheet.png', 250, 312);
      this.game.load.spritesheet('rocksheet2', 'img/games/rafting/rock2_sheet.png', 250, 266);
      this.game.load.spritesheet('rocksheet3', 'img/games/rafting/rock3_sheet.png', 250, 329);
      this.game.load.spritesheet('rocksheet4', 'img/games/rafting/rock4_sheet.png', 250, 310);
      this.game.load.image('gametitle', 'img/games/CTA.png');
      this.game.load.image('scorebg', 'img/games/rafting/pointsback.png');
    },
    create: function () {},
    update: function () {
      if (!!this.ready) {
        this.game.state.start('GameTitle');
      }
    },

    onLoadComplete: function (){
      $(".loader").removeClass("visible");
      this.ready = true;
    }
  };

  window['rafting'] = window['rafting'] || {};
  window['rafting'].preload = preload;
})();