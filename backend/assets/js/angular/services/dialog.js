'use strict';

angular.module('dialogModule',['ngDialog'])
.controller('experienceList',['$rootScope','$scope','ngDialog','$window',function($rootScope,$scope,ngDialog,$window){

	/*$scope.dialog_open = function(message, buttonShow, closeCallBack){

		$rootScope.message = message;
		$rootScope.buttonShow = buttonShow;
		$scope.dialog = ngDialog.open({
			template: 'registrationDialog',
			className: 'ngdialog-theme-plain',
			showClose: false
		});

		if(closeCallBack){
			$scope.dialog.closePromise.then(closeCallBack);
		}

	}

	$scope.dialog_close = function(){

		$scope.dialog.close();

	}*/

	$scope.openConfirm = function (url) {

		ngDialog.openConfirm({
			template: 'modalDialogId',
			className: 'ngdialog-theme-default'
		}).then(function (value) {
			$window.location.href=url;
		}, function (reason) {
			 
		});

	};

	
}])
.controller('activityList',['$rootScope','$scope','ngDialog','$window',function($rootScope,$scope,ngDialog,$window){

	

	$scope.openConfirm = function (url) {

		ngDialog.openConfirm({
			template: 'modalDialogId',
			className: 'ngdialog-theme-default'
		}).then(function (value) {
			$window.location.href=url;
		}, function (reason) {
			 
		});

	};

	
}]);


