(function(){

	/* profile dropdown*/
	$(".main-nav .profile").on("click", function(e){
		e.preventDefault();
		$('.profile-dropdown').toggleClass("active");
		$('header .activities').removeClass("active");
	});

	$(".profile-dropdown .x-close").on("click", function(e){
		e.preventDefault();
		$('.profile-dropdown').toggleClass("active");
	});

	
	$(".sliderActivities").on("click", function(e){
		e.preventDefault();
		$("header .activities").toggleClass("active");
		$('.profile-dropdown').removeClass("active");
	});


	$("header .burger").on("click", function(e){
		e.preventDefault();
		var el = $(this);
		var activities = $('header .activities');
		var mainnav = $('header .container');
		var profile = $('header .profile-dropdown');
		var head = $('header');

		if(!head.hasClass('active')){
			el.addClass('active');
			head.addClass('active');
			mainnav.addClass('active');
		}else{

			if(profile.hasClass('active')){
				profile.removeClass('active');
			}
			else if(activities.hasClass('active')){
				activities.removeClass('active');
			}
			else{
				el.removeClass('active');
				head.removeClass('active');
				mainnav.removeClass('active');
			}

		}
	});


	$('#termsToggle').click(function(){
		popup.load(siteUrl+'popups/terms');
	})
	$('#contactusToggle').click(function(){
		popup.load(siteUrl+'popups/contactus');
	})

	$('#privacyToggle').click(function(){
		popup.load(siteUrl+'popups/privacy');
	})

	$('#mechanicsToggle').click(function(){
		popup.load(siteUrl+'popups/mechanics');
	})
baseUrl

	//* gifts */
	$('.giftsCatalogue .infos a').click(function(e){
		e.preventDefault();
		popup.show({
			html : '#giftmodal',
			onOpen: function(){
				$('.gift-popup-slider').removeClass('confirm').removeClass('complete')
			}

		});
	})
	
	$('#iwantthis').click(function(){
		$('.gift-popup-slider').addClass('confirm')
	});

	$('#iwantthis').click(function(){
		$('.gift-popup-slider').addClass('confirm')
	});

	$('#giftconfirm').click(function(){
		$('.gift-popup-slider').removeClass('confirm')
		$('.gift-popup-slider').addClass('complete')
	});
})();