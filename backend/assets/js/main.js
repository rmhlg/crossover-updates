$(window).load(function(){
	homepageWelcomePopup();
	setInterval(function(){
		updateUserPoints();
	}, 5000);
});
function filterGift(region_id) {
	if($("#region"+region_id).attr("class") == "active" || $("#region"+region_id).attr("class") == "locked"){
		$.post(siteUrl+"gifts/filterGift", {'region_id':region_id}, function(data){
			var array = $.parseJSON(data),
				html = "",
				ctr = 0,
				className = "",
				buttonTxt = "I WANT THIS",
				clickableButton = 'class="iWantButton"';

			for(var i=0; i<array.length; i++){
				className = "";
				clickableButton = 'class="iWantButton"';
				buttonTxt = "I WANT THIS";

				if(array[i]['stock']==0){
					className = "outStock";
					clickableButton = "disabled='disabled'";
					buttonTxt = "OUT OF STOCK";
				}else if(array[i]['claimed']){
					className = "claimed";
					clickableButton = "disabled='disabled'";
					buttonTxt = "CLAIMED";
				}else if(array[i]['locked']){
					className = "locked";
					clickableButton = "disabled='disabled'";
					buttonTxt = "LOCKED";
				}else if(array[i]['premium']){
					className = "premium";
				}
				button = '<a href="#" id="popUpButton'+array[i]['gift_id']+'" onclick="loadGiftInfo('+array[i]['gift_id']+')" '+clickableButton+' data-giftid="'+array[i]['gift_id']+'" >'+buttonTxt+'</a>';
				if(buttonTxt != "I WANT THIS"){
					button = '<p class="cantClick">'+buttonTxt+'</p>';
				}

				if(ctr==0){
					html = html + '<div class="row"><div id="popUpWrap'+array[i]['gift_id']+'" class="gift col-md-4 '+className+'"><div class="picture"><img src="'+siteUrl+'uploads/'+array[i]['image']+'"></div><div class="infos"><p class="name"><span>'+array[i]['name']+'</span><span class="km">/ <span>'+array[i]['km_points']+'</span> km</span></p><p class="description">'+array[i]['description']+'</p>'+button+'</div></div>';
					ctr=1;
				}else if(ctr==1){
					html = html + '<div id="popUpWrap'+array[i]['gift_id']+'" class="gift col-md-4 '+className+'"><div class="picture"><img src="'+siteUrl+'uploads/'+array[i]['image']+'"></div><div class="infos"><p class="name"><span>'+array[i]['name']+'</span><span class="km">/ <span>'+array[i]['km_points']+'</span> km</span></p><p class="description">'+array[i]['description']+'</p>'+button+'</div></div>';
					ctr=2;
				}else{
					html = html + '<div id="popUpWrap'+array[i]['gift_id']+'" class="gift col-md-4 '+className+'"><div class="picture"><img src="'+siteUrl+'uploads/'+array[i]['image']+'"></div><div class="infos"><p class="name"><span>'+array[i]['name']+'</span><span class="km">/ <span>'+array[i]['km_points']+'</span> km</span></p><p class="description">'+array[i]['description']+'</p>'+button+'</div></div></div>';
					ctr=0;
				}

			}
			$(".catalogue").html(html);	
		});
	}
}
function setDatepicker(element){
	element.datepicker({dateFormat : "yy/mm/dd"});
}
function loadGiftInfo(gift_id){
	var claim = "";;
	$.post(siteUrl+"gifts/checkClaiming", {'gift_id':gift_id}, function(data) {
		$("#preLoader").show();
		if(data=="b"){
			alert('Oops! Your KM points is short to claim this gift.')
		}else{
			$(".lytebox-wrap").show();
			$(".lytebox-wrap").css({
				'height' : '1354px'
			});
			$(".lytebox-content-holder").css({
				"position": "fixed",
				"top": "0",
				"width": "100%",
				"height": "100%",
				"overflow": "auto"
			});
			$(".lytebox-wrap").addClass('lytebox');
			$(".lytebox-wrapped-content").wrap("<div class='lytebox-content' style='width: 770px; left: 575px; top: 163px; margin-bottom: 100px'>");
			$(".lytebox-wrapped-content").show();
			$(".gift-popup-slider").removeClass('confirm');
			$(".gift-popup-slider").removeClass('complete');
			$.post(siteUrl+"gifts/loadGiftInfo", {'gift_id':gift_id}, function(data){
				$("#preLoader").hide();
				var dataArr = jQuery.parseJSON(data);

				$("#popUpImg").attr('src', siteUrl+'uploads/'+dataArr['image']);
				$("#popUpImg").attr('alt', dataArr['name']);
				$("#popUpGiftName").text(dataArr['name']);
				$("#popUpKm").text("");
				$("#popUpKm").text(dataArr['km_points']);
				$("#popUpDescription").html(dataArr['description']);
				$("#giftconfirm").data('popUpId', dataArr['gift_id']);
				$("#street").data('street', dataArr['street']);
				$("#brgy").data('brgy', dataArr['brgy']);
				$("#city").data('city', dataArr['city']);
				$("#postal").data('postal', dataArr['postal']);
				$("#province").data('province', dataArr['province']);
				$("#street").val(dataArr['street']);
				$("#brgy").val(dataArr['brgy']);
				$("#city").val(dataArr['city']);
				$("#postal").val(dataArr['postal']);
				$("#province").val(dataArr['province']);
			});
			return false;
		}
	});
}
function insertIWantThis(element){
	$("#preLoader").show();
	var gift_id = element.data("popUpId"),
		street = $("#street").val(),
		brgy =$("#brgy").val(),
		city = $("#city").val(),
		postal = $("#postal").val(),
		province = $("#province").val();

	$.post(siteUrl+"gifts/insertIWantThis", {"gift_id":gift_id, 'street':street, 'brgy':brgy, 'city':city, 'postal':postal, 'province': province}, function(data){
		$("#preLoader").hide();
		$(".catalogue .row").find("#popUpWrap"+gift_id).addClass('claimed');
		// $(".catalogue .row").find("#popUpButton"+gift_id).text("CLAIMED");
		// $(".catalogue .row").find("#popUpButton"+gift_id).removeAttr("onclick");
		// $(".catalogue .row").find("#popUpButton"+gift_id).attr("disabled", "disabled")
		// $(".catalogue .row").find("#popUpButton"+gift_id).removeAttr("data-toggle");
		$(".catalogue .row").find("#popUpButton"+gift_id).remove();
		$(".catalogue .row").find("#popUpWrap"+gift_id+" .description").after("<p class='cantClick'>CLAIMED</p>");
		$.post(siteUrl+"gifts/getUserCreditKm", function(data){
			$("#creditKm").html(data);
			$(".credit_km_points").html(data);
		})
	});
}
function imGoing(element, event_id, registrant_id){
	$.post(siteUrl+"events/imGoing", {'event_id':event_id, 'registrant_id': registrant_id}, function(data){
		element.html("<i></i>Going");
		element.addClass("active");
		element.attr("disabled", "disabled");
		$('#eventTitle').html("<b>"+element.data('title')+"</b>");
		$('#eventDate').html("<b>"+element.data('date')+"</b>");
		$('#eventVenue').html("<b>"+element.data('venue')+"</b>");
	});
}
function filterEvent(venue, region, eventType, startdate, enddate){
	$.post(siteUrl+"events/filterEvent", {'venue':venue, 'region':region, 'eventType': eventType,'startdate':startdate, 'enddate':enddate}, function(data){
		var html = "";

		//$(".light").html(data);	
		var dataArray = JSON.parse(data);
		ctr = 0;
		for(var i =0; i<dataArray.length; i++){
			if(dataArray[i]['disabled']){
				html = html+"<div class='item'><div class='thumbnails'><img src='"+siteUrl+'uploads/backstage/events/'+dataArray[i]['image']+"' alt='thumbnail'/></div><div class='content'><h3>"+dataArray[i]['name']+"</h3><h6>"+dataArray[i]['address']+" <br/> "+dataArray[i]['start_date']+"  TO "+dataArray[i]['end_date']+" </h6><br/><p>"+dataArray[i]['description']+"</p><button disabled='disabled' class='active'><i></i>GOING</button></div></div>"
			}else{
				html = html+"<div class='item'><div class='thumbnails'><img src='"+siteUrl+'uploads/backstage/events/'+dataArray[i]['image']+"' alt='thumbnail'/></div><div class='content'><h3>"+dataArray[i]['name']+"</h3><h6>"+dataArray[i]['address']+" <br/> "+dataArray[i]['start_date']+"  TO "+dataArray[i]['end_date']+" </h6><br/><p>"+dataArray[i]['description']+"</p><button  data-toggle='modal' data-target='#myModal' data-venue='"+dataArray[i]['address']+"' data-title='"+dataArray[i]['name']+"' data-date='"+dataArray[i]['start_date']+" to "+dataArray[i]['end_date']+"' onclick='imGoing($(this), "+dataArray[i]['event_id']+", 1)'><i></i>I'M GOING</button></div></div>"
			}
			if(ctr==0){
				if(i==(dataArray.length-1)){
					html = html+"<br class='clear' />";
				}
				ctr = 1;
			}else{
				html = html+"<br class='clear' />";
				ctr = 0;
			}
		}
		$(".events-list .event-content").html(html);
	});
}

function updateUserPoints()
{
	$.getJSON(siteUrl+'points/user_points', function (response) {
		$('.collected_km_points_padded').html(response[0].total_km_points_padded+' <i>KM</i>');
		$('.credit_km_points').html(response[0].credit_km_points);
		$('.collected_km_points').html(response[0].total_km_points);
	});
}

function validateCode()
{
	var formCode = $('#form-header').find('input:first-child').val();
	var error_container = $('#form-header .notifMsg');
	var congrats_container = $('#form-header .congrats');
	var congrats_text = congrats_container.find('span');
	var err_container = $('#form-header .error');
	var err_text = err_container.find('span');
	var emphasize = congrats_container.find('strong');

	console.log(siteUrl);

	$.post(siteUrl+'code_validation', {code : formCode}, function (response) {
		error_container.hide();
		congrats_container.hide();
		err_container.hide();
		response = $.parseJSON(response);
		if(response.status != 'Success') {
			error_container.show();
			err_container.show();
			err_text.html(response.status);
			return false;
		} else {
			error_container.show();
			congrats_container.show();
			congrats_text.html(response.message);
			updateUserPoints();
		}
		return false;
	});
	return false;
}

function validateCodeProfile()
{
	var formCode = $('#form-profile').find('input:first-child').val();
	var error_container = $('#form-profile .notifMsg');
	var congrats_container = $('#form-profile .congrats');
	var congrats_text = congrats_container.find('span');
	var err_container = $('#form-profile .error');
	var err_text = err_container.find('span');
	var emphasize = congrats_container.find('strong');

	$.post(siteUrl+'code_validation', {code : formCode}, function (response) {
		error_container.hide();
		congrats_container.hide();
		err_container.hide();
		response = $.parseJSON(response);
		if(response.status != 'Success') {
			error_container.show();
			err_container.show();
			err_text.html(response.status);
			return false;
		} else {
			error_container.show();
			congrats_container.show();
			congrats_text.html(response.message);
			updateUserPoints();
		}
		return false;
	});
	return false;
}

function earnPoints(origin_id, sub_origin_id, main_sub_origin_id)
{
	$.post(siteUrl+'points/earn_user_points', {origin_id:origin_id, sub_origin_id:sub_origin_id, main_sub_origin_id:main_sub_origin_id}, function(response) {
		updateUserPoints();
	});
}

function homepageVideoPopup()
{
	popup.dialog({type:'alert', message:'You just completed your first activity, you get 50KM. Visit more pages to get more kilometers.', onClose:function() {
		$('#video-close-btn').attr('onclick', '');
	}});
}

function homepageWelcomePopup()
{
	if(first_login) {
		popup.dialog({type:'alert', message:'Welcome to the CROSSOVER community! Here’s 20 points to get you started.', onClose:function(){
			$.get(siteUrl+'home/first_login');
		}});
	}
}

function validate(form){
	var error = false;
	var eformat = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	var atpos;
	var dotpos;

	$('.req',form).each(function(){

		var el = $(this);
		var val = $.trim(el.val());

		
		if( val == '' ){
			error = 'Please input '+ el.attr('placeholder');
			el.focus();
			return false;
		}
		if(el.attr('name')=='email[]'){

			atpos = val.indexOf("@");
			dotpos = val.lastIndexOf(".");

			if (atpos< 1 || dotpos<atpos+2 || dotpos+2>=val.length) {
				error = 'Please input a valid email address.';
				el.focus();
				return false;
			}
		 }
		if(el.attr('type')=='checkbox'){
			if(!el.is(':checked')){
				error = 'Please confirm to proceed.';
				return false;
			}
		}
		
	});

	if( error ){
		$('#error').show().html('<span>'+error+'</span>');
		return false;
	}
}
$(document).ready(function(){
	$("#startdate").datepicker({dateFormat : "yy/mm/dd"});
	$("#enddate").datepicker({dateFormat : "yy/mm/dd"});
	$(".popup-close, .closeLyte").bind('click', function(event) {
		$(".lytebox-wrap").hide();
		$(".lytebox-wrapped-content").unwrap();
	});
	$(this).mouseup(function(e){
		var container = $(".lytebox-content");
		if(!container.is(e.target) && container.has(e.target).length === 0){
			if($(".lytebox-wrapped-content").parent().hasClass('lytebox-content')){
				$(".lytebox-wrapped-content").unwrap();
			}
		}
	});
});