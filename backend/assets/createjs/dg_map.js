//JS initial preloader
var preload_loader = {
	position : 'absolute',
	width    : '200',
	height   : '20px',
	border   : '3px solid #aaa',
	'z-index': 1,
	top      : '50%',
	left     : '50%',
	'-ms-transform'     : 'translate(-50%,-50%)',
	'-webkit-transform' : 'translate(-50%,-50%)',
	'transform'         : 'translate(-50%,-50%)',
	'background-color'	: '#444'

}
var preload_progress = {
	width 				: '1%',
	height 				: '100%',
	'background-color' 	: "#14F6EE",
	'overflow'			: 'hidden'
}

var preload_status = {
	float        : 'right',
	display      : 'inline-block',
	'font-size'  : '10px',
	'font-family': 'arial',
	'font-weight': 'bold',
	color 		 : '#444',
	padding		 : '2px 5px 0'
}

var map_scaleX = 0;
var map_scaleY = 0;

visualLoader = $("#xOverMap").parent();
visualLoader.prepend("<div class='xLoader'></div>");
visualLoader.find(".xLoader").append("<div class='xProgress'></div>");
visualLoader.find(".xProgress").append("<span class='xStatus'>0%</span>")

visualLoader.find(".xProgress").css(preload_progress);
visualLoader.find(".xLoader").css(preload_loader);
visualLoader.find(".xStatus").css(preload_status);

visualLoader.find(".xLoader").show();

var AssetLib = {};
var queue;
var loader;
var manifest;
var isReady  = false;
var hasMap   = false;
var baseFont = 'bebas-neue, sans-serif';
var myUrl    = "http://marlboro-stage2.yellowhub.com/spice/marlboro-crossover/backend/map/getAllDetails";

///////////////////////////////////////
//
//	ASSETS QUEUE
//
///////////////////////////////////////
manifest = [
		{ src: "btnDiamondSpt.png", id: "diamond"},
		{ src: "fullmap.png", id: "fullmap"},

		{ src: "californiatrail_black.png", id: "californiatrail_black"},
		{ src: "californiatrail_red.png", id: "californiatrail_red"},

		{ src: "alaskatrail_black.png", id: "alaskatrail_black"},
		{ src: "alaskatrail_red.png", id: "alaskatrail_red"},

		{ src: "canadatrail_black.png", id: "canadatrail_black"},
		{ src: "canadatrail_red.png", id: "canadatrail_red"},

		{ src: "dottedRed.png", id: "dottedRed" },
		{ src: "extraLine01_red.png", id: "extraLine01_red" },
		{ src: "extraLine01_black.png", id: "extraLine01_black" },

		{ src: "loc_california.png", id: "loc_california" },
		{ src: "loc_alaska.png", id: "loc_alaska" },
		{ src: "loc_canada.png", id: "loc_canada" },

		{ src: "takeTheChallenge.png", id: "takeTheChallenge" },

		{ src: "popUpTriangle.png", id: "popUpTriangle" },
		{ src: "pointsBubble.png", id: "pointsBubble" },

		{ src: "redCheck.png", id: "redCheck" },

		{ src: "redLoader.jpg", id: "redLoader" },

		{ src: "btnX.png", id: "btnX" }
	];

queue = new createjs.LoadQueue(true, "img/map/");
queue.on("fileload", handleFileLoad);
queue.on("progress", handleOverallProgress);
queue.on("fileprogress", handleFileProgress);
queue.on("complete", handleFileComplete);
queue.on("error", handleFileError);

queue.setMaxConnections(5);//don't know
processQueue();

function handleFileLoad(event){
	if(event.item.type=="image"){
		AssetLib[event.item.id] = event.result;
	}
}

function handleOverallProgress(event){

}

function handleFileProgress(event) {

	if(visualLoader) {
		var percent = queue.progress;
		var total   = 200;

		visualLoader.find(".xProgress").css({ width : total * percent });
		percent = percent * 100;
		var strVal = percent.toString();
			strVal = strVal.substr(0, 5);
		visualLoader.find(".xStatus").text(strVal+"%");
	}
}

function handleFileError(event) {
	console.log("error");
}

function handleFileComplete(event){
	initStage();
	isReady = true;
	visualLoader.find(".xLoader").hide();
}

//Manages queues
function loadAnother(){
	var item = manifest.shift();
		queue.loadFile(item);
}

function processQueue(){
	while (manifest.length > 0) {
		loadAnother();
	}
}

///////////////////////////////////////
//
//	INIT STAGE
//
///////////////////////////////////////
var canvas, stage;
var	mapContainer;
var diamondSpt;
var extraLine01_red;
var extraLine01_black;
var dotTotop;

//CALIFORIA
var californiaList = [];
var btn_Node01;
var btn_Node02;
var btn_Node03;
var california_nodeCont;

var cali_black_bmp;
var cali_red_bmp;
var cali_mask_shp;

var txt01, txt02, txt03;
var lbl_california_shp01, lbl_california_shp02, lbl_california_shp03;
var node01_color = "#000000";
var node02_color = "#000000";
var node03_color = "#000000";
var california_lineCont;
var california_holder;

var Benchmark_california;
var CALIFORIA_STARTPOINT = 0;
var CALIFORNIA_MAXPOINTS = 1399; //0 to 1,399

//ALASKA
var alaskaList = [];
var btn_Node04;
var btn_Node05;
var btn_Node06;
var alaska_nodeCont;

var alaska_black_bmp;
var alaska_red_bmp;
var alaska_mask_shp;

var txt04, txt05, txt06;
var lbl_alaska_shp01, lbl_alaska_shp02, lbl_alaska_shp03;
var node04_color = "#000000";
var node05_color = "#000000";
var node06_color = "#000000";

var alaska_holder;
var alaska_lineCont;

var Benchmark_alaska;
var ALASKA_STARTPOINT = 1400;
var ALASKA_MAXPOINTS  = 3999; //1,400 to 3,999

//CANADA
var canadaList = [];
var btn_Node07;
var btn_Node08;
var btn_Node09;
var canada_nodeCont;

var canada_black_bmp;
var canada_red_bmp;
var canada_mask_shp;

var txt07, txt08, txt09;
var lbl_canada_shp01, lbl_canada_shp02, lbl_canada_shp03;
var node07_color = "#000000";
var node08_color = "#000000";
var node09_color = "#000000";

var canada_holder;
var canada_lineCont;

var Benchmark_canada;
var CANADA_STARTPOINT = 4000;
var CANADA_MAXPOINTS  = 5500; //4,000 to 5,500

//COMPONENTS
var Map;

var popUpView;
var popUpHolder;

var currLoc = MY_CALIFORNIA;
var currBtnClicked;
var currActivity = 1;
var currBtnOldX;
var currBtnOldY;
var dd_offset;

//POPUP model
var txt_region;
var txt_challenge;
var txt_subtitle;
var popUp_bmp;
var popUp_bmp_list=[];

var line_shp;

var txt_content;

var wInnerMask;
var dynamicBmpHolder;
var wRedCheck;
var btnX;

var takeChallengeSpt;
var redLoaderSpt;

var selectedMaxPointRef;
var errorTxt;

var errorWindow;
var errorDelay  = 0;
var loaderDelay = 0;
var JSON;

var MY_CALIFORNIA    = "california";
var MY_ALASKA        = "alaska";
var MY_CANADA        = "canada";

var DGpreload;
var gcurrIndex       = 0;
var enable_mouseDown = false;


function initStage(){
	var self = this;

	canvas = document.getElementById("xOverMap");
	stage = new createjs.Stage(canvas);
	stage.enableMouseOver(10);
	createjs.Touch.enable(stage);
	stage.mouseMoveOutside = true;

	var arr = [];
	var itr = 0;

	Map = {

		initpopUpModel : function (){
			var paddingLeft = 23;
			//text
			txt_region   = new createjs.Text('not set', 'bold 18px MaybeBold', "#000000");
			txt_region.x = paddingLeft;
			txt_region.y = 23;
			txt_region.font = '18px bebas-neue';

			txt_challenge   = new createjs.Text('not set', 'bold 30px MaybeBold', "#000000");
			txt_challenge.x = paddingLeft;
			txt_challenge.y = 43;
			txt_challenge.font = '30px bebas-neue';

			txt_subtitle   = new createjs.Text('HIKING VS. CLIMBING', 'bold 18px MaybeBold', "#000000");
			txt_subtitle.x = paddingLeft;
			txt_subtitle.y = 210;
			txt_subtitle.font ='18px bebas-neue';

			line_shp 	   = new createjs.Shape();
			line_shp.graphics.beginFill("#cccccc").drawRect(23,237,322-46,1);

			txt_content    = new createjs.Text('undefined', 'bold 15px MaybeBold', "#000000");
			txt_content.x = paddingLeft;
			txt_content.y = 246;
			txt_content.font = '15px bebas-neue';

			txt_content.text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
			txt_content.lineWidth  = 322-46;
			txt_content.lineHeight = 17;
			txt_content.textBaseline = "top";
			txt_content.textAlign = "left";

			var preloadData = {
				framerate: 30,
				"images": [queue.getResult("redLoader")],
				"frames": {"regX": 0, "height": 28, "count": 9, "regY": 0, "width": 28},
				"animations": {
					frame_0: 0, frame_1: 1, frame_2: 2, frame_3: 3, frame_4: 4, frame_5: 5, frame_6: 6, frame_7: 7, frame_8: 8
				}
			};

			var preloadSpt = new createjs.SpriteSheet(preloadData);
				redLoaderSpt = new createjs.Sprite(preloadSpt, "frame_0");
				//redLoaderSpt.x = paddingLeft;
				redLoaderSpt.x = 150;
				redLoaderSpt.y = 125;

			//image - not mask
			wInnerMask = new createjs.Shape();
			wInnerMask.graphics.beginFill("#cccccc").drawRect(0,0,275,119);
			wInnerMask.x = paddingLeft;
			wInnerMask.y = 80;

			//dynamicBmp-holder
			dynamicBmpHolder = new createjs.Container();
			dynamicBmpHolder.x = paddingLeft;
			dynamicBmpHolder.y = 80;

			wRedCheck = new createjs.Bitmap(AssetLib['redCheck']);
			wRedCheck.x = 267;
			wRedCheck.y = wInnerMask.y

			btnX = new createjs.Bitmap( AssetLib['btnX']);
			btnX.x = 275;
			btnX.y = 20;

			//btn
			var tcbtnData = {
				framerate: 30,
				"images": [queue.getResult("takeTheChallenge")],
				"frames": {"regX": 0, "height": 37, "count": 3, "regY": 0, "width": 273},
				"animations": {
					enable: 0,
					disable: 1
				}
			};

			var btnCTS = new createjs.SpriteSheet(tcbtnData);
			takeChallengeSpt = new createjs.Sprite(btnCTS, "disable");
			takeChallengeSpt.x = paddingLeft;
			takeChallengeSpt.y = 412 - 70;

			errorWindow = new createjs.Bitmap(AssetLib['pointsBubble']);
			errorWindow.y = 412-32;
			errorWindow.x = paddingLeft + 30;
			errorWindow.visible =false;

			errorTxt      = new createjs.Text('loading..', 'bold 16px MaybeBold', "#000000");
			errorTxt.text = "You must have atleast undefined points in order to continue to this challenge.";
			errorTxt.lineWidth  = 215;
			errorTxt.lineHeight = 17;
			errorTxt.textBaseline = "top";
			errorTxt.textAlign = "left";

			errorTxt.font = '16px bebas-neue';

			errorTxt.x =  paddingLeft  + 50;
			errorTxt.y = errorWindow.y + 20;
			errorTxt.visible = false;

			//popUpView - overall
			popUpView  = new createjs.Shape();
			popUpView.graphics.beginFill("#ffffff").drawRect(0,0,322,412);
			popUpView.shadow = new createjs.Shadow("#444444", 1, 2, 15);
			pTriangle = new createjs.Bitmap(AssetLib['popUpTriangle']);

			popUpHolder = new createjs.Container();
			popUpHolder.addChild(popUpView);
			popUpHolder.addChild(pTriangle);

			popUpHolder.addChild(wInnerMask);
			popUpHolder.addChild(redLoaderSpt);
			popUpHolder.addChild(dynamicBmpHolder);

			popUpHolder.x = 0;
			popUpHolder.y = 0;
			popUpHolder.visible = false;

			popUpHolder.addChild(txt_region);
			popUpHolder.addChild(txt_challenge);
			popUpHolder.addChild(txt_subtitle);
			popUpHolder.addChild(line_shp);
			popUpHolder.addChild(txt_content);
			popUpHolder.addChild(errorWindow);
			popUpHolder.addChild(errorTxt);
			popUpHolder.addChild(takeChallengeSpt);
			popUpHolder.addChild(wRedCheck);
			popUpHolder.addChild(btnX);
		},

		openPopUpWhite	: function( region, activity, challengeTxt, maxPoint, index ){
			//dynamic
			txt_content.text   = JSON[index]["description"];
			txt_challenge.text = JSON[index]["title"];
			gcurrIndex = index;
 			/*var bitmap = new createjs.Bitmap( JSON[0]["image"] );
 				stage.addChild(bitmap);*/

			//static
			errorTxt.visible    = false;
			errorWindow.visible = false;
			selectedMaxPointRef = maxPoint;
			errorTxt.text = "You must have atleast "+Math.floor(maxPoint)+" points in order to continue to this challenge."
			clearTimeout(errorDelay);
			clearTimeout(loaderDelay);

			if(popUp_bmp!=undefined || popUp_bmp!=null){
				for( var _bmp in popUp_bmp_list ){
					dynamicBmpHolder.removeChild(popUp_bmp_list[_bmp]);
					popUp_bmp_list[_bmp]=null;
					stage.update();
				}
				popUp_bmp_list = [];
			}

			var iitr = 0;
			loaderDelay = setInterval( function(){
				iitr++;
				redLoaderSpt.gotoAndStop("frame_"+iitr);
				if(iitr>8) iitr =0;
			},100);

			//POP images
			var _e = {
				onLoaded : function(evt){

					if(evt.item.type=="image"){
						popUp_bmp     = new createjs.Bitmap(evt.result);
						popUp_bmp_list.push(popUp_bmp);

						dynamicBmpHolder.addChild(popUp_bmp);

						var popUpWidth = 275;
						var scaleVal = popUpWidth/popUp_bmp.getBounds().width;

						popUp_bmp.scaleX = scaleVal;
						popUp_bmp.scaleY = scaleVal;

						//80,119
						dynamicBmpHolder.mask = wInnerMask;
						dynamicBmpHolder.y    = wInnerMask.y - 30;
						/*popUp_bmp.x = 23;
						popUp_bmp.y = wInnerMask.y - 30;*/


						var hitArea = new createjs.Shape;
							hitArea.graphics.beginFill("#000").drawRect(dynamicBmpHolder.getBounds().x,
																		dynamicBmpHolder.getBounds().y,
																		dynamicBmpHolder.getBounds().width,
																		dynamicBmpHolder.getBounds().height );

							dynamicBmpHolder.hitArea  = hitArea;

						enable_mouseDown = true;
						clearTimeout(loaderDelay);
					}

				},
				error : function(){ console.log("error loading: JSON[index][image]") }
			}

			enable_mouseDown = false;

			var q = new createjs.LoadQueue(false);
				q.on("fileload", _e.onLoaded);
				q.on("error"   , _e.error);
				q.loadFile(JSON[index]["image"]);

			//POPUP btn
			if( gUserPoint >= maxPoint ){
				takeChallengeSpt.gotoAndStop("enable");
			} else {
				takeChallengeSpt.gotoAndStop("disable");
			}

			//POPUP position
			if(region == MY_CALIFORNIA){
				txt_region.text = "LIBERATE | CALIFORNIA";
				if(activity==1){
					popUpHolder.x = 873;
					popUpHolder.y = 280;
				}else if(activity==2){
					popUpHolder.x = 836;
					popUpHolder.y = 224;
				}else if(activity==3){
					popUpHolder.x = 821;
					popUpHolder.y = 174;
				}
				//bottom triangle
				pTriangle.y = 412 + 22;
				pTriangle.x = 250;
				pTriangle.rotation = 180;

				var cc = {
					handleComplete : function (evt) {
			       		createjs.Tween.removeTweens(evt.target);
			    	}
				}
				popUpHolder.y = popUpHolder.y + 20;
			 	popUpHolder.alpha = 0;
			    createjs.Tween.get(popUpHolder)
			         .to({alpha:1, y: popUpHolder.y-20}, 100 )
			         .call( cc.handleComplete );
			}
			if(region == MY_ALASKA){
				txt_region.text = "ELEVATE | ALASKA";
				if(activity==1){
					popUpHolder.x = 514;
					popUpHolder.y = 212;

				}else if(activity==2){

					popUpHolder.x = 597;
					popUpHolder.y = 154;

				}else if(activity==3){
					popUpHolder.x = 642;
					popUpHolder.y = 104;
				}

				//bottom triangle
				pTriangle.y = 60;
				pTriangle.x = -23;
				pTriangle.rotation = -90;


				var cc2 = {
					handleComplete : function (evt) {
			       		createjs.Tween.removeTweens(evt.target);
			    	}
				}
				popUpHolder.x = popUpHolder.x - 20;
			 	popUpHolder.alpha = 0;
			    createjs.Tween.get(popUpHolder)
			         .to({alpha:1, x: popUpHolder.x+20}, 150)
			         .call( cc2.handleComplete );

			}
			if(region == MY_CANADA){
				txt_region.text = "FASCINATE | CANADA";
				if(activity==1){
					popUpHolder.x = 636;
					popUpHolder.y = 263;
				}else if(activity==2){
					popUpHolder.x = 665;
					popUpHolder.y = 324;
				}
				else if(activity==3){
					popUpHolder.x = 618;
					popUpHolder.y = 293;

					pTriangle.rotation = 90;
					pTriangle.x =345;
					pTriangle.y = 30;
				}

				if(activity==1 || activity ==2){
					var cc3 = {
						handleComplete : function (evt) {
				       		createjs.Tween.removeTweens(evt.target);
				    	}
					}
					popUpHolder.y = popUpHolder.y - 20;
				 	popUpHolder.alpha = 0;
				    createjs.Tween.get(popUpHolder)
				         .to({alpha:1, y: popUpHolder.y+20}, 150)
				         .call( cc3.handleComplete );
				} else {
					var cc4 = {
						handleComplete : function (evt) {
				       		createjs.Tween.removeTweens(evt.target);
				    	}
					}

					popUpHolder.x = popUpHolder.x + 20;
				 	popUpHolder.alpha = 0;
				    createjs.Tween.get(popUpHolder)
				         .to({alpha:1, x: popUpHolder.x-20}, 150)
				         .call( cc4.handleComplete );
				}

				//bottom triangle
				if(activity==1 || activity==2){
					pTriangle.y = -22;
					pTriangle.x = 260;
					pTriangle.rotation = 0;
				}
			}

			if( currActivity == activity && currLoc == region && popUpHolder.visible == true ){
				popUpHolder.visible = false;
			} else {
				popUpHolder.visible = true;
			}

			var _maxX = canvas.width - mapContainer.getBounds().width;
				console.log( _maxX );

			var popUpFocus = {
					handleComplete : function (evt) {
			       		createjs.Tween.removeTweens(evt.target);
			    	},
			    	handleChange : function (evt){
			    		if( evt.currentTarget.target.y > 0){

			    			evt.currentTarget.target.y = 0;
			    		
			    		}
			    		
			    		if( evt.currentTarget.target.x > 0){
			    		
			    			evt.currentTarget.target.x = 0;
			    		
			    		}

			    		var fullWidth = (evt.currentTarget.target.x + mapContainer.getBounds().width) - wW;

			    		if( fullWidth < 0 ){
			    			evt.currentTarget.target.x = 0;
			    		}
			    	}
				}

			    var offsetX = popUpHolder.x - ( (canvas.width*0.5)-150 );
			    var offsetY = (canvas.height/2)-(popUpHolder.getBounds().height/2);

			   createjs.Tween.get(mapContainer, { onChange: popUpFocus.handleChange })

			         .to({ y: -( popUpHolder.y-offsetY ), x: -offsetX}, 300 )

			         .call( popUpFocus.handleComplete );
		},

		initMap : function(){
			var fullmap    = new createjs.Bitmap(AssetLib['fullmap']);
			mapContainer   = new createjs.Container();
			mapContainer.addChild(fullmap);

			var offsetY = mapContainer.getBounds().height - canvas.height;
			mapContainer.y -= offsetY;

			/*mapContainer.x = ( canvas.width/2 - mapContainer.getBounds().width/2 ) -150;*/

			//static positions
			dotTotop    = new createjs.Bitmap(AssetLib['dottedRed']);

			extraLine01_red    = new createjs.Bitmap(AssetLib['extraLine01_red']);
			extraLine01_red.x  = 264;
			extraLine01_red.y  = -95;
			extraLine01_red.visible = false;

			extraLine01_black    = new createjs.Bitmap(AssetLib['extraLine01_black']);
			extraLine01_black.x  = extraLine01_red.x;
			extraLine01_black.y  = extraLine01_red.y;
			extraLine01_black.visible = true;

			var loc_california   = new createjs.Bitmap(AssetLib['loc_california']);//red dot
				loc_california.x = 1210;
				loc_california.y = 640;

			var loc_alaska       = new createjs.Bitmap(AssetLib['loc_alaska']);//red dot
				loc_alaska.x = 420;
				loc_alaska.y = 80;

			var loc_canada       = new createjs.Bitmap(AssetLib['loc_canada']);//red dot
				loc_canada.x = 1080;
				loc_canada.y = 250;

			mapContainer.addChild(loc_california);
			mapContainer.addChild(loc_alaska);
			mapContainer.addChild(loc_canada);
		},

		setNodeFigure : function( node, data ) {
			node.gotoAndStop( data );
		},

		iniTrail : function() {
			//@CALIFORNIA
			california_holder 	  = new createjs.Container();
			california_lineCont   = new createjs.Container();

			cali_black_bmp        = new createjs.Bitmap(AssetLib['californiatrail_black']);
			cali_red_bmp          = new createjs.Bitmap(AssetLib['californiatrail_red']);

			Benchmark_california  = cali_black_bmp.getBounds().height;
			//@Mask
			cali_mask_shp         = new createjs.Shape();
 			cali_mask_shp.graphics.beginFill("#cccccc").drawRect(0,0,cali_black_bmp.getBounds().width, cali_black_bmp.getBounds().height );
 			cali_mask_shp.alpha   = 0;

 			california_lineCont.rotation = -33.46;
			california_lineCont.addChild(cali_red_bmp);
			california_lineCont.addChild(cali_mask_shp);
			california_lineCont.addChild(cali_black_bmp);
			cali_black_bmp.mask   = cali_mask_shp;

			california_lineCont.x = 0;
			california_lineCont.y = 0;

			//@ALASKA
			alaska_holder    = new createjs.Container();
			alaska_lineCont  = new createjs.Container();

			//force add
			alaska_holder.addChild(extraLine01_black);
			alaska_holder.addChild(extraLine01_red);

			alaska_black_bmp = new createjs.Bitmap(AssetLib['alaskatrail_black']);
			alaska_red_bmp   = new createjs.Bitmap(AssetLib['alaskatrail_red']);

			Benchmark_alaska = alaska_black_bmp.getBounds().height;
			//@Mask
			alaska_mask_shp  = new createjs.Shape();
			alaska_mask_shp.graphics.beginFill("#cccccc").drawRect(0,0,alaska_black_bmp.getBounds().width, alaska_black_bmp.getBounds().height );
			alaska_mask_shp.alpha = 0;

			alaska_lineCont.rotation = 50.98;
			alaska_lineCont.addChild(alaska_red_bmp);
			alaska_lineCont.addChild(alaska_mask_shp);
			alaska_lineCont.addChild(alaska_black_bmp);
			alaska_black_bmp.mask   = alaska_mask_shp;

			//@CANADA
			canada_holder    = new createjs.Container();
			canada_lineCont  = new createjs.Container();

			canada_black_bmp = new createjs.Bitmap(AssetLib['canadatrail_black']);
			canada_red_bmp   = new createjs.Bitmap(AssetLib['canadatrail_red']);

			Benchmark_canada = canada_black_bmp.getBounds().height;
			//@Mask
			canada_mask_shp  = new createjs.Shape();
			canada_mask_shp.graphics.beginFill("#cccccc").drawRect(0,0, canada_black_bmp.getBounds().width, canada_black_bmp.getBounds().height );
			canada_mask_shp.alpha = 0;

			canada_lineCont.rotation = 146.47;
			canada_lineCont.addChild(canada_red_bmp);
			canada_lineCont.addChild(canada_black_bmp);
			canada_lineCont.addChild(canada_mask_shp);
			canada_black_bmp.mask = canada_mask_shp;


		},

		setProperty : function ( arr, str, sprite, place, _x, _y, pt, activity, challenge, num, isRotate ){
			arr[str]       	  = sprite;
			arr[str].place	  = place;
			arr[str].x     	  = _x;
			arr[str].y     	  = _y;
			arr[str].pt    	  = pt;
			arr[str].activity = activity;
			arr[str].challenge= challenge;
			arr[str].num      = num;
			arr[str].isRotate = isRotate;
		},

		initRoute : function(){
			//shape
			var diamondData = {
				framerate: 30,
				"images": [queue.getResult("diamond")],
				"frames": {"regX": 15, "regY": 15, "height": 32, "count": 3, "width": 32},

				"animations": {
					show: 0,
					check: 1,
					hide: 2
				}
			};

			//btn core
			var btnDiamond = new createjs.SpriteSheet(diamondData);
				diamondSpt = new createjs.Sprite(btnDiamond, "show");

				//@CALIFORNIA
				btn_Node01 = diamondSpt.clone();
				btn_Node02 = diamondSpt.clone();
				btn_Node03 = diamondSpt.clone();

				/*
				Map.setProperty( californiaList, "creativity"   , btn_Node01, MY_CALIFORNIA, 729, 640,                      1, 1, "CREATIVITY"     , 0);
				Map.setProperty( californiaList, "testTheEngine", btn_Node02, MY_CALIFORNIA, 694, 591, CALIFORNIA_MAXPOINTS/2, 2, "TEST THE ENGINE", 1);
				Map.setProperty( californiaList, "riseToTheTop" , btn_Node03, MY_CALIFORNIA, 678, 538,   CALIFORNIA_MAXPOINTS, 3, "CREATIVITY"     , 2);
				*/
				dd_offset = -15;

				Map.setProperty( californiaList, "creativity"   , btn_Node01, MY_CALIFORNIA, 52-dd_offset, 106-dd_offset,                          0, 1, "CREATIVITY"     , 0, false );
				Map.setProperty( californiaList, "testTheEngine", btn_Node02, MY_CALIFORNIA, 15-dd_offset, 50-dd_offset, /*CALIFORNIA_MAXPOINTS/2*/0, 2, "TEST THE ENGINE", 1, false );
				Map.setProperty( californiaList, "riseToTheTop" , btn_Node03, MY_CALIFORNIA, 0-dd_offset, 0-dd_offset,   /*CALIFORNIA_MAXPOINTS*/  0, 3, "CREATIVITY"     , 2, false );

					//txt01
					txt01 = new createjs.Text('CREATIVITY', 'bold 13px', "#ffffff");
					txt01.font = baseFont;
					txt01.x = 95;
					txt01.y = 115;

					lbl_california_shp01 = new createjs.Shape();
					lbl_california_shp01.graphics.beginFill(node01_color).drawRect(0,0,1,18);
					lbl_california_shp01.x = txt01.x-5;
					lbl_california_shp01.y = txt01.y-4;

					//txt02
					txt02 = new createjs.Text('TEST THE ENGINE', 'bold 13px', "#ffffff");
					txt02.font = baseFont;
					txt02.x = 60;
					txt02.y = 60;

					lbl_california_shp02 = new createjs.Shape();
					lbl_california_shp02.graphics.beginFill(node02_color).drawRect(0,0,1, 18);
					lbl_california_shp02.x = txt02.x-5;
					lbl_california_shp02.y = txt02.y-4;

					//txt03
					txt03 = new createjs.Text('RISE TO THE TOP', 'bold 13px', "#ffffff");
					txt03.font = baseFont;
					txt03.x = 46;
					txt03.y = 8;

					lbl_california_shp03 = new createjs.Shape();
					lbl_california_shp03.graphics.beginFill(node03_color).drawRect(0,0,1, 18);
					lbl_california_shp03.x = txt03.x-5;
					lbl_california_shp03.y = txt03.y-4;

					california_holder.addChild(lbl_california_shp01);
					california_holder.addChild(txt01);

					california_holder.addChild(lbl_california_shp02);
					california_holder.addChild(txt02);

					california_holder.addChild(lbl_california_shp03);
					california_holder.addChild(txt03);

				//@ALASKA
				btn_Node04 = diamondSpt.clone();
				btn_Node05 = diamondSpt.clone();
				btn_Node06 = diamondSpt.clone();

				Map.setProperty( alaskaList, "tackleTheRain"   , btn_Node04, MY_ALASKA, 0-dd_offset,   0-dd_offset, /*ALASKA_STARTPOINT*/      ALASKA_STARTPOINT-1, 1, "TACKLE THE RAIN"   , 3 , false );
				Map.setProperty( alaskaList, "captureTheMoment", btn_Node05, MY_ALASKA, 82-dd_offset, -56-dd_offset, /*ALASKA_MAXPOINTS*0.75*/ ALASKA_STARTPOINT-1, 2, "CAPTURE THE MOMENT", 4 , false );
				Map.setProperty( alaskaList, "commandTheSnow"  , btn_Node06, MY_ALASKA, 128-dd_offset,-107-dd_offset, /*ALASKA_MAXPOINTS */    ALASKA_STARTPOINT-1, 3, "COMMAND THE SNOW"  , 5 , false );

					//txt04
					txt04 = new createjs.Text('TACKLE THE TERRAIN', 'bold 13px', "#ffffff");
					txt04.font = baseFont;
					txt04.x = 45;
					txt04.y = 8;

					lbl_alaska_shp01 = new createjs.Shape();
					lbl_alaska_shp01.graphics.beginFill(node04_color).drawRect(0,0,1,18);
					lbl_alaska_shp01.x = txt04.x-5;
					lbl_alaska_shp01.y = txt04.y-4;

					//txt05
					txt05 = new createjs.Text('CAPTURE THE MOMENT', 'bold 13px', "#ffffff");
					txt05.font = baseFont;
					txt05.x = 125;
					txt05.y = -48;

					lbl_alaska_shp02 = new createjs.Shape();
					lbl_alaska_shp02.graphics.beginFill(node05_color).drawRect(0,0,1,18);
					lbl_alaska_shp02.x = txt05.x-5;
					lbl_alaska_shp02.y = txt05.y-4;

					//txt06
					txt06 = new createjs.Text('COMMAND THE SNOW', 'bold 13px', "#ffffff");
					txt06.font = baseFont;
					txt06.x = 173;
					txt06.y = -95;//100;

					lbl_alaska_shp03 = new createjs.Shape();
					lbl_alaska_shp03.graphics.beginFill(node06_color).drawRect(0,0,1,18);
					lbl_alaska_shp03.x = txt06.x-5;
					lbl_alaska_shp03.y = txt06.y-4;

					alaska_holder.addChild(lbl_alaska_shp01);
					alaska_holder.addChild(txt04);

					alaska_holder.addChild(lbl_alaska_shp02);
					alaska_holder.addChild(txt05);

					alaska_holder.addChild(lbl_alaska_shp03);
					alaska_holder.addChild(txt06);

				//@CANADA
				btn_Node07 = diamondSpt.clone();
				btn_Node08 = diamondSpt.clone();
				btn_Node09 = diamondSpt.clone();

				Map.setProperty( canadaList, "scaleTheMountain", btn_Node07, MY_CANADA, 0-dd_offset, 1-dd_offset,                        CANADA_STARTPOINT-1, 1, "SCALE THE MOUNTAIN", 6 , false );
				Map.setProperty( canadaList, "IntoTheIce"      , btn_Node08, MY_CANADA, 30-dd_offset, 60-dd_offset, /*4700*/             CANADA_STARTPOINT-1, 2, "INTO THE ICE"      , 7 , false );
				Map.setProperty( canadaList, "MasterTheRapids" , btn_Node09, MY_CANADA, 62-dd_offset, 118-dd_offset, /*CANADA_MAXPOINTS*/CANADA_STARTPOINT-1, 3, "MASTER THE RAPIDS" , 8 , false );

				//txt07
				txt07 = new createjs.Text('SCALE THE MOUNTAIN', 'bold 13px', "#ffffff");
				txt07.font = baseFont;
				txt07.x = 43;
				txt07.y = 8;

				lbl_canada_shp01 = new createjs.Shape();
				lbl_canada_shp01.graphics.beginFill(node07_color).drawRect(0,0,1,18);
				lbl_canada_shp01.x = txt07.x-5;
				lbl_canada_shp01.y = txt07.y-4;

				canada_holder.addChild(lbl_canada_shp01);
				canada_holder.addChild(txt07);

				//txt08
				txt08 = new createjs.Text('INTO THE ICE', 'bold 13px', "#ffffff");
				txt08.font = baseFont;
				txt08.x = 75;
				txt08.y = 68;

				lbl_canada_shp02 = new createjs.Shape();
				lbl_canada_shp02.graphics.beginFill(node08_color).drawRect(0,0,1,18);
				lbl_canada_shp02.x = txt08.x-5;
				lbl_canada_shp02.y = txt08.y-4;

				canada_holder.addChild(lbl_canada_shp02);
				canada_holder.addChild(txt08);

				//txt09
				txt09 = new createjs.Text('MASTER THE RAPIDS', 'bold 13px', "#ffffff");
				txt09.font = baseFont;
				txt09.x = 108;
				txt09.y = 127;

				lbl_canada_shp03 = new createjs.Shape();
				lbl_canada_shp03.graphics.beginFill(node09_color).drawRect(0,0,1,18);
				lbl_canada_shp03.x = txt09.x-5;
				lbl_canada_shp03.y = txt09.y-4;

				canada_holder.addChild(lbl_canada_shp03);
				canada_holder.addChild(txt09);
		},

		allToStage : function(){
			//@CALIFORNIA
			california_holder.x = 1035;
			california_holder.y = 608;
			california_holder.addChild(california_lineCont);
			california_lineCont.x = 2;
			california_lineCont.y = 40;

			california_lineCont.addChild(dotTotop);
			dotTotop.x = - 260;
			dotTotop.y = - 623;
			dotTotop.rotation = 33;

			//ALASKA
			alaska_holder.x = 460;
			alaska_holder.y = 235;
			alaska_holder.addChild(alaska_lineCont);
			alaska_lineCont.x = 125;
			alaska_lineCont.y = -96;

			/*
			alaska_holder.addChild(extraLine01_black);
			alaska_holder.addChild(extraLine01_red);
			*/

			//CANADA
			canada_holder.x = 900;
			canada_holder.y = 209;
			canada_holder.addChild(canada_lineCont);
			canada_lineCont.x = 85;
			canada_lineCont.y = 115;

			//@CALIFORNIA
			for(places in californiaList){
				californiaList[places].alpha = 1; //debug
				california_holder.addChild(californiaList[places]);
			}

			//@ALASKA
			for(places in alaskaList){
				alaskaList[places].alpha = 1; //debug
				alaska_holder.addChild(alaskaList[places]);
			}

			//@CANADA
			for(places in canadaList){
				canadaList[places].alpha = 1; //debug
				canada_holder.addChild(canadaList[places]);
			}

			/*mapContainer.x = 130;
			mapContainer.y = 130;*/

			mapContainer.addChild(california_holder);
			mapContainer.addChild(alaska_holder);
			mapContainer.addChild(canada_holder);
			mapContainer.addChild(popUpHolder);

			stage.addChild(mapContainer);/*
			stage.addChild(popUpHolder);*/
			//before rendering
			hasMap = true;
		},

		extractData : function(){
			//ocal request
			/*var e = {
				handleFileLoaded : function(evt){
					JSON = eval("("+ evt.rawResult +")");
					Map.renderAll();
				},
				errorHandler : function(){
					console.log("JSON.errorHandler");
				}
			}
			DGpreload = new createjs.LoadQueue(true, "assets/static/");
			DGpreload.loadFile("sampleData.json",true);
			DGpreload.on("fileload", e.handleFileLoaded);
			DGpreload.on("error", e.errorHandler);*/

			//online request

			$.get( myUrl, function(data){
				JSON = data;
				Map.renderAll();
			});
		},

		translatePercentage: function(userPoint, startPoint, maxPoint, benchmark, reverse ) {
			var percent;
			percent      = ( (userPoint-startPoint) / (maxPoint-startPoint) ) * 100;
			//decimal
			if( reverse ) {
				percent = 1 - ( percent/100 );
			} else {
				percent = ( percent/100 );
			}
			return percent;
		},

		checkNode : function (place, maxPoint, uPnt ) {
			//@CALIFORNIA
			if(place==MY_CALIFORNIA) {
				//reset
				californiaList["creativity"].gotoAndStop("show");
				californiaList["testTheEngine"].gotoAndStop("show");
				californiaList["riseToTheTop"].gotoAndStop("show");

				if(uPnt > 0/*CALIFORIA_STARTPOINT*/ ) {
					californiaList["creativity"].gotoAndStop("check");
					node01_color = "#ff0000";
				}
				if(uPnt > 0/*CALIFORNIA_MAXPOINTS/2*/ ) {
					californiaList["testTheEngine"].gotoAndStop("check");
					node02_color = "#ff0000";
				}
				if(uPnt>= 0/*CALIFORNIA_MAXPOINTS*/ ) {
					californiaList["riseToTheTop"].gotoAndStop("check");
					node03_color = "#ff0000";
				}
			}
			//@ALASKA
			if(place == MY_ALASKA) {
				alaskaList["tackleTheRain"].gotoAndStop("show");
				alaskaList["captureTheMoment"].gotoAndStop("show");
				alaskaList["commandTheSnow"].gotoAndStop("show");

				if(uPnt >= ALASKA_STARTPOINT ) {
					alaskaList["tackleTheRain"].gotoAndStop("check");
					node04_color = "#ff0000";

					alaskaList["captureTheMoment"].gotoAndStop("check");
					node05_color = "#ff0000";

					alaskaList["commandTheSnow"].gotoAndStop("check");
					node06_color = "#ff0000";
				}

				/*if(uPnt >  ALASKA_MAXPOINTS * 0.75 ) {
					alaskaList["captureTheMoment"].gotoAndStop("check");
					node05_color = "#ff0000";
				}
				if(uPnt >= ALASKA_MAXPOINTS ) {
					alaskaList["commandTheSnow"].gotoAndStop("check");
					node06_color = "#ff0000";
				}*/
			}

			//@CANADA
			if(place == MY_CANADA){
				canadaList["scaleTheMountain"].gotoAndStop("show");
				canadaList["IntoTheIce"].gotoAndStop("show");
				canadaList["MasterTheRapids"].gotoAndStop("show");

				if(uPnt >= CANADA_STARTPOINT ) {
					canadaList["scaleTheMountain"].gotoAndStop("check");
					node07_color = "#ff0000";

					canadaList["IntoTheIce"].gotoAndStop("check");
					node08_color = "#ff0000";

					canadaList["MasterTheRapids"].gotoAndStop("check");
					node09_color = "#ff0000";
				}

				/*if(uPnt >= 4700 ) {
					canadaList["IntoTheIce"].gotoAndStop("check");
					node08_color = "#ff0000";
				}
				if(uPnt >= CANADA_MAXPOINTS ) {
					canadaList["MasterTheRapids"].gotoAndStop("check");
					node09_color = "#ff0000";
				}*/
			}
		},

		locationStatus : function() {
			var californiaTrail = Map.translatePercentage( gUserPoint, CALIFORIA_STARTPOINT,   CALIFORNIA_MAXPOINTS, Benchmark_california, true );
				cali_mask_shp.set({ scaleY: californiaTrail });
				Map.checkNode( MY_CALIFORNIA , CALIFORNIA_MAXPOINTS, gUserPoint );

			var alaskaTrail     = Map.translatePercentage( gUserPoint, ALASKA_STARTPOINT, ALASKA_MAXPOINTS, Benchmark_alaska, true );
				alaska_mask_shp.set({ scaleY: alaskaTrail });
				Map.checkNode( MY_ALASKA, ALASKA_MAXPOINTS, gUserPoint );

			var canadaTrail     = Map.translatePercentage( gUserPoint, CANADA_STARTPOINT, CANADA_MAXPOINTS, Benchmark_canada, true );
				canada_mask_shp.set({ scaleY: canadaTrail });
				Map.checkNode( MY_CANADA, CANADA_MAXPOINTS, gUserPoint );
		},

		fixText : function (){
			var hOffset = 9;
			var wOffset = 10;
			//@CALIFORNIA

			lbl_california_shp01.graphics.clear();
			lbl_california_shp01.graphics.beginFill(node01_color).drawRect(0,0,txt01.getMeasuredWidth()+wOffset, txt01.getMeasuredHeight() + hOffset );
			lbl_california_shp02.graphics.clear();
			lbl_california_shp02.graphics.beginFill(node02_color).drawRect(0,0,txt02.getMeasuredWidth()+wOffset, txt02.getMeasuredHeight() + hOffset);
			lbl_california_shp03.graphics.clear();
			lbl_california_shp03.graphics.beginFill(node03_color).drawRect(0,0,txt03.getMeasuredWidth()+wOffset, txt03.getMeasuredHeight() + hOffset);
			//@ALASKA
			lbl_alaska_shp01.graphics.clear();
			lbl_alaska_shp01.graphics.beginFill(node04_color).drawRect(0,0,txt04.getMeasuredWidth()+wOffset, txt04.getMeasuredHeight() + hOffset);
			lbl_alaska_shp02.graphics.clear();
			lbl_alaska_shp02.graphics.beginFill(node05_color).drawRect(0,0,txt05.getMeasuredWidth()+wOffset, txt05.getMeasuredHeight() + hOffset);
			lbl_alaska_shp03.graphics.clear();
			lbl_alaska_shp03.graphics.beginFill(node06_color).drawRect(0,0,txt06.getMeasuredWidth()+wOffset, txt06.getMeasuredHeight() + hOffset);
			//@CANADA
			lbl_canada_shp01.graphics.clear();
			lbl_canada_shp01.graphics.beginFill(node07_color).drawRect(0,0,txt07.getMeasuredWidth()+wOffset, txt07.getMeasuredHeight() + hOffset);
			lbl_canada_shp02.graphics.clear();
			lbl_canada_shp02.graphics.beginFill(node08_color).drawRect(0,0,txt08.getMeasuredWidth()+wOffset, txt08.getMeasuredHeight() + hOffset);
			lbl_canada_shp03.graphics.clear();
			lbl_canada_shp03.graphics.beginFill(node09_color).drawRect(0,0,txt09.getMeasuredWidth()+wOffset, txt09.getMeasuredHeight() + hOffset);

			if(gUserPoint>ALASKA_MAXPOINTS) {
				extraLine01_black.visible=false;
				extraLine01_red.visible=true;
			}
		},

		resetBtnByNode : function( arrList, activity ) {
			var region;
			for ( node in arrList ){
				region = arrList[node].place;
				if( arrList[node].activity != activity ){

					if( arrList[node].currentAnimation=="check" || arrList[node].pt < gUserPoint ){
						arrList[node].gotoAndStop("check");
					}else{
						arrList[node].gotoAndStop("show");
					}
				}
			}
		},

		resetBtnByRegion : function( region ){
			if(region!=MY_CALIFORNIA){
				Map.checkNode( MY_CALIFORNIA , CALIFORNIA_MAXPOINTS, gUserPoint );
			}
			if(region!=MY_ALASKA){
				Map.checkNode( MY_ALASKA , ALASKA_MAXPOINTS, gUserPoint );
			}
			if(region!=MY_CANADA){
				Map.checkNode( MY_CANADA , CANADA_MAXPOINTS, gUserPoint );
			}
		},

		listen : function( arrList, region ){
			for ( place in arrList ){
				//over
				arrList[place].on("rollover", function (evt) {
            		evt.target.cursor = 'pointer';
            		isOverNodes = true;
     				mapContainer.cursor = 'pointer';
            	});
				//out
				arrList[place].on("rollout", function (evt) {
            		evt.target.cursor = 'pointer';
            		isOverNodes = false;
            		mapContainer.cursor = 'default';
            	});

				//click
				arrList[place].on("mousedown", function (evt) {
					evt.target.cursor = 'pointer';

					if(enable_mouseDown){
	            		if(	gUserPoint >= evt.target.pt ){
							if( evt.target.currentAnimation=="hide"){
								evt.target.gotoAndStop("check");
							} else if( evt.target.currentAnimation=="check"){
								evt.target.gotoAndStop("hide");
							}
						} else {
							if( evt.target.currentAnimation=="show"){
								evt.target.gotoAndStop("hide");
							} else if(evt.target.currentAnimation=="hide"){
								evt.target.gotoAndStop("show");
							}
						}

						//WINDOW_WHITE
						Map.openPopUpWhite( region, evt.target.activity, evt.target.challenge, evt.target.pt, evt.target.num );
						currActivity   = evt.target.activity;
						currLoc        = region;
						currBtnClicked = evt.target;

						//BTN RESET
						Map.resetBtnByNode( arrList, currActivity );
						Map.resetBtnByRegion( region );
						
						//ANIMATE
						currBtnOldX = evt.target.x;
						currBtnOldY = evt.target.y;

						/*evt.target.regY = 15;
						evt.target.regX = 15;

						evt.target.x += 15;
						evt.target.y += 15;*/

						evt.target.isRotate = (evt.target.isRotate) ? false : true;
						var rv = (evt.target.isRotate)? 180 : -180; 

						var animate = {
							handleComplete : function (evt) {
								evt.target.rotation = 0;
								createjs.Tween.removeTweens(evt.target);
					    	}
						}

						createjs.Tween.get(evt.target)
				       .to({ rotation: rv }, 200 )
				       .call( animate.handleComplete );
					}
        		});
			}

			//BUTTON X ADDITIONAL
			btnX.on("rollover", function (evt) {
        		evt.target.cursor = 'pointer';
        		isOverNodes = true;
 				mapContainer.cursor = 'pointer';
        	});

			btnX.on("rollout", function (evt) {
        		evt.target.cursor = 'pointer';
        		isOverNodes = false;
        		mapContainer.cursor = 'default';
        	});
			
        	btnX.on("mousedown", function (evt) {
        		popUpHolder.visible = false;
        		//MAJOR RESET
        		evt.target.cursor = 'pointer';
				Map.resetBtnByRegion( MY_CALIFORNIA );
				Map.resetBtnByRegion( MY_ALASKA );
				Map.resetBtnByRegion( MY_CANADA );
				
				var animate = {
					handleComplete : function (evt) {
						evt.target.rotation = 0;
						createjs.Tween.removeTweens(evt.target);
			    	}
				}

				createjs.Tween.get( currBtnClicked )
		        .to({ rotation: -180, 
		        	         x: currBtnClicked.x , 
		        	         y: currBtnClicked.y }, 250 )
		        .call( animate.handleComplete );   
        	});
		},

		initListeners : function() {
			Map.listen( californiaList, MY_CALIFORNIA );
			Map.listen( alaskaList, MY_ALASKA );
			Map.listen( canadaList, MY_CANADA );

			var _delay;

			//event
			takeChallengeSpt.on("rollover", function (evt) { 
				mapContainer.cursor = 'pointer';
			});

			takeChallengeSpt.on("rollout", function (evt) {
				mapContainer.cursor = 'default';
			});

			takeChallengeSpt.on("mousedown", function (evt) {

			_delay = setTimeout(function(){
				clearTimeout(_delay);
				 isMouseDown = false;
			},100);

				if( gUserPoint < selectedMaxPointRef ){
					errorTxt.visible    = true;
					errorWindow.visible = true;

					errorDelay = setTimeout(function(){
						clearTimeout(errorDelay);
						errorTxt.visible    = false;
						errorWindow.visible = false;
					},2000);

				} else {
					window.open( JSON[gcurrIndex]["url"] );
				}
			});

			mapContainer.on("mousedown", function (evt) {
				this.offset = {x: this.x - evt.stageX, y: this.y - evt.stageY};
				isMouseDown = true;
			});

			mapContainer.on("pressup", function(){
				isMouseDown = false;
				mapContainer.cursor = "default";
			});

			mapContainer.on("pressmove", function (evt) {
				
				if(isMouseDown){
					mapContainer.cursor = "move";

					var x = evt.stageX + this.offset.x;
					var y =  evt.stageY + this.offset.y;

					var maxX = canvas.width - mapContainer.getBounds().width;
					var maxY = canvas.height- mapContainer.getBounds().height;

					this.x = x;
					this.y = y;

					if(x>0 && x>maxX){
						this.x = 0;
					}
					if(x<=maxX) {
						this.x = maxX;
					}

					if(y>0 && y>maxY){
						this.y = 0;
					}
					if(y<=maxY) {
						this.y = maxY;
					}
				}
			});
		},

		renderAll : function(){
			gUserPoint = 0; //JSON[9]["total_km_points"]; /*console.log("currentScore:", JSON[9]["total_km_points"]);*/

			var maxPoint = JSON[9]["total_km_points"];

			var animDelay = setInterval(function(){
				
				if( gUserPoint < maxPoint ){
					gUserPoint += 10;
					console.log("call");
					Map.locationStatus();
				}else{
					clearInterval(animDelay);
					enable_mouseDown = true;
				}
			},16);


			Map.initMap();

			Map.iniTrail();

			Map.initRoute();

			Map.initpopUpModel();

			Map.initListeners();

			Map.allToStage();

		}
	}

		//LOAD JSON FIRST
		Map.extractData();


	this.onResize = function(){

		if(wW < 1920){
			$("#xOverMap").attr('width', wW);
		}else {
			$("#xOverMap").attr('width', 1920);
		}

		if(wH < canvas.height){
			$("#xOverMap").attr('height', wH);
		}

		if(wH > canvas.height ){
			$("#xOverMap").attr('height', $('.kmap-wrapper').height() );
		}
	}

	window.onresize = function(){
		self.onResize();
	}

	self.onResize();
}

var gUserPoint;
var isMouseDown = false;
var isOverNodes = false;
var wW = window.innerWidth;
var wH = window.innerHeight;
















///////////////////////////////////////
//
//	GLOBAL TICKER
//
///////////////////////////////////////
createjs.Ticker.addEventListener("tick", renderer);
function renderer(event) {
    if(isReady) stage.update();
    if(hasMap) Map.fixText();
}

