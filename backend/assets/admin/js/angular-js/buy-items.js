angular.module('fileUpload', [ 'angularFileUpload' ]);

var MyCtrl = [ '$scope', '$http', '$timeout', '$upload',  function($scope, $http, $timeout, $upload) {
	$scope.fileReaderSupported = window.FileReader != null;
	$scope.uploadRightAway = true;

	$scope.upload = [];
	$scope.uploadResult = [];
	$scope.selectedFiles = [];
	$scope.progress = [];
	$scope.dataUrls = [];
	$scope.setOfUpload = 0;
	$scope.invalidUpload = [];

	$scope.changeAngularVersion = function() {
		window.location.hash = $scope.angularVersion;
		window.location.reload(true);
	}
	$scope.hasUploader = function(index) {
		return $scope.upload[index] != null;
	};
	$scope.abort = function(index) {
		$scope.upload[index].abort(); 
		$scope.upload[index] = null;
	};
	$scope.angularVersion = window.location.hash.length > 1 ? window.location.hash.substring(1) : '1.2.0';
	$scope.onFileSelect = function($files) {
		
		// if ($scope.upload && $scope.upload.length > 0) {
		// 	for (var i = 0; i < $scope.upload.length; i++) {
		// 		if ($scope.upload[i] != null) {
		// 			$scope.upload[i].abort();
		// 		}
		// 	}
		// }
		
		var j = $scope.setOfUpload ? $scope.selectedFiles.length : 0;
		for (key in $files) {
			$scope.selectedFiles.push($files[key]);
		}
		for ( var i = 0; i < $files.length; i++) {
			var k = j + i;
			var $file = $files[i];
			var valid = false;
			if (window.FileReader && $file.type.indexOf('image') > -1 && $file.size < ( 1024 * 1024 * 3)) {
				var fileReader = new FileReader();
				fileReader.readAsDataURL($file);
				var loadFile = function(fileReader, index) {
					fileReader.onload = function(e) {
						$timeout(function() {
							$scope.dataUrls[index] = e.target.result;
						});
					}
				}(fileReader, k);
				valid = true;
			} else if ($file.type.indexOf('video') > -1 && $file.size < ( 1024 * 1024 * 10)) {
				valid = true;
			}

			if (valid) {
				$scope.progress[k] = -1;
				if ($scope.uploadRightAway) {
					$scope.start(k);
				}
			} else {
				$scope.selectedFiles.splice(k, 1);
			}
		}
		$scope.setOfUpload += 1;
	}

	$scope.remove = function(index) {
		$scope.selectedFiles.splice(index, 1);
		$scope.dataUrls.splice(index, 1);
		$scope.progress.splice(index, 1);
		$scope.uploadResult.splice(index, 1);
	}
	
	$scope.start = function(index) {
		$scope.progress[index] = 0;
		if ($scope.howToSend == 1) {
			$scope.upload[index] = $upload.upload({
				url : uploadURL,
				method: $scope.httpMethod,
				headers: {'my-header': 'my-header-value'},
				data : {
					myModel : $scope.myModel
				},
				/* formDataAppender: function(fd, key, val) {
					if (angular.isArray(val)) {
                        angular.forEach(val, function(v) {
                          fd.append(key, v);
                        });
                      } else {
                        fd.append(key, val);
                      }
				}, */
				/* transformRequest: [function(val, h) {
					console.log(val, h('my-header')); return val + 'aaaaa';
				}], */
				file: $scope.selectedFiles[index],
				fileFormDataName: 'myFile'
			}).then(function(response) {
				$scope.uploadResult[index] = response.data;
			}, null, function(evt) {
				$scope.progress[index] = parseInt(100.0 * evt.loaded / evt.total);
			}).xhr(function(xhr){
				xhr.upload.addEventListener('abort', function(){console.log('aborted complete')}, false);
			});
		} else {
			var fileReader = new FileReader();
            fileReader.onload = function(e) {
		        $scope.upload[index] = $upload.http({
		        	url: 'upload',
					headers: {'Content-Type': $scope.selectedFiles[index].type},
					data: e.target.result
				}).then(function(response) {
					$scope.uploadResult.push(response.data);
				}, null, function(evt) {
					// Math.min is to fix IE which reports 200% sometimes
					$scope.progress[index] = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
				});
            }
	        fileReader.readAsArrayBuffer($scope.selectedFiles[index]);

		}
	}
} ];
