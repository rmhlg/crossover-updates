angular.module('fileUpload', [ 'angularFileUpload' ]);

var MyCtrl = [ '$scope', '$http', '$timeout', '$upload',  function($scope, $http, $timeout, $upload) {
	$scope.fileReaderSupported = window.FileReader != null;
	$scope.uploadRightAway = true; 

	$scope.hasUploader = function(index) {
		return $scope.upload[index] != null;
	};
	$scope.abort = function(index) {
		$scope.upload[index].abort(); 
		$scope.upload[index] = null;
	};
	$scope.onFileSelect = function($files) {
		$scope.selectedFiles = [];
		$scope.progress = [];
		$scope.uploadError = [];
 		if ($scope.upload && $scope.upload.length > 0) {
			for (var i = 0; i < $scope.upload.length; i++) {
				if ($scope.upload[i] != null) {
					$scope.upload[i].abort();
				}
			}
		}
		$scope.upload = [];
		$scope.selectedFiles = $files;
		$scope.dataUrls = [];
		$scope.uploadResult = [];
		for ( var i = 0; i < $files.length; i++) {
			var $file = $files[i];
			$scope.progress[i] = -1;
			$scope.start(i);		
		}

	}

	$scope.remove = function(index) {

  		var media = $scope.uploadResult[index];

		if(media){
			$http({url: $scope.removeURL,method:'POST',headers: {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'},data:media})
				.success(function(response){
					console.log(response);
				});
		}

		$scope.selectedFiles.splice(index, 1);
		$scope.dataUrls.splice(index, 1);
		$scope.progress.splice(index, 1);
		$scope.uploadError.splice(index, 1);
		$scope.uploadResult.splice(index,1);

	}
	
	$scope.start = function(index) {
		$scope.progress[index] = 0;
		$scope.upload[index] = $upload.upload({
			url : $scope.uploadURL,
			method: 'POST',
			headers: {'my-header': 'my-header-value'},
			data : {
				myModel : $scope.myModel
			},
			file: $scope.selectedFiles[index],
			fileFormDataName: 'myFile'
		}).then(function(response) {
			var response = response.data;

			if(response.upload_error){
				$scope.uploadError[index] =  response.upload_error;
				$scope.progress[index] = -1;
			}else{
				$scope.dataUrls[index] = response.image_thumbnail;
			}

			$scope.uploadResult[index] = JSON.stringify(response.upload_result);

		}, null, function(evt) {
			$scope.progress[index] = parseInt(100.0 * evt.loaded / evt.total);
		}).xhr(function(xhr){
			xhr.upload.addEventListener('abort', function(){console.log('aborted complete')}, false);
		});

	}

	$scope.submit_media = function(){

	}
} ];
