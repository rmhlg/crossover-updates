angular.module('apiService', [])
 
	.factory('Experience', function($resource) {
	 return $resource('../api/cms/experiences',{id:'@_id'},{
	 	update: {
	      method: 'PUT'
	    },
	    delete: {
	    	method:'DELETE',
	    	headers:{'Content-Type': 'application/json'},
	    	params: {id: '@_id'} 
	    }
	 }); // Note the full endpoint address
	});

