'use strict';

var CMSApp = angular.module('activityModule',['angularFileUpload','textAngular']);


CMSApp.controller('activityCtrl',['$rootScope','$scope','$upload','$timeout','$http','$window',function($rootScope,$scope,$upload,$timeout,$http,$window){

 
 	$scope.assetBaseURL = '../assets/';
  	$scope.siteURL = '../';
  	$scope.media_files = [];
  	$scope.current_media_files =[];

 

	$scope.abort = function(index) {
		$scope.upload[index].abort(); 
		$scope.upload[index] = null;
	};

	$scope.onFileSelect = function($files)
	{
		$scope.selectedFiles = $files;
		$scope.upload = [];		
		$scope.dataUrls = [];
		$scope.progress = [];
		$scope.errorUpload = [];

		if ($scope.upload && $scope.upload.length > 0) {
			for (var i = 0; i < $scope.upload.length; i++) {
				if ($scope.upload[i] != null) {
					$scope.upload[i].abort();
				}
			}
		}

		for ( var i = 0; i < $files.length; i++) {
			var $file = $files[i];
			if (window.FileReader && $file.type.indexOf('image') > -1) {
				var fileReader = new FileReader();
				fileReader.readAsDataURL($files[i]);
				var loadFile = function(fileReader, index) {
					fileReader.onload = function(e) {
						$timeout(function() {
							$scope.dataUrls[index] = e.target.result;
						});
					}
				}(fileReader, i);
			}
			$scope.progress[i] = -1;
 			$scope.start(i);
		}


	};

	$scope.onFileSelectDecisionLeft = function($files)
	{
		$scope.selectedFilesDecisionLeft = $files;
		$scope.upload = [];		
		$scope.dataUrls = [];
		$scope.progress = [];
		$scope.errorUpload = [];

		if ($scope.upload && $scope.upload.length > 0) {
			for (var i = 0; i < $scope.upload.length; i++) {
				if ($scope.upload[i] != null) {
					$scope.upload[i].abort();
				}
			}
		}

		for ( var i = 0; i < $files.length; i++) {
			var $file = $files[i];
			if (window.FileReader && $file.type.indexOf('image') > -1) {
				var fileReader = new FileReader();
				fileReader.readAsDataURL($files[i]);
				var loadFile = function(fileReader, index) {
					fileReader.onload = function(e) {
						$timeout(function() {
							$scope.dataUrls[index] = e.target.result;
						});
					}
				}(fileReader, i);
			}
			$scope.progress[i] = -1;
 			$scope.startDecisionLeft(i);
		}


	};

	$scope.startDecisionLeft = function(index) {

		$scope.progress[index] = 0;

		$scope.upload[index] = $upload.upload({
			url : $scope.siteURL+'activity/upload',
			method: 'POST',
			headers: {'my-header': 'my-header-value'},
			file: $scope.selectedFiles[index],
			fileFormDataName: 'image'
		}).then(function(response) {
			var response = response.data;
			
			if(!response.error){
				$scope.media_files[index] = response.new_image;
			}else{
				$scope.errorUpload[index] = response.error;
			}
 
		}, null, function(evt) {
			$scope.progress[index] = parseInt(100.0 * evt.loaded / evt.total);
		}).xhr(function(xhr){
			xhr.upload.addEventListener('abort', function(){console.log('aborted complete')}, false);
		});

	}

	$scope.start = function(index) {

		$scope.progress[index] = 0;	

		$scope.upload[index] = $upload.upload({
			url : $scope.siteURL+'activity/upload',
			method: 'POST',
			headers: {'my-header': 'my-header-value'},
			file: $scope.selectedFiles[index],
			fileFormDataName: 'image'
		}).then(function(response) {
			var response = response.data;
			
			if(!response.error){
				$scope.media_files[index] = response.new_image;
			}else{
				$scope.errorUpload[index] = response.error;
			}
 
		}, null, function(evt) {
			$scope.progress[index] = parseInt(100.0 * evt.loaded / evt.total);
		}).xhr(function(xhr){
			xhr.upload.addEventListener('abort', function(){console.log('aborted complete')}, false);
		});

	}


	$scope.remove = function(index) {

		
		 
		$scope.selectedFiles.splice(index, 1);
		$scope.dataUrls.splice(index, 1);
		$scope.progress.splice(index, 1);
		$scope.errorUpload.splice(index, 1);
		$scope.media_files.splice(index,1);
		console.log($scope.media_files);
 
	}

	$scope.removeCurrent = function(index){

 		$scope.current_media_files.splice(index,1);


	}


	$scope.addItem = function(){

		
		var current = $scope.current_media_files;
		var new_obj = $scope.media_files;

		angular.forEach(new_obj,function(value,key){
			current.push(value);
		});

		$scope.item.medias = current;
 

		$http.post($scope.siteURL+'api/activity',$scope.item).success(function(response){
				if(response.success){
					$window.location.href = $scope.siteURL+'activity';
				}else{
					$scope.errorUpload = response.message;
				}
		});

	}


}]);