var AppControllers = angular.module('AppControllers',['apiService','angularFileUpload','ngDialog','textAngular']);

AppControllers.controller('mainCtrl',['$rootScope','$scope','$route','$http','$window','$resource',function($rootScope,$scope,$route,$http,$window,$resource){

	$rootScope.$on('$routeChangeStart',function(){

	});

	$rootScope.$on('$routeChangeSuccess',function(){
	 	
	 	$rootScope.currentPage = $route.current.title;
		
		$http.get('../api/cms/login_check').success(function(data){
			if(data.success=='0'){
				$rootScope.login_status = false;
  				$window.location.href = '#/login';
			}else{
				$rootScope.login_status = true;
			}
		});

	});
	 
	
}]);

AppControllers.controller('loginCtrl',['$rootScope','$scope','$http','$window',function($rootScope,$scope,$http,$window){


 	$scope.login = function()
	{

		var param = $scope.user;
		$scope.login_message = false;

		$http.post('../api/cms/login',param).success(function(data){

			if(data.success){
				$rootScope.login_status=true;
				$window.location.href = '#/home';
			}else{
				$rootScope.login_status = false;
				$scope.login_message = data.message;
			}
		});

	}
	
 	 
}]);


AppControllers.controller('homeCtrl',['$rootScope','$scope','$http',function($rootScope,$scope,$http){

 
}]);


AppControllers.controller('experienceCtrl',['$rootScope','$scope','$http','Experience',function($rootScope,$scope,$http,Experience){

	$rootScope.experiencePage = "active";
	$scope.items = [];

	$scope.loadItems = function(){
		$scope.items = Experience.query();
	};

	$scope.loadItems();


}]);

AppControllers.controller('experienceAddCtrl',['$rootScope','$scope','$http','Experience',function($rootScope,$scope,$http,Experience){

	 


}]);
