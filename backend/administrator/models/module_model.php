<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Module_Model extends CI_Model {
	
	var $_access;
	public function __construct() {
		parent::__construct();
		$module = $this->uri->segment(1);
		$section = is_numeric($this->uri->segment(2)) || !$this->uri->segment(2) ? 'view' : $this->uri->segment(2);
		$section = $section == 'approve' || $section == 'disapprove' || $section == 'activate' || $section == 'deactivate' ? 'edit' : $section;
		
		//$this->_access = $this->check_all_access(); 
		if($this->has_module_restriction($this->uri->segment(1)) && isset($this->_access[$module][$section]) && !$this->_access[$module][$section])
			redirect('');
		
	}
	
	private function has_module_restriction($module) {
		$modules = array('accounts', '', 'auth', 'module', 'ajax', 'export', 'settings', 'cron');
		return !in_array($module, $modules);
	}
	
	public function save_module($is_update = false) {
		$name = $this->input->post('module_name');
		$uri = $this->input->post('uri');
		$group = $this->input->post('group');
		$id = $this->input->post('id');
		$post['module_name'] = $name;
		$post['uri'] = $uri;
		$post['group'] = $group;
		if(!$is_update) {
			$this->db->insert('tbl_modules', $post);
			$id = $this->db->insert_id();
		} else {
			$this->db->where('module_id', $id)
					 ->update('tbl_modules', $post);
		}
		$this->save_module_permissions($id);	
	}
	
	public function save_module_permissions($id) {
		$users = $this->db->select('*')
							  ->from('tbl_cms_users')
							  ->get()
							  ->result_array();	
		if($users) {
			foreach($users as $k => $v) {
				$post['cms_user_id'] = $v['cms_user_id'];
				$post['module_id'] = $id;
				$this->db->insert('tbl_permissions', $post);
			}
		}
	}
	
	public function get_module($where = '', $page = 1, $per_page = PER_PAGE, &$records = 0) {
		$user = array();
		$offset = ($page - 1) * $per_page;
		if(is_array($where)) {
			$where['`group` !='] = 0;
			$this->db->limit($per_page, $offset);
			$user = $this->db->get_where('tbl_modules', $where);
			$this->db->get_where('tbl_modules', $where);
			$records = $this->db->count_all_results();
		} else {
			$query = "SELECT * FROM tbl_modules WHERE is_deleted = 0 AND `group` != 0 " . $where . " ORDER BY `group` LIMIT $offset, $per_page";
			$user = $this->db->query($query);
			$query = "SELECT COUNT(*) AS num_rows FROM tbl_modules WHERE is_deleted = 0 AND `group` != 0 " . $where;
			$records =  $this->db->query($query)->row_array()['num_rows'];
		}
		return $user->result_array();
	}
	
	public function get_all_module() {
		$where = "is_deleted = 0 AND `group` != 0 AND uri != 'module' AND uri != 'user'";
		$modules = $this->db->select('*')
							->order_by('group')
							->from('tbl_modules')
							->where($where)
							->get();
		return $modules->result_array();
	}
	
	public function get_module_group() {
		return array(USER_MANAGEMENT => 'User Management', CONTENT_MANAGEMENT => 'Content Management', MODERATOR => 'Moderator', REPORTS => 'Report');
	}
	
	
	public function check_access($module = '') {
		$user = $this->login_model->extract_user_details();
		if($module)
			$where['uri'] = $module;
		$where['cms_user_id'] = $user['cms_user_id'];
		/*if($section == 'approve' || $section == 'disapprove' || $section == 'activate' || $section == 'deactivate')
			$where['edit'] =  1;*/
		unset($where['approve']);
		unset($where['activate']);
		unset($where['deactivate']);
		unset($where['disapprove']);
		$access = $this->db->select('*')
						   ->from('vw_module_permissions')
						   ->where($where)
						   ->get()
						   ->result_array();
		return @$access[0];	
	} 
	
	public function check_all_access() {
		$user = $this->login_model->extract_user_details();
		$where['cms_user_id'] = $user['cms_user_id'];
		/*if($section == 'approve' || $section == 'disapprove' || $section == 'activate' || $section == 'deactivate')
			$where['edit'] =  1;*/
		
		unset($where['approve']);
		unset($where['activate']);
		unset($where['deactivate']);
		unset($where['disapprove']);
		$access = $this->db->select('*')
						   ->from('vw_module_permissions')
						   ->where($where)
						   ->get()
						   ->result_array();
		//print_r($where);
		$accesses = array();
		if($access) {
			foreach($access as $k => $v) {
				$accesses[$v['uri']] = $v;
			}
		}
		return $accesses;	
	} 
	
	public function save_audit_trail($data) {
	/*	var_dump($this->session->userdata('logged'));
		if(isset($this->session->userdata('logged')))  ){
		$user = $this->login_model->extract_user_details();
	}*/
		$post = $data;
		$post['ip']	= $_SERVER['REMOTE_ADDR'];
		$post['activity'] = $post['description'];
		/*if($user) {
			$post['cms_user_id'] = $user['cms_user_id'];
			$post['token'] = $user['cms_user_id'];
		}*/
		$this->db->insert('tbl_audit_trail', $this->process_fields($post));
	}
	
	
	public function get_audit_trail($where = '', $page = 1, $per_page = PER_PAGE, &$records = 0, $is_all = false) {
		$user = array();
		$offset = ($page - 1) * $per_page;
		$table = 'vw_audit_trail';
		if(is_array($where)) {
			if($is_all)
				$this->db->limit($per_page, $offset);
			$user = $this->db->get_where($table, $where);
			$this->db->get_where($table, $where);
			$records = $this->db->count_all_results();
		} else {
			$query = !$is_all ?  "SELECT * FROM $table WHERE 1 " . $where . " ORDER BY timestamp DESC LIMIT $offset, $per_page" : "SELECT * FROM $table WHERE 1 " . $where . " ORDER BY timestamp DESC";
			$user = $this->db->query($query);
			$query = "SELECT COUNT(*) AS num_rows FROM $table WHERE 1 " . $where;
			$records =  $this->db->query($query)->row_array()['num_rows'];
		}
		$at = $user->result_array();
		$result = array();
		$cms_users = $this->get_cms_users();
		if($at) {
			foreach($at as $k => $v) {
				$v = $this->process_fields($v, 0);	
				$v['cms_user_name'] = $cms_users[$v['cms_user_id']];
				
				if($v['table'] != '' && $v['record_id'] != '' && isset($key[0]['Column_name'])) {
					
					$key = $this->db->query("SHOW KEYS FROM $v[table] WHERE Key_name = 'PRIMARY'")->result_array();
					$record = $this->db->get_where($v['table'], array($key[0]['Column_name']	=> $v['record_id']))->result_array();
					switch($v['table']) {
						case 'tbl_registrants':
							$v['record_name'] = @$record[0]['first_name'] . ' ' . @$record[0]['third_name'];
							break;
						case 'tbl_statements':
							$v['record_name'] = @$record[0]['statement'];
							break;
						case 'tbl_move_forward':
							$v['record_name'] = @$record[0]['move_forward_title'];
							break;
						case 'tbl_prizes':
							$v['record_name'] = @$record[0]['prize_name'];
							break;
						case 'tbl_birthday_offers':
							$v['record_name'] = @$record[0]['prize_name'];
							break;
						case 'tbl_flash_offers':
							$v['record_name'] = @$record[0]['prize_name'];
							break;
						default:
							$v['record_name'] = '';
							break;
					}
				}
			
				$result[] = $v;
			}
		}

		return $result;
	}
	
	public function get_module_names() {
		$modules = $this->get_all_module();
		$module_array = array();
		if($modules) {
			foreach($modules as $k => $v) {
				$module_array[$v['module_id']] = $v['module_name'];
			}
		}	
		return $module_array;
	}
	
	public function process_fields($array, $encrypt = 1) {
		$un_encryptable = array('audit_trail_id', 'timestamp', 'table', 'record_id');
		$esp_encryptable = array('token', 'activity');
		$new_array = array();
		$this->load->library('encrypt');
		if($array) {
			foreach($array as $k => $v) {
				if(!in_array($k, $un_encryptable)) {
					if(!in_array($k, $esp_encryptable)) {
						if($encrypt)
							$new_array[$k] = $this->encrypt->encode($v);
						else { 
							$new_array[$k] = $this->encrypt->decode($v);
						}
					} else {
						$new_array[$k] = md5($v . $this->config->item('encryption_key'));	
					}
				}
				else {
					$new_array[$k] = $v;
				}
			}
		} 
		return $new_array;
	}
	
	private function get_cms_users() {
		$users = $this->db->get_where('tbl_cms_users', array('is_deleted'	=> 0))->result_array();
		$new_array = array();
		if($users) {
			foreach($users as $k => $v) {
				$new_array[$v['cms_user_id']] = $v['name']; 
			}
		}
		$new_array[0] = 'Anonymous';
		return $new_array;
	}
	
	public function get_activities() {
		$activities = $this->db->select('description')
				 ->from('tbl_audit_trail')
				 ->group_by('activity')
				 ->get()
				 ->result_array();
		$activities_arr = array();
		if($activities) {
			foreach($activities as $k => $v) {
				$enc_desc = $v['description'];
				$v = $this->process_fields($v, 0);
				$v['enc_desc'] = $enc_desc;
				$activities_arr[] = $v;
			}
		}
		return $activities_arr;
	}
	
	public function get_nav_data() {
		$modules = $this->get_all_module();
		$module_array = array();
		$counts = array();
		if($modules) {
			foreach($modules as $k => $v) {
				if(isset($this->_access[$v['uri']]['view']) && $this->_access[$v['uri']]['view']) {
					$module_array[$v['group']][] = $v['uri'];
					switch($v['uri']) {
						case 'registrant':
							$this->db->where('status', PENDING_CSR)
									 ->from('tbl_registrants');
							$ctr = $this->db->count_all_results();
							$counts[$v['uri']] = $ctr;
							$module_array[$v['group']]['total'] = isset($module_array[$v['group']]['total']) ? $module_array[$v['group']]['total'] + $ctr : $ctr;
							break;
						case 'profile':
							$this->db->where(array('picture_status' => PENDING, 'new_picture <>' => ''))
									 ->from('tbl_registrants');
							$ctr = $this->db->count_all_results();
							$counts[$v['uri']] = $ctr;
							$module_array[$v['group']]['total'] = isset($module_array[$v['group']]['total']) ? $module_array[$v['group']]['total'] + $ctr : $ctr;
							break;
						case 'activity_log':
							$this->db->where('DATE(timestamp)', date('Y-m-d'))
									 ->from('tbl_audit_trail');
							$ctr = $this->db->count_all_results();
							$counts[$v['uri']] = $ctr;
							$module_array[$v['group']]['total'] = isset($module_array[$v['group']]['total']) ? $module_array[$v['group']]['total'] + $ctr : $ctr;
							break;
						case 'game_log':
							$this->db->where('DATE(date_played)', date('Y-m-d'))
									 ->from('tbl_game_scores');
							$ctr = $this->db->count_all_results();
							$counts[$v['uri']] = $ctr;
							$module_array[$v['group']]['total'] = isset($module_array[$v['group']]['total']) ? $module_array[$v['group']]['total'] + $ctr : $ctr;
							break;
						case 'statement':
							$this->db->where(array('DATE(date_created)' => date('Y-m-d'), 'is_deleted'	=> 0))
									 ->from('tbl_statements');
							$ctr = $this->db->count_all_results();
							$counts[$v['uri']] = $ctr;
							$module_array[$v['group']]['total'] = isset($module_array[$v['group']]['total']) ? $module_array[$v['group']]['total'] + $ctr : $ctr;
							break;
						case 'user_statement':
							$this->db->where('DATE(date_created)', date('Y-m-d'))
									 ->from('tbl_user_statements');
							$ctr = $this->db->count_all_results();
							$counts[$v['uri']] = $ctr;
							$module_array[$v['group']]['total'] = isset($module_array[$v['group']]['total']) ? $module_array[$v['group']]['total'] + $ctr : $ctr;
							break;
						case 'event_photos':
							$this->db->where(array('status' => '0', 'is_deleted' => '0'))
									 ->from('tbl_about_events_photos');
							$ctr = $this->db->count_all_results();
							$counts[$v['uri']] = $ctr;
							$module_array[$v['group']]['total'] = isset($module_array[$v['group']]['total']) ? $module_array[$v['group']]['total'] + $ctr : $ctr;
							break;
						case 'user_birthday':
							$this->db->where('prize_status', '0')
									 ->from('tbl_user_birthday_prizes');
							$ctr = $this->db->count_all_results();
							$counts[$v['uri']] = $ctr;
							$module_array[$v['group']]['total'] = isset($module_array[$v['group']]['total']) ? $module_array[$v['group']]['total'] + $ctr : $ctr;
							break;
						case 'user_videos':
							$this->db->where(array('status' => '0', 'is_deleted' => '0'))
									 ->from('tbl_about_videos');
							$ctr = $this->db->count_all_results();
							$counts[$v['uri']] = $ctr;
							$module_array[$v['group']]['total'] = isset($module_array[$v['group']]['total']) ? $module_array[$v['group']]['total'] + $ctr : $ctr;
							break;
						case 'comment':
							$this->db->where('comment_status','0')
									 ->from('tbl_comments');
							$ctr = $this->db->count_all_results();
							//die($ctr . 'a');
							$counts[$v['uri']] = $ctr;
							$module_array[$v['group']]['total'] = isset($module_array[$v['group']]['total']) ? $module_array[$v['group']]['total'] + $ctr : $ctr;
							break;
						case 'reply':
							$this->db->where('comment_reply_status','0')
									 ->from('tbl_comment_replies');
							$ctr = $this->db->count_all_results();
							//die($ctr . 'a');
							$counts[$v['uri']] = $ctr;
							$module_array[$v['group']]['total'] = isset($module_array[$v['group']]['total']) ? $module_array[$v['group']]['total'] + $ctr : $ctr;
							break;
						case 'move_fwd_gallery':
							$this->db->where('mfg_status','0')
									 ->from('tbl_move_forward_gallery');
							$ctr = $this->db->count_all_results();
							$counts[$v['uri']] = $ctr;
							$module_array[$v['group']]['total'] = isset($module_array[$v['group']]['total']) ? $module_array[$v['group']]['total'] + $ctr : $ctr;
							break;
						case 'move_fwd_prizes':
							$this->db->where(array('prize_delivered' => '0', 'move_forward_choice_status' => '1'))
									 ->from('tbl_move_forward_choice');
							$ctr = $this->db->count_all_results();
							$counts[$v['uri']] = $ctr;
							$module_array[$v['group']]['total'] = isset($module_array[$v['group']]['total']) ? $module_array[$v['group']]['total'] + $ctr : $ctr;
							break;
						case 'backstage_event_photos':

							$this->db->join('tbl_backstage_events e','e.backstage_event_id=p.backstage_event_id');
							$this->db->select('p.photo_id');
							$ctr = $this->db->get_where('tbl_backstage_events_photos as p',array('p.is_deleted'=>0,'e.is_deleted'=>0,'p.status'=>0))->num_rows();
							$counts[$v['uri']] = $ctr;
							$module_array[$v['group']]['total'] = isset($module_array[$v['group']]['total']) ? $module_array[$v['group']]['total'] + $ctr : $ctr;
							break;
						case 'user_flash_offers':
							$this->db->where('prize_status','0')
									 ->from('tbl_user_flash_prizes');
							$ctr = $this->db->count_all_results();
							$counts[$v['uri']] = $ctr;
							$module_array[$v['group']]['total'] = isset($module_array[$v['group']]['total']) ? $module_array[$v['group']]['total'] + $ctr : $ctr;
							break;

					}
				}
			}
		}
		$data['counts'] = $counts;
		$data['modules'] = $module_array;
		//$access = $this->check_access('user');
		$data['admin'] = isset($this->_access['user']['view']) ? $this->_access['user']['view'] : 0;
		/*echo '<pre>';
		print_r($data);
		die();*/
		return $data;
	}
	
	public function get_origins($exclude = null) {
		$origin[MOVE_FWD] = 'Move FWD';
		// $origin[EXPLORE] = 'Explore'; 
		$origin[BIRTHDAY_OFFERS] = 'Birthday Offers';
		$origin[FLASH_OFFERS] = 'Flash Offers';
 		$origin[ABOUT_NEWS] = 'About News';
		$origin[ABOUT_VIDEOS] = 'About Videos';
		$origin[ABOUT_PHOTOS] = 'About Photos';
		$origin[MOVE_FWD_GALLERY] = 'Move FWD Gallery';
		$origin[BACKSTAGE_PHOTOS] = 'Backstage Pass Photos';
		if($exclude) {
			foreach ($exclude as $value) {
				unset($origin[$value]);
			}
		}
		return $origin;
	}
}