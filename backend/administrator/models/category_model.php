<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Category_Model extends CI_Model {
	
	var $_table;
	public function __construct() {
		parent::__construct();
		$this->_table = 'tbl_categories';
	}
	
	public function save_category($is_update = false, &$error = false) {
		if($_FILES['photo']['name']) {
			$filename = $this->upload('photo', $error);
			if($error)
				return false;
			$post['category_image'] = $filename;
			$this->generate_thumb($filename);
		} else {
			if($is_update) 
				$this->generate_thumb($this->input->post('filename'));
		}
		$id = $this->input->post('id');
		$post['category_name'] = $this->input->post('category_name');
		$post['parent'] = $this->input->post('parent');
		$post['origin_id'] = $this->input->post('origin_id');
		$post['description'] = $this->input->post('description');
		$t_meta['x1'] = $this->input->post('x');
		$t_meta['y1'] = $this->input->post('y');
		$t_meta['x2'] = $this->input->post('x2');
		$t_meta['y2'] = $this->input->post('y2');
		$t_meta['width'] = $this->input->post('width');
		$t_meta['height'] = $this->input->post('height');
		$post['thumbnail_meta'] = serialize($t_meta);
		if(!$is_update) {
			$this->db->insert($this->_table, $post);
			$id = $this->db->insert_id();
		} else {
			$this->db->where('category_id', $id)
					 ->update($this->_table, $post);
		}
	}
	
	public function get_category($where = '', $page = 1, $per_page = PER_PAGE, &$records = 0, $all = false, $order = false) {
		$user = array();
		$offset = ($page - 1) * $per_page;
		if(is_array($where)) {
			if(!$all)
				$this->db->limit($per_page, $offset);
			$user = $this->db->get_where($this->_table, $where);
			
			$this->db->get_where($this->_table, $where);
			$records = $this->db->count_all_results();
		} else {
			if($order)
				$order = " ORDER BY " . $order;
			$limit = !$all ? " LIMIT $offset, $per_page" : ""; 
			$query = "SELECT * FROM $this->_table WHERE is_deleted = 0 " . $where . $order . $limit;
			$user = $this->db->query($query);
			$query = "SELECT COUNT(*) AS num_rows FROM $this->_table WHERE is_deleted = 0 " . $where;
			$records =  $this->db->query($query)->row_array()['num_rows'];
		}
		return $user->result_array();
	}
	
	private function upload($name, &$error = false) {
		$ext = end(explode('.', $_FILES[$name]['name']));
		$config['upload_path'] = './uploads/category';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size']	= '3072';
		$config['file_name']  = uniqid() . '.' . $ext;
		$data = array();

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload('photo')) {
			$error =  $this->upload->display_errors();
			return;
		}
		else {
			$data =  $this->upload->data();
		}
		return $data['file_name'];
	}
	
	private function generate_thumb($filename) {
		$src = 'uploads/category/' . $filename;
		$output_filename = 'uploads/category/thumb_' . $filename;
		list($width2, $height2) = getimagesize($src);
		$targ_w = $targ_h = 150;
		$jpeg_quality = 90;
		$width = $this->input->post('width');
		$height = $this->input->post('height');
		$img_r = imagecreatefromjpeg($src);
		
		$x = $this->input->post('x');
		$y =  $this->input->post('y');
		$x2 = $this->input->post('x2');
		$y2 = $this->input->post('y2');
		$new_point_x = ($x / $width) * $width2;
		$new_point_y = ($y / $width) * $height2;
		$new_width = ($x2 - $x);
		$new_height = ($y2 - $y);
		$dst_r = ImageCreateTrueColor($new_width, $new_height);
		
		imagecopyresampled($dst_r,$img_r,0,0,$new_point_x,$new_point_y, $width,$height,$width2,$height2);
		imagejpeg($dst_r, $output_filename, $jpeg_quality);
		
		
		$config['image_library'] = 'gd2';
		$config['source_image'] = $src;
		$config['create_thumb'] = FALSE;
		$config['maintain_ratio'] = TRUE;
		$config['width'] = 200;
		$config['height'] = 200;
		$config['new_image'] = 'uploads/category/resized_' . $filename;
		
		$this->load->library('image_lib', $config);
		$this->image_lib->resize();
 			
	}
	
	function get_parent_categories() {
		$child_categories = $this->db->select('*')
							   ->from($this->_table)
							   ->group_by('parent')
							   ->where('parent !=', 0)
							   ->get()
							   ->result_array();
		$parent_ids = array();
		if($child_categories) {
			foreach($child_categories as $k => $v) {
				$parent_ids[] = $v['parent'];
			}
		} else 
			return false;
		
		$parent_categories = $this->db->select('*')
									  ->from($this->_table)
									  ->where_in('category_id', $parent_ids)
									  ->order_by('category_name')
									  ->get()
									  ->result_array();
		
		return $parent_categories;
	}
}