<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Game_Model extends CI_Model {
	 
 	public function __construct()
	{
		parent::__construct(); 
	}
	
	public function get_game($where = '', $page = 1, $per_page = PER_PAGE, &$records = 0, $all = false, $order = 'date_played DESC') {
		$user = array();
		$offset = ($page - 1) * $per_page;
		if($this->input->get('field')) {
			if($this->input->get('sort')) {
				$order = $this->input->get('field') . ' ' . $this->input->get('sort');
			} else {
				$order = $this->input->get('field') . ' ASC';
			}
		}
		if(is_array($where)) {
			if(!$all)
				$this->db->limit($per_page, $offset);
			$user = $this->db->get_where($this->_table, $where);
			
			$this->db->get_where($this->_table, $where);
			$records = $this->db->count_all_results();
		} else {
			$order = " ORDER BY " . $order;
			$limit = !$all ? " LIMIT $offset, $per_page" : ""; 
			$query = "SELECT gs.score, gs.date_played,gs.earned_km_points, r.first_name,
			 r.third_name, g.name, r.user_id, gs.is_cheater,
			 (SELECT AVG(score) FROM tbl_game_score WHERE 1 $where) AS avg_score,
			 (SELECT AVG(earned_km_points) FROM tbl_game_score WHERE 1 $where) AS avg_kmpoints
			  FROM tbl_game_score gs
			    INNER JOIN tbl_registrants r ON gs.user_id = r.user_id
  				INNER JOIN tbl_game g ON g.game_id = gs.game_id
			  WHERE g.game_id = gs.game_id AND r.user_id = gs.user_id " . $where . $order . $limit;
			$user = $this->db->query($query);
			$query = "SELECT COUNT(*) AS num_rows FROM tbl_game_score gs, tbl_game g, tbl_registrants r WHERE g.game_id = gs.game_id AND r.user_id = gs.user_id " . $where;
			$records =  $this->db->query($query)->row_array()['num_rows'];
		}
		return $user->result_array();
	}
}