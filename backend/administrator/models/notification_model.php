<?php

class Notification_Model extends CI_Model
{
	public function get_by_user($user_id)
	{
		return $this->db->select()
			->from('tbl_notifications')
			->where('registrant_id', $user_id)
			->where('notification_deleted', 0)
			->order_by('notification_date_created', 'DESC')
			->get()
			->result_array();
	}

	public function notify($user_id, $origin, array $params)
	{
		$this->db->insert('tbl_notifications', array(
			'registrant_id' => $user_id,
			'notification_message' => $params['message'],
			'notification_origin_id' => $origin,
			'notification_suborigin_id' => $params['suborigin']
		));
		if ($this->db->affected_rows()) {
			$this->db->set('total_notifications', 'total_notifications + 1', false)
				->where('registrant_id', $user_id)
				->update('tbl_registrants');
			if ($this->db->affected_rows()) {
				return true;
			}
		}
		return false;
	}

	public function read_notification($user_id, $notification_id)
	{
		$this->db->set('notification_status', 1)
			->set('notification_date_read', date('Y-m-d H:i:s'))
			->where('notification_id', $notification_id)
			->where('registrant_id', $user_id)
			->where('notification_status', 0)
			->where('notification_deleted', 0)
			->update('tbl_notifications');
		if ($this->db->affected_rows()) {
			$this->db->set('total_notifications', 'total_notifications - 1', false)
				->where('registrant_id', $user_id)
				->update('tbl_registrants');
			if ($this->db->affected_rows()) {
				return true;
			}
		}
		return false;
	}

	public function get_notification($user_id, $notification_id)
	{
		return (array) $this->db->select()
			->from('tbl_notifications')
			->where('notification_id', $notification_id)
			->where('registrant_id', $user_id)
			->where('notification_deleted', 0)
			->limit(1)
			->get()
			->row();
	}

	public function get_read_by_user($user_id)
	{
		return $this->db->select()
			->from('tbl_notifications')
			->where('registrant_id', $user_id)
			->where('notification_status', 1)
			->where('notification_deleted', 0)
			->order_by('notification_date_created', 'DESC')
			->get()
			->result_array();
	}

	public function get_read_count_by_user($user_id)
	{
		return $this->db->from('tbl_notifications')
			->where('registrant_id', $user_id)
			->where('notification_status', 1)
			->where('notification_deleted', 0)
			->count_all_results();
	}

	public function get_unread_by_user($user_id)
	{
		return $this->db->select()
			->from('tbl_notifications')
			->where('registrant_id', $user_id)
			->where('notification_status', 0)
			->where('notification_deleted', 0)
			->order_by('notification_date_created', 'DESC')
			->get()
			->result_array();
	}

	public function delete_notifications($user_id, array $ids)
	{	
		if ($ids) {
			$this->db->start_cache();
			$this->db->where_in('notification_id', $ids);
		} else {
			return false;
		}
		$this->db->where('registrant_id', $user_id)
			->where('notification_deleted', 0);
		$this->db->stop_cache();

		$now = date('Y-m-d H:i:s');
		$this->db->where('notification_status', 0)
			->update('tbl_notifications', array(
				'notification_deleted' => 1,
				'notification_date_deleted' => $now
			));
		$unread = $this->db->affected_rows();
		$this->db->where('notification_status', 1)
			->update('tbl_notifications', array(
				'notification_deleted' => 1,
				'notification_date_deleted' => $now
			));
		$this->db->flush_cache();
		if ($unread) {
			$this->db->set('total_notifications', 'total_notifications - '.$unread, false)
				->where('registrant_id', $user_id)
				->update('tbl_registrants');
			if (!$this->db->affected_rows()) {
				return false;
			}
		} elseif (!$this->db->affected_rows()) {
			return false;
		}
		return true;
	}

	public function get_unread_count_by_user($user_id)
	{
		$user = $this->db->from('tbl_registrants')
			->where('registrant_id', $user_id)
			->limit(1)
			->get()
			->row();
		if (!$user) {
			return false;
		}
		return $user->total_notifications;
	}
}