<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3>Login Spread (Dates)</h3>
               <div class="actions">
                    <a href="<?php echo SITE_URL ?>/date_logins/export?<?php echo http_build_query($_GET) ?>" class="btn btn-primary">Export</a>
               </div>
		  </div>

          <!-- Nav tabs --> 
          <ul class="nav nav-tabs">
            <li class="active"><a href="#graph" data-toggle="tab">Graph</a></li>
            <li class=""><a href="#summary" data-toggle="tab">Summary</a></li>
          </ul>  

          <!-- Tab panes -->
          <div class="tab-content">
               <div class="tab-pane" id="summary">
                    <table class="table table-bordered" style="border-top:none;">
                         <thead>
                              <tr>
                                   <th width="1%">#</th>
                                   <th>Date</th>
                                   <th>Visits</th>
                                   <th>Visitors</th>
                                   <td>Operation</td>
                              </tr>
                         </thead>
                         <tbody>
                              <form>
                                   <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>From:<input name="fromdate" class="form-control from" value="<?php echo isset($_GET['fromdate']) ? $_GET['fromdate'] : '' ?>" /><br> To:<input name="todate" class="form-control to" value="<?php echo isset($_GET['todate']) ? $_GET['todate'] : '' ?>" /><input name="type" type="hidden" value="<?php echo $this->input->get('type') ? $this->input->get('type') : 'age' ?>" /><br>
                                          Arrange by:<br>
                                          <select class="form-control" name="orderby">
                                            <option <?= $this->input->get('orderby') == 'tlog' ? 'selected' : '' ?> value="tlog">Date</option>
                                            <option <?= $this->input->get('orderby') == 'count' ? 'selected' : '' ?> value="count">Visits</option>
                                            <option <?= $this->input->get('orderby') == 'visitors' ? 'selected' : '' ?> value="visitors">Visitors</option>
                                          </select>&nbsp;
                                          <select class="form-control" name="arr">
                                            <option <?= $this->input->get('arr') == 'ASC' ? 'selected' : '' ?> value="ASC">ASC</option>
                                            <option <?= $this->input->get('arr') == 'DESC' ? 'selected' : '' ?> value="DESC">DESC</option>
                                          </select>
                                          <br>
                                          <button class="btn btn-primary" name="search" value="1">Go</button></td>
                                   </tr>
                              </form>
                         		<?php if($records): $totalVisits = 0; $totalVisitors = 0;
          					foreach($records as $k => $v): $totalVisits += $v['count']; $totalVisitors += $v['visitors']; ?>
                              <tr>
                                   <td><?php echo $k + 1 ?></td>
                                   <td><?php echo date("F d, Y", strtotime($v['tlog'])) ?></td>
                                   <td><?php echo $v['count']?></td>
                                   <td><?php echo $v['visitors'] ?></td>
                                   <td></td>
                              </tr>
                              <?php 
          					endforeach; ?>
                    <tr>
                      <td></td>
                      <td><strong>Total</strong></td>
                      <td><strong><?= $totalVisits ?></strong></td>
                      <td><strong><?= $totalVisitors ?></strong></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td><strong>Average</strong></td>
                      <td><strong><?= round($totalVisits / count($records)) ?></strong></td>
                      <td><strong><?= round($totalVisitors / count($records)) ?></strong></td>
                      <td></td>
                    </tr>
          					<?php else:
          						echo '<tr><td colspan="7">No records found</td></tr>';
          					endif; ?>
                         </tbody>
                    </table>
               </div>
               <div class="tab-pane active" id="graph">
                    <div id="chart_entries" style="border:1px solid #DDDDDD; border-top:none;height:300px;">
                         
                    </div>
                    <br >
                 </div>
           </div>
           
     </div>
     <!--end main content -->

     <script type="text/javascript" src="<?php echo BASE_URL ?>admin_assets/js/google-api.js"></script>
     <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Date', 'Visits', 'Visitors'],
          <?php foreach($records as $k => $v): ?>
          ['<?php echo date("F d", strtotime($v["tlog"])) ?>',  <?php echo isset($v['count']) ? $v['count'] : 0 ?>, <?php echo $v['visitors']?>],
          <?php endforeach; ?>
        ]);

        var options = {
          title: 'Site Logins <?php echo $this->input->get("fromdate") && $this->input->get("todate") ? "(" . date("F d, Y", strtotime($this->input->get("fromdate"))) . " - " .  date("F d, Y", strtotime($this->input->get("todate"))) . ")" : ""?>'
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('chart_entries'));
        chart.draw(data, options);
      }
    </script>

  
