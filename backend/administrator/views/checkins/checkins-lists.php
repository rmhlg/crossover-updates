<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3>Find A Bar : Checkins <span class="badge badge-important"><?php echo isset($total) ? $total : 0 ?></span></h3>

               <div class="actions">
                    <a href="<?php echo SITE_URL ?>/user_checkins/export?<?php echo http_build_query($_GET) ?>" class="btn btn-primary">Export</a>
               </div>
          </div>

          <ul class="nav nav-tabs">
              <li class="<?= ! isset($_GET['search']) && ! $this->uri->segment(2) ? 'active' : '' ?>"><a href="#graph" id="graph-btn" data-toggle="tab">Graph</a></li>
              <li class="<?= (isset($_GET['search']) && $_GET['search']) || $this->uri->segment(2) ? 'active' : '' ?>"><a href="#summary" id="summary-btn" data-toggle="tab">List</a></li>
          </ul>

          <div class="tab-content">
              <div class="tab-pane <?= ! isset($_GET['search']) && ! $this->uri->segment(2) ? 'active' : '' ?>" id="graph">
                    <div id="graph_1" style="border:1px solid #DDDDDD; border-top:none;height:300px;">
                         
                    </div> 
              </div>

              <div class="tab-pane <?= (isset($_GET['search']) && $_GET['search']) || $this->uri->segment(2) ? 'active' : '' ?>" id="summary">
                  <table class="table table-bordered">
                         <thead>
                              <tr>
                                   <th>#</th>
                                   <th>Name</th>
                                   <th>Type</th>
                                   <th>Checked In Date</th>
                              </tr>
                         </thead>
                         <tbody>
                             <form action="<?php echo SITE_URL ?>/user_checkins">
                                   <tr>
                                       <td></td>
                                       <td><input name="name" class="form-control" value="<?php echo isset($_GET['name']) ? $_GET['name'] : '' ?>" /></td>
                                       <td>
                                          <select name="type" class="form-control from">
                                            <option value="0" <?= isset($_GET['type']) && $_GET['type'] == '0' ? 'selected' : '' ?>>Events</option>
                                            <option value="5" <?= isset($_GET['type']) && $_GET['type'] == '5' ? 'selected' : '' ?>>LAMP</option>
                                          </select>
                                       </td>
                                       <td>
                                        <div class="row">
                                          <button class="btn btn-primary" name="search" value="1">Go</button>
                                        </div>
                                      </td>
                                  </tr>
                                  </form>
                                  
                              <?php if($users): 
                              foreach($users as $k => $v): 
                               
                               ?>
                              <tr>
                                   <td><?php echo $offset + $k + 1 ?></td>
                                   <td><a href="javascript:;" onclick="showRegistrantDetails('<?php echo $v['registrant_id'] ?>')"><?php echo $v['first_name'] . ' ' . $v['third_name']?></a></td>
                                   <td><?php echo $v['event_title']?></td>
                                   <td><?php echo date('F d, Y H:i:s', strtotime($v['date_created'])) ?></td>
                               </tr>
                              <?php 
                    endforeach;
                    else:
                      echo '<tr><td colspan="5">No records found</td></tr>';
                    endif; ?>
                         </tbody>
                    </table>

                    <ul class="pagination pagination-sm pull-right">
                         <?php echo $pagination ?>
                    </ul>

               </div>
              </div>
          </div>
       

      <script type="text/javascript" src="<?php echo BASE_URL ?>admin_assets/js/google-api.js"></script>
     <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Date', 'Event', 'LAMP'],
          <?php foreach($graph['daily'] as $k => $v): ?>
            <?php if($v[1] || $v[2]): ?>
              ['<?= $v[0] ?>', <?= $v[1] ?>, <?= $v[2] ?>],
            <?php endif; ?>
          <?php endforeach; ?>
        ]);

        var options = {
          title: 'Find a Bar Checkins for the month of <?= date("F Y") ?>',
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('graph_1'));
        chart.draw(data, options);
      }
</script>

     <!--end main content -->