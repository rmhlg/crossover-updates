<!-- js -->
<script type="text/javascript">
  $(function() {
    $('#left-image-preview').hide();
  });

  function uploadImage(pos) {
    
  }
</script>
<!-- js -->

<div class="page-header">

               <h3><?=$header_text?></h3>

          </div>

          <form class="form-horizontal" role="form" method="POST" action="<?=site_url('activity_media/save')?>" enctype="multipart/form-data">
              <input type="hidden" name="activity_media_id" value="<?=$row && $row->activity_media_id ? $row->activity_media_id : ''?>">

               <div class="form-group">

                    <label class="col-sm-2 control-label">Activity</label>

                    <div class="col-sm-4">

                         <select class="form-control" name="activity_id">
                              <option value=""></option>

                             <?php if($activities){
                                        foreach($activities as $v){ ?>   
                                             <option value="<?=$v->activity_id?>" <?=isset($row->activity_id) && $row->activity_id == $v->activity_id ? 'selected' : ''?>><?=$v->title?></option>
                                  <?php }
                             } ?>
                         </select>

                    </div>

               </div>
               
               <div class="form-group">

                    <label class="col-sm-2 control-label">Decision</label>

                    <div class="col-sm-4">

                         <select class="form-control" name="decision_title">
                              <option value=""></option>

                              <?php if($decisions->num_rows()): foreach($decisions->result() as $v): ?>
                                  <option value="<?= $v->decision_left_title ?>" <?= isset($row->decision_title) && $row->decision_title == $v->decision_left_title ? 'selected' : '' ?>><?= $v->decision_left_title ?></option>
                                  <option value="<?= $v->decision_right_title ?>" <?= isset($row->decision_title) && $row->decision_title == $v->decision_right_title ? 'selected' : '' ?>><?= $v->decision_right_title ?></option>
                              <?php endforeach; endif; ?>
                         </select>

                    </div>

               </div>

               <div class="form-group">

                    <label class="col-sm-2 control-label">Title</label>

                    <div class="col-sm-4">

                          <input type="text" name="media_title" class="form-control" value="<?=isset($row->media_title) ? $row->media_title : ''?>">

                    </div>

               </div>

               <div class="form-group">

                    <label class="col-sm-2 control-label">Sub Title</label> 
 
                    <div class="col-sm-4">  
  
                         <textarea name="media_sub_title" class="form-control"><?=isset($row->media_sub_title) ? $row->media_sub_title : ''?></textarea>

                    </div>

               </div>               

               <div class="form-group">

                    <label class="col-sm-2 control-label">Description</label> 
 
                    <div class="col-sm-4">  
 
 
                         <textarea type="text" name="media_description" class="form-control"><?=isset($row->media_description) ? $row->media_description : ''?></textarea>
                    </div>

               </div>

               <div class="form-group">

                    <label class="col-sm-2 control-label">Media Type</label> 
 
                    <div class="col-sm-4">  
                          <select class="form-control" name="media_type">
                              <option value="1" <?= isset($row->media_type) && $row->media_type == 1 ? 'selected' : '' ?>>Image</option>
                              <option value="2" <?= isset($row->media_type) && $row->media_type == 2 ? 'selected' : '' ?>>Video</option>
                          </select>
                    </div>

               </div>

               <div class="form-group">

                    <label class="col-sm-2 control-label">Media</label> 
 
                    <div class="col-sm-4">  
                          <input type="file" class="form-control" name="media" onchange="Image(this)">
                    </div>

               </div>

               <div class="form-group">

                    <label class="col-sm-2 control-label"></label> 

                  <div class="col-sm-4">

                    <img src="<?=isset($row->media) && $row->media ? $row->media : ''?>" id="left-image">

                  </div>

               </div>

               <div class="form-group">

                    <div class="col-sm-offset-2 col-sm-4">
                         <a href="<?=site_url($this->router->class)?>" class="btn btn-default">Cancel</a>
                         <button type="submit" class="btn btn-primary">Submit</button>
                    </div>

               </div>

          </form>

<script>
  function Image(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();

          reader.onload = function (e) {
              $('#left-image').attr('src', e.target.result);
          }

          reader.readAsDataURL(input.files[0]);
      }
  }
</script>