 
 
<!--start main content -->
 <div class="container main-content">
      <div class="page-header">
           <h3><?php echo $header_text; ?></h3>
      </div>

      <?php if($error){ ?>
       <div class="alert alert-danger"> <?php echo $error; ?> </div>
      <?php } ?>

       <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
        <input type="hidden" name="image_counter" value="1" />
 
          <div class="form-group">
              <label class="col-sm-2 control-label">Activity Name :</label>
              <div class="col-sm-4">
                   <input type="text" value="<?php echo ($row) ? $row->name : '';  ?>" name="name" class="form-control required">
              </div>
          </div>

          <div class="form-group">
              <label class="col-sm-2 control-label">ID :</label>
              <div class="col-sm-4">
                   <input type="text" value="<?php echo ($row) ? $row->setting_activity_id : '';  ?>" name="setting_activity_id" class="form-control required">
              </div>
          </div>

          <div class="form-group">
              <div class="col-sm-offset-2 col-sm-4"> 
                   <a href="<?php echo SITE_URL.'/'.$this->uri->segment(1); ?>" class="pull-left"><< Back</a>
                    <button type="submit" class="btn btn-primary pull-right">Submit</button>
              </div>
          </div>
           
           
      </form>
      
 </div>
 <!--end main content -->