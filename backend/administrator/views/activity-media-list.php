<div class="page-header">

               <h3><?=$header_text?></h3>


               <div class="actions">

                    <a href="<?=site_url($this->router->class.'/add')?>" class="btn btn-primary">New Item</a>

               </div>

          </div>

          <div ng-app="dialogModule" ng-controller="activityList" >

          <table class="table table-bordered">
               <thead>
                    <tr>
                         <th>Media Title</th>
                         <th>Date Created</th>
                         <th>Action</th>
                    </tr>
               </thead>

               <tbody>
                    <tr>
                      <form action="<?= site_url('activity_media') ?>">
                        <td>
                            <select name="category" class="form-control">
                                  <option value="">All</option>
                                  <option value="<?= REGION_CALIFORNIA ?>" <?=isset($_GET['category']) && $_GET['category'] == REGION_CALIFORNIA ? 'selected' : ''?>>California</option>
                                  <option value="<?= REGION_ALASKA ?>" <?=isset($_GET['category']) && $_GET['category'] == REGION_ALASKA ? 'selected' : ''?>>Alaska</option>
                                  <option value="<?= REGION_CANADA ?>" <?=isset($_GET['category']) && $_GET['category'] == REGION_CANADA ? 'selected' : ''?>>Canada</option>
                             </select>
                        </td>
                        <td></td>
                        <td><input type="submit" class="btn btn-default"></td>
                      </form>
                    </tr>

                    <?php if($rows){

                         foreach($rows->result() as $row){ ?>

                         <tr>
                              <td><?=$row->media_title?></td>
                              <td><?=$row->created?></td>
                              <td>
                                <!-- <a href="<?=base_url($this->router->class.'/edit?activity_id='.$row->activity_id)?>" class="btn btn-success">
                                        <span class="glyphicon glyphicon-search"></span> Media</a> -->
                                   <a href="<?=site_url($this->router->class.'/edit?activity_media_id='.$row->activity_media_id)?>" class="btn btn-default">
                                        <span class="glyphicon glyphicon-pencil"></span> Edit</a>
                                   <a ng-click="openConfirm('<?=site_url($this->router->class.'/delete?activity_media_id='.$row->activity_media_id)?>')" class="btn btn-danger">
                                        <span class="glyphicon glyphicon-trash"></span> Remove</a>
                              </td>
                         </tr>

                    <?php }
                    } ?>                    

               </tbody>



          </table>

          <script type="text/ng-template" id="modalDialogId">
              <div class="ngdialog-message">        
                <p>Are you sure you want to remove this?</p>
              </div>
              <div class="ngdialog-buttons">
                <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="confirm(confirmValue)">Confirm</button>
                <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog('button')">Cancel</button>
              </div>
          </script>

     </div>

          