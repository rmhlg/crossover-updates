<!--start main content -->
<div class="container main-content">
	<div class="page-header">
		<h3>LAMPS Reservations<span class="badge badge-important"><?= $total ?></span></h3>

		<div class="actions">
			<a href="<?= SITE_URL ?>/lamps_reservation/export?<?= http_build_query($this->input->get() ? $this->input->get() : array()) ?>" class="btn btn-primary">Export</a>
		</div>
	</div>
	<?php if ($this->input->get('error')): ?>
	<div class="alert alert-danger"><?= $this->input->get('error') ?></div>
	<?php endif ?>
	<table class="table table-bordered">
		<thead>
				<tr>
					<th>#</th>
					<th>Registrant</th>
					<th>Mobile #</th>
					<th>Title</th>
					<th>Guest(s)</th>
					<th>Guest Name</th>
					<th>Booking Date</th>
					<th>Date Created</th>
					<th>Status</th>
					<th></th>
				</tr>
		</thead>
		<tbody>
			<form action="<?= SITE_URL ?>/lamps_reservation">
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td><input name="lamp_name" placeholder="LAMPS Name" class="form-control" value="<?= $this->input->get('lamp_name') ? $this->input->get('lamp_name') : '' ?>" /></td>
						<td></td>
						<td></td>
						<td>
							<div class="row">
                                   <div class="col-xs-6">
                                        <input name="from_booking_date" placeholder="From" class="form-control from" value="<?= $this->input->get('from_booking_date') ?>" />
                                   </div>
                                   <div class="col-xs-6">
                                        <input name="to_booking_date" placeholder="To" class="form-control to" value="<?= $this->input->get('to_booking_date') ?>" />
                                   </div>
                          </div>
                      </td>
						<td>
							<div class="row">
                                   <div class="col-xs-6">
                                        <input name="from_date_created" placeholder="From" class="form-control from" value="<?= $this->input->get('from_date_created') ?>" />
                                   </div>
                                   <div class="col-xs-6">
                                        <input name="to_date_created" placeholder="To" class="form-control to" value="<?= $this->input->get('to_date_created') ?>" />
                                   </div>
                          </div>
                      </td>
						<td>
						<select class="form-control" name="status">
							<option value=""></option>
							<option <?= (string) $this->input->get('status') == '1' ? 'selected="selected"' : '' ?> value="1">Approved</option>
							<option <?= (string) $this->input->get('status') == '0' ? 'selected="selected"' : '' ?> value="0">Pending</option>
						</select>
						</td>
						<td><button class="btn btn-primary" name="search" type="submit">Go</button></td>
					</tr>
				</form>
				<?php foreach ($reservations as $k => $record): if (in_array($record->lamp_id, $lamp_ids)): ?>
				<tr>
						<td><?= $offset + $k + 1 ?></td>
						<td><a href="javascript:void(0)" onclick="showRegistrantDetails(<?= $record->registrant_id ?>)"><?= $record->first_name ?> <?= $record->third_name ?></a></td>
						<td><?= $record->lamp_reserve_mobile ? $record->lamp_reserve_mobile : $record->mobile_phone ?></td>
						<td><?= $record->lamp_name ?></td>
						<td><?= count(json_decode($record->lamp_reserve_guests)) + 1 ?></td>
						<td>
							<?php 
							$guests = json_decode($record->lamp_reserve_guests);
							if($guests) {
								foreach ($guests as $key => $value) {
									echo $value->name . '<br>';
								}
							}
							?>
						</td>
                        <td><?= date('F d, Y l', strtotime($record->date)) ?></td>
                        <td><?= date('F d, Y l', strtotime($record->lamp_reserve_date_created)) ?></td>
                        <td><?php
                        	if ($record->lamp_reserve_status == 1) {
                        		echo '<span class="label label-success">Approved</span>';
                        	} elseif ($record->lamp_reserve_status == 2) {
                        		echo '<span class="label label-danger">Disapproved</span>';
                        	} else {
                        		echo '<span class="label label-warning">Pending</span>';
                        	} ?></td>
						<td>
						<?php if (!$record->lamp_reserve_status): ?>
						<a class="btn change-status-btn btn-success" href="<?= SITE_URL ?>/lamps_reservation/change_status?id=<?= $record->lamp_reserve_id ?>&status=1">Approve</a>
						<a class="btn change-status-btn btn-danger" href="<?= SITE_URL ?>/lamps_reservation/change_status?id=<?= $record->lamp_reserve_id ?>&status=2">Disapprove</a>
						<?php elseif ($record->lamp_reserve_status == 1): ?>
						<a class="btn change-status-btn btn-danger" href="<?= SITE_URL ?>/lamps_reservation/change_status?id=<?= $record->lamp_reserve_id ?>&status=2">Disapprove</a>
						<?php elseif ($record->lamp_reserve_status == 2): ?>
						<a class="btn change-status-btn btn-success" href="<?= SITE_URL ?>/lamps_reservation/change_status?id=<?= $record->lamp_reserve_id ?>&status=1">Approve</a>
						<?php endif ?>
						</td>
					</tr>
				<?php endif; endforeach; ?>
		</tbody>
	</table>

	<ul class="pagination pagination-sm pull-right">
		<?= !empty($pagination) ? $pagination : '' ?>
	</ul>

</div>
<!--end main content -->
<script>
	$(".change-status-btn").click( function(e) {
		var href = $(this).prop("href");
		bootbox.confirm("Are you sure?", function(confirm) {
			if (confirm) {
				window.location = href;
			}
		});
		e.preventDefault();
	});
</script>