
<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3>LAMPS <span class="badge badge-important"><?php echo isset($total) ? $total : 0 ?></span></h3>

               <div class="actions">
                    <?php if($add): ?>
                    <a href="<?php echo SITE_URL ?>/lamps/add" class="btn btn-primary">Add LAMP</a>
                    <?php endif; ?>
                    <a href="<?php echo SITE_URL ?>/lamps/export?<?php echo http_build_query($_GET) ?>" class="btn btn-primary">Export</a>   
               </div>
          </div>
          
          <table class="table table-bordered">
               <thead>
                    <tr>
                         <th>#</th>
                         <th>User</th>
                         <th>LAMP</th>
                         <th>Operation</th>
                    </tr>
               </thead>
               <tbody>
               		 <form action="<?php echo SITE_URL ?>/lamps">
                     <tr>
                        <td></td>
                        <td><input name="lamp_name" class="form-control" value="<?php echo isset($_GET['lamp_name']) ? $_GET['lamp_name'] : '' ?>" /></td>
                         <!-- <td><input name="slots" class="form-control" value="<?php echo isset($_GET['slots']) ? $_GET['slots'] : '' ?>" /></td> -->
                         <td><input name="lamp_name" class="form-control" value="<?php echo isset($_GET['lamp_name']) ? $_GET['lamp_name'] : '' ?>" /></td>
                         <td><button class="btn btn-primary" name="search" value="1">Go</button></td>
                    </tr>
                    </form>
                    <?php if($categories): 
					foreach($categories as $k => $v): ?>
                    <tr>
                         <td><?php echo $offset + $k + 1 ?></td>
                         <td><?php echo $v['name'] ?></td>
                         <td><?php echo $v['lamp_name'] ?></td>
                         <td>
                            <?php if($delete): ?>
                            <a href="<?php echo SITE_URL ?>/lamps/delete/<?php echo $v['lamp_id'] ?>/<?php echo md5($v['lamp_id'] . ' ' . $this->config->item('encryption_key')) ?>" onclick="return confirmDeletion(this, 'LAMP')" class="btn btn-danger">Delete</a>
                            <?php endif; ?>
                         </td>
                    </tr>
                    <?php 
					endforeach;
					endif; ?>
               </tbody>
          </table>

          <ul class="pagination pagination-sm pull-right">
               <?php echo $pagination ?>
          </ul>

     </div>
     <!--end main content -->