<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3>Perks : LAMP Guestlists <span class="badge badge-important"><?php echo isset($total) ? $total : 0 ?></span></h3>

               <div class="actions">
                    <a href="<?php echo SITE_URL ?>/lamp_guestlists/export?<?php echo http_build_query($_GET) ?>" class="btn btn-primary">Export</a>
               </div>
          </div>

          <ul class="nav nav-tabs">
              <li class="active"><a href="#summary" id="summary-btn" data-toggle="tab">List</a></li>
              <li class=""><a href="#graph" id="graph-btn" data-toggle="tab">Graph</a></li>
          </ul>

          <div class="tab-content">
              <div class="tab-pane" id="graph">
                    <div id="graph_1" style="border:1px solid #DDDDDD; border-top:none;height:300px;">
                         
                    </div> 
              </div>

              <div class="tab-pane active" id="summary">
                  <table class="table table-bordered">
                         <thead>
                              <tr>
                                   <th>#</th>
                                   <th>Name</th>
                                   <th>Event</th>
                                   <th>Date Added</th>
                              </tr>
                         </thead>
                         <tbody>
                             <form action="<?php echo SITE_URL ?>/lamp_guestlists">
                                   <tr>
                                       <td></td>
                                       <td><input name="name" class="form-control" value="<?php echo isset($_GET['name']) ? $_GET['name'] : '' ?>" /></td>
                                       <td>
                                          <div class="row">
                                            <button class="btn btn-primary" name="search" value="1">Go</button>
                                          </div>
                                       </td>
                                       <td></td>
                                  </tr>
                              </form>
                                  
                              <?php if($users): 
                              foreach($users as $k => $v): 
                               
                               ?>
                              <tr>
                                   <td><?php echo $offset + $k + 1 ?></td>
                                   <td><?php echo $v['name']?></td>
                                   <td><?php echo $v['event_title']?></td>
                                   <td><?php echo $v['date_created']?></td>
                               </tr>
                              <?php 
                    endforeach;
                    else:
                      echo '<tr><td colspan="5">No records found</td></tr>';
                    endif; ?>
                         </tbody>
                    </table>

                    <ul class="pagination pagination-sm pull-right">
                         <?php echo $pagination ?>
                    </ul>

               </div>
              </div>
          </div>
          
          
     <!--end main content -->

     <script>
     function confirm() {
      bootbox.confirm('Are you sure you want approve all selected comments?', function(result) {
          if(result == 1) {
            $.post('<?php echo SITE_URL ?>/comment/approve_all', {ids: ids}, function() {
              window.location.reload();
            })
            return true;
          }
        });

     }
     </script>