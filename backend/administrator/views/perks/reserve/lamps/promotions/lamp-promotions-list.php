<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3>Perks : LAMP Promotions <span class="badge badge-important"><?php echo isset($total) ? $total : 0 ?></span></h3>

               <div class="actions">
                    <a href="<?php echo SITE_URL ?>/lamp_promotions/add" class="btn btn-primary">Add Lamp Promotion</a>
                    <a href="<?php echo SITE_URL ?>/lamp_promotions/export?<?php echo http_build_query($_GET) ?>" class="btn btn-primary">Export</a>
               </div>
          </div>

          <ul class="nav nav-tabs">
              <li class="<?php echo ! isset($_GET['search']) && ! $this->uri->segment(2) ? 'active' : '' ?>"><a href="#graph" id="graph-btn" data-toggle="tab">Graph</a></li>
              <li class="<?php echo (isset($_GET['search']) && $_GET['search']) || $this->uri->segment(2) ? 'active' : '' ?>"><a href="#summary" id="summary-btn" data-toggle="tab">List</a></li>
          </ul>

          <div class="tab-content">
              <div class="tab-pane <?php echo ! isset($_GET['search']) && ! $this->uri->segment(2) ? 'active' : '' ?>" id="graph">
                    <div id="graph_1" style="border:1px solid #DDDDDD; border-top:none;height:300px;">
                         
                    </div> 
              </div>

              <div class="tab-pane <?php echo (isset($_GET['search']) && $_GET['search']) || $this->uri->segment(2) ? 'active' : '' ?>" id="summary">
                  <table class="table table-bordered">
                         <thead>
                              <tr>
                                   <th>#</th>
                                   <th>LAMP Name</th>
                                   <th>Promotion Title</th>
                                   <th>Date Created</th>
                                   <th>Operation</th>
                              </tr>
                         </thead>
                         <tbody>
                             <form action="<?php echo SITE_URL ?>/lamp_promotions">
                                   <tr>
                                       <td></td>
                                       <td></td>
                                       <td><input name="title" class="form-control" value="<?php echo isset($_GET['title']) ? $_GET['title'] : '' ?>" /></td>
                                       <td></td>
                                      <td>
                                        <div class="row">
                                          <button class="btn btn-primary" name="search" value="1">Go</button>
                                        </div>
                                      </td>
                                  </tr>
                                  </form>
                                  
                              <?php if($users): 
                              foreach($users as $k => $v): 
                               
                               ?>
                              <tr>
                                   <td><?php echo $offset + $k + 1 ?></td>
                                   <td><?php echo $v['lamp_name']?></td>
                                   <td><?php echo $v['promotion_title']?></td>
                                   <td><?php echo date('F d, Y H:i:s', strtotime($v['date_created'])) ?></td>
                                   <td>
                                        <a href="<?php echo SITE_URL ?>/lamp_promotions/edit/<?php echo $v['lamp_promotion_id'] ?>" class="btn btn-primary">Edit</a>
                                        <a href="<?php echo SITE_URL ?>/lamp_promotions/delete/<?php echo $v['lamp_promotion_id'] .'/'. md5($v['lamp_promotion_id'] . ' ' . $this->config->item('encryption_key')) ?>" onclick="return confirmDeletion(this, 'promotion')" class="btn btn-danger">Delete</a>
                                   </td>
                               </tr>
                              <?php 
                    endforeach;
                    else:
                      echo '<tr><td colspan="5">No records found</td></tr>';
                    endif; ?>
                         </tbody>
                    </table>

                    <ul class="pagination pagination-sm pull-right">
                         <?php echo $pagination ?>
                    </ul>

               </div>
              </div>
          </div>
          
      <script type="text/javascript" src="<?php echo BASE_URL ?>admin_assets/js/google-api.js"></script>
     <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Date', 'Promotions'],
          <?php foreach($graph['daily'] as $k => $v): ?>
            <?php if($v[1]): ?>
              ['<?php echo $v[0] ?>', <?php echo $v[1] ?>],
            <?php endif; ?>
          <?php endforeach; ?>
        ]);

        var options = {
          title: 'LAMP Promotions for the month of <?php echo date("F Y") ?>',
          legend: {position: 'none'}
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('graph_1'));
        chart.draw(data, options);
      }
</script>
     <!--end main content -->