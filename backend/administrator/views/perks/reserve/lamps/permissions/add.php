<!--start main content -->
<!-- <link type="text/css" rel="stylesheet" href="<?= BASE_URL ?>admin_assets/css/angular-js/common.css"> -->
 <div class="container main-content" >
			<div class="page-header">
					 <h3>LAMP Permissions</h3>
			 <div class="actions">
				<!-- <a href="<?= SITE_URL ?>/product_info" class="btn btn-primary">Back</a> -->
			 </div>
			</div>
			<?php if (!$error): ?>
			<div class="alert alert-danger" style="display:none;"></div>
			<?php else: ?>
			<div class="alert alert-danger"><?= $error ?></div>
			<?php endif ?>
			 <form class="form-horizontal" role="form" method="post" onsubmit="return submitFrm()" enctype="multipart/form-data">
					 <div class="form-group">
								<label class="col-md-1 control-label">LAMP</label>
								<div class="col-md-4">
										<select class="form-control" name="lamp_id" id="lamp">
											<option></option>
											<?php if($lamps) {
												foreach($lamps as $k => $v) { ?>
												<option value="<?php echo $v['lamp_id'] ?>"><?php echo $v['lamp_name'] ?></option>
												<?php }
											} ?>
										</select>
								</div>
					 </div>
					 <div class="form-group">
								<label class="col-md-1 control-label">User</label>
								<div class="col-md-4">
										<select class="form-control" name="cms_user_id" id="user">
											 	<option></option>
											<?php if($users) {
												foreach($users as $k => $v) { ?>
												<option value="<?php echo $v['user_id'] ?>"><?php echo $v['name'] ?></option>
												<?php }
											} ?>  
										</select>
								</div>
					 </div>

			
			<br ><br >
					 <div class="form-group">
						<div class="col-md-offset-1 col-md-4">
						 <button type="submit" class="btn btn-primary" name="submit" value="1">Add Permission</button>
						 <a class="btn btn-warning" href="<?php echo BASE_URL ?>admin/lamp_permissions">Cancel</a>
					 </div>
					</div>
					<!-- <input type="hidden" id="remove-files-index" name="remove-file" value=""> -->
			</form>
				<!-- <div > -->
				<!-- <div class="sel-file img-thumbnail" >
					{{($index + 1) + '.'}}
					<img ng-show="dataUrls[$index]" ng-src="{{dataUrls[$index]}}">
					<button class="button" ng-click="start($index)" ng-show="progress[$index] < 0">Start</button>
					<span class="progress" ng-show="progress[$index] >= 0">           
						<div style="width:{{progress[$index]}}%">{{progress[$index]}}%</div>
					</span>       
					<button class="button" ng-click="abort($index)" ng-show="hasUploader($index) && progress[$index] < 100">Abort</button>
					{{f.name}} - size: {{f.size}}B - type: {{f.type}}
				</div> -->
			<!-- </div>
			</div> -->
 </div>

 <script>

// $("#add-form-product-info").submit( function() {
//  bootbox.dialog({
//    message: "<center><strong>Loading</strong></center>",
//    show: true,
//    backdrop: true,
//    closeButton: false
//  });
// });
$(function() {
	$("#product-info-photo").change( function() {
	var oFReader = new FileReader();
	oFReader.readAsDataURL($(this).prop("files")[0]);
	oFReader.onload = function (oFREvent) {
	$("#product-info-preview").prop("src", oFREvent.target.result);
	};
 });

	$('#lamp').change(function() {
		lampId = $(this).val();
		if(lampId == '') return;
		$.post('<?php echo SITE_URL ?>/lamp_permissions/get_avail_users', {'id': lampId}, function(data) {
			$('#user').html(data);
		})
	});

});


function removeValue(el) {
	elem = $(el);
	elem.parent().parent().parent().parent().remove();
}
 </script>
 <!--end main content 