<script src="<?php echo BASE_URL ?>admin_assets/js/bootstrap-tagsinput.js"></script>
<?php $this->load->view('editor.php') ?>
<script type="text/javascript">
var jscrop_api;
function initJCrop(elem) {
	<?php if(isset($record['thumbnail_meta'])):  
		$points = isset($record['thumbnail_meta']) ? unserialize($record['thumbnail_meta']) : null; ?>
		x1 = <?php echo $points['x1'] != '' ? $points['x1'] : 0 ?>;
		x2 = <?php echo $points['x2'] != '' ? $points['x2'] : 0 ?>;
		y1 = <?php echo $points['y1'] != '' ? $points['y1'] : 0 ?>;
		y2 = <?php echo $points['y2'] != '' ? $points['y2'] : 0 ?>;
	<?php else: ?>  
		x1 = 0;
		y1 = 0;
		x2 = 200;
		y2 = 200;			
	<?php endif; ?>;
	
	setTimeout(function() {
		if(jscrop_api)
			jcrop_api.destroy();
		$('#preview').Jcrop({
		  onChange:   showCoords,
		  onSelect:   showCoords,
		  setSelect: [x1, y1, x2, y2],
		  maxSize: [ 300, 300 ]
		},function(){
		  jcrop_api = this;
		});	
		$('#width').val($('#preview').width());
		$('#height').val($('#preview').height());
	}, 500);
}

function showCoords(c) {
    $('#x').val(c.x);
    $('#y').val(c.y);
    $('#x2').val(c.x2);
    $('#y2').val(c.y2);
};

$(function() {
	<?php if(isset($record['move_forward_image']) && $record['move_forward_image'] != ''): ?>
	initJCrop($('#photo'));
	<?php endif; ?>

  $('#type').change(function() {
    if($(this).val() == 'Redemption') {
      $('#r-address-container').show();
    } else {
      $('#r-address-container').hide();
    }
  });

});

</script>
<link rel="stylesheet" href="<?php echo BASE_URL ?>admin_assets/css/jquery.Jcrop.css" type="text/css" />
<link rel="stylesheet" href="<?php echo BASE_URL ?>admin_assets/css/bootstrap-tagsinput.css" type="text/css" />
<!--start main content -->
 <div class="container main-content" ng-app="fileUpload" ng-controller="MyCtrl">
      <div class="page-header">
           <h3>Perks</h3>
      </div>
       <div class="alert alert-danger" <?php if(!$error) echo 'style="display:none;"' ?>><?php echo $error; ?></div>
       <form class="form-horizontal" role="form" method="post" onsubmit="return submitFrm()" enctype="multipart/form-data">
           <div class="form-group">
                <label class="col-sm-2 control-label">Buy</label>
                <div class="col-sm-4">
                     <input type="file" id="photo-buy" name="photo_buy" value="" data-name="module name" class="form-control" onchange="previewImage('photo-buy', 'preview-buy', initJCrop, this);"><br>
                     <strong>Width: 400px. Height: 300px.</strong>
                </div>
           </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">Image Preview</label>
                <div class="col-sm-4">
                   <img id="preview-buy" class="preview-buy" <?php echo isset($record['buy']['description']) ? "style='max-width:250px;' src='" . BASE_URL . 'uploads/perks/' . $record['buy']['description'] . "'" : '' ?>  />
                </div>
           </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">Bid</label>
                <div class="col-sm-4">
                     <input type="file" id="photo" name="photo_bid" value="" data-name="module name" class="form-control" onchange="previewImage('photo', 'preview', initJCrop, this);"><br>
                     <strong>Width: 400px. Height: 300px.</strong>
                </div>
           </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">Image Preview</label>
                <div class="col-sm-4">
                   <img id="preview" class="preview" <?php echo isset($record['bid']['description']) ? "style='max-width:250px;' src='" . BASE_URL . 'uploads/perks/' . $record['bid']['description'] . "'" : '' ?>  />
                </div>
           </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">Reserve</label>
                <div class="col-sm-4">
                     <input type="file" id="photo-reserve" name="photo_reserve" value="" data-name="module name" class="form-control" onchange="previewImage('photo-reserve', 'preview-reserve', initJCrop, this);"><br>
                     <strong>Width: 400px. Height: 300px.</strong>
                </div>
           </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">Image Preview</label>
                <div class="col-sm-4">
                   <img id="preview" class="preview-reserve" <?php echo isset($record['reserve']['description']) ? "style='max-width:250px;' src='" . BASE_URL . 'uploads/perks/' . $record['reserve']['description'] . "'" : '' ?>  />
                </div>
           </div>
          
          <div class="form-group">
                <div class="col-sm-offset-2 col-sm-4">
                     <input type="hidden" name="bid_item_image" value="<?php echo isset($record['bid_item_image']) ? $record['bid_item_image'] : '' ?>" />
                     <input type="hidden" ng-model="httpMethod" ng-init="httpMethod = 'POST'" value="POST"/>
                     <input type="hidden" name="howToSend" ng-model="howToSend" value="1" ng-init="howToSend = 1">
                     <button type="submit" id="submit" class="btn btn-primary">Update</button>
                </div>
           </div>
      </form>
      
 </div>
 <script>

// $("#add-form-product-info").submit( function() {
//  bootbox.dialog({
//    message: "<center><strong>Loading</strong></center>",
//    show: true,
//    backdrop: true,
//    closeButton: false
//  });
// });

function removeValue(el) {
  elem = $(el);
  elem.parent().parent().parent().parent().remove();
}
 </script>
 <!--end main content -->
 