<!-- js -->
<script>
  $(function() {
    $("#startdate, #enddate").datepicker({dateFormat : "yy-mm-dd"});
    $(".number-only").keypress(validateNumber);
    $("#quantity-if-bulk").hide();
    $('.alert').hide();
    var request;
  });

  function submitForm() {
    $('.alert').hide();
    theForm = $('form');
    error = validateForm(theForm);
    data = false;
    if(error) {
      $('.alert').show().html(error);
      $(document).scrollTop(0);
      return false;
    } else {
      proceed = confirm("Generate " + $("input[name=quantity]").val() + " pack codes?");
      if(proceed) {
          $("#loader").show();
          // Show progress
          setInterval(function(){
              $("#progress").load("<?=base_url()?>ajax_logs.txt");
          }, 3000);
          // /Show progress
          var inputs = $("#add-form-product-info").serialize();
          $('input[type=radio], input[type=text], select, a, button, textarea').attr('disabled', true);
          $.post(siteUrl+"/pack_codes/generate", inputs, function(response) {
            console.log(response);
              if(response) {
                  window.location.href = siteUrl+"/pack_codes";
              }
          });
      }
      return false;
    }
    return false;
  }

  function changeUnit(unit) {
      switch(unit) {
        case 'single': $("#quantity-if-bulk").hide(); $("input[name=quantity]").removeClass("required"); $("input[name=quantity]").val('1'); break;
        case 'bulk': $("#quantity-if-bulk").show(); $("input[name=quantity]").addClass("required"); $("input[name=quantity]").val(''); break;
      }
  }

  function validateNumber(event) {
    var key = window.event ? event.keyCode : event.which;

    if (event.keyCode == 8 || event.keyCode == 46
     || event.keyCode == 37 || event.keyCode == 39) {
        return true;
    }
    else if ( key < 48 || key > 57 ) {
        return false;
    }
    else return true;
}

$('.manage-category').click(function() {

});
</script>
<!-- /js -->
<noscript>
  <style>
  .col-xs-4 {
    display: none;
  }
  </style>
</noscript>
<script src="<?php echo BASE_URL ?>admin_assets/js/bootstrap-tagsinput.js"></script>
<!--start main content -->
 <div class="container main-content">
      <div class="page-header">
           <h3>Add Pack Code</h3>
      </div>

       <div class="alert alert-danger">  </div>

       <form class="form-horizontal" role="form" method="post" id="add-form-product-info" onsubmit="return submitForm()">
            <div class="form-group">
                <label class="col-sm-2 control-label">Unit</label>
                <div class="col-sm-4">
                    <input type="radio" value="0" class="prop-check" name="unit" onchange="changeUnit('single')" checked> Single
                    <input type="radio" value="1" class="prop-check" name="unit" onchange="changeUnit('bulk')"> Bulk
                </div>
           </div>

           <div class="form-group" id="quantity-if-bulk">
                <label class="col-sm-2 control-label">Quantity</label>
                <div class="col-sm-4">
                     <input type="text" name="quantity" data-name="quantity" class="form-control number-only" onkeydown="checkDigit(this)" value="1">
                </div>
           </div>

           <div class="form-group">
                <label class="col-sm-2 control-label">Max Usage</label>
                <div class="col-sm-4">
                     <input type="text" name="stock" data-name="max usage" class="required form-control number-only">
                </div>
           </div>

           <div class="form-group">
                <label class="col-sm-2 control-label">Value</label>
                <div class="col-sm-4">
                     <input type="text" name="km_points" data-name="value" class="required form-control number-only">
                </div>
           </div>

           <div class="form-group">
                <label class="col-sm-2 control-label">Validity</label>
                <div class="col-sm-4">
                     <input type="text" name="start_date" value="" data-name="validity start date" class="required form-control" id="startdate"><br>
                     <input type="text" name="end_date" value="" data-name="validity end date" class="required form-control" id="enddate">
                </div>
           </div>

           <div class="form-group">
                <label class="col-sm-2 control-label">Notes</label>
                <div class="col-sm-4">
                    <textarea class="form-control" name="notes"></textarea>
                </div>
           </div>

           <div class="form-group">
                <label class="col-sm-2 control-label">Category</label>
                <div class="col-sm-4">
                    <select name="category" data-name="category" class="required form-control" style="width: 80%; float: left">
                        <option value="Pack Codes">Pack Codes</option>
                        <option value="LAMP Drive">LAMP Drive</option>
                        <option value="ELD">ELD</option>
                        <option value="Leave Behind">Leave Behind</option>
                        <option value="KA Codes">KA Codes</option>
                        <?php /* <option value="Hidden Packs">Hidden Packs</option> */ ?>
                    </select>
                </div>
           </div>

           <?php /*
           <div class="form-group">
                <label class="col-sm-2 control-label">Category</label>
                <div class="col-sm-4">
                    <select name="category" data-name="category" class="required form-control" style="width: 80%; float: left">
                        <option value="Pack Codes">Pack Codes</option>
                        <?php if($categories): foreach($categories as $k => $v): ?>
                            <option value="<?=$v['pack_code_category_id']?>"><?=$v['name']?></option>
                        <?php endforeach; endif; ?>
                    </select>
                    &nbsp;&nbsp;&nbsp;
                    <a href="#" class="btn btn-primary manage-category" onclick="javascript:void(0)" data-toggle="modal" data-target="#myModal">Manage</a>
                </div>
           </div>
           */ ?>

           <div class="form-group">
                <label class="col-sm-2 control-label"></label>
                <div class="col-sm-4">
                      <a href="<?= SITE_URL ?>/lamp_events" class="btn btn-primary">Back</a>
                     <button type="submit" class="btn btn-primary" name="submit" value="1" id="start-generation">Submit</button>
                     <img style="display:none" src="<?=BASE_URL?>/admin_assets/images/spinner.gif" id="loader">
                     <span id="progress"></span>
                </div>
           </div>
      </form>
      
      <!-- Modal -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
              ...
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary">Save changes</button>
            </div>
          </div>
        </div>
      </div>


 </div>
 <!--end main content -->
 