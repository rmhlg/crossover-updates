<!-- js -->
<script>
  $(function() {
      $("#startdate, #enddate").datepicker({dateFormat : "yy-mm-dd"});
      $("#check-all").click(function(){
          $("input:checkbox").not(this).prop('checked', this.checked);
      });
  });
</script>
<!-- /js -->
<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3>Pack Codes <span class="badge badge-important"><?php echo isset($count) ? $count : 0 ?></span></h3>

               <div class="actions">
                    <a href="<?php echo SITE_URL ?>/pack_codes/add" class="btn btn-primary">Add Pack Code</a>
                    <a href="<?php echo SITE_URL ?>/pack_codes/export?<?php echo http_build_query($_GET) ?>" class="btn btn-primary">Export</a>
               </div>
          </div>

                  <table class="table table-bordered">
                         <thead>
                              <tr>
                                   <th>#</th>
                                   <th>Pack Code</th>
                                   <th>Max Usage</th>
                                   <th>Value (as KM)</th>
                                   <th>Validity Start Date</th>
                                   <th>Validity End Date</th>
                                   <th>Action</th>
                              </tr>
                         </thead>
                         <tbody>
                             <form action="<?php echo SITE_URL ?>/pack_codes">
                                   <tr>
                                       <td></td>
                                       <td><input class="form-control" name="code" value="<?php echo isset($_GET['code']) ? $_GET['code'] : '' ?>"></td>
                                       <td><input class="form-control" name="stock" value="<?php echo isset($_GET['stock']) ? $_GET['stock'] : '' ?>"></td>
                                       <td><input class="form-control" name="km_points" value="<?php echo isset($_GET['km_points']) ? $_GET['km_points'] : '' ?>"></td>
                                       <td><input class="form-control" name="start_date" value="<?php echo isset($_GET['start_date']) ? $_GET['start_date'] : '' ?>" id="startdate"></td>
                                       <td><input class="form-control" name="end_date" value="<?php echo isset($_GET['end_date']) ? $_GET['end_date'] : '' ?>" id="enddate"></td>
                                      <td>
                                        <div class="row">
                                          <button class="btn btn-primary" name="search" value="1">Go</button>
                                      </form>
                                        </div>
                                      </td>
                                  </tr>

                              <?php if($items): foreach($items as $k => $v): ?>
                              <tr id="row-<?php echo $v['pack_code_id'] ?>">
                                   <td><?php echo $offset + $k + 1 ?></td>
                                   <td><?php echo $v['code'] ?></td>
                                   <td><?php echo $v['stock'] ?></td>
                                   <td><?php echo $v['km_points'] ?></td>
                                   <td><?php echo date('Y-m-d', strtotime($v['start_date'])) ?></td>
                                   <td><?php echo date('Y-m-d', strtotime($v['end_date'])) ?></td>
                                   <td></td>
                               </tr>
                              <?php endforeach; else: ?>
                                <tr><td colspan="10"><center>No records found</center></td></tr>
                              <?php endif; ?>
                         </tbody>
                    </table>

                    <ul class="pagination pagination-sm pull-right">
                         <?php echo $pagination ?>
                    </ul>
          </div>
     <!--end main content -->