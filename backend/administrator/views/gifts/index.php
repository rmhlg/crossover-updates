<!-- js -->
<script>
  $(function() {
      $("#startdate, #enddate").datepicker({dateFormat : "yy-mm-dd"});
      $("#check-all").click(function(){
          $("input:checkbox").not(this).prop('checked', this.checked);
      });
      $("#delete").on("click", function(){
          var items = $("input[name='delete_item[]']:checked").map(function () {return this.value;}).get().join(",");
          if(items) {
              var proceed = confirm("Delete this item/s?");
              if(proceed) {
                  var explode = items.split(",");
                  $.post(siteUrl+"/gifts/delete", {ids:explode}, function(){
                      for(i = 0; i < explode.length; i++) {
                          $("#row-"+explode[i]).hide();
                      }    
                  });                
              }
          }
      });
  });
</script>
<!-- /js -->
<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3>Gifts <span class="badge badge-important"><?php echo isset($count) ? $count : 0 ?></span></h3>

               <div class="actions">
                    <a href="<?= site_url() ?>/gifts/add" class="btn btn-primary">Add Gift</a>
                    <a href="<?= site_url() ?>/gifts/export?<?php echo http_build_query($_GET) ?>" class="btn btn-primary">Export</a>
               </div>
          </div>

                  <table class="table table-bordered">
                         <thead>
                              <tr>
                                   <th>#</th>
                                   <th>Image</th>
                                   <th>Title</th>
                                   <th>Required Points (KM)</th>
                                   <th>Stock</th>
                                   <th>Action</th>
                              </tr>
                         </thead>
                         <tbody>
                             <form action="<?php echo site_url() ?>/gifts">
                                   <tr>
                                       <td>
                                          <select name="category" class="form-control">
                                              <option value="">All</option>
                                              <option value="<?= REGION_CALIFORNIA ?>" <?=isset($_GET['category']) && $_GET['category'] == REGION_CALIFORNIA ? 'selected' : ''?>>California</option>
                                              <option value="<?= REGION_ALASKA ?>" <?=isset($_GET['category']) && $_GET['category'] == REGION_ALASKA ? 'selected' : ''?>>Alaska</option>
                                              <option value="<?= REGION_CANADA ?>" <?=isset($_GET['category']) && $_GET['category'] == REGION_CANADA ? 'selected' : ''?>>Canada</option>
                                         </select>
                                       </td>
                                       <td><input class="form-control" name="name" value="<?= isset($_GET['name']) ? $_GET['name'] : '' ?>"></td>
                                       <td><input class="form-control" name="km_points" value="<?= isset($_GET['km_points']) ? $_GET['km_points'] : '' ?>"></td>
                                       <td><input class="form-control" name="stock" value="<?= isset($_GET['stock']) ? $_GET['stock'] : '' ?>"></td>
                                      <td>
                                        <div class="row">
                                          <button class="btn btn-primary" name="search" value="1">Go</button>
                                      </form>
                                        </div>
                                      </td>
                                  </tr>

                              <?php if($items): foreach($items as $k => $v): ?>
                              <tr id="row-<?= $v['gift_id'] ?>">
                                   <td><?= $offset + $k + 1 ?></td>
                                   <td><img width="100" height="100" src="<?php echo SITE_URL.'/../uploads/'.$v['image']; ?>" />
                                   </td>
                                   <td><?= $v['name'] ?></td>
                                   <td><?= $v['km_points'] ?></td>
                                   <td><?= $v['stock'] ?></td>
                                   <td>
                                        <a href="<?php echo site_url() ?>/gifts/edit/<?php echo $v['gift_id'] ?>" class="btn btn-primary">Edit</a>
                                        <a href="<?php echo site_url() ?>/gifts/delete/<?php echo $v['gift_id'] .'/'. md5($v['gift_id'] . ' ' . $this->config->item('encryption_key')) ?>" onclick="return confirmDeletion(this, 'promotion')" class="btn btn-danger">Delete</a>
                                   </td>
                               </tr>
                              <?php endforeach; else: ?>
                                <tr><td colspan="10"><center>No records found</center></td></tr>
                              <?php endif; ?>
                         </tbody>
                    </table>

                    <ul class="pagination pagination-sm pull-right">
                         <?php echo $pagination ?>
                    </ul>
          </div>