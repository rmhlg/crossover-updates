
<!-- Brand and toggle get grouped for better mobile display -->

<div class="navbar-header">

  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">

    <span class="sr-only">Toggle navigation</span>

    <span class="icon-bar"></span>

    <span class="icon-bar"></span>

    <span class="icon-bar"></span>

  </button>

  <a class="navbar-brand" href="#">NuWorks</a>

</div>



<!-- Collect the nav links, forms, and other content for toggling -->

<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

 <ul class="nav navbar-nav">

  <li class="<?=$this->router->class=='home'? 'active' :''?>"><a href="<?=site_url('home')?>">Home</a></li>                    
                  <!-- <li>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Activities <b class="caret"></b></a>
                        <ul class="dropdown-menu" role="menu">
                          <?php /* <li class="<?=$this->router->class=='activity'? 'active' :''?>"> <a href="<?=site_url('activity')?>">Activities</a></li> */ ?>
                         <li class="<?=$this->router->class=='experience'? 'active' :''?>"><a href="<?=site_url('experience')?>">Experiences</a></li>
                          <li class="<?=$this->router->class=='activity_media'? 'active' :''?>"> <a href="<?=site_url('activity_media')?>">Medias</a></li>
                    <li class="<?=$this->router->class=='gifts'? 'active' :''?>"><a href="<?=site_url('gifts')?>" >Gifts</a></li>                                    
                    
                        </ul>
                      </li>-->
                      <?php /* <li class="<?=$this->router->class=='pack_codes'? 'active' :''?>"><a href="<?=site_url('pack_codes')?>" >Pack Codes</a></li> */ ?>
                  <!--  <li>
                      <a href="#" data-toggle="dropdown">Events <b class="caret"></b></a>
                      <ul class="dropdown-menu" role="menu">
                          <?php /* <li class="<?=$this->router->class=='activity'? 'active' :''?>"> <a href="<?=site_url('activity')?>">Activities</a></li> */ ?>
                          <li class="<?=$this->router->class=='backstage_events'? 'active' :''?>"> <a href="<?=site_url('backstage_events')?>">Events</a></li>
                          <li class="<?=$this->router->class=='backstage_event_attendance'? 'active' :''?>"> <a href="<?=site_url('backstage_event_attendance')?>">Attendance</a></li>
                        </ul>
                      </li>-->
                      <li>
                        <a href="#" data-toggle="dropdown">Moderation<b class="caret"></b></a>
                        <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu2">
                          <li class="dropdown-submenu"><a tabindex="-1" href="#">Events <span class="badge badge-important"></span></a>
                            <ul class="dropdown-menu">
                              <li>
                                <a href="<?php echo SITE_URL ?>/backstage_event_photos">User Photos <span class="badge badge-important"></span>
                                </a>
                              </li>
                            </ul>
                          </li>
                        </ul>
                      </li>
                  <!--  <li>
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown">Promo <b class="caret"></b></a>
                      <ul class="dropdown-menu" role="menu">
                         <li class="<?=$this->router->class=='hidden_packs'? 'active' :''?>"><a href="<?=site_url('hidden_packs')?>">Hidden Packs</a></li>
                        <li class="<?=$this->router->class=='activity_media'? 'active' :''?>"> <a href="<?=site_url('flash_offer')?>">Flash Offer</a></li>
                      </ul>
                    </li>-->
                    <li>
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown">Reports <b class="caret"></b></a>
                      <ul class="dropdown-menu" role="menu">
                        <li class="<?=$this->router->class=='points'? 'active' :''?>"> <a href="<?=site_url('points')?>">Points</a></li>
                        <li class="<?=$this->router->class=='user_activity'? 'active' :''?>"> <a href="<?=site_url('user_activity')?>">User Activity</a></li>
                        <li class="<?=$this->router->class=='login_logs'? 'active' :''?>"> <a href="<?=site_url('login_logs')?>">Login Logs</a></li>
                        <li class="dropdown-submenu"><a tabindex="-1" href="#">User <span class="badge badge-important"></span></a>
                          <ul class="dropdown-menu">
                            <li><a href="<?php echo SITE_URL ?>/user_checkins">User Check Ins <span class="badge badge-important"></span></a></li>
                            <li><a href="<?php echo SITE_URL ?>/user_birthday">Birthday Prizes</a></li>
                            <li><a href="<?php echo SITE_URL ?>/calendar_sync">Event Calendar Sync</a></li>
                          </ul>
                        </li>

                          <li class="dropdown-submenu"><a tabindex="-1" href="#">Games <span class="badge badge-important"></span></a>
                          <ul class="dropdown-menu">
                            <li><a href="<?php echo SITE_URL ?>/game_log">Logs <span class="badge badge-important"></span></a></li>
                            <li><a href="<?php echo SITE_URL ?>/gameplay">Summary</a></li>
                            <li><a href="<?php echo SITE_URL ?>/game_visits">Visit</a></li>
                          </ul>
                        </li>
                      </ul>
                    </li>
                    <li> 
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown">Content Management  <b class="caret"></b></a>
                      
                      <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu2">
                       <li class="<?=$this->router->class=='pack_codes'? 'active' :''?>"><a href="<?=site_url('pack_codes')?>" >Pack Codes</a></li>
                       <li class="<?=$this->router->class=='experience'? 'active' :''?>"><a href="<?=site_url('experience')?>">Experiences</a></li>
                       <li class="<?=$this->router->class=='activity_media'? 'active' :''?>"> <a href="<?=site_url('activity_media')?>">Medias</a></li>
                       <li class="<?=$this->router->class=='gifts'? 'active' :''?>"><a href="<?=site_url('gifts')?>" >Gifts</a></li>                                    
                       <li class="<?=$this->router->class=='backstage_events'? 'active' :''?>"> <a href="<?=site_url('backstage_events')?>">Events</a></li>
                       <li class="<?=$this->router->class=='backstage_event_attendance'? 'active' :''?>"> <a href="<?=site_url('backstage_event_attendance')?>">Attendance</a></li>
                       
                       
                       <li class="dropdown-submenu"><a tabindex="-1" href="#">System Notifications</a>
                        <ul class="dropdown-menu">
                          <li><a href="<?php echo SITE_URL ?>/birthday_offer">Birthday Offers</a></li>
                          <li class="<?=$this->router->class=='hidden_packs'? 'active' :''?>"><a href="<?=site_url('hidden_packs')?>">Hidden Packs</a></li>
                          <li class="<?=$this->router->class=='activity_media'? 'active' :''?>"> <a href="<?=site_url('flash_offer')?>">Flash Offer</a></li>
                        </ul>
                      </li>
                      <li class="dropdown-submenu"><a tabindex="-1" href="#">Perks</a>
                        <ul class="dropdown-menu">   
                          <li><a href="<?php echo SITE_URL; ?>/lamps">LAMPS</a></li>
                          <li><a href="<?php echo SITE_URL; ?>/lamp_permissions">LAMP Permissions</a></li>
                          <li><a href="<?php echo SITE_URL; ?>/lamp_promotions">LAMP Promotions</a></li>
                          <li><a href="<?php echo SITE_URL; ?>/lamp_events">LAMP Events</a></li>
                        </ul>
                      </li>
                      <li class="dropdown-submenu"><a tabindex="-1" href="#">Settings</a>
                        <ul class="dropdown-menu">  
                          <li><a href="<?php echo SITE_URL ?>/activity_settings">Activities</a></li>
                          <li><a href="<?php echo SITE_URL ?>/section_settings">Sections</a></li>
                        </ul>
                      </li>
                    </ul>
                  </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                  <li><a href="<?=site_url('login/destroy')?>">Logout</a></li>
                </ul>
              </div><!-- /.navbar-collapse -->
     <!--end header-->