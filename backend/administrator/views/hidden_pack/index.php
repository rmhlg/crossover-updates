<!--start main content -->
 <div class="container main-content">
      <div class="page-header">
           <h3>Hidden Marlboro Pack</h3>
      </div>
       <div class="alert alert-danger" <?php if(!$error) echo 'style="display:none;"' ?>><?php echo $error; ?></div>
       <form class="form-horizontal" role="form" method="post" onsubmit="return submitFrm()">
           <div class="form-group">
                <label class="col-sm-2 control-label">Status</label>
                <div class="col-sm-8">
                     <p class="form-control-static"><?php echo $settings['description'] == 1 ? 'Enabled' : 'Disabled' ?></p>
                </div>
           </div>
          <?php if($edit): ?>
          <div class="form-group">
                <div class="col-sm-offset-2 col-sm-4">
                     <input type="hidden" name="submit" value="1" />
                     <input type="hidden" name="description" id="description" value="<?php echo $settings['description'] == 1 ? 0 : 1 ?>" />
                     <button type="button" id="cancel" class="btn">Cancel</button>
                     <?php if($settings['description'] == 1): ?>
                     <button type="submit" id="submit" class="btn btn-danger">Disable</button>
                     <?php else: ?>
                     <button type="submit" id="submit" class="btn btn-success">Enable</button>
                     <?php endif;?>

                </div>
           </div>
         <?php endif; ?>
      </form>
      
 </div>
 <!--end main content -->

 <script>
  $(function() {
    $('#cancel').click(function() {
      window.location = '<?php echo SITE_URL ?>';
    });
  })
 </script>
 