
<script src="<?=BASE_URL?>/assets/admin/js/angular-js/angular-file-upload-shim.js"></script>
<script src="<?=BASE_URL?>/assets/admin/js/angular-js/angular.js"></script>
<script src="<?=BASE_URL?>/assets/admin/js/angular-js/angular-file-upload.js"></script>
<script src="<?=BASE_URL?>/assets/admin/js/angular-js/upload-hidden-pack.js"></script>

 <!--start main content -->
 <div class="container main-content" ng-app="uploadHiddenPack" ng-controller="MyCtrl">
      <div class="page-header">
           <h3><?php echo $header_text; ?></h3>
      </div>

      <?php if($error){ ?>
       <div class="alert alert-danger"> <?php echo $error; ?> </div>
      <?php } ?>

       <form action="<?=$action?>" class="form-horizontal" role="form" method="post" enctype="multipart/form-data" id="form">

           <div class="form-group">
                <label class="col-sm-2 control-label">Image :</label>
                <div class="col-sm-4">
                  <input type="file" ng-file-select="onFileSelect($files)" multiple>
                  <input type="hidden" name="image" value="{{image_pack}}" ng-model="image_pack">
                  <?php if($row){ ?>
                  <input type="hidden" name="current_image" value="<?=$row->image?>">
                  <?php } ?><br>
                  <strong>Width: 106px. Height: 63px.</strong>
                </div>
           </div>

           <?php if($row){ ?>
           <div class="form-group" ng-show="selectedFiles == null">
                <label class="col-sm-2 control-label"></label>
                <div class="col-sm-4">
                  <img src="<?=BASE_URL?>/uploads/hidden_packs/100_100_<?=$row->image?>">
                </div>
           </div>
           <?php } ?>

          <div class="form-group" >
            <label class="col-sm-2 control-label"></label>
           <div class="col-sm-10" >
            <div class="row" ng-show="selectedFiles != null">
              <div class="col-md-3 col-xs-3" ng-repeat="f in selectedFiles">
                <div class="thumbnail">
                  <img ng-show="dataUrls[$index]" ng-src="{{dataUrls[$index]}}">
                  <div class="caption">
                    <p class="text-danger">{{uploadError[$index]}} {{removeError[$index]}}</p>
                    <span class="progress progress-striped" ng-class="{ active : !uploadResult[$index] }" ng-show="progress[$index] >= 0">           
                    <div class="progress-bar" style="width:{{progress[$index]}}%">{{progress[$index]}}%</div>
                    </span>
                    <div class="clearfix"></div>
                    <p>size: {{f.size}}B<br/>type: {{f.type}}</p>
                    <button type="button" class="btn" ng-click="abort($index)" ng-show="hasUploader($index) && !uploadResult[$index]">Abort</button>
                  </div>
                </div>                  
              </div>
            </div> 
           </div>
          </div>
 
           <div class="form-group">
                <label class="col-sm-2 control-label">Title :</label>
                <div class="col-sm-4">
                     <input type="text" value="<?php echo ($row) ? $row->title : '';  ?>" name="title" class="form-control required" >
                </div>
           </div>

           <div class="form-group">
                <label class="col-sm-2 control-label">Description :</label>
                <div class="col-sm-4">
                    <textarea name="description" class="form-control required" ><?php echo ($row) ? $row->description : '';  ?></textarea>
                 </div>
           </div>
           <div class="form-group">
                <label class="col-sm-2 control-label">Popup Layout :</label>
                <div class="col-sm-4">
                    <select name="popup_layout" class="form-control">
                      <option <?=($row && $row->popup_layout=='Default') ? 'selected' : ''?>>Default</option>
                      <option <?=($row && $row->popup_layout=='Premium Black') ? 'selected' : ''?>>Premium Black</option>                      
                    </select>                
                </div>
           </div>

           <div class="form-group">
                <label class="col-sm-2 control-label">Start Date :</label>
                <div class="col-sm-4">
                     <input type="text" value="<?php echo ($row) ? date('Y-m-d',strtotime($row->start_date)) : '';  ?>" name="start_date" class="form-control required from" >
                </div>
           </div>

           <div class="form-group">
                <label class="col-sm-2 control-label">End Date :</label>
                <div class="col-sm-4">
                     <input type="text" value="<?php echo ($row) ? date('Y-m-d',strtotime($row->end_date)) : '';  ?>" name="end_date" class="form-control required to" >
                </div>
           </div>

           <div class="form-group">
           <?php 
            $pages = $this->input->post('page') ? (array)$this->input->post('page') : array();
            
            if($row && @$row->{"page"} && !$pages){ 
              $pages = explode(',',$row->page);
            }
            ?>
                <label class="col-sm-2 control-label">Pages:</label>
                <div class="col-sm-4">
                    <label> <input type="checkbox" name="page[]" <?=in_array('welcome', $pages) ? 'checked' : ''?> value="welcome"> HOME</label><br>
                    <label> <input type="checkbox" name="page[]" <?=in_array('about', $pages) ? 'checked' : ''?> value="about"> ABOUT</label><br>
                    <label> <input type="checkbox" name="page[]" <?=in_array('backstage_pass', $pages) ? 'checked' : ''?> value="backstage_pass"> BACKSTAGE PASS</label><br>
                    <label> <input type="checkbox" name="page[]" <?=in_array('perks', $pages) ? 'checked' : ''?> value="perks"> PERKS</label><br>
                    <label> <input type="checkbox" name="page[]" <?=in_array('move_forward', $pages) ? 'checked' : ''?> value="move_forward"> MOVEFWD</label><br>
                    <label> <input type="checkbox" name="page[]" <?=in_array('games', $pages) ? 'checked' : ''?> value="games"> GAMES</label><br>
                    <label> <input type="checkbox" name="page[]" <?=in_array('profile', $pages) ? 'checked' : ''?> value="profile"> PROFILE</label>
                </div>

           </div>

           <div class="form-group">
                <div class="col-sm-offset-2 col-sm-4"> 
                     <a href="<?php echo SITE_URL.'/'.$this->uri->segment(1); ?>" class="pull-left"><< Back</a>
                      <button type="submit" class="btn btn-primary pull-right">Submit</button>
                </div>
           </div>
           <input type="text" ng-model="uploadURL" ng-init="uploadURL='<?=SITE_URL.'/'.$this->router->class?>/upload_photo'" style="visibility:hidden;"> 
                      
      </form>
      
 </div>
 <!--end main content --> 
