<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3> <?php echo $header_text; ?> <span class="badge badge-important"><?php echo (int)$total_rows; ?></span></h3>

               <div class="actions">
                    <?php if($add): ?>
                    <a href="<?php echo SITE_URL.'/'.$this->uri->segment(1).'/add'; ?>" class="btn btn-primary">Add Event</a>
                   	<?php endif; ?>
                    <a href="<?php echo SITE_URL.'/'.$this->uri->segment(1).'/export'; ?>" class="btn btn-primary">Export</a>   
               </div>
          </div>
          
          <table class="table table-bordered">
               <thead>
                    <tr>
                         <th>Image</th>
                         <th>Event</th>
                         <th>Description</th>
                         <th>Region</th>
                         <th>Venue</th>
                         <th>Start Date</th>
                         <th>End Date</th>
                         <th>Date Added</th>
                         <th><?php if($edit || $delete) : ?>Operation<?php endif; ?></th>                         
                    </tr>
               </thead>
               <tbody>
               		 <form action="<?php echo SITE_URL.'/'.$this->uri->segment(1); ?>">
                     <tr>
                        <th></th>
                        <th><input type="text" name="title" class="form-control" value="<?php echo $this->input->get('title'); ?>"></th>
                        <th><input type="text" name="description" class="form-control" value="<?php echo $this->input->get('description'); ?>"/></th>
                        <th>
                          <select name="region_id" class="form-control">
                              <option value=""></option>
                              <?php if($regions->num_rows()){
                                      foreach($regions->result() as $r){ 
                                        if($this->input->get('region_id')==$r->region_id){?>
                                          <option selected="selected" value="<?=$r->region_id?>"><?=$r->name?></option>
                              <?php     }else{ ?>
                                          <option value="<?=$r->region_id?>"><?=$r->name?></option>
                              <?php     }
                                        
                                      }
                                    } ?>
                                  </select></th>
                        <th>
                        <select name="venue" class="form-control">
                        <option value=""></option>
                          <?php if($venues->num_rows()){
                                  foreach($venues->result() as $r){ 
                                    if(strtolower($this->input->get('venue'))==strtolower($r->name)){?>
                                      <option selected="selected" value="<?=$r->name?>"><?=$r->name?></option>
                          <?php     }else{ ?>
                                      <option value="<?=$r->name?>"><?=$r->name?></option>
                          <?php     }
                                  }

                                } ?>
                        </select>
                        <th><input name="start_date" class="form-control from" value="<?php echo $this->input->get('start_date'); ?>" /></th>
                        <th><input name="end_date" class="form-control to" value="<?php echo $this->input->get('end_date'); ?>" /></th>                         
                        <th>From:<input name="from_date_added" class="form-control from" value="<?php echo $this->input->get('from_date_added'); ?>" /><br> 
                            To:<input name="to_date_added" class="form-control to" value="<?php echo $this->input->get('to_date_added'); ?>" /></th>                       
                       <th><?php if($edit || $delete) : ?><button type="submit" class="btn btn-primary">Go</button><?php endif; ?></th>
                       
                     </tr>
                  </form>
                  <?php if($rows->num_rows()){

                            $edit_url = SITE_URL.'/'.$this->uri->segment(1).'/edit/';
 
                            foreach($rows->result() as $v){ ?>
                              <tr>
                                  <td><center><a class="view-video" href="<?=SITE_URL.'/'.$this->uri->segment(1).'/view_photo/'.$v->backstage_event_id?>"><img src="<?php echo BASE_URL.'/uploads/backstage/events/'.'/50_50_'.$v->image.'?id='.uniqid(); ?>?" /></a></center></td>
                                  <td><?=$v->title; ?></td>
                                  <td><?=$v->description?></td>
                                  <td><?=$v->region_name?></td>
                                  <td><?=$v->venue?></td>
                                  <td><?=$v->start_date?></td>
                                  <td><?=$v->end_date?></td>
                                  <td><?=$v->date_added ?></td>                                  
                                  <td>
                                    <?php if($edit) : ?>
                                    <a href="<?php echo $edit_url.$v->backstage_event_id; ?>" class="btn btn-primary">Edit</a>
                                    <?php endif; ?>


                                   <a href="<?php echo SITE_URL.'/backstage_events/photos?backstage_event_id='.$v->backstage_event_id; ?>" class="btn btn-primary">View Photos</a>


    
                                    <?php if($delete) : ?>
                                    <a href="<?php echo SITE_URL.'/backstage_events/delete/'.$v->backstage_event_id .'/'.md5($v->backstage_event_id. ' ' . $this->config->item('encryption_key')); ?>" onclick="return confirmDeletion(this, 'event')" class="btn btn-danger">Delete</a>
                                    <?php endif; ?>                                    
                                  </td>
                                
                              </tr>
                  <?php     }
                        }else{ ?>
                          <tr><td colspan="7">No records found.</td></tr>
                  <?php  } ?>
                  
               </tbody>
          </table>

          <ul class="pagination pagination-sm pull-right">
               <?php echo $pagination ?>
          </ul>

     </div>
     <!--end main content -->