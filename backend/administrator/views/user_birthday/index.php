<?php 
  $controller = $this->uri->segment(1); 
  $label = 'User Birthday Prize';
?>
<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3><?php echo $label ?>s <span class="badge badge-important"><?php echo isset($total_rows) ? $total_rows : 0 ?></span></h3>
               <div class="actions">
                <a href="<?php echo SITE_URL ?>/user_birthday/export?<?php echo http_build_query($_GET) ?>" class="btn btn-primary">Export</a>        
               </div>
          </div>
          
          <table class="table table-bordered">
               <thead>
                    <tr>
                          <th>Name</th>
                         <th>Prize</th>
                         <th>Thumbnail</th>
                         <th>Status</th>
                         <th>Type</th>
                         <th>Date Availed</th>
                         <th>Date Claimed</th>
                         <th>Operation</th>
                    </tr>
               </thead>
               <tbody>
                    <form action="<?php echo SITE_URL ?>/user_birthday">
                     <tr>
                          <td><input name="registrant_name" class="form-control" value="<?php echo isset($_GET['registrant_name']) ? $_GET['registrant_name'] : '' ?>" /></td>
                         <td><input name="prize_name" class="form-control" value="<?php echo isset($_GET['prize_name']) ? $_GET['prize_name'] : '' ?>" /></td>
                         <td></td>
                         <td>
                          <select name="prize_status" class="form-control">
                            <option></option>
                            <option value="0" <?php echo $this->input->get('prize_status') != '' && $this->input->get('prize_status') == '0' ? 'selected' : '' ?>>Pending</option>
                            <option value="1" <?php echo $this->input->get('prize_status') != '' && $this->input->get('prize_status') == '1' ? 'selected' : '' ?>>Delivered</option>
                            <option value="2" <?php echo $this->input->get('prize_status') != '' && $this->input->get('prize_status') == '2' ? 'selected' : '' ?>>Redeemed</option>
                          </select>
                        </td>
                         <td>
                          <select name="send_type" class="form-control">
                            <option></option>
                            <option <?php echo $this->input->get('send_type') != '' && $this->input->get('send_type') == 'Delivery' ? 'selected' : '' ?>>Delivery</option>
                            <option <?php echo $this->input->get('send_type') != '' && $this->input->get('send_type') == 'Redemption' ? 'selected' : '' ?>>Redemption</option>
                          </select>
                         </td>
                         <td>
                              <div class="row">
                                <div class="col-md-4">
                                     <input name="fromavail" placeholder="From" class="form-control from" value="<?php echo isset($_GET['fromavail']) ? $_GET['fromavail'] : '' ?>" />
                                </div>
                                <div class="col-md-4">
                                     <input name="toavail" placeholder="To" class="form-control to" value="<?php echo isset($_GET['toavail']) ? $_GET['toavail'] : '' ?>" />
                                </div>
                              </div>
                        </td>
                         <td>
                          <div class="row">
                              <div class="col-md-4">
                                   <input name="fromredeem" placeholder="From" class="form-control from" value="<?php echo isset($_GET['fromredeem']) ? $_GET['fromredeem'] : '' ?>" />
                              </div>
                              <div class="col-md-4">
                                   <input name="toredeem" placeholder="To" class="form-control to" value="<?php echo isset($_GET['toredeem']) ? $_GET['toredeem'] : '' ?>" />
                              </div>
                            </div>
                        </td>
                         <td><button class="btn btn-primary" name="search" value="1">Go</button></td>
                    </tr>
                    </form>
               		  <?php if($rows->num_rows()): 
					foreach($rows->result_array() as $k => $v): ?>
                    <tr>
                        <td><a href="#" onclick="showRegistrantDetails('<?=$v['registrant_id']?>')"><?php echo $v['registrant_name'] ?></a></td>
                         <td><?php echo $v['prize_name'] ?></td>
                         <td><img src="<?php echo $v['prize_image'] ? SITE_URL . '/../uploads/prize/' . $v['prize_image'] : SITE_URL . '/../images/no_image.jpg' ?>?uniqid=<?php echo uniqid() ?>" width="150" /></td>
                         <td>
                            <?php 
                              $status = 'Pending';
                              if($v['prize_status'] == 1) {
                                 $status = 'Delivered';                                                                  
                              }else if($v['prize_status'] == 2){
                                $status = 'Redeemed';    
                              } 
                              echo $status;
                            ?>
                         </td>
                         <td><?php echo $v['send_type'] ?></td>
                         <td><?php echo date('F d, Y H:i:s', strtotime($v['date_created'])) ?></td>
                         <td><?php echo $v['prize_status'] == 0 ? 'N/A' : date('F d, Y H:i:s', strtotime($v['date_claimed'])) ?></td>
                         <td>
                            <?php if($edit): ?>
                            <a href="<?php echo SITE_URL ?>/<?php echo $controller ?>/edit/<?php echo $v['user_birthday_prize_id'] ?>" class="btn btn-primary">Edit</a>
                            <?php endif; ?>

                            <?php if(!$v['prize_status'] && $v['send_type']=='Delivery'){ ?>
                                    <a class="btn btn-warning" href="<?=SITE_URL?>/user_birthday/update_status/<?=$v['user_birthday_prize_id']?>/<?=md5($v['user_birthday_prize_id']. ' ' . $this->config->item('encryption_key'))?>/1" onclick="return confirmAction(this,'redeem');">Deliver</a>
                            <?php } ?>

                            <?php if(!$v['prize_status'] && $v['send_type']=='Redemption'){ ?>
                                    <a class="btn btn-warning" href="<?=SITE_URL?>/user_birthday/update_status/<?=$v['user_birthday_prize_id']?>/<?=md5($v['user_birthday_prize_id']. ' ' . $this->config->item('encryption_key'))?>/2" onclick="return confirmAction(this,'redeem');">Redeem</a>
                            <?php } ?>
                         </td>
                    </tr>
                    <?php 
					endforeach;
           else:
            echo '<tr><td colspan="9">No records found</td></tr>';
					endif; ?>
               </tbody>
          </table>
          <ul class="pagination pagination-sm pull-right">
               <?php echo $pagination ?>
          </ul>

          

     </div>
     <!--end main content -->