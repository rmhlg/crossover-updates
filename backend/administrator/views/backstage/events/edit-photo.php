<div class="modal-header">
   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
   <h4 class="modal-title">Rotate</h4>
  </div>
<div class="modal-body text-center">
  <?php if($row){
  $file = file_exists('uploads/backstage/photos/'.$row->media_image_filename) ? BASE_URL.'uploads/backstage/photos/'.$row->media_image_filename : STAGING_BASE_URL.'/../uploads/backstage/photos/'.$row->media_image_filename ; ?>
  <button class="btn btn-default btn-sm rotation-btn" id="img-rotate-left"><span class="glyphicon glyphicon-chevron-left"></span> </button>
  <button class="btn btn-default btn-sm rotation-btn" id="img-rotate-right"><span class="glyphicon glyphicon-chevron-right"></span> </button>
  <button class="btn btn-primary btn-sm rotation-btn" id="img-save">Save</button>
  <br>
  <center id="edit-photo-loading" style="display:none;">Please wait...</center>
  <figure style="padding-top: 10%;">
    <img id="edit-photo-img" style="max-width: 50%; max-height: 50%;" src="<?=$file?>?rand=<?= rand() ?>" />
   </figure>
  <?php } ?>
</div>
	
	<script type="text/javascript">
		var rotateImg = 0;
		var imagePreview = $("#edit-photo-img");
		var rotationBtns = $(".rotation-btn");
		var editPhotoLoading = $("#edit-photo-loading");
		var plugin = new Customizer("body");
		plugin.setImage("<?= $base64 ?>");
		plugin.submitCallback = function (canvas) {
			rotationBtns.prop("disabled", true);
			editPhotoLoading.show();
			$.post("<?= BASE_URL ?>admin/backstage_event_photos/edit_photo_submit",
				{
					image : canvas.toDataURL(),
					filename : "<?= $row->media_image_filename ?>"
				}, function (response) {
					editPhotoLoading.hide();
					rotationBtns.prop("disabled", false);
				});
		}
		$("#img-rotate-right").click( function () {
			rotateImg += 90;
			imagePreview.css("transform", "rotate(" + rotateImg + "deg)");
			plugin.rotateRight();
		});
		$("#img-rotate-left").click( function () {
			rotateImg -= 90;
			imagePreview.css("transform", "rotate(" + rotateImg + "deg)");
			plugin.rotateLeft();
		});
		$("#img-save").click( function () {
			plugin.submit();
		});
		
	</script>