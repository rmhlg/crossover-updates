
<!--
<script src="<?=BASE_URL?>assets/admin/js/angular-js/angular-file-upload-shim.js"></script>
<script src="<?=BASE_URL?>assets/admin/js/angular-js/angular.js"></script>
<script src="<?=BASE_URL?>assets/admin/js/angular-js/angular-file-upload.js"></script>
<script src="<?=BASE_URL?>assets/admin/js/angular-js/upload.js"></script>
 
-->
 <script src="<?= SITE_URL ?>/../admin_assets/js/angular-js/angular-file-upload-shim.js"></script>
 <script src="<?= SITE_URL ?>/../admin_assets/js/angular-js/angular.js"></script>
 <script src="<?= SITE_URL ?>/../admin_assets/js/angular-js/angular-file-upload.js"></script>
 <script src="<?= SITE_URL ?>/../admin_assets/js/angular-js/upload.js"></script>

<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
  <h4 class="modal-title"><?=$header_title?></h4>
</div>

<div class="modal-body text-center"  ng-controller="MyCtrl">

   <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" id="form">
    <input type="hidden" name="myModel" ng-model="myModel" value="1"/>

       <div class="form-group">
            <label class="col-sm-3 control-label"><?=$file_label?></label>
            <div class="col-sm-5">
                 <input type="file" ng-file-select="onFileSelect($files)" multiple><br>
                 <strong>Width: 1160px. Height: 600px.</strong>
            </div>
       </div>
        
       <div class="form-group preview" style="display:none">
           <div class="col-md-12" >
            <div class="row" ng-show="selectedFiles != null">

              <div class="col-md-3 col-xs-3" ng-repeat="f in selectedFiles">
                <div class="thumbnail">
                  <img ng-show="dataUrls[$index]" ng-src="{{dataUrls[$index]}}">
                  <div class="caption">
                      <p class="text-danger">{{uploadError[$index]}} {{removeError[$index]}}</p>
                      <span class="progress progress-striped" ng-show="progress[$index] >= 0">           
                      <div class="progress-bar" style="width:{{progress[$index]}}%">{{progress[$index]}}%</div>
                    </span>
                    <div class="clearfix"></div>
                    <p>size: {{f.size}}B<br/>type: {{f.type}}</p>
                    <button type="button" class="btn" ng-click="abort($index)" ng-show="hasUploader($index) && progress[$index] < 100">Abort</button>
                    <button type="button" class="btn btn-danger " ng-click="remove($index)" ng-show="progress[$index] >= 100"><span class="glyphicon glyphicon-trash"></span> Remove</button>
                  </div>

                </div>

                <div class="{{$index + 1 % 4 == 0 ? 'clearfix' : ''}}"></div>
                 
              </div>

            </div> 
          </div>

       </div>
       <input type="text" ng-model="uploadURL" ng-init="uploadURL='<?=SITE_URL?>/backstage_events/upload_media?backstage_event_id=<?=$backstage_event_id?>'" style="visibility:hidden;"> 
       <input type="text" ng-model="removeURL" ng-init="removeURL='<?=SITE_URL?>/backstage_events/remove_media'" style="visibility:hidden;">
        

   </form>

</div>


<script>
 
    angular.element(document).ready(function() {
      angular.bootstrap(document, ['fileUpload']);
      $('.preview').show();
    });
 
</script>
  
     
      