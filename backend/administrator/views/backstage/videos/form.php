<script>
  var thumbnails;
  <?php if($row) { ?>
    thumbnails = [<?php 
    if(isset($thumbnails)) {
      foreach($thumbnails as $thumbnail) {
        echo "'" . $thumbnail . "',";
      }  
    }
    
  ?>];<?php } else { ?>
    thumbnails = null;
    <?php } ?>
</script>


<?php if($row && $row->user_type=='administrator'){ ?>
<script type="text/javascript" src="<?=BASE_URL?>scripts/jwplayer/jwplayer.js"></script>
<?php } ?>
<?php $this->load->view('editor.php') ?>
<script src="<?= BASE_URL ?>admin_assets/js/angular-js/angular-file-upload-shim.js"></script>
<script type="text/javascript" src="<?= BASE_URL ?>admin_assets/js/angular-js/angular.js"></script>  
<script src="<?= BASE_URL ?>admin_assets/js/angular-js/angular-file-upload.js"></script>
<script type="text/javascript">
var uploadURL = '<?= SITE_URL ?>/backstage_videos/upload_media/';
</script>
<script src="<?= BASE_URL ?>admin_assets/js/angular-js/upload-video.js"></script> 

<!--start main content -->
 <div class="container main-content" ng-app="fileUpload" ng-controller="MyCtrl">
      <div class="page-header">
           <h3><?php echo $header_text; ?></h3>
      </div>

      <?php if($error){ ?>
       <div class="alert alert-danger"> <?php echo $error; ?> </div>
      <?php } ?>

       <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" id="add-form-product-info">
          <input type="hidden" value="<?=($row) ? $row->user_type : ''?>" name="user_type"/>
           <div class="form-group">
                <label class="col-sm-2 control-label">Title</label>
                <div class="col-sm-4">
                     <input type="text" value="<?php echo ($row) ? $row->title : '';  ?>" name="title" class="form-control required" data-name="statement">
                </div>
           </div>
           <?php
           if($this->router->method == 'add' || ($row && $row->user_type=='administrator')) { ?>
           <div class="form-group">
                <label class="col-sm-2 control-label">Video</label>
                <div class="col-sm-4">
                     <!-- <input type="file" name="video" ng-file-select="onFileSelect($files)" id="tmp-file-upload" data-name="module name" class="form-control"> -->
                     <input type="file" name="multiple_images[]" class="form-control" ng-file-select="onFileSelect($files)" id="tmp-file-upload">
                 </div>
           </div>
           <div class="form-group">
              <label class="col-sm-2 control-label"></label>
              <div class="col-sm-4">Recommended Video Formats: MP4 or FLV</div>
           </div>
           <?php } ?>

          <div class="form-group" ng-show="selectedFiles != null">
              <label class="col-sm-2 control-label">Media Preview</label>
              <div class="col-md-8">
                <div class="row">

                <div class="col-xs-4 col-md-3" ng-repeat="f in selectedFiles">
                  <div class="thumbnail">
                    <img ng-show="dataUrls[$index]" ng-src="{{dataUrls[$index]}}">
                    <div class="caption">
                     <!--  <h5>{{f.name}}</h5> -->
                      <!-- <input name="media_title[{{$index}}]" type="text" style="width: 100%;" placeholder="Enter title here"> -->
                      <input name="video" type="hidden" ng-show="uploadResult[$index]" value="{{uploadResult[$index]}}">
                      <br><br>
                      <p>size: {{f.size}}B - type: {{f.type}}</p>
                      <div class="progress progress-striped" ng-show="progress[$index] >= 0">
                      <div class="progress-bar"  role="progressbar" aria-valuemin="0" aria-valuenow="{{progress[$index]}}" aria-valuemax="100" style="width: {{progress[$index]}}%">
                        <span class="sr-only">{{progress[$index]}}% Complete</span>
                      </div>
                    </div>
                      <p><a href="javascript:void(0)" class="btn btn-primary" role="button" ng-click="start($index)" ng-show="progress[$index] < 0">Start</a> 
                      <a href="javascript:void(0)" class="btn btn-warning" ng-click="abort($index)" ng-show="hasUploader($index) && progress[$index] < 100" role="button">Abort</a> 
                    <a href="javascript:void(0)" class="btn btn-danger" ng-click="remove($index)" ng-show="progress[$index] >= 100" role="button">&times;&nbsp;&nbsp;Remove</a></p>
                    </div>
                  </div>
                </div>
              </div>
              </div>
            </div> 
            <div class="form-group" ng-show="isUploading">
              <label class="col-sm-2 control-label">&nbsp;</label>
              <div class="col-md-8"><img src="<?php echo BASE_URL ?>images/preloader.gif" width="30" /> Generating thumbnails... Please wait.</div>
            </div>
            <div class="form-group" ng-show="thumbnails != null">
              <label class="col-sm-2 control-label">Thumbnails</label>
              <div class="col-md-8">
                <div class="row">
                <div class="col-xs-4 col-md-3" ng-repeat="f in thumbnails">
                  <div class="thumbnail">
                    <img ng-show="thumbnails[$index]" ng-src="<?php echo BASE_URL ?>uploads/backstage/videos/{{thumbnails[$index]}}">
                    <?php if(!$row)  {?>
                    <center ng-if="$index == 0">
                        <input type="radio" checked name="thumbnail" value="{{thumbnails[$index]}}" />
                   </center>
                   <center ng-if="$index > 0">
                        <input type="radio" name="thumbnail" value="{{thumbnails[$index]}}" />
                   </center>
                   <?php } else { ?>
                   <center ng-if="$index == 0" ng-hide="thumbnails[$index] == '<?php echo $row->thumbnail ?>'">
                        <input type="radio" checked name="thumbnail" value="{{thumbnails[$index]}}" />
                   </center>
                   <center ng-if="thumbnails[$index] == '<?php echo $row->thumbnail ?>'">
                        <input type="radio" checked name="thumbnail" value="{{thumbnails[$index]}}" />
                   </center>
                   <center ng-if="thumbnails[$index] != '<?php echo $row->thumbnail ?>'" ng-hide="$index == 0">
                        <input type="radio" name="thumbnail" value="{{thumbnails[$index]}}" class="{{$index}}" />
                   </center>
                   <?php } ?>
                  </div>
                  
                </div>
              </div>
              </div>
            </div> 
           <?php if($row){

                  if(file_exists('uploads/backstage/videos/'.@$row->video) && @$row->video){
                      $video_mime_type = get_mime_by_extension('uploads/about/videos/'.@$row->video); 
                      //$key = strpos($row->video,'.');
                      //$raw = substr($row->video,0,$key);
                      $thumbnail = ''; //BASE_URL.'uploads/about/videos/'.$raw.'.jpg'; ?>

                      <div class="form-group">
                      <label class="col-sm-2 control-label">Video Preview</label>
                          <div class="col-sm-4">
                             <video width="100%" height="100%"  id="video"  controls="controls" >
                             <source src="<?=BASE_URL.'uploads/backstage/videos/'.$row->video?>" type="<?=$video_mime_type?>" />
                               <script type="text/javascript">
                              function tryFlash(){
                                // flash fallback
                                jwplayer("video").setup({ flashplayer: "<?=BASE_URL?>scripts/jwplayer/player.swf",
                                            modes: [{ type: "html5" }, { type: "flash", src: "<?=BASE_URL?>scripts/jwplayer/player.swf"}]});  
                              }
                              function initVideo(){
                                var video = document.getElementById("video");
                                // check if html5 video tag is supported if not fallback to flash
                                if(!video.play ? true : false)
                                  tryFlash();
                              }
                              initVideo();
                              </script>
                              </video>

                          </div>
                      </div>

               <?php } 
               } ?>
             
           <div class="form-group">
                <label class="col-sm-2 control-label">Status</label>
                <div class="col-sm-4">
                	<select class="form-control" name="status">
                      <!-- <option value="1" <?php echo ($row && $row->status==PENDING) ? 'selected' : '' ?>>Pending</option> -->
                     	<option value="1" <?php echo ($row && $row->status==APPROVED) ? 'selected' : '' ?>>Approved</option>
                      <option value="2" <?php echo ($row && $row->status==DISAPPROVED) ? 'selected' : '' ?>>Disapproved</option>
                  </select>
                </div>
           </div> 

           <div class="form-group">
                <div class="col-sm-offset-2 col-sm-4">
                     <a href="<?php echo SITE_URL.'/'.$this->uri->segment(1); ?>" class="pull-left"><< Back</a>
                      <input type="hidden" ng-model="httpMethod" ng-init="httpMethod = 'POST'" value="POST"/>
                     <input type="hidden" name="howToSend" ng-model="howToSend" value="1" ng-init="howToSend = 1">
                     <button type="submit" class="btn btn-primary pull-right" onclick="$('#add-form-product-info').submit()">Submit</button>

                </div>
           </div>
           
           
      </form>
      
 </div>
 <!--end main content -->
 