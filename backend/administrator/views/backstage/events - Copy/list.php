<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3> <?php echo $header_text; ?> <span class="badge badge-important"><?php echo (int)$total_rows; ?></span></h3>

               <div class="actions">
                    <?php if($add): ?>
                    <a href="<?php echo SITE_URL.'/'.$this->uri->segment(1).'/add'; ?>" class="btn btn-primary">Add Event</a>
                   	<?php endif; ?>
                    <a href="<?php echo SITE_URL.'/'.$this->uri->segment(1).'/export'; ?>" class="btn btn-primary">Export</a>   
               </div>
          </div>
          
          <table class="table table-bordered">
               <thead>
                    <tr>
                          <th>Event</th>
                           <th>Status</th>
                          <th>Date Added</th>
                         <th><?php if($edit || $delete) : ?>Operation<?php endif; ?></th>
                         
                    </tr>
               </thead>
               <tbody>
               		 <form action="<?php echo SITE_URL.'/'.$this->uri->segment(1); ?>">
                     <tr>
                        <th><input type="text" name="title" class="form-control" value="<?php echo $this->input->get('title'); ?>"></th>
                       
                     
                        <th><select name="event_status" class="form-control">
                              <option value=""></option>
                              <option value="2" <?php echo ($this->input->get('event_status')==2) ? 'selected' : ''; ?> >Unpublished</option>
                              <option value="1" <?php echo ($this->input->get('event_status')==1) ? 'selected' : ''; ?> >Published</option>
                            </select></th>
                        <th>From:<input name="event_from_date_added" class="form-control from" value="<?php echo $this->input->get('event_from_date_added'); ?>" /><br> 
                            To:<input name="event_to_date_added" class="form-control to" value="<?php echo $this->input->get('event_to_date_added'); ?>" /></th>
                       
                       <th><?php if($edit || $delete) : ?><button type="submit" class="btn btn-primary">Go</button><?php endif; ?></th>
                       
                     </tr>
                  </form>
                  <?php if($rows->num_rows()){

                            $edit_url = SITE_URL.'/'.$this->uri->segment(1).'/edit/';
 
                            foreach($rows->result() as $v){ ?>
                              <tr>
                                  <td><?php echo $v->title; ?></td>
                                   <td><?php echo ($v->status==1) ? 'Published' : 'Unpublished'; ?></td>
                                   <td><?php echo $v->date_added ?></td>                                  
                                  <td>
                                    <?php if($edit) : ?>
                                    <a href="<?php echo $edit_url.$v->backstage_event_id; ?>" class="btn btn-primary">Edit</a>
                                    <?php endif; ?>

                                     <a href="<?php echo SITE_URL.'/backstage_events/photos?backstage_event_id='.$v->backstage_event_id; ?>" class="btn btn-primary">View Photos</a>

                                    <?php if($delete) : ?>
                                    <a href="<?php echo SITE_URL.'/backstage_events/delete/'.$v->backstage_event_id .'/'.md5($v->backstage_event_id. ' ' . $this->config->item('encryption_key')); ?>" onclick="return confirmDeletion(this, 'event')" class="btn btn-danger">Delete</a>
                                    <?php endif; ?>                                    
                                  </td>
                                
                              </tr>
                  <?php     }
                        }else{ ?>
                          <tr><td colspan="5">No records found.</td></tr>
                  <?php  } ?>
                  
               </tbody>
          </table>

          <ul class="pagination pagination-sm pull-right">
               <?php echo $pagination ?>
          </ul>

     </div>
     <!--end main content -->