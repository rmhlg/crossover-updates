 
 
<!--start main content -->
 <div class="container main-content">
      <div class="page-header">
           <h3><?php echo $header_text; ?></h3>
      </div>

      <?php if($error){ ?>
       <div class="alert alert-danger"> <?php echo $error; ?> </div>
      <?php } ?>

       <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
        <input type="hidden" name="image_counter" value="1" />
 
           <div class="form-group">
                <label class="col-sm-2 control-label">Title :</label>
                <div class="col-sm-4">
                     <input type="text" value="<?php echo ($row) ? $row->title : '';  ?>" name="title" class="form-control required" data-name="statement">
                </div>
           </div>

           
 
           <div class="form-group">
                <label class="col-sm-2 control-label">Status :</label>
                <div class="col-sm-4">
                  <select class="form-control" name="status">
                      <option value="1" <?php echo ($row && $row->status==APPROVED) ? 'selected' : '' ?>>Published</option>
                      <option value="2" <?php echo ($row && $row->status==DISAPPROVED) ? 'selected' : '' ?>>Unpublished</option>
                  </select>
                </div>
           </div> 

           <div class="form-group">
                <div class="col-sm-offset-2 col-sm-4"> 
                     <a href="<?php echo SITE_URL.'/'.$this->uri->segment(1); ?>" class="pull-left"><< Back</a>
                      <button type="submit" class="btn btn-primary pull-right">Submit</button>
                </div>
           </div>
           
           
      </form>
      
 </div>
 <!--end main content -->