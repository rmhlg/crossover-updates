<!--start main content -->
     <div class="container main-content">
          <div class="page-header">
               <h3> <?php echo $header_text; ?> <span class="badge badge-important"><?php echo (int)$total_rows; ?></span></h3>

               <div class="actions">
                    <?php if($add): ?>
                    <a href="<?php echo SITE_URL.'/'.$this->uri->segment(1).'/add'; ?>" class="btn btn-primary">Add Venue</a>
                   	<?php endif; ?>
                    <a href="<?php echo SITE_URL.'/'.$this->uri->segment(1).'/export'.$query_strings; ?>" class="btn btn-primary">Export</a>   
               </div>
          </div>
          
          <table class="table table-bordered">
               <thead>
                    <tr>
                          <th>Venues</th>
                          <th>Date Added</th>                          
                         <th><?php if($edit || $delete) : ?>Operation<?php endif; ?></th>
                         
                    </tr>
               </thead>
               <tbody>
               		 <form action="<?php echo SITE_URL.'/'.$this->uri->segment(1); ?>">
                     <tr>
                        <th><input type="text" name="name" class="form-control" value="<?php echo $this->input->get('name'); ?>"></th>
                       
                      
                         <th>From:<input name="from_date_added" class="form-control from" value="<?php echo $this->input->get('from_date_added'); ?>" /><br> 
                            To:<input name="to_date_added" class="form-control to" value="<?php echo $this->input->get('to_date_added'); ?>" /></th>                      
                       <th><?php if($edit || $delete) : ?><button type="submit" class="btn btn-primary">Go</button><?php endif; ?></th>
                       
                     </tr>
                  </form>
                  <?php if($rows->num_rows()){

                            $edit_url = SITE_URL.'/'.$this->uri->segment(1).'/edit/';
 
                            foreach($rows->result() as $v){ ?>
                              <tr>
                                  <td><?php echo $v->name; ?></td>
                                  <td><?php echo $v->date_added; ?></td>
                                  <td>
                                    <?php if($edit) : ?>
                                    <a href="<?php echo $edit_url.$v->venue_id; ?>" class="btn btn-primary">Edit</a>
                                    <?php endif; ?>

                                    <?php if($delete) : ?>
                                    <a href="<?php echo SITE_URL.'/venues/delete/'.$v->venue_id .'/'.md5($v->venue_id. ' ' . $this->config->item('encryption_key')); ?>" onclick="return confirmDeletion(this, 'venue')" class="btn btn-danger">Delete</a>
                                    <?php endif; ?>                                    
                                  </td>
                                
                              </tr>
                  <?php     }
                        }else{ ?>
                          <tr><td colspan="5">No records found.</td></tr>
                  <?php  } ?>
                  
               </tbody>
          </table>

          <ul class="pagination pagination-sm pull-right">
               <?php echo $pagination ?>
          </ul>

     </div>
     <!--end main content -->