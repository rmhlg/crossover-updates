<div class="page-header">

               <h3><?=$header_text?></h3>

          </div>

          <div ng-app="experienceModule" ng-controller="experienceCtrl">

          <form class="form-horizontal" role="form" ng-submit="addItem()">
              <?php if($row){ ?>
                   <input type="hidden" ng-model="item.experience_id" ng-init="item.experience_id='<?=$row->experience_id?>'">
              <?php } ?>           

                <div class="form-group" ng-show="errorUpload">
                    <label class="col-sm-2 control-label"></label>
                    <div class="col-sm-7">
                         <div class="alert alert-danger" role="alert">{{errorUpload}}</div>
                    </div>
               </div>

               <?php /* if($prerequisite->num_rows()){ ?>
               <div class="form-group">

                    <label class="col-sm-2 control-label">Prerequisite</label>

                    <div class="col-sm-4">

                         <select name="parent_id" ng-init="item.parent_id=<?=$row ? $row->parent_id : ''?> " ng-model="item.parent_id">
                              <option value="Canada">Canada</option>
                              <option value="Alaska">Alaska</option>
                              <option value="Philippines">Philippines</option>

                              <?php foreach($prerequisite->result() as $v){?>   
                                             <option value="<?=$v->experience_id?>"><?=$v->title?></option>
                                  <?php } ?>
                         </select>

                    </div>

               </div>
               <?php } */ ?>

               <div class="form-group">

                    <label class="col-sm-2 control-label">Category</label>

                    <div class="col-sm-4">

                         <select ng-model="item.category_id" ng-init="item.category_id=<?=$row ? $row->category_id : ''?> ">
                              <option value="1">Alaska</option>
                              <option value="2">Canada</option>
                              <option value="3">California</option>
                         </select>

                    </div>

               </div>


               <div class="form-group">

                    <label class="col-sm-2 control-label">Title</label>

                    <div class="col-sm-4">

                         <input type="text" ng-init="item.title='<?=$row ? $row->title : ''?>'"  ng-model="item.title" class="form-control">

                    </div>

               </div>

               <div class="form-group">

                    <label class="col-sm-2 control-label">Points in KM</label>

                    <div class="col-sm-1">

                         <input type="text" ng-init="item.km_points='<?=$row ? $row->km_points : ''?>'" ng-model="item.km_points" class="form-control">

                    </div>

               </div>

               

               <div class="form-group">

                    <label class="col-sm-2 control-label">Media Type</label>

                    <div class="col-sm-4">

                         <select ng-model="item.media_type" ng-init="item.media_type=<?=$row ? $row->media_type : ''?>" >
                              <option value=""></option>
                              <option value="1">Image</option>
                              <option value="2">Video</option>
                            
                          </select>

                    </div>

               </div>

               <div class="form-group" ng-show="item.media_type">

                    <label class="col-sm-2 control-label">{{item.media_type=='1' ? 'Image' : 'Video'}}</label>
                    <input type="hidden" ng-init="item.media='<?=$row ? $row->media : ''?>'" ng-model="item.media">

                    <div class="col-sm-2" >
                         <input type="file" class="form-control" ng-file-select="onFileSelect($files)">      
                    </div>
 
               </div>

               

               <div class="form-group" ng-show="selectedFiles" >

                    <label class="col-sm-2 control-label"></label>
 
                    <div class="col-sm-10" ng-repeat="f in selectedFiles">

                         <!-- Image -->
                         <div class="col-sm-6 col-md-3">

                              <div class="thumbnail">

                                   <img ng-src="{{dataUrl}}">

                                   <div class="caption">

                                        <div class="progress" ng-show="progress >= 0">
                                          <div class="progress-bar" role="progressbar" aria-valuenow="{{progress}}" aria-valuemin="0" aria-valuemax="100" style="width: {{progress}}%;">
                                            <span>{{progress}}%</span>
                                          </div>
                                        </div>

                                   </div>

                              </div>
                         </div>
                         <!-- Image -->

                    </div>                           

               </div>

               <?php if($row){ ?>

                    <div class="form-group" >

                      <label class="col-sm-2 control-label"></label>
   
                      <div class="col-sm-10">

                           <!-- Image -->
                           <div class="col-sm-6 col-md-3">

                                <div class="thumbnail">

                                  <img src="<?=base_url('uploads/experience/'.$row->media)?>">                                    

                                </div>
                           </div>
                           <!-- Image -->

                      </div>                           

                    </div> 

               <?php } ?> 

               <div class="form-group">

                    <div class="col-sm-offset-2 col-sm-4">
                         <a href="<?=base_url('experience')?>" class="btn btn-default">Cancel</a>
                         <button type="submit" class="btn btn-primary">{{progress >= 0 && progress < 100 && !article.image ? 'Uploading Image...' : 'Submit'}}</button>
                    </div>

               </div>

          </form>

     </div>