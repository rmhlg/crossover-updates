<!--start main content -->
 <div class="container main-content">
      <div class="page-header">
           <h3>User KM Points <span class="badge badge-important"><?php echo isset($count) ? $count : 0 ?></span></h3>

           <div class="actions">
                <a href="<?= SITE_URL ?>/points/export?<?php echo http_build_query($_GET) ?>" class="btn btn-primary">Export</a>
           </div>
      </div>

              <table class="table table-bordered">
                     <thead>
                          <tr>
                               <th>#</th>
                               <th>Name</th>
                               <th>Total KM Points</th>
                               <th>Credit KM Points</th>
                          </tr>
                     </thead>
                     <tbody>
                         <form action="<?php echo SITE_URL ?>/points">
                               <tr>
                                   <td></td>
                                   <td><input class="form-control" name="name" value="<?= isset($_GET['name']) ? $_GET['name'] : '' ?>"></td>
                                   <td></td>
                                   <td><button class="btn btn-primary" name="search" value="1">Go</button></td>
                                  </form>
                              </tr>

                          <?php if($items): foreach($items as $k => $v): ?>
                          <tr id="row-<?= $k['registrant_id'] ?>">
                               <td><?= $offset + $k + 1 ?></td>
                               <td><?= $v['name'] ?></td>
                               <td><?= $v['total_km_points'] ?></td>
                               <td><?= $v['credit_km_points'] ?></td>
                           </tr>
                          <?php endforeach; else: ?>
                            <tr><td colspan="10"><center>No records found</center></td></tr>
                          <?php endif; ?>
                     </tbody>
                </table>

                <ul class="pagination pagination-sm pull-right">
                     <?php echo $pagination ?>
                </ul>
      </div>
<!--end main content -->