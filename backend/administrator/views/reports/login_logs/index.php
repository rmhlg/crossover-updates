<!--start main content -->
 <div class="container main-content">
      <div class="page-header">
           <h3>Login Logs <span class="badge badge-important"><?php echo isset($count) ? $count : 0 ?></span></h3>

           <div class="actions">
                <a href="<?= SITE_URL ?>/login_logs/export?<?php echo http_build_query($_GET) ?>" class="btn btn-primary">Export</a>
           </div>
      </div>

              <table class="table table-bordered">
                     <thead>
                          <tr>
                               <th>#</th>
                               <th>Name</th>
                               <th>Source</th>
                               <th>Date Login</th>
                          </tr>
                     </thead>
                     <tbody>
                          <?php if($items): foreach($items as $k => $v): ?>
                          <tr id="row-<?= $k['registrant_id'] ?>">
                               <td><?= $offset + $k + 1 ?></td>
                               <td><?= $v['name'] ?></td>
                               <td><?= $v['source'] == 1 ? 'Mobile' : 'Web'; ?></td>
                               <td><?= date('M d, Y', strtotime($v['date_login'])) ?></td>
                           </tr>
                          <?php endforeach; else: ?>
                            <tr><td colspan="10"><center>No records found</center></td></tr>
                          <?php endif; ?>
                     </tbody>
                </table>

                <ul class="pagination pagination-sm pull-right">
                     <?php echo $pagination ?>
                </ul>
      </div>
<!--end main content -->