 
 
<!--start main content -->
 <div class="container main-content">
      <div class="page-header">
           <h3><?php echo $header_text; ?></h3>
      </div>

      <?php if($message){ ?>
       <div class="alert alert-<?=$class?>"> <?php echo $message; ?> </div>
      <?php } ?>

       <form action="<?=SITE_URL.'/'.$this->router->class.'/edit'?>" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
        <input type="hidden" name="image_counter" value="1" />        
        <?php if($rows){ 
                  foreach($rows as $v){ ?>
                    <input type="hidden" value="<?=$v->setting_id?>" name="setting_id[]">
                    <input type="hidden" value="<?=$v->name?>" name="name[]">
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?=$v->name?></label>
                        <div class="col-sm-4">
                             <input type="text" value="<?=$v->setting_section_id?>" name="setting_section_id[]" class=" form-control required">
                        </div>
                    </div>
            <?php } 
              } ?>

           <div class="form-group">
                <div class="col-sm-offset-2 col-sm-4"> 
                      <button type="submit" class="btn btn-primary pull-right">Submit</button>
                </div>
           </div>
           
           
      </form>
      
 </div>
 <!--end main content -->