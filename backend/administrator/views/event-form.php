

<div class="page-header">

               <h3><?=$header_text?></h3>

          </div>

          <div ng-app="eventModule" ng-controller="eventCtrl">

          <form class="form-horizontal" role="form" ng-submit="addItem()">
              <?php if($row){ ?>
                   <input type="hidden" ng-model="item.event_id" ng-init="item.event_id='<?=$row->event_id?>'">
              <?php } ?>           

                <div class="form-group" ng-show="errorUpload">
                    <label class="col-sm-2 control-label"></label>
                    <div class="col-sm-7">
                         <div class="alert alert-danger" role="alert">{{errorUpload}}</div>
                    </div>
               </div>



               <div class="form-group">

                    <label class="col-sm-2 control-label">Name</label>

                    <div class="col-sm-4">

                          <input type="text" ng-init="item.name='<?=$row ? $row->name : ''?>'" ng-model="item.name" class="form-control">

                    </div>

               </div>

               <div class="form-group">

                    <label class="col-sm-2 control-label">Description</label> 
 
                    <div class="col-sm-4">  
 
 
                         <textarea ng-init="item.description='<?=$row ? $row->description : ''?>'" ng-model="item.description" class="form-control"></textarea>
                    </div>

               </div>

               <div class="form-group">

                    <label class="col-sm-2 control-label">Address</label>

                    <div class="col-sm-4">

                          <input type="text" ng-init="item.address='<?=$row ? $row->address : ''?>'" ng-model="item.address" class="form-control">

                    </div>

               </div>

               <div class="form-group">

                    <label class="col-sm-2 control-label">Points in KM</label>

                    <div class="col-sm-2">

                          <input type="text" ng-init="item.km_points='<?=$row ? $row->km_points : ''?>'" ng-model="item.km_points" class="form-control">

                    </div>

               </div>
                <div class="form-group">

                    <label class="col-sm-2 control-label">Venue</label>

                    <div class="col-sm-2">
                        <select name="venues" ng-init="item.venues='<?=$row ? $row->venues : ''?>'" ng-model="item.venues" class="form-control">
                          <option value="">Select Venue</option>
                          <?php
                          foreach($venues as $venue){
                            ?>
                            <option value="<?php echo $venue->venue_id ?>" <?= isset($row->venues) && $row->venues==$venue->venue_id ? "selected='selected'" : "" ?>><?php echo $venue->name ?></option>
                            <?php
                          }
                          ?>
                        </select>
                    </div>

               </div>
               <div class="form-group">
                    <label class="col-sm-2 control-label">Region</label>
                    <div class="col-sm-2">
                        <select name="region" ng-init="item.region='<?=$row ? $row->region : ''?>'" ng-model="item.region" class="form-control">
                          <option value="">Select Region</option>
                          <option value="1">California</option>
                          <option value="2">Alaska</option>
                          <option value="3">Canada</option>
                        </select>
                    </div>
               </div>
               <div class="form-group">

                    <label class="col-sm-2 control-label">Event Type:</label>

                    <div class="col-sm-2">
                        <select name="eventType" ng-init="item.eventType='<?=$row ? $row->eventType : ''?>'" ng-model="item.eventType" class="form-control">
                          <option value="">Select Event Type</option>
                          <?php
                          foreach($eventTypes as $eventType){
                            ?>
                            <option value="<?php echo $eventType->event_type_id ?>" <?= isset($row->eventType) && $row->eventType==$eventType->event_type_id ? "selected='selected'" : "" ?>><?php echo $eventType->name ?></option>
                            <?php
                          }
                          ?>
                        </select>
                    </div>

               </div>
                <div class="form-group">

                    <label class="col-sm-2 control-label">Start Date</label>

                    <div class="col-sm-2">

                          <input id="datepicker" type="text" ng-init="item.start_date='<?=$row ? $row->start_date : ''?>'"  name="start_date_pick" ng-model="item.start_date_pick" class="form-control">
                          <input id="dpHidden" type="hidden" name="start_date" ng-init="item.start_date='<?=$row ? $row->start_date : ''?>'" ng-model="item.start_date" />
                    </div>

               </div>

                <div class="form-group">

                    <label class="col-sm-2 control-label">End Date</label>

                    <div class="col-sm-2">

                          <input id="datepicker2" type="text" ng-init="item.end_date='<?=$row ? $row->end_date : ''?>'" name="end_date_pick" ng-model="item.end_date_pick" class="form-control">
                          <input id="dp2Hidden" type="hidden" name="end_date" ng-init="item.end_date='<?=$row ? $row->end_date : ''?>'" ng-model="item.end_date" />

                    </div>

               </div>


                <div class="form-group">

                    <label class="col-sm-2 control-label">Event Code</label>

                    <div class="col-sm-2">

                          <input type="text" ng-init="item.event_code='<?=$row ? $row->event_code : ''?>'" ng-model="item.event_code" class="form-control">

                    </div>

               </div>


               
               

               <div class="form-group">

                    <label class="col-sm-2 control-label">Image</label>
 
                    <div class="col-sm-2">

                         <input type="file" class="form-control" ng-file-select="onFileSelect($files)">
                         <input type="hidden" ng-model="item.image" ng-init="item.image='<?=$row ? $row->image : ''?>'" >
                    </div>   


               </div>

               <?php if($row && $row->image){  ?>

               <div class="form-group" ng-show="selectedFiles.length < 1">

                    <label class="col-sm-2 control-label"></label>
 
                    <div class="col-sm-10" >

                          

                             <!-- Image -->
                             <div  class="col-sm-6 col-md-3"  >

                                  <div class="thumbnail">

                                       <img ng-src="<?=base_url('uploads/event/'.$row->image)?>">
                                  </div>
                             </div>
                             <!-- Image -->

 
                    </div>                           

               </div>
               <?php } ?>

               <div class="form-group">

                    <label class="col-sm-2 control-label"></label>
 
                    <div class="col-sm-10"  >

                         <!-- Image -->
                         <div class="col-sm-6 col-md-3" ng-repeat="f in selectedFiles">

                              <div class="thumbnail">

                                   <img ng-show="dataUrls[$index]" ng-src="{{dataUrls[$index]}}">

                                   <div class="caption">
                                        <p ng-show="errorUpload[$index]">{{errorUpload[$index]}}</p>

                                        <div class="progress" ng-show="progress[$index] >= 0">
                                          <div class="progress-bar" role="progressbar" aria-valuenow="{{progress[$index]}}" aria-valuemin="0" aria-valuemax="100" style="width: {{progress[$index]}}%;">
                                            <span>{{progress[$index]}}%</span>
                                          </div>
                                        </div>

                                        <button type="button" class="btn" ng-click="abort($index)" ng-show="progress[$index] >= 0 && progress[$index] < 100">Abort</button>
                                        <button type="button" class="btn btn-danger " ng-click="remove($index)" ng-show="progress[$index] >= 100"><span class="glyphicon glyphicon-trash"></span> Remove</button>

                                   </div>

                              </div>
                         </div>
                         <!-- Image -->

                    </div>                           

               </div>


               <div class="form-group">

                    <div class="col-sm-offset-2 col-sm-4">
                         <a href="<?=site_url($this->router->class)?>" class="btn btn-default">Cancel</a>
                         <button type="submit" class="btn btn-primary">Submit</button>
                    </div>

               </div>

          </form>

     </div>

     <script type="text/javascript"> 
  $(function() {
    $( "#datepicker" ).datepicker({
      dateFormat: "yy-mm-dd",
      onSelect: function(date) {
        $("#dpHidden").val(date);
      }
    });
    $( "#datepicker2" ).datepicker({
      dateFormat: "yy-mm-dd",
      onSelect: function(date) {
        $("#dp2Hidden").val(date);
      }
    }); 
  });
  </script> 