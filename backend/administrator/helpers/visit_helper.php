<?php

function match_page_section($section, $uri)
{
	$section = str_replace("/", "\/", $section);
	return preg_match("/" . $section ."/", $uri, $match);
}