<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function date_differ($date1, $date2, $identifier = 'd') {
	$diff;
	switch($identifier) {
		case 'd':
			$datediff = $date1 - $date2;
			$diff =  floor($datediff/(60*60*24));
			break;
		case 'h':
			$datediff = $date1 - $date2;
			$diff =  floor($datediff/(60*60));
			break;
		case 'i':
			$datediff = $date1 - $date2;
			$diff =  floor($datediff/60);
			break;
	}
	return $diff;
}

/* End of file datediff_helper.php */
/* Location: ./system/helpers/date_helper.php */