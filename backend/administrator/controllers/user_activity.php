<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_Activity extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$data = array(
			"title" => "User KM Points",
			"main_content" => $this->main_content(),
			"nav" => $this->nav_items()
		);
		$this->load->view("main-template", $data);
	}

	public function export()
	{
		$this->_export();
	}
	
	private function nav_items()
	{
 		return $this->load->view('navigations', '', true);		
	}


	/******************************************************/

	public function main_content()
	{
		$page = $this->uri->segment(2, 1);
		$limit = isset($_GET['limit']) ? $_GET['limit'] : 15;
		$data['offset'] = ($page - 1) * $limit;

		$this->db->start_cache();
		$this->db->select("*, CONCAT((tbl_registrants.first_name), (' '), (tbl_registrants.third_name)) as name")->from('tbl_reports_user_activity');
		if(isset($_GET['name']) && $_GET['name']) {
			$this->db->like("CONCAT((first_name), (' '), (third_name))", $_GET['name']);
		}
		$this->db->join('tbl_registrants', 'tbl_registrants.user_id = tbl_reports_user_activity.user_id');
		$this->db->order_by('registrant_id', 'DESC');
		$data['count'] = $this->db->count_all_results();
		$this->db->limit($limit, $data['offset']);
		$data['items'] = $this->db->get()->result_array();
		$this->db->stop_cache();
		$this->db->flush_cache();
		$data['pagination'] = $this->global_model->pagination($data['count'], $page, SITE_URL.'/user_activity', $limit);

		$content = $this->load->view('reports/user_activity/index', $data, TRUE);
		return $content;
	}

	public function _export()
	{
		$this->load->library('to_excel_array');

		$this->db->start_cache();
		$this->db->select("*, CONCAT((tbl_registrants.first_name), (' '), (tbl_registrants.third_name)) as name")->from('tbl_reports_user_activity');
		if(isset($_GET['name']) && $_GET['name']) {
			$this->db->like("CONCAT((first_name), (' '), (third_name))", $_GET['name']);
		}
		$this->db->join('tbl_registrants', 'tbl_registrants.user_id = tbl_reports_user_activity.user_id');
		$this->db->order_by('registrant_id', 'DESC');
		$export_items = $this->db->get()->result_array();
		$this->db->stop_cache();
		$this->db->flush_cache();

		$row[] = array(
			'#',
			'Name',
			'Activity',
			'Activity Details',
			'Date Created'
		);
		if($export_items) {
			$i = 0;
			foreach($export_items as $k => $v) {
				$i++;
				$row[] = array(
					$i,
					$v['name'],
					$v['activity'],
					$v['activity_details'],
					$v['timestamp']
				);
			}
		}
		$this->to_excel_array->to_excel($row, 'reports_user_activity_'.date('YmdHis'));
	}

	public function _remap($params)
	{
		if(is_numeric($params)) {
			$this->index();
		} else {
			$this->{$params}();
		}
	}
}

?>
