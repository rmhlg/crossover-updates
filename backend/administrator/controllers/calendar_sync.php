<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

class Calendar_Sync extends CI_Controller {

	public function __construct() {

		parent::__construct();

		$this->_table = 'tbl_calendar_sych';

	}

	public function index() {

		$data = array(
			'main_content' 	=> $this->main_content(),
			'nav'			=> $this->nav_items()
		);
		$this->load->view('main-template', $data);

	}

	private function nav_items() {

		$data = $this->module_model->get_nav_data();
		return $this->load->view('navigations', $data, TRUE);

	}

	public function main_content() {

		//$access = $this->module_model->check_access('calendar_sync');
		$page = $this->uri->segment(2, 1);
		$like = array();
		$where = 'tbl_calendar_sych.synch_id LIKE \'%%\'';
		$data['offset'] = ( $page - 1 ) * PER_PAGE;

		if( isset($_GET['search']) ) {
			$where .= isset( $_GET['event'] ) && $_GET['event'] ? ' AND tbl_backstage_events.backstage_event_id = \'' . $_GET['event'] . '\'' : FALSE;
			$where .= isset( $_GET['name'] ) && $_GET['name'] ? " AND concat(first_name, ' ', third_name) LIKE '%" . $_GET['name'] . "%'" : FALSE;
		}

		$param = array(
			'offset'	=> $data['offset'],
			'limit'	=> PER_PAGE,
			'table'	=> $this->_table,
			'fields'	=> '*, tbl_calendar_sych.timestamp as date_added',
			'order_by'	=> array(
						'field' => 'timestamp',
						'order' => 'DESC'
					),
			'join'	=> array(array('table'=>'tbl_backstage_events','on'=>'tbl_backstage_events.backstage_event_id = tbl_calendar_sych.id'),
								array('table'=>'tbl_registrants','on'=>'tbl_registrants.registrant_id = tbl_calendar_sych.registrant_id')					
					),
			'where'	=> $where
		);
		$data['users'] = $this->global_model->get_rows( $param )->result_array();


		if( isset($_GET['search']) ) {
			$param = array(
			'table'	=> $this->_table,
			'fields'	=> '*, tbl_calendar_sych.timestamp as date_added',
			'order_by'	=> array(
						'field' => 'timestamp',
						'order' => 'DESC'
					),
			'join'	=> array(array('table'=>'tbl_backstage_events','on'=>'tbl_backstage_events.backstage_event_id = tbl_calendar_sych.id'),
								array('table'=>'tbl_registrants','on'=>'tbl_registrants.registrant_id = tbl_calendar_sych.registrant_id')					
					),
			'where'	=> $where
		);
		$count= $this->global_model->get_rows( $param )->result_array();
			$records = count($count);
		}else
		{
			//$records = $this->global_model->get_total_rows( $param );
			$records = count($data['users'] );
		}

		
		$data['pagination'] = $this->global_model->pagination( $records, $page, SITE_URL . '/calendar_sync' );
		$param = array(
			'table' => 'tbl_backstage_events',
		);
		$data['events'] = $this->global_model->get_rows($param)->result_array();
		$data['total'] = $records;
		$data['graph'] = $this->graph();

		/*echo '<pre>';
		print_r($data['users']);
		exit;*/
		
		return $this->load->view('calendar_synch/index', $data, TRUE);

	}

	public function export() {

		$where = 'tbl_calendar_sych.synch_id LIKE \'%%\'';

		if( isset($_GET['search']) ) {
			$where .= isset( $_GET['event'] ) && $_GET['event'] ? ' AND tbl_backstage_events.backstage_event_id = \'' . $_GET['event'] . '\'' : FALSE;
			$where .= isset( $_GET['name'] ) && $_GET['name'] ? " AND concat(first_name, ' ', third_name) LIKE '%" . $_GET['name'] . "%'" : FALSE;
		}

		$param = array(
			'offset'	=> 0,
			'table'	=> $this->_table,
			'fields'	=> '*',
			'join'	=> array(array('table'=>'tbl_backstage_events','on'=>'tbl_backstage_events.backstage_event_id = tbl_calendar_sych.id'),
								array('table'=>'tbl_registrants','on'=>'tbl_registrants.registrant_id = tbl_calendar_sych.registrant_id')					
					),
			'where'	=> $where
		);

		$records = $this->global_model->get_rows( $param )->result_array();

		$row[] = array(
			'#',
			'Name',
			'Event',
			'Date'
		);

		if( $records ) {
			foreach( $records as $k => $v ) {
				$row[] = array(
					$k + 1,
					$v['first_name'] . ' ' . $v['third_name'],
					$v['title'],
					date('F d, Y H:i:s', strtotime($v['timestamp']))
				);
			}
		}

		$this->load->library('to_excel_array');
		$this->to_excel_array->to_excel($row, 'calendar_sync_' . date('YmdHis'));

	}

	private function graph() {

 		$function_get_days = function($param) {
 			$date = date("t", strtotime($param));

			for($i = 1; $i <= $date; $i++) {
				$every_date = date("Y") . "-" . date("m") . "-" . str_pad($i, 2, "0", STR_PAD_LEFT);

				$data[] = array($every_date);
			}

			return $data;
 		};

 		$param = array(
 			'table' => $this->_table,
 		);
 		$raw_result = $this->global_model->get_rows($param)->result_array();

 		foreach($raw_result as $k => $v) {
 			$final[$k]['timestamp'] = $v['timestamp'];
 		}

 		$date1 = @date('Y-m-d', strtotime(min($final[0])));
 		$date2 = @date('Y-m-d', strtotime(max($final[0])));

 		$dates_count = $function_get_days($date1);

 		for($i = 0; $i < count($dates_count); $i++) {
 			$param = array(
 				'table' => $this->_table,
 				'where' => "date(timestamp) = '" . $dates_count[$i][0] . "'",
 				'fields' => 'COUNT(*) as total'
 			);
 			$items = $this->global_model->get_rows($param)->result_array();
 			if($items) {
 				$checkins_daily['daily'][] = array($dates_count[$i][0], $items[0]['total']);
 			} else {
 				$checkins_daily["daily"][] = array($dates_count[$i][0], 0);
 			}
 		}

 		return $checkins_daily;

 	}

	public function _remap($method) {

		if( is_numeric( $method ) ) {
			$this->index();
		} else {
			$this->{$method}();
		}

	}

}