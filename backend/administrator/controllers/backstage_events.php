<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

class Backstage_events extends CI_Controller {
	

	public function __construct() {
		parent::__construct();
		$this->load->model('explore_model');
	} 


	public function index()
	{
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);

	}

	
	private function nav_items() {
		return $this->load->view('navigations', null, true);		
	}


	public function main_content()
	{
		
		$this->load->helper(array('simplify_datetime_range','paging'));
		$offset = (int)$this->input->get('per_page');
		$limit = PER_PAGE;
		$filters = $this->get_filters(); 

		//$access = $this->module_model->check_access('backstage_events');
		$total_rows = $this->explore_model->get_rows(array('table'=>'tbl_backstage_events',
			'where'=>array_merge($filters['where_filters'],array('tbl_backstage_events.is_deleted'=>0)),
			'like'=>$filters['like_filters']																 
			)
		)->num_rows();
		$data['header_text'] = 'Events';
		$data['edit'] = true;
		$data['delete'] = true;
		$data['add'] = true;
		$data['total_rows'] = $total_rows;

		$data['rows'] = $this->explore_model->get_rows(array('table'=>'tbl_backstage_events',
			'where'=>array_merge($filters['where_filters'],array('tbl_backstage_events.is_deleted'=>0)),
			'like'=>$filters['like_filters'],
			'order_by'=>array('field'=>'date_added','order'=>'DESC'),
			'join'=>array('table'=>'tbl_regions as r','on'=>'r.region_id=tbl_backstage_events.region_id','type'=>'left'),
			'offset'=>$offset,
			'limit'=>$limit,
			'fields'=>'tbl_backstage_events.*, r.name as region_name'
			)
		);
		$data['regions'] = $this->explore_model->get_rows(array('table'=>'tbl_regions',
			'where'=>array('is_deleted'=>0),
			'order_by'=>array('field'=>'name','order'=>'ASC')
			)
		);
		$data['venues'] = $this->explore_model->get_rows(array('table'=>'tbl_venues',
			'where'=>array('is_deleted'=>0),
			'order_by'=>array('field'=>'name','order'=>'ASC')
			)
		);


		$data['pagination'] = paging($total_rows,$limit,$offset,SITE_URL.'/'.$this->uri->segment(1).$filters['query_strings']);
		return $this->load->view('backstage/events/list',$data,TRUE);


	}


	public function upload_media()
	{
		$this->load->helper('video_helper');
		$this->load->helper(array('upload','resize'));

		$upload_path = 'uploads/backstage/photos/';
		$upload_path_thumbnail = $upload_path.'thumbnails/';
		$event = $this->explore_model->get_row(array('table'=>'tbl_backstage_events','where'=>array('backstage_event_id'=>$this->input->get('backstage_event_id'),'is_deleted'=>0)));
		$event_photo = $this->explore_model->get_row(array('table'=>'tbl_backstage_events_photos','where'=>array('photo_id'=>$this->input->get('photo_id'),'backstage_event_id'=>$this->input->get('backstage_event_id'),'is_deleted'=>0)));
		$user =array('cms_user_id'=>$this->session->userdata('user_id'),
						'name'=>'name_user'	);// $this->login_model->extract_user_details(); //reference only jeromejose
		$response = array('uploadError'=>null,'video_thumbnail'=>null,'recordID'=>0,'removeParam'=>array());
		$max_size = (strpos($_FILES['myFile']['type'],'image') === false) ? VIDEO_UPLOAD_LIMIT : FILE_UPLOAD_LIMIT;
		$video_duration = '';

		if($event){

			$params = array('upload_path'=>$upload_path,
				'allowed_types'=>FILE_UPLOAD_ALLOWED_TYPES.'|mp4','file_name'=>uniqid(),
				'max_size'=>$max_size,
				'max_width'=>0,
				'max_height'=>0,
				'do_upload'=>'myFile');

			$file = upload($params);


			if(is_array($file)){

				$image_filename = $file['file_name'];
				$media_type = 'photo';

				if(!$file['is_image']){

					$upload_path = $upload_path;
					$dir = dirname(__FILE__) . '/../../';
					$ret = system('ffmpeg -i ' . $dir . $upload_path . $file['file_name'] . ' -ss 00:00:03.000 -f image2 -vframes 1 ' . $dir .$upload_path . $file['raw_name'] . '.jpg');

					$image_filename = $file['raw_name'].'.jpg';
					$media_type = 'video';
					$response['video_thumbnail'] = BASE_URL.$upload_path.$image_filename;

					$video_duration = get_video_duration($dir . $upload_path . $file['file_name']);


				}

				$params = array('width'=>100,'height'=>100,'source_image'=>$upload_path.$image_filename,'new_image_path'=>$upload_path_thumbnail,'file_name'=>$image_filename);
				resize($params);	

				$params = array('width'=>331,'height'=>176,'source_image'=>$upload_path.$image_filename,'new_image_path'=>$upload_path_thumbnail,'file_name'=>$image_filename);
				resize($params);


				$data = array('backstage_event_id'=>$event->backstage_event_id,
					'uploader_id'=>$user['cms_user_id'],
					'uploader_name'=>$user['name'],
					'user_type'=>'administrator',
					'is_deleted'=>0,
					'status'=>1,
					'media'=>$file['file_name'],
					'media_image_filename'=>$image_filename,
					'media_type'=>$media_type,
					'date_added'=>TRUE,
					'media_duration' => $video_duration);

				if($event_photo){ /** Edit mode **/

					if($event_photo->media_type=='video'){ // exclude media & media type update when replace new thumbnail for video media

						unset($data['media']);
						unset($data['media_type']);

					}else{

						if(file_exists($upload_path.$event_photo->media))
							unlink($upload_path.$event_photo->media);
					}

					unset($data['date_added']);
					
					$media_id = $event_photo->photo_id;
					$this->explore_model->update('tbl_backstage_events_photos',$data,array('photo_id'=>$event_photo->photo_id,'backstage_event_id'=>$event->backstage_event_id));
					
					/** Thumbnails **/
					if(file_exists($upload_path.$event_photo->media_image_filename))
						unlink($upload_path.$event_photo->media_image_filename);

					if(file_exists($upload_path_thumbnail.'100_100_'.$event_photo->media_image_filename))
						unlink($upload_path_thumbnail.'100_100_'.$event_photo->media_image_filename);

					if(file_exists($upload_path_thumbnail.'331_176_'.$event_photo->media_image_filename))
						unlink($upload_path_thumbnail.'331_176_'.$event_photo->media_image_filename);

				}else{
					$media_id = $this->explore_model->insert('tbl_backstage_events_photos',$data);
				}
				
				$response['removeParam'] = '?backstage_event_id='.$event->backstage_event_id.'&media_id='.$media_id.'&token='.md5($media_id. ' ' . $this->config->item('encryption_key'));
				
			}else{

				$response['uploadError'] = strip_tags($file);

			}

		}else{

			$response['uploadError'] = "Event not found.";

		}

		echo json_encode($response);		

	}

	public function add_media()
	{
		$data['header_title'] = 'Add Media';
		$data['file_label'] = 'Select Media: ';
		$data['backstage_event_id'] = $this->input->get('backstage_event_id');		
		$this->load->view('backstage/events/add-media',$data);

	}

	public function edit_media()
	{
		$this->load->helper('file');
		$data['header_title'] = 'Edit Media';
		$data['file_label'] = 'Change Media: ';
		$data['photo_id'] = $this->uri->segment(3);
		$data['backstage_event_id'] = $this->uri->segment(4);
		$data['row'] = $this->explore_model->get_row(array('table'=>'tbl_backstage_events_photos','where'=>array('photo_id'=>$this->uri->segment(3))));		
		$this->load->view('backstage/events/edit-media',$data);
	}

	public function photos()
	{
		$this->load->helper('paging');

		$backstage_event_id = $this->input->get('backstage_event_id');
		$offset = (int)$this->input->get('per_page');
		$limit = PER_PAGE;
		$filters = $this->get_filters();

		//$access = $this->module_model->check_access('events');
		$total_rows = $this->explore_model->get_rows(array('table'=>'tbl_backstage_events_photos',
			'where'=>array_merge($filters['where_filters'],array('tbl_backstage_events_photos.is_deleted'=>0,'tbl_backstage_events_photos.backstage_event_id'=>$backstage_event_id)),
			'like'=>$filters['like_filters'],
			'fields'=>'tbl_backstage_events_photos.photo_id'
			)
		)->num_rows();
		$data['backstage_event_id'] = $backstage_event_id;
		$data['header_text'] = 'Events';
		$data['edit'] = true;
		$data['delete'] = true;
		$data['add'] = true;
		$data['total_rows'] = $total_rows;
		$data['event'] = $this->explore_model->get_row(array('table'=>'tbl_backstage_events','where'=>array('backstage_event_id'=>$backstage_event_id,'is_deleted'=>0)));
		$data['events'] = $this->explore_model->get_rows(array('table'=>'tbl_backstage_events',
			'where'=>array('is_deleted'=>0)
			)
		);
		$data['rows'] = $this->explore_model->get_rows(array('table'=>'tbl_backstage_events_photos',
			'where'=>array_merge($filters['where_filters'],array('tbl_backstage_events_photos.is_deleted'=>0,'tbl_backstage_events_photos.backstage_event_id'=>$backstage_event_id)),
			'join'=>array('table'=>'tbl_backstage_events as e','on'=>'e.backstage_event_id=tbl_backstage_events_photos.backstage_event_id'),
			'like'=>$filters['like_filters'],
			'limit'=>$limit,
			'offset'=>$offset,
			'order_by'=>array('field'=>'tbl_backstage_events_photos.date_added','order'=>'DESC'),
			'fields'=>'tbl_backstage_events_photos.photo_id,tbl_backstage_events_photos.uploader_id,tbl_backstage_events_photos.uploader_name,tbl_backstage_events_photos.backstage_event_id,tbl_backstage_events_photos.media,tbl_backstage_events_photos.media_type,tbl_backstage_events_photos.media_image_filename,tbl_backstage_events_photos.user_type,tbl_backstage_events_photos.status,tbl_backstage_events_photos.winner_status,tbl_backstage_events_photos.date_added,e.title as event_title'
			)
		);

$data['query_strings'] = $filters['query_strings'];
$data['pagination'] = paging($total_rows,$limit,$offset,SITE_URL.'/'.$this->uri->segment(1).'/photos/'.$filters['query_strings']);
$data['main_content'] = $this->load->view('backstage/events/photos_list',$data,TRUE);
$data['nav'] = $this->nav_items();
$this->load->view('main-template', $data);


}


public function add()
{

	$error = '';
	$row = '';

	if($this->input->post()) {

		$post_data = $this->input->post();	


		if($this->validate_form()) {


			$data = $post_data;

				$data['date_added'] = array(array('field'=>'date_added','value'=>'NOW()'),array('field'=>'start_date','value'=>$this->input->post('start_date')),array('field'=>'end_date','value'=>$this->input->post('end_date'))); // Set NOW() value to date_added column field
				
				$data['url_title'] = url_title(ucwords(strtolower(trim($post_data['title']))));

				$id = $this->explore_model->insert('tbl_backstage_events',$data);
				//$image_filename = $this->do_upload2($id,'image');



				/* upload image jerome 03-18-2015*/

				$config['upload_path'] = 'uploads/backstage/events/';
				if(!is_dir($config['upload_path'])){
					mkdir($config['upload_path'],0777,true);
				}

				$config['allowed_types'] = 'gif|png|jpg';
				$config['do_upload'] = 'image';
				$config['max_width'] = 2000;
				$config['max_height'] = 1600;
				$config['max_size'] =  8072;
				$config['file_name'] = $id;

				$this->load->library('upload', $config);

				if ( ! $this->upload->do_upload('image'))
				{
					$error = array('error' => $this->upload->display_errors());
					print_r($error);
				}
				else
				{
					$arr_image = array('upload_data' => $this->upload->data());
					$params1 = array('width'=>331,'height'=>176,'source_image'=>$config['upload_path'].$arr_image['upload_data']['file_name'],'new_image_path'=>$config['upload_path'],'file_name'=>$arr_image['upload_data']['file_name']);
					$params = array('width'=>50,'height'=>50,'source_image'=>$config['upload_path'].$arr_image['upload_data']['file_name'],'new_image_path'=>$config['upload_path'],'file_name'=>$arr_image['upload_data']['file_name']);
					$this->load->helper(array('resize'));
					resize($params);
					resize($params1);
					
					$image_filename = $arr_image['upload_data']['orig_name'];
				} /* upload image jerome */


				if($image_filename) 
					$this->explore_model->update('tbl_backstage_events',array('image'=>$image_filename),array('backstage_event_id'=>$id));

	   // 		    $post['url'] = SITE_URL . '/'.$this->uri->segment(1).'/add';
				// $post['description'] = 'added a new event for backstage pass.';
				// $post['table'] = 'tbl_backstage_events';
				// $post['record_id'] = $id;
				// $post['type'] = 'add';
				// $this->module_model->save_audit_trail($post);

				redirect($this->uri->segment(1)); 

			} else {

				$post_data['image'] = '';
				$error = validation_errors();
				$row = json_decode(json_encode($post_data),false);

			}

		} 
		
		$data['regions'] = $this->explore_model->get_rows(array('table'=>'tbl_regions',
			'where'=>array('is_deleted'=>0),
			'order_by'=>array('field'=>'name','order'=>'ASC')
			)
		);
		$data['venues'] = $this->explore_model->get_rows(array('table'=>'tbl_venues',
			'where'=>array('is_deleted'=>0),
			'order_by'=>array('field'=>'name','order'=>'ASC')
			)
		);
		$data['event_types'] = $this->explore_model->get_rows(array('table'=>'tbl_backstage_event_types',
			'where'=>array('is_deleted'=>0),
			'order_by'=>array('field'=>'name','order'=>'ASC')
			)
		);	 		 
		$data['error'] = $error;
		$data['row'] = $row;
		$data['row_photos'] =  '';
		$data['header_text'] = 'Event';
		$data['main_content'] = $this->load->view('backstage/events/form',$data,TRUE);
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);

	} 



	public function edit()
	{

		$error = '';
		$id = $this->uri->segment(3);
		$row = $this->explore_model->get_row(array('table'=>'tbl_backstage_events','where'=>array('backstage_event_id'=>$id,'is_deleted'=>0)));

		if($this->input->post())
		{
			$post_data = $this->input->post();

			if($this->validate_form()){

				$photo = json_decode(json_encode($row),TRUE);


				/* Insert action history */

				$fields = array('title','status');
				$status = array('', 'Published', 'Unpublished');

				$new_content = array();
				$old_content = array();

				foreach($photo as $k => $v) {
					if(in_array($k, $fields)) {
						if($photo[$k] != $this->input->post($k)) {

							if($k == 'status') {
								
								$new_content[$k] = $status[$this->input->post($k)];
								$old_content[$k] = $status[$photo[$k]];

							}else {
								$new_content[$k] = $this->input->post($k);
								$old_content[$k] = $photo[$k];
							}

						}
					}
				}

				// $post['url'] = SITE_URL . '/'.$this->uri->segment(1).'/edit/' . $id;
				// $post['description'] = 'updated an event for backstage pass';
				// $post['table'] = 'tbl_backstage_events';
				// $post['record_id'] = $id;
				// $post['type'] = 'edit';
				// $post['field_changes'] = serialize(array('old'	=> $old_content,
				// 										 'new'	=> $new_content));
				// $this->module_model->save_audit_trail($post);

				/* End Insert action history */

				$post_data['date_updated']	= TRUE; // Set NOW() value to column date_updated
				$post_data['url_title'] = url_title(ucwords(strtolower(trim($post_data['title']))));
			    $data['date_added'] = array(array('field'=>'start_date','value'=>$this->input->post('start_date')),array('field'=>'end_date','value'=>$this->input->post('end_date'))); // Set NOW() value to date_added column field

			    $this->explore_model->update('tbl_backstage_events',$post_data,array('backstage_event_id'=>$id,'is_deleted'=>0));

			 //   $image_filename = $this->do_upload2($id,'image');



			    /* upload image jerome 03-18-2015*/

			    $config['upload_path'] = 'uploads/backstage/events/';
			    if(!is_dir($config['upload_path'])){
			    	mkdir($config['upload_path'],0777,true);
			    }

			    $config['allowed_types'] = 'gif|png|jpg';
			    $config['do_upload'] = 'image';
			    $config['max_width'] = 2000;
			    $config['max_height'] = 1600;
			    $config['max_size'] =  8072;
			    $config['file_name'] = uniqid() . '' . $id;

			    $this->load->library('upload', $config);

			    if ( ! $this->upload->do_upload('image'))
			    {
			    	$error = array('error' => $this->upload->display_errors());
			    	print_r($error);
			    }
			    else
			    {
			    	$arr_image = array('upload_data' => $this->upload->data());
			    	$params1 = array('width'=>331,'height'=>176,'source_image'=>$config['upload_path'].$arr_image['upload_data']['file_name'],'new_image_path'=>$config['upload_path'],'file_name'=>$arr_image['upload_data']['file_name']);
			    	$params = array('width'=>50,'height'=>50,'source_image'=>$config['upload_path'].$arr_image['upload_data']['file_name'],'new_image_path'=>$config['upload_path'],'file_name'=>$arr_image['upload_data']['file_name']);
			    	$this->load->helper(array('resize'));
			    	resize($params);
			    	resize($params1);

			    	$image_filename = $arr_image['upload_data']['orig_name'];
			    } /* upload image jerome */






			    if($image_filename) 
			    {


			    		/* unlink the photo to save server space jeromejose 05-19-2015*/
					$unlink_data = $this->db->select()->from('tbl_backstage_events')->where('backstage_event_id', $id)->get()->result_array();
					$unlink_image = 'uploads/backstage/events/'. $unlink_data[0]['image'];
					$unlink_image1 = 'uploads/backstage/events/50_50_'. $unlink_data[0]['image'];
					$unlink_image2 = 'uploads/backstage/events/331_176_'. $unlink_data[0]['image'];
					if (getimagesize($unlink_image) !== false) {
					 unlink($unlink_image);
					}
					if (getimagesize($unlink_image1) !== false) {
					 unlink($unlink_image1);
					}
					if (getimagesize($unlink_image2) !== false) {
					 unlink($unlink_image2);
					}
					/* unlink the photo to save server space jeromejose 05-19-2015*/

$this->explore_model->update('tbl_backstage_events',array('image'=>$image_filename),array('backstage_event_id'=>$id));

			    }
			    	
			    redirect($this->uri->segment(1));

			}else{

				$post_data['image'] = $row->image;
				$error = validation_errors();
				$row = json_decode(json_encode($post_data),false);

			}


		}

		$data['regions'] = $this->explore_model->get_rows(array('table'=>'tbl_regions',
			'where'=>array('is_deleted'=>0),
			'order_by'=>array('field'=>'name','order'=>'ASC')
			)
		);
		$data['venues'] = $this->explore_model->get_rows(array('table'=>'tbl_venues',
			'where'=>array('is_deleted'=>0),
			'order_by'=>array('field'=>'name','order'=>'ASC')
			)
		);

		$data['event_types'] = $this->explore_model->get_rows(array('table'=>'tbl_backstage_event_types',
			'where'=>array('is_deleted'=>0),
			'order_by'=>array('field'=>'name','order'=>'ASC')
			)
		);

		$data['row'] = $row; 
		$data['error'] = $error;
		$data['header_text'] = 'Event';
		$data['main_content'] = $this->load->view('backstage/events/form',$data,TRUE);
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);

	}


	public function featured()
	{

		$id = $this->uri->segment(3);
		$token = $this->uri->segment(4);

		if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . 'admin/'.$this->uri->segment(1)) !== false && $token == md5($id . ' ' .	$this->config->item('encryption_key'))) {
			$this->explore_model->update('tbl_backstage_events',array('is_featured'=>1),array('backstage_event_id'=>$id));

			$post['url'] = @$_SERVER['HTTP_REFERER'];
			$post['description'] = 'featured an event from backstage pass';
			$post['table'] = 'tbl_backstage_events';
			$post['record_id'] = $id;
			//$this->module_model->save_audit_trail($post);
		}
		redirect($this->uri->segment(1));
	}


	public function delete()
	{

		$id = $this->uri->segment(3);
		$token = $this->uri->segment(4);

		if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . 'admin/'.$this->uri->segment(1)) !== false && $token == md5($id . ' ' .	$this->config->item('encryption_key'))) {
			$this->explore_model->update('tbl_backstage_events',array('is_deleted'=>1),array('backstage_event_id'=>$id));

			$post['url'] = @$_SERVER['HTTP_REFERER'];
			$post['description'] = 'deleted an event from backstage pass';
			$post['table'] = 'tbl_backstage_events';
			$post['record_id'] = $id;
			//$this->module_model->save_audit_trail($post);
		}
		redirect($this->uri->segment(1));

	}


	public function remove_media()
	{

		$id = ($this->input->get('media_id')) ? $this->input->get('media_id') : $this->uri->segment(3);
		$token = ($this->input->get('token')) ? $this->input->get('token') : $this->uri->segment(5);
		$backstage_event_id = ($this->input->get('backstage_event_id')) ? $this->input->get('backstage_event_id') :  $this->uri->segment(4); 		
		$event = $this->explore_model->get_row(array('table'=>'tbl_backstage_events','where'=>array('backstage_event_id'=>$backstage_event_id)));
		$event_photo = $this->explore_model->get_row(array('table'=>'tbl_backstage_events_photos','where'=>array('photo_id'=>$id,'backstage_event_id'=>$backstage_event_id)));
		$response = array('removeError'=>null);

		if($event && $event_photo && strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/'.$this->uri->segment(1)) !== false && $token == md5($id . ' ' .	$this->config->item('encryption_key'))) {
			$this->explore_model->update('tbl_backstage_events_photos',array('is_deleted'=>1),array('photo_id'=>$id));

			$post['url'] = $_SERVER['HTTP_REFERER'];
			$post['description'] = 'deleted a '.$event_photo->media_type.' in '.$event->title.' event';
			$post['table'] = 'tbl_backstage_events_photos';
			$post['record_id'] = $id;
			$this->module_model->save_audit_trail($post);
			$response['removeError'] = 'success';
		}else{
			$response['removeError'] = 'Something went wrong, Please check your access permission.';
		}


		if($this->input->is_ajax_request()){
			echo json_encode($response);
		}else{
			redirect($this->uri->segment(1).'/photos?backstage_event_id='.$backstage_event_id);
		}

	}

	public function validate_form()
	{

		$this->load->library('form_validation');

		$rules = array(
			array(
				'field'   => 'title',
				'label'   => 'Title',
				'rules'   => 'required|callback_validate_title'
				),
			array(
				'field'   => 'description',
				'label'   => 'Description',
				'rules'   => 'required'
				),
			array(
				'field'   => 'region_id',
				'label'   => 'Region',
				'rules'   => 'required'
				),
			array(
				'field'   => 'start_date',
				'label'   => 'Start Date',
				'rules'   => 'required|callback_validate_start_date|callback_validate_date_range'
				),
			array(
				'field'   => 'end_date',
				'label'   => 'End Date',
				'rules'   => 'required|callback_validate_start_date'
				),
			array(
				'field'   => 'event_type',
				'label'   => 'Event Type',
				'rules'   => 'required'
				)
			);

		$this->form_validation->set_rules($rules);
		
		return $this->form_validation->run();
	}

	public function validate_date_range()
	{

		$start_date = date('Y-m-d',strtotime($this->input->post('start_date')));
		$end_date = date('Y-m-d',strtotime($this->input->post('end_date')));
		$where = array('DATE(start_date) >= DATE('.$start_date.')'=>null,'DATE(end_date) <= DATE('.$end_date.')'=>null);
		$row = $this->explore_model->get_row(array('table'=>'tbl_backstage_events','where'=>$where));

		if($row && $row->backstage_event_id!=$this->uri->segment(3)){
			$this->form_validation->set_message('validate_date_range','Conflict schedule from the other event.'); 
			return false;
		}else if(strtotime($this->input->post('start_date')) > strtotime($this->input->post('end_date'))){
			$this->form_validation->set_message('validate_date_range','Invalid Start Date.'); 
			return false;
		}

		return true;

	}

	public function validate_start_date($date)
	{

		$date = strtotime($date);
		$month = date('m',$date);
		$day = date('d',$date);
		$year = date('Y',$date);

		if(!checkdate($month, $day, $year)){
			$this->form_validation->set_message('validate_event_date','Invalid event date.');
			return false;
		}else
		return true;

	}

	public function validate_end_date($date)
	{

		$date = strtotime($date);
		$month = date('m',$date);
		$day = date('d',$date);
		$year = date('Y',$date);

		if(!checkdate($month, $day, $year)){
			$this->form_validation->set_message('validate_event_date','Invalid event date.');
			return false;
		}else
		return true;

	}

	public function validate_title($title)
	{	

		$row = $this->explore_model->get_row(array('table'=>'tbl_backstage_events',
			'where'=>array('url_title'=>url_title(ucwords(strtolower(trim($title)))))
			)
		);

		if($row && !$this->uri->segment(3)){
			$this->form_validation->set_message('validate_title','Title already exists.');
			return false;
		}else{
			return true;
		}

	}


	public function get_filters()
	{

		$where_filters = array('DATE(date_added) >='=>'event_from_date_added',
			'DATE(date_added) <='=>'event_to_date_added',
			'status'=>'event_status',
			'tbl_backstage_events_photos.backstage_event_id'=>'backstage_event_id',
			'tbl_backstage_events_photos.status'=>'status',
			'DATE(tbl_backstage_events_photos.date_added) >='=>'from_date_added',
			'DATE(tbl_backstage_events_photos.date_added) <='=>'to_date_added',
			'DATE(tbl_backstage_events_photos.start_date) >='=>'start_date',
			'DATE(tbl_backstage_events_photos.end_date) <='=>'end_date',
			'tbl_backstage_events_photos.region_id'=>'region_id',
			'DATE(schedule) ='=>'schedule',
			'tbl_backstage_events_photos.is_featured'=>'is_featured'
			);

		$like_filters = array('tbl_backstage_events_photos.uploader_name'=>'submitted_by',
			'title'=>'title',
			'description'=>'description',
			'venue'=>'venue',
			'platform_restriction'=>'platform_restriction');

		$query_strings = array();

		$valid_where_filters = array();
		$valid_like_filters = array();

		foreach($where_filters as $column_field=>$filter){

			if($this->input->get($filter) || $this->input->get($filter) ==='0'){
				$valid_where_filters[$column_field] = trim($this->input->get($filter));
				$query_strings[$filter] = $this->input->get($filter);
			}


		} 


		foreach($like_filters as $column_field=>$filter){

			if($this->input->get($filter)){
				$valid_like_filters[$column_field] = trim($this->input->get($filter));
				$query_strings[$filter] = $this->input->get($filter);
			}



		} 

		$query_strings = ($query_strings) ? http_build_query($query_strings) : '';

		return array('where_filters'=>$valid_where_filters,'like_filters'=>$valid_like_filters,'query_strings'=>'?'.$query_strings);		   

	} 


	public function export()
	{
		$this->load->library('to_excel_array');
		$filters = $this->get_filters();
		$query = $this->explore_model->get_rows(array('table'=>'tbl_backstage_events',
			'where'=>array_merge($filters['where_filters'],array('tbl_backstage_events.is_deleted'=>0)),
			'like'=>$filters['like_filters'],
			'order_by'=>array('field'=>'date_added','order'=>'DESC'),
			'join'=>array('table'=>'tbl_regions as r','on'=>'r.region_id=tbl_backstage_events.region_id','type'=>'left'),

			'fields'=>'tbl_backstage_events.*, r.name as region_name'
			)
		);
		$res[] = array('Title','Description','Region','Venue','Event Type','Featured','Status','Start Date','End Date','Date Added');
		if($query->num_rows()){
			foreach($query->result() as $v){

				$featured = $v->is_featured ? 'Yes' : 'No';
				$status = $v->status ? 'Published' : 'Unpublished';
				$res[] = array($v->title,$v->description,$v->region_name,$v->venue,$v->event_type,$featured,$status,$v->start_date,$v->end_date,$v->date_added);

			}

		}

		$this->to_excel_array->to_excel($res,  $this->uri->segment(1).date("Y-m-d_H-i-s"));
	}




	public function do_upload2($new_filename,$upload_file)
	{

		$this->load->helper(array('upload','resize'));
		$upload_path = './uploads/backstage/events/';

		if(!is_dir($upload_path)){
			mkdir($upload_path,0777,true);
		}

		$params = array('upload_path'=>$upload_path,'allowed_types'=>'png|jpg|gif','file_name'=>$new_filename,'max_size'=>'2097152','max_width'=>0,'max_height'=>0,'do_upload'=>$upload_file);
		$file = upload($params);


		if(is_array($file)){

			$params = array('width'=>50,'height'=>50,'source_image'=>$upload_path.'/'.$file['file_name'],'new_image_path'=>$upload_path,'file_name'=>$file['file_name']);
			resize($params);

			$params = array('width'=>331,'height'=>176,'source_image'=>$upload_path.'/'.$file['file_name'],'new_image_path'=>$upload_path,'file_name'=>$file['file_name']);
			resize($params); 	

		}	    

		return (is_array($file) && count($file) > 0) ? $file['file_name'] : '';

	}


	public function export_photos()
	{
		$this->load->library('to_excel_array');
		$filters = $this->get_filters();
		$where_filters = $filters['where_filters'];
		$like_filters = $filters['like_filters'];

		$rows = $this->explore_model->get_rows(array('table'=>'tbl_backstage_events_photos',
			'where'=>array_merge($filters['where_filters'],array('tbl_backstage_events_photos.is_deleted'=>0)),
			'join'=>array('table'=>'tbl_backstage_events as e','on'=>'e.backstage_event_id=tbl_backstage_events_photos.backstage_event_id'),
			'like'=>$filters['like_filters'],
			'order_by'=>array('field'=>'tbl_backstage_events_photos.date_added','order'=>'DESC'),
			'fields'=>'tbl_backstage_events_photos.photo_id,tbl_backstage_events_photos.uploader_id,tbl_backstage_events_photos.uploader_name,tbl_backstage_events_photos.backstage_event_id,tbl_backstage_events_photos.image,tbl_backstage_events_photos.user_type,tbl_backstage_events_photos.status,tbl_backstage_events_photos.winner_status,tbl_backstage_events_photos.date_added,e.title as event_title'
			)
		);
		$res[] = array('Image','Backstage Event','Submitted By','Status','Date Added');
		if($rows->num_rows()){
			foreach($rows->result() as $v){
				$image = BASE_URL.'/uploads/backstage/photos/'.$v->image;
				$status = ($v->status) ? 'Published' : 'Unpublished';
				$res[] = array($image,$v->event_title,$v->uploader_name,$status,$v->date_added);
			}
		}
		$this->to_excel_array->to_excel($res,  $this->uri->segment(1).date("Y-m-d_H-i-s"));

	}





	public function win()
	{
		$id = $this->uri->segment(3);
		$backstage_event_id = $this->uri->segment(4);
		$token = $this->uri->segment(5);
		$row = $this->explore_model->get_row(array('table'=>'tbl_backstage_events','where'=>array('backstage_event_id'=>$backstage_event_id)));

		if($row && strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/'.$this->uri->segment(1)) !== false && $token == md5($id . ' ' .	$this->config->item('encryption_key'))) {
			$this->explore_model->update('tbl_backstage_events_photos',array('winner_status'=>1),array('photo_id'=>$id));

			$post['url'] = @$_SERVER['HTTP_REFERER'];
			$post['description'] = 'set a photo entry winner in '.ucwords($row->title).' event';
			$post['table'] = 'tbl_backstage_events_photos';
			$post['record_id'] = $id;
			$this->module_model->save_audit_trail($post);

			$this->Points_Model->earn(BACKSTAGE_PHOTOS, array('suborigin' => $id));
			
		}

		redirect($this->uri->segment(1).'/photos?backstage_event_id='.$backstage_event_id);
	}

	public function not_win()
	{

		$id = $this->uri->segment(3);
		$backstage_event_id = $this->uri->segment(4);
		$token = $this->uri->segment(5);
		$row = $this->explore_model->get_row(array('table'=>'tbl_backstage_events','where'=>array('backstage_event_id'=>$backstage_event_id)));

		if($row && strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/'.$this->uri->segment(1)) !== false && $token == md5($id . ' ' .	$this->config->item('encryption_key'))) {
			$this->explore_model->update('tbl_backstage_events_photos',array('winner_status'=>0),array('photo_id'=>$id));
			$post['url'] = @$_SERVER['HTTP_REFERER'];
			$post['description'] = 'reset a photo entry as not winner in '.ucwords($row->title).' event';
			$post['table'] = 'tbl_backstage_events_photos';
			$post['record_id'] = $id;
			$this->module_model->save_audit_trail($post);
		}

		redirect($this->uri->segment(1).'/photos?backstage_event_id='.$backstage_event_id);

	}

	public function view_photo()
	{
		$id = $this->uri->segment(3);
		$data['row'] = $this->explore_model->get_rows(array('table'=>'tbl_backstage_events_photos',
			'where'=>array('tbl_backstage_events_photos.photo_id'=>$id),
			'join'=>array('table'=>'tbl_backstage_events as e','on'=>'e.backstage_event_id=tbl_backstage_events_photos.backstage_event_id'),
			'fields'=>'e.title,tbl_backstage_events_photos.media,tbl_backstage_events_photos.media_type,tbl_backstage_events_photos.media_image_filename,tbl_backstage_events_photos.uploader_name'
			)
		)->row();
		$this->load->view('backstage/events/view-event-main-photo',$data);

	}

   // Incase needed

   /*

   public function do_upload()
	{

		   $this->load->helper(array('resize','upload'));

		   $max = (int)$this->input->post('image_counter');
  
		   

		   $i = 1;
		   $temp = 1;
		   $thumb = '_';		   
		   $id = (int)$this->explore_model->get_last_id('tbl_backstage_events_photos','photo_id') + 1;	   

		   $images = array();
		   $backstage_event_id = $this->uri->segment(3);
 		   $user = $this->login_model->extract_user_details();
 		   $upload_path = 'uploads/backstage/photos/';
 		   $upload_path_thumbnail = 'uploads/backstage/photos/thumbnails/';
 		   $new_filename = '';

 		   if(!is_dir($upload_path)){
 		   		mkdir($upload_path,0777,true);
 		   }

 		   if(!is_dir($upload_path_thumbnail)){
 		   		mkdir($upload_path_thumbnail,0777,true);
 		   }
		   

  		   while($i<=$max){
			   

			   if(isset($_FILES["slider-image".$i]["error"]) && $_FILES["slider-image".$i]["error"] < 1){ 	   
 

 				   $params = array('upload_path'=>$upload_path,
 				   					'allowed_types'=>FILE_UPLOAD_ALLOWED_TYPES,
 				   					'file_name'=>'',
 				   					'max_size'=>'2097152',
 				   					'max_width'=>0,
 				   					'max_height'=>0,
 				   					'do_upload'=>'slider-image'.$i);
				   $file = upload($params);    

				   if(is_array($file)){


					   $new_filename = strtolower($id.$thumb.$i.$file['file_ext']);	/// New filename			   
					   while(file_exists($upload_path.$new_filename)){ //Check file exists
					 	  $temp++;
					 	  $new_filename = strtolower($id.$thumb.$temp.$file['file_ext']);
					    }		   

					   rename($upload_path.$file['file_name'],'uploads/backstage/photos/'.$new_filename); // Rename file

 				   	  $params = array('width'=>100,'height'=>100,'source_image'=>$upload_path.$new_filename,'new_image_path'=>$upload_path_thumbnail,'file_name'=>$new_filename);
					  resize($params);	

					  $params = array('width'=>331,'height'=>176,'source_image'=>$upload_path.$new_filename,'new_image_path'=>$upload_path_thumbnail,'file_name'=>$new_filename);
					  resize($params);

					  $data[] = array('backstage_event_id'=>$backstage_event_id,
				   					'uploader_id'=>$user['cms_user_id'],
				   					'uploader_name'=>$user['name'],
				   					'user_type'=>'administrator',
 				   					'is_deleted'=>0,
				   					'status'=>1,
				   					'image'=>$new_filename,
				   					'date_added'=>TRUE);
 				   }

			   }

			   $i++;			

		   }

		   return $data;


 	} 
	 

	public function add_photosX()
	{

		$id = (int)$this->uri->segment(3);

		$error = '';
 
		if($this->input->post()) {

			$post_data = $this->input->post();
			$event = $this->explore_model->get_row(array('table'=>'tbl_backstage_events','where'=>array('backstage_event_id'=>$id)));

			if($event) {
 				 
				$photos = $this->do_upload();

				if($photos){
					 
					$this->explore_model->insert_batch('tbl_backstage_events_photos',$photos);
					$post['url'] = SITE_URL . '/'.$this->uri->segment(1).'/add';
					$post['description'] = ($temp > 1) ? 'added a new photos in '.$event->title.' event' : 'added a new photo '.$event->title.' event';
					$post['table'] = 'tbl_backstage_events';
					$post['record_id'] = $id;
					$post['type'] = 'add';
					$this->module_model->save_audit_trail($post);

				}		   
				 
	   		    redirect($this->uri->segment(1).'/photos?backstage_event_id='.$this->uri->segment(3)); 

			} else {

				$post_data['image'] = '';
				$error = validation_errors();
				$row = json_decode(json_encode($post_data),false);

			}
 
		} 
		

		$data['error'] = $error;
		$data['event'] = $this->explore_model->get_row(array('table'=>'tbl_backstage_events','where'=>array('backstage_event_id'=>$id),'fields'=>'title,backstage_event_id'));
		$data['row_photos'] =  '';
 		$data['header_text'] = 'Event';
 		$data['backstage_event_id'] = $id;

 		$data['main_content'] = $this->load->view('backstage/events/photos_form',$data,TRUE);
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
 
	} 

	*/


}