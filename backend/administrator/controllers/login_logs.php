<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login_logs extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data = array(
            "title" => "Login_logs",
            "main_content" => $this->main_content(),
            "nav" => $this->nav_items()
        );
        $this->load->view("main-template", $data);
    }

    public function export()
    {
        $this->_export();
    }
    
    private function nav_items()
    {
        return $this->load->view('navigations', '', true);      
    }


    /******************************************************/

    public function main_content()
    {
        $page = $this->uri->segment(2, 1);
        $limit = isset($_GET['limit']) ? $_GET['limit'] : 15;
        $data['offset'] = ($page - 1) * $limit;

        $this->db->start_cache();
        $this->db->select('*, CONCAT(tbl_registrants.first_name, (""),COALESCE(tbl_registrants.third_name,"")) as name',false)->from('tbl_login');
$this->db->join('tbl_registrants', 'tbl_registrants.registrant_id = tbl_login.registrant_id');
        $this->db->order_by('date_login', 'DESC');
        

        $this->db->limit($limit, $data['offset']);
        $data['items'] = $this->db->get()->result_array();
        $data['count'] = $this->db->count_all_results();
        $this->db->stop_cache();
        $this->db->flush_cache();
        $data['pagination'] = $this->global_model->pagination($data['count'], $page, SITE_URL.'/login_logs', $limit);

        $content = $this->load->view('reports/login_logs/index', $data, TRUE);
        return $content;
    }

    public function _export()
    {
        $this->load->library('to_excel_array');

        $this->db->start_cache();
        $this->db->select("*")->from('tbl_login');
        
        $this->db->order_by('registrant_id', 'DESC');
        $export_items = $this->db->get()->result_array();
        $this->db->stop_cache();
        $this->db->flush_cache();

        $row[] = array(
            '#',
            'Name',
            'Source',
            'Date Login',
        );
        if($export_items) {
            $i = 0;
            foreach($export_items as $k => $v) {
                $i++;
                
                $source = $v['source'] == 1 ? 'Mobile' : 'Web';

                $row[] = array(
                    $i,
                    $v['registrant_id'],
                    $source,
                    $v['date_login'],
                );
            }
        }
        $this->to_excel_array->to_excel($row, 'reports_login_logs_'.date('YmdHis'));
    }

    public function _remap($params)
    {
        if(is_numeric($params)) {
            $this->index();
        } else {
            $this->{$params}();
        }
    }
}

?>
