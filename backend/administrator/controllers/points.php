<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Points extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$data = array(
			"title" => "User KM Points",
			"main_content" => $this->main_content(),
			"nav" => $this->nav_items()
		);
		$this->load->view("main-template", $data);
	}

	public function export()
	{
		$this->_export();
	}
	
	private function nav_items()
	{
 		return $this->load->view('navigations', '', true);		
	}


	/******************************************************/

	public function main_content()
	{
		$page = $this->uri->segment(2, 1);
		$limit = isset($_GET['limit']) ? $_GET['limit'] : 15;
		$data['offset'] = ($page - 1) * $limit;

		$this->db->start_cache();
		//$this->db->select("*, CONCAT((first_name), (' '), (third_name)) as name")->from('tbl_registrants');
		$this->db->select('*, CONCAT(first_name, (""),COALESCE(third_name,"")) as name',False)->from('tbl_registrants');
		if(isset($_GET['name']) && $_GET['name']) {
		
			$this->db->like("CONCAT_WS((first_name), (' '), (third_name))", $_GET['name']);
		}
		$this->db->order_by('registrant_id', 'DESC');
		$data['count'] = $this->db->count_all_results();

		$this->db->limit($limit, $data['offset']);
		$data['items'] = $this->db->get()->result_array();
		$this->db->stop_cache();
		$this->db->flush_cache();
		$data['pagination'] = $this->global_model->pagination($data['count'], $page, SITE_URL.'/points', $limit);

		$content = $this->load->view('reports/points/index', $data, TRUE);
		return $content;
	}

	public function _export()
	{
		$this->load->library('to_excel_array');

		$this->db->start_cache();
		$this->db->select("*, CONCAT((first_name), (' '), (third_name)) as name")->from('tbl_registrants');
		if(isset($_GET['name']) && $_GET['name']) {
			$this->db->like("CONCAT((first_name), (' '), (third_name))", $_GET['name']);
		}
		$this->db->order_by('registrant_id', 'DESC');
		$export_items = $this->db->get()->result_array();
		$this->db->stop_cache();
		$this->db->flush_cache();

		$row[] = array(
			'#',
			'Name',
			'Total KM Points',
			'Credit KM Points',
		);
		if($export_items) {
			$i = 0;
			foreach($export_items as $k => $v) {
				$i++;
				$row[] = array(
					$i,
					$v['name'],
					$v['total_km_points'],
					$v['credit_km_points'],
				);
			}
		}
		$this->to_excel_array->to_excel($row, 'reports_points_'.date('YmdHis'));
	}

	public function _remap($params)
	{
		if(is_numeric($params)) {
			$this->index();
		} else {
			$this->{$params}();
		}
	}
}

?>
