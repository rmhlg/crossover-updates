<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

class Lamp_Events extends CI_Controller {
	
	public function __construct() {

		parent::__construct();

		$this->_table = 'tbl_lamp_events';

 	}

 	public function index() {

 		$data = array(
 			'main_content'	=> $this->main_content(),
 			'nav'			=> $this->nav_items()
 		);
 		$this->load->view('main-template', $data);

 	}

 	public function nav_items() {

 		$data = $this->module_model->get_nav_data();
 		return $this->load->view('navigations', $data, TRUE);

 	}

 	public function add() {

 		$data = array(
 			'main_content' => $this->add_content(),
 			'nav' => $this->nav_items()
 		);
 		$this->load->view('main-template', $data);

 	}

 	public function main_content() {

 		$page = $this->uri->segment(2, 1);
 		$data['offset'] = ( $page - 1 ) * PER_PAGE;
 		$where = "tbl_lamp_events.is_deleted = 0";

 		if( isset($_GET['search']) ) {
 			$where .= isset($_GET['lamp_id']) && $_GET['lamp_id'] ? " AND tbl_lamp_events.lamp_id = '$_GET[lamp_id]'" : FALSE;
 			$where .= isset($_GET['event_title']) && $_GET['event_title'] ? " AND tbl_lamp_events.event_title LIKE '%$_GET[event_title]%'" : FALSE;
 		}

 		$param = array(
 			'table' => 'tbl_lamps'
 		);
 		$data['lamps'] = $this->global_model->get_rows( $param )->result_array();

 		$param = array(
 			'offset'	=> $data['offset'],
 			'limit'	=> PER_PAGE,
 			'table'	=> $this->_table,
 			'fields'	=> '*',
 			'where'	=> $where,
 			'join'	=> array('table'=> 'tbl_lamps','on'=>'tbl_lamps.lamp_id =  tbl_lamp_events.lamp_id')
 		);
 		$data['users'] = $this->global_model->get_rows( $param )->result_array();
 		$records = $this->global_model->get_total_rows( $param );
 		$data['pagination'] = $this->global_model->pagination( $records, $page, SITE_URL . '/lamp_events' );
 		$data['total'] = $records;

 		return $this->load->view('perks/reserve/lamps/events/index', $data, TRUE);

 	}

 	public function export() {

 		$where = "tbl_lamp_events.is_deleted = 0";

 		if( isset($_GET['search']) ) {
 			$where .= isset($_GET['lamp_id']) && $_GET['lamp_id'] ? " AND tbl_lamp_events.lamp_id = '$_GET[lamp_id]'" : FALSE;
 			$where .= isset($_GET['event_title']) && $_GET['event_title'] ? " AND tbl_lamp_events.event_title LIKE '%$_GET[event_title]%'" : FALSE;
 		}

 		$param = array(
 			'offset'	=> 0,
 			'table'	=> $this->_table,
 			'fields'	=> '*',
 			'where'	=> $where,
 			'join'	=> array(
 						'tbl_lamps' => 'tbl_lamps.lamp_id = tbl_lamp_events.lamp_id'
 					)
 		);
 		$records = $this->global_model->get_rows( $param )->result_array();

 		$row[] = array(
 			'#',
 			'LAMP Name',
 			'Event Name',
 			'Date Created'
		);
		if( $records ) {
			foreach( $records as $k => $v ) {
				$row[] = array(
					$k + 1,
					$v['lamp_name'],
					$v['event_title'],
					date('F d, Y H:i:s', strtotime($v['date_created']))
				);
			}
		}

		$this->load->library('to_excel_array');
		$this->to_excel_array->to_excel( $row, 'lamp_events_' . date('YmdHis') );

 	}

 	public function add_content() {

 		$error = '';

 		if($this->input->post('submit')) {
 			$this->load->library('form_validation');
 			$this->form_validation->set_rules($this->get_rules());
 			$valid = $this->form_validation->run();
 			if($valid) {
 				$post = $this->input->post();
 				$id = $this->global_model->insert($this->_table, $post);

 				$trail_data = array(
 					'url' => SITE_URL . '/lamp_events/add',
 					'description' => 'added a new lamp event',
 					'table' => 'tbl_lamp_events',
 					'record_id' => $id,
 					'type' => 'add'
 				);
 				$this->module_model->save_audit_trail($trail_data);

 				redirect('lamp_events');
 			} else {
 				$error = validation_errors();
 			}
 		}

 		$data['error'] = $error;

 		$param = array(
 			'table' => 'tbl_lamps'
 		);
 		$data['lamps'] = $this->global_model->get_rows($param)->result_array();
 		
 		$data['action'] = SITE_URL . '/lamp_events/add';

 		return $this->load->view('perks/reserve/lamps/events/add', $data, TRUE);

 	}

 	public function edit() {

 		$data = array(
 			'main_content'	=> $this->edit_content($this->uri->segment(3)),
 			'nav'			=> $this->nav_items()
 		);
 		$this->load->view('main-template', $data);

 	}

 	private function edit_content($id) {

 		$error = '';
 		$param = array(
 			'table' => $this->_table,
 			'where' => array(
 					'lamp_event_id' => $id
 				)
 		);
 		$record = (array)$this->global_model->get_row($param);

 		$param = array(
 			'table' => 'tbl_lamps',
 			'order_by' => array(
 				'field' => 'lamp_name',
 				'order' => 'ASC'
 			)
 		);

 		$data['lamps'] = $this->global_model->get_rows($param)->result_array();
 		$data['action'] = SITE_URL . '/lamp_events/edit/' . $id;

 		$old_content = $new_content = array();

 		if($this->input->post('submit')) {
 			$this->load->library('form_validation');
 			$this->form_validation->set_rules($this->get_rules());
 			$valid = $this->form_validation->run();
 			if($valid) {
 				$post = $this->input->post();
 				$this->global_model->update($this->_table, $post, array('lamp_event_id' => $id));

 				$trail_data = array(
 					'url' => SITE_URL . '/lamp_events/edit/' . $id,
 					'description' => 'updated a lamp event',
 					'table' => 'tbl_lamp_events',
 					'record_id' => $id,
 					'type' => 'edit'
 				);
 				$this->module_model->save_audit_trail($trail_data);

 				redirect('lamp_events');
 			} else {
 				$error = validation_errors();
 			}
 		}

 		$data['error'] = $error;
 		$data['record'] = $record;

 		return $this->load->view('perks/reserve/lamps/events/add', $data, TRUE);

 	}

 	private function get_rules() {

 		$config = array(
 			array(
 				'field' => 'lamp_id',
 				'label' => 'lamp id',
 				'rules' => 'required'
 			),
 			array(
 				'field' => 'event_title',
 				'label' => 'event title',
 				'rules' => 'required'
 			),
 			array(
 				'field' => 'status',
 				'label' => 'status',
 				'rules' => 'required'
 			)
 		);

 		return $config;

 	}

 	public function delete() {

 		$table = $this->_table;
		$id = $this->uri->segment(3);
		$field = 'lamp_event_id';
		$token = $this->uri->segment(4);
		if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/lamp_events') !== false && $token == md5($id . ' ' .	$this->config->item('encryption_key'))) {
			$where[$field] = $id;
			$this->global_model->delete($table, $where);

			$trail_data = array(
				'url' => @$_SERVER['HTTP_REFERER'],
				'description' => 'deleted a lamp event',
				'table' => 'tbl_lamp_events',
				'record_id' => $id
			);
			$this->module_model->save_audit_trail($trail_data);
		}
		redirect('lamp_events');

 	}

 	public function _remap($method) {

 		if( is_numeric($method) ) {
 			$this->index();
 		} elseif($method == 'edit') {
 			$this->edit();
 		} else {
 			$this->{$method}();
 		}

 	}
 
}