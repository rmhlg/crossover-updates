<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gameplay extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
	}
	
	public function index()
	{
		if(!$this->input->get('fromdate') || !$this->input->get('todate')) {
			redirect('gameplay?fromdate=' . date('Y-m-d', strtotime('-30 days')) .  '&todate=' . date('Y-m-d'));
		}
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
	}

	public function export()
	{
		$this->load->library('to_excel_array');
		$records = $this->get_demographics($from, $to);
		$total = 0;
		if($records) {
			foreach($records as $k => $v) {
				$total += $v['count'];
			}
		}
		$row[] = $from && $to ? array('Game Summary (' . date('F d, Y', strtotime($from)) . ' - ' . date('F d, Y', strtotime($to)) . ')') : array('Game Summary');

		$row[] = array('Name', 'Count', 'Percentage');
		if($records) {
			foreach($records as $k => $v) {
				$row[] = array($v['name'],
							   $v['count'],
							   number_format(($v['count'] / $total) * 100, 2));
			}
		}
		$this->to_excel_array->to_excel($row, 'game_summary_'.date("YmdHis"));
	}
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('navigations', $data, true);		
	}
	
	private function main_content() {
		$data['records'] = $this->get_demographics();
		$total = 0;
		if($data['records']) {
			foreach($data['records'] as $k => $v) {
				$total += $v['count'];
			}
		}
		$data['total'] = $total;
		return $this->load->view('gameplay/index', $data, true);		
	}

	private function get_demographics(&$from = false, &$to = false) {
		$param['fields'] = 'COUNT(score_id) AS count, name, tbl_game.game_id';
		$param['table'] = 'tbl_game';
		$param['join'] =array('table'=> 'tbl_game_score','on'=>'tbl_game.game_id = tbl_game_score.game_id');
		$field = $this->input->get('field') ? $this->input->get('field') : 'count'; 
		$sort = $this->input->get('sort') ? $this->input->get('sort') : 'asc'; 
		$param['order_by'] = array('field'	=>  $field, 'order'	=> $sort);
		$param['group_by'] = 'tbl_game.game_id';
		$where = '';
		if($this->input->get('fromdate') && $this->input->get('todate')) {
			$from = $this->input->get('fromdate') > $this->input->get('todate') ?  $this->input->get('todate') :  $this->input->get('fromdate');
			$to = $this->input->get('todate') < $this->input->get('fromdate') ?  $this->input->get('fromdate') :  $this->input->get('todate');
			$where['DATE(date_played) <='] = $to;
			$where['DATE(date_played) >='] = $from;
		}
		$param['where'] = $where;



		$records = $this->global_model->get_rows($param)->result_array();
		return $records;
	}
	
	public function _remap($method) {
		if($method == 'edit')
			$this->edit($this->uri->segment(3));
		elseif($method == 'delete')
			$this->delete($this->uri->segment(3));
		elseif($method == 'export')
			$this->export();
		else
			$this->index();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */