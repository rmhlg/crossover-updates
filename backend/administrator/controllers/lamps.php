<?php 

class Lamps extends CI_Controller {
	
	var $_table;
	public function __construct() {
		parent::__construct();
		$this->_table = 'tbl_lamps';
 	}
	
	public function index()
	{
		//$this->output->enable_profiler(TRUE);
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
	}
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('navigations', $data, true);		
	}

	private function get_user_lamp_ids() {
	//	$user = $this->login_model->extract_user_details();
		$param['table'] = 'tbl_lamp_permissions';
		$param['where'] = array('cms_user_id'	=> 1);//$user['cms_user_id']);
		$lamps = $this->global_model->get_rows($param)->result_array();

		$lamp_ids = array();
		if($lamps) {
			foreach ($lamps as $key => $value) {
				$lamp_ids[] = $value['lamp_id'];
			}
		}
		return $lamp_ids;	
	}

	
	private function main_content() {
		$page = $this->uri->segment(2, 1);
		$like = array();
		$data['offset'] = ($page - 1) * PER_PAGE;
		if(isset($_GET['search'])) {
			$like = $this->input->get();
			unset($like['search']);
		}
		$data['lamp_ids'] = $this->get_user_lamp_ids();
		
		//$param['having'] = $like;
		$param['offset'] = $data['offset'];
		$param['limit'] = PER_PAGE;
		$param['table'] = $this->_table;
		//$param['join'] = array('tbl_challenges', 'tbl_challenges.move_forward_id = tbl_move_forward.move_forward_id'); 
		$param['order_by'] = array('field'=>'created_date','order'=>'DESC');
		$param['where'] = array('is_deleted' => 0, 
		// 'where_in' => array('field' => 'lamp_id', 'arr' => $data['lamp_ids'])
			//'lamp_id'=> $data['lamp_ids']
		 );
		//$param['where_in'] = array('field' => 'lamp_id', 'arr' => $data['lamp_ids']);
		//$param['having'] = array('challenges LIKE ' => '%' . $challenge . '%', 'is_deleted' => 0);
		$param['like'] = $like;
		$data['categories'] = $this->global_model->get_rows($param)->result_array();
		//$param['where'] = array('is_deleted' => 0);
		$records = $this->global_model->get_total_rows($param);

		
	/*	$categories = array();
		foreach( $data['categories'] as $k => $v )
		{
			$categories[$k] = $v;

			if( ! in_array($v['lamp_id'], $this->get_user_lamp_ids()))
			{
				unset($categories[$k]);
			}
		}
		$data['categories'] = $categories;*/
		$data['pagination'] = $this->global_model->pagination($records, $page , SITE_URL . '/lamps');
		//$access = $this->module_model->check_access('lamps');
		$data['edit'] = 1;// $access['edit'];
		$data['delete'] = 1;//$access['delete'];
		$data['add'] = 1;//$access['add'];
		$data['total'] = $records;
		return $this->load->view('perks/reserve/lamps/index', $data, true);		
	}

	public function export() {
		$like = array();
		if(isset($_GET['search'])) {
			$like = $this->input->get();
			unset($like['search']);
		}

		//$param['having'] = $like;
		$param['table'] = $this->_table;
		//$param['join'] = array('tbl_challenges', 'tbl_challenges.move_forward_id = tbl_move_forward.move_forward_id'); 
		$param['order_by'] = array('field'=>'created_date','order'=>'DESC');
		$param['where'] = array('is_deleted' => 0);
		//$param['having'] = array('challenges LIKE ' => '%' . $challenge . '%', 'is_deleted' => 0);
		$param['like'] = $like;
		$records = $this->global_model->get_rows($param)->result_array();
		$row[] = array(	'Name',
						'Description',
						 'Status',
						 'Date Created');
		if($records) {
				foreach($records as $k => $v) {
					$row[] = array($v['lamp_name'], strip_tags($v['description']), $v['status'], $v['created_date']);
				}
			}		
		$this->load->library('to_excel_array');
		$this->to_excel_array->to_excel($row, 'lamps_'.date("YmdHis"));
	}
	
	public function add() {
		$data['main_content'] = $this->add_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);	
	}
	
	private function add_content() {
		$error = '';
		if($this->input->post('submit')) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules($this->get_rules());
			$valid = !(strtotime($this->input->post('start_date')) > strtotime($this->input->post('end_date')));
			if(!$valid) {
				$error = "Sorry, start date must come before the end date";
			} else {
				$valid = $this->form_validation->run();	
			}
			if($valid) {
				$post = $this->input->post();
				//$post['original_slots'] = $post['slots'];
				//$user = $this->login_model->extract_user_details();
				$user['cms_user_id'] = 1;
				$post['created_by'] = $user['cms_user_id'];
				$post['lamp_image'] = '';
				$post['is_deleted'] = 0;
				$post['radius'] = 0;
				
				if($_FILES['photo']['name']) {
					//$post['lamp_image'] = $this->upload_image($error, $config);

					/* upload image jerome */
							$config['upload_path'] = 'uploads/reserve/';
							$config['allowed_types'] = 'gif|png|jpg';
							$config['do_upload'] = 'photo';
							$config['max_width'] = 2000;
							$config['max_height'] = 1600;
							$config['max_size'] =  8072;
							$config['file_name'] = uniqid() . '.' . end(explode('.', $_FILES['photo']['name']));


							$this->load->library('upload', $config);

							if ( ! $this->upload->do_upload('photo'))
							{
												$error = array('error' => $this->upload->display_errors());
												print_r($error);
							}
							else
							{
																$arr_image = array('upload_data' => $this->upload->data());
																$params1 = array('new_filename' => $arr_image['upload_data']['file_name'], 
																	'width'=>319,'height'=>300,
																	'source_image'=>$config['upload_path'].$arr_image['upload_data']['file_name'],
																	'new_image_path'=>$config['upload_path'],
																	'file_name'=>$arr_image['upload_data']['file_name']);
																$this->load->helper(array('resize'));
										    					resize($params1);

																$post['lamp_image'] = $arr_image['upload_data']['orig_name'];
																$error = FALSE;
							} /* upload image jerome */


					/*if(!$error) {
						$params = array_merge($config,array('file_path'=>$config['file_path'],'full_path'=>$config['full_path'],'file_name'=>$config['file_name']));	    
						//$this->generate_thumb($params);		
					}*/
				}
				if(!$error) {
					$id = $this->global_model->insert($this->_table, $post);

								if($this->input->post('media_upload')) {
									$media = $this->input->post('media_upload');
									$title = $this->input->post('media_title');
									for($i = 0; $i < count($media); $i++) {
										$param = array();
										$param['media_title'] = $title[$i];
										$param['media_content'] = $media[$i];
										$param['media_type'] = 1;
										$param['origin_id'] = PERKS_RESERVE;
										$param['suborigin_id'] = $id;
										$param['cms_user_id'] = $user['cms_user_id'];
										$param['media_date_created'] = date('Y-m-d H:i:s');
										$this->global_model->insert('tbl_media', $param);
									}
								}


								//insert in tbl_lamp_permission 
								/*		$param123 = array();
										$param123['lamp_id'] = $id;
										$param123['cms_user_id'] = $user['cms_user_id'];
										$param123['is_deleted'] =0;
									$test = $this->global_model->insert('tbl_lamp_permissions', $param123);*/
									

					for($i = 1; $i <= 2; $i++) {
						if($this->input->post('property' . $i) && $this->input->post('value' . $i) && count($this->input->post('value' . $i)) > 0) {
							$param = array();
							$param['buy_item_id'] = $id;
							$param['property_name'] = $this->input->post('property' . $i);
							$property_id = $this->global_model->insert('tbl_properties', $param);

							foreach($this->input->post('value' . $i) as $prop_value) {
								$param = array();
								$param['property_id'] = $property_id;
								$param['value'] = $prop_value;
								$this->global_model->insert('tbl_property_values', $param);
							} 	
						}
					}	
					
					$post = array();
					$post['url'] = SITE_URL . '/move_fwd/add';
					$post['description'] = 'added a new move fwd item';
					$post['table'] = 'tbl_move_forward';
					$post['record_id'] = $id;
					$post['type'] = 'add';
					$this->module_model->save_audit_trail($post);
					redirect('lamps');	
				}
			} else {
				$error = !$error ? validation_errors() : $error;
			}
		}
		$data['error'] = $error;
		$data['record'] = $_POST;
		$data['action'] = SITE_URL . '/lamps/add';
		return $this->load->view('perks/reserve/lamps/add', $data, true);			
	}

	public function edit() {
		$id = $this->uri->segment(3);
		$data['main_content'] = $this->edit_content($id);
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);	
	}
	
	private function edit_content($id) {
		$error = '';

		$param['table'] = $this->_table;
		$param['where'] = array('lamp_id'	=> $id);

		$record = (array)$this->global_model->get_row($param);
		$media = $this->get_media($id);
		
		$data['record'] = $_POST ? $_POST : $record;
		$data['media'] = $media;
		$old_content = $new_content = array();
		

		if($this->input->post('submit')) {
			if($media) {
				$to_remove = array();
				foreach($media as $k => $v) {
					if(!@in_array($v['media_id'], $this->input->post('media_ids'))) {
						$to_remove[] = $v['media_id'];
					}
				}
				
				# delete all removed media
				if($to_remove) {
					$where = 'media_id IN(' .implode(',', $to_remove) . ')';
					$this->global_model->delete('tbl_media', $where);	
				}
				
			
			}

			$this->load->library('form_validation');
		
			$this->form_validation->set_rules($this->get_rules());
			$valid = !(strtotime($this->input->post('start_date')) > strtotime($this->input->post('end_date')));
			if(!$valid) {
				$error = "Sorry, start date must come before the end date";
			} else {
				$valid = $this->form_validation->run();	
			}
			if($valid) {
				$post = $this->input->post();
				//$user = $this->login_model->extract_user_details();
				$user['cms_user_id'] = $this->session->userdata('user_id');
				$post['created_by'] = $user['cms_user_id'];
				$post['status'] = $this->input->post('status');
				$post['radius'] = $user['cms_user_id'];
				
				if($_FILES['photo']['name']) {

			


											/* unlink the photo to save server space jeromejose 05-19-2015*/
											$unlink_data = $this->db->select()->from($this->_table)->where('lamp_id', $id)->get()->result_array();
											$unlink_image = 'uploads/reserve/'. $unlink_data[0]['lamp_image'];
											$unlink_image1 = 'uploads/reserve/main_'. $unlink_data[0]['lamp_image'];
											if (getimagesize($unlink_image) !== false) {
											 unlink($unlink_image);
											}
											if (getimagesize($unlink_image1) !== false) {
											 unlink($unlink_image1);
											}
											/* unlink the photo to save server space jeromejose 05-19-2015*/

				//	$post['buy_item_image'] = $this->upload_image($error, $config);
							/* upload image jerome */
							$config['upload_path'] = 'uploads/reserve/';
							$config['allowed_types'] = 'gif|png|jpg';
							$config['do_upload'] = 'photo';
							$config['max_width'] = 2000;
							$config['max_height'] = 1600;
							$config['max_size'] =  8072;


							$this->load->library('upload', $config);

							if ( ! $this->upload->do_upload('photo'))
							{
												$error = array('error' => $this->upload->display_errors());
												print_r($error);
							}
							else
							{
																$arr_image = array('upload_data' => $this->upload->data());
															
									$params1 = array('new_filename' => 'main_' .$arr_image['upload_data']['file_name'], 
																	'width'=>319,'height'=>300,
																	'source_image'=>$config['upload_path'].$arr_image['upload_data']['file_name'],
																	'new_image_path'=>$config['upload_path'],
																	'file_name'=>$arr_image['upload_data']['file_name']);
									$this->load->helper(array('resize'));
										    	resize($params1);

																$post['lamp_image'] = $arr_image['upload_data']['orig_name'];
																$error = FALSE;
							} /* upload image jerome */


					if(!$error) {
						$params = array_merge($config,array('file_path'=>$config['file_path'],'full_path'=>$config['full_path'],'file_name'=>$config['file_name']));	    
						//$this->generate_thumb($params);		
					}
				}
				
				if(!$error) {
					

					$test = $this->global_model->update($this->_table, $post, array('lamp_id'	=> $id));

					if($this->input->post('media_upload')) {
						$media = $this->input->post('media_upload');
						$title = $this->input->post('media_title');
						for($i = 0; $i < count($media); $i++) {
							$param = array();
							$param['media_title'] = $title[$i];
							$param['media_content'] = $media[$i];
							$param['media_type'] = 1;
							$param['origin_id'] = PERKS_RESERVE;
							$param['suborigin_id'] = $id;
							$param['cms_user_id'] = $user['cms_user_id'];
							$param['media_date_created'] = date('Y-m-d H:i:s');
							$this->global_model->insert('tbl_media', $param);
						}
					}

					$fields = array('bid_item_name', 'description', 'bid_item_image', 'start_date', 'end_date', 'slots', 'status');
					$status = array('Active', 'Inactive');
					foreach($record as $k => $v) {
						if(in_array($k, $fields)) {
							if($record[$k] != $this->input->post($k)) {
								if($k == 'status') {
									$new_content[$k] = $status[$this->input->post($k)];
									$old_content[$k] = $status[$record[$k]];
								} else {
									if($k == 'bid_item_image' && $this->input->post($k) == '')
										continue;
									$new_content[$k] = $this->input->post($k);
									$old_content[$k] = $record[$k];
								}
							}
						}
					}
					$post = array();
					$post['url'] = SITE_URL . '/bid_items/edit/' . $id;
					$post['description'] = 'updated a bid item';
					$post['table'] = $this->_table;
					$post['record_id'] = $id;
					$post['type'] = 'edit';
					$post['field_changes'] = serialize(array('old'	=> $old_content,
													  		  'new'	=> $new_content));
					$this->module_model->save_audit_trail($post);				

					redirect('lamps');
				}
				
			} else {
				$error = !$error ? validation_errors() : $error;
			}

		}
		$data['error'] = $error;
		$data['action'] = SITE_URL . '/lamps/edit/' . $id;
		return $this->load->view('perks/reserve/lamps/add', $data, true);					
	}

	public function delete() {
		$table = $this->_table;
		$id = $this->uri->segment(3);
		$field = 'lamp_id';
		$token = $this->uri->segment(4);
		if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/lamps') !== false && $token == md5($id . ' ' .	$this->config->item('encryption_key'))) {
			$where[$field] = $id;
			$this->global_model->delete($table, $where);
		}
		redirect($_SERVER['HTTP_REFERER']);
	}
	
	private function get_rules() {
		$config = array(
		   array(
				 'field'   => 'lamp_name',
				 'label'   => 'item name',
				 'rules'   => 'required'
			  ),
		   array(
				 'field'   => 'description',
				 'label'   => 'description',
				 'rules'   => 'required'
			  )
		);
		return $config;
	}

	private function get_media($id) {
		$param['table'] = 'tbl_media';
		$param['where'] = array('suborigin_id'	=> $id, 'origin_id'	=> PERKS_RESERVE);
		$media = $this->global_model->get_rows($param);
		return $media->result_array();
	} 

	private function upload_image(&$error = false, &$uploaded = false) {
		$this->load->helper(array('upload_helper', 'resize'));
		$config['upload_path'] = 'uploads/reserve/';
		$config['allowed_types'] = 'gif|png|jpg';
		$config['do_upload'] = 'photo';
		$config['max_width'] = 2000;
		$config['max_height'] = 1600;
		$config['max_size'] =  8072;
		$config['file_name'] = uniqid() . '.' . end(explode('.', $_FILES['photo']['name']));
		$uploaded = upload($config);
		if(!is_array($uploaded)) {
			$error = $uploaded;
			return;
		} else {

			$params = array('new_filename' => 'main_' .$config['file_name'], 'width'=>319,'height'=>300,'source_image'=>$config['upload_path'].'/'.$uploaded['file_name'],'new_image_path'=>$config['upload_path'].'/','file_name'=>$config['file_name']);
	    	resize($params);
	    }
		return $uploaded['file_name'];
	}

	private function generate_thumb($uploaded) {
		list($width, $height) = getimagesize($uploaded['full_path']); 
		if($width < 300) {
			copy($uploaded['full_path'], $uploaded['file_path'].'thumb_'.$uploaded['file_name']);
			return;
		}

		$this->load->helper('resize');
		// $config['x'] = $this->input->post('x');
		// $config['y'] = $this->input->post('y');
		// $config['x2'] = $this->input->post('x2');
		// $config['y2'] = $this->input->post('y2');
		// $config['width'] = $this->input->post('width');
		// $config['height'] = $this->input->post('height');
		// $params = array_merge($config, $uploaded);	    
		resize($uploaded);	

	}

	public function upload_media()
	{
		$config = array();
		$path = './uploads/reserve/';
		if (!is_dir($path)) {
			mkdir($path);
		}
		$path .= 'media/';
		if (!is_dir($path)) {
			mkdir($path);
		}

		$config['upload_path'] = $path;
		$config['allowed_types'] = PRODUCT_INFO_FILE_UPLOAD_TYPES;
		if (strpos($_FILES['myFile']['type'], 'image') !== false) {
			$config['max_size'] = FILE_UPLOAD_LIMIT;
		} else {
			$config['max_size']	= '20000';
		}
		$config['max_size'] = FILE_UPLOAD_LIMIT;
		$config['file_name'] = sha1(uniqid()).'.'.pathinfo($_FILES['myFile']['name'], PATHINFO_EXTENSION);

		$this->load->helper('resize');
		$this->load->library('upload', $config);

		if ($this->upload->do_upload('myFile')) {
			$data = $this->upload->data();
			if ($data['is_image']) {
				resize(array(
					'width' => 150,
					'height'=> 150,
					'source_image' => $path.$config['file_name'],
					'new_image_path' => $path,
					'file_name'=> $config['file_name']
				));
				resize(array(
					'width' => 576,
					'height'=> 296,
					'source_image' => $path.$config['file_name'],
					'new_image_path' => $path,
					'file_name'=> $config['file_name']
				));
			} else {
				$realdir = realpath($path);
				if ($realdir) {
					$realdir = str_replace("\\", '/', rtrim($realdir, DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR);
$thumbfilename = 'thumb_'.$config['file_name'].'.jpg';
exec('ffmpeg -i '.$realdir.$config['file_name'].' -ss 00:00:03.000 -f image2 -vframes 1 '.$realdir.$thumbfilename);
resize(array(
'width' => 150,
'height'=> 150,
'source_image' => $path.$thumbfilename,
'new_image_path' => $path,
'file_name'=> $thumbfilename
));
}
}
//$this->output->set_output($config['file_name'].' - '.($data['is_image'] ? 1 : 2));
$this->output->set_output($config['file_name']);
}


	}
	
public function _remap($method) {
if($method == 'edit')
$this->edit($this->uri->segment(3));
elseif($method == 'delete')
$this->delete($this->uri->segment(3));
elseif($method == 'add')
$this->add();
elseif($method == 'export')
$this->export();
elseif(is_numeric($method))
$this->index();
else
$this->{$method}();
}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */