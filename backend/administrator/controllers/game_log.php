<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Game_log extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('game_model');
	}
	
	public function index()
	{
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
	}
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('navigations', $data, true);		
	}
	
	private function main_content() {
		$page = $this->uri->segment(2, 1);
		$where = '';
		if(isset($_GET['search'])) {
			if($_GET['name'] != '' || $_GET['name'] != null)
			{
				$where =  " AND CONCAT(first_name, ' ', third_name) LIKE '%$_GET[name]%'";
			}


			if(isset($_GET['earned_km_points']) || $_GET['earned_km_points'] != '' || $_GET['earned_km_points'] != null)
			{
					$where .=  " AND earned_km_points LIKE '%$_GET[earned_km_points]%'";
			}

			if($_GET['score'] != '' || $_GET['score'] != null)
			{
					$where .=  " AND score LIKE '%$_GET[score]%'";
			}

			if($_GET['game_id'] != '' || $_GET['game_id'] != null)
			{
				$where .= $_GET['game_id'] != '' ? " AND g.game_id = '$_GET[game_id]'" : "";
			}
			if($_GET['is_cheater'] != '' || $_GET['is_cheater'] != null)
			{
				$where .= $_GET['is_cheater'] != '' ? " AND gs.is_cheater = '$_GET[is_cheater]'" : "";
			}
			if($_GET['from'] != '' || $_GET['from'] != null)
			{
				$where .= $_GET['from'] ? " AND DATE(date_played) >= '$_GET[from]'" : "";
			}
			if($_GET['to'] != '' || $_GET['to'] != null)
			{
				$where .= $_GET['to'] ? " AND DATE(date_played) <= '$_GET[to]'" : "";
			}

			
		}
		$data['logs'] = $this->game_model->get_game($where, $page, PER_PAGE, $records);
		$data['offset'] = ($page - 1) * PER_PAGE;
		$param = array();
		$param['table'] = 'tbl_game';
		$param['order_by'] = array('field' => 'name', 'order'	=> 'ASC');
		$param['fields'] = 'game_id, name';
		$games = $this->global_model->get_rows($param)->result_array();
		$data['games'] = $games;

		$data['pagination'] = $this->global_model->pagination($records, $page , SITE_URL . '/game_log');
		//$access = $this->module_model->check_access('game_log');
		$data['edit'] = 1;// $access['edit'];
		$data['delete'] = 1;//$access['delete'];
		$data['add'] = 1;//$access['add'];
		$data['total'] = $records;
		return $this->load->view('game_log/index', $data, true);		
	}

	public function export() {
		$this->load->library('to_excel_array');
		$where = '';
		if(isset($_GET['search'])) {
				if($_GET['name'] != '' || $_GET['name'] != null)
			{
				$where =  " AND CONCAT(first_name, ' ', third_name) LIKE '%$_GET[name]%'";
			}

			if($_GET['score'] != '' || $_GET['score'] != null)
			{
					$where .=  " AND score LIKE '%$_GET[score]%'";
			}
if($_GET['earned_km_points'] != '' || $_GET['earned_km_points'] != null)
			{
					$where .=  " AND earned_km_points LIKE '%$_GET[earned_km_points]%'";
			}
			if($_GET['game_id'] != '' || $_GET['game_id'] != null)
			{
				$where .= $_GET['game_id'] != '' ? " AND g.game_id = '$_GET[game_id]'" : "";
			}
			if($_GET['is_cheater'] != '' || $_GET['is_cheater'] != null)
			{
				$where .= $_GET['is_cheater'] != '' ? " AND gs.is_cheater = '$_GET[is_cheater]'" : "";
			}
			if($_GET['from'] != '' || $_GET['from'] != null)
			{
				$where .= $_GET['from'] ? " AND DATE(date_played) >= '$_GET[from]'" : "";
			}
			if($_GET['to'] != '' || $_GET['to'] != null)
			{
				$where .= $_GET['to'] ? " AND DATE(date_played) <= '$_GET[to]'" : "";
			}

		}
		$records = $this->game_model->get_game($where, null, null, $total, true);
		$row[] = array('Name', 
					   'Game',
					   'Score',
					   'Cheater',
					   'Date Played');
		if($records) {
			foreach($records as $k => $v) {
				$cheater = $v['is_cheater'] ? 'Yes' : 'No';
				$row[] = array($v['first_name'] . ' ' . $v['third_name'],
							  $v['name'],
							  $v['score'],
							  $cheater,
							  $v['date_played']);
			}
		}
		$this->to_excel_array->to_excel($row, 'games_'.date("YmdHis"));
	}

	public function _remap($method) {
		if($method == 'edit')
			$this->edit($this->uri->segment(3));
		elseif($method == 'delete')
			$this->delete($this->uri->segment(3));
		elseif($method == 'export')
			$this->export();
		else
			$this->index();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */