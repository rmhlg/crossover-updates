<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

class Activity_Media extends CI_Controller {
	

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('video_helper');
 	} 

 	 
 	public function index()
	{
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
 	}
 	 
	
	private function nav_items()
	{
 		return $this->load->view('navigations', '', true);		
	}


	public function main_content()
	{
		$this->load->helper('paging');
		$page = $this->uri->segment(2, 1);
	    	$limit = isset($_GET['limit']) ? $_GET['limit'] : 15;
	    	$offset = ($page - 1) * $limit;
	    	$filters = $this->get_filters(); 

		// $total_rows = $this->global_model->get_rows(array('table'=>'activity',
		// 												 'where'=>$filters['where_filters'],
		// 												 'like'=>$filters['like_filters'],
		// 												 'fields'=>'activity_id'																 
		// 												 )
		// 										    )->num_rows();
 		$data['header_text'] = 'Activity Media';
		// $data['total_rows'] = $total_rows;
		// $data['query_strings'] = $filters['query_strings'];

		// $data['rows'] = $this->global_model->get_rows(array('table'=>'activity',
		// 												     'where'=>$filters['where_filters'],
		// 												     'like'=>$filters['like_filters'],
		// 												     'order_by'=>array('field'=>'activity.date_created','order'=>'DESC'),
		// 												     'offset'=>$offset,
		// 												     'limit'=>$limit,
		// 												     'join'=>array('table'=>'activity_media','on'=>'activity.activity_id=activity_media.activity_id'),
		// 												     'fields'=>'activity_media.activity_media_id as activity_id, activity.title as title, activity_media.media_title, activity_media.date_created'
  // 														     )
		// 												);

		$this->db->start_cache();
		$this->db->select('*, b.date_created as created')->from('tbl_activity_media b');
		$this->db->join('tbl_activity a', 'a.activity_id = b.activity_id', 'left');
		$this->db->join('tbl_experience c', 'c.experience_id = a.experience_id', 'left');
		if(isset($_GET['category']) && $_GET['category']) {
			$this->db->where('c.category_id', $_GET['category']);
		}
		$data['total_rows'] = $this->db->count_all_results();
		$this->db->limit($limit, $offset);
		$data['rows'] = $this->db->get();
		$this->db->stop_cache();
		$this->db->flush_cache();
  
 		$data['pagination'] = paging($data['total_rows'],$limit,$offset,site_url().'/'.$this->uri->segment(1).$filters['query_strings']);
 		$data['header_css_assets'] = array(base_url().'assets/css/ngDialog.min.css',base_url().'assets/css/ngDialog-theme-default.min.css');
 		$data['header_js_assets'] = array(base_url().'assets/js/angular/libraries/angular.min.js');
		$data['footer_js_assets'] = array(base_url().'assets/js/angular/services/dialog.js',
											base_url().'assets/js/angular/services/ngDialog.min.js');
 		return $this->load->view('activity-media-list',$data,TRUE);


	}
 

	public function add()
	{		 
		$data['row_photos'] =  '';
 		$data['header_text'] = 'Add Activity Media';
 		// $data['experiences'] = $this->global_model->get_rows(array('table'=>'experience'));
 		// $data['activities'] = $this->global_model->get_rows(array('table'=>'activity'));
 		$data['activities'] = $this->db->select()->from('tbl_activity')->join('tbl_experience', 'tbl_experience.experience_id = tbl_activity.experience_id')->get()->result();
 		$data['row'] = false;
 		$data['medias'] = false;
 		$data['decisions'] = $this->db->select('decision_left_title, decision_right_title')->from('tbl_activity')->get();
 
 		$data['main_content'] = $this->load->view('activity-media-form',$data,TRUE);
		$data['nav'] = $this->nav_items();
		$data['header_js_assets'] = array(base_url().'assets/js/angular/libraries/angular.min.js');
		$data['footer_js_assets'] = array(base_url().'assets/js/angular/services/textAngular-sanitize.min.js',
											base_url().'assets/js/angular/services/textAngular.min.js',
											base_url().'assets/js/angular/services/angular-file-upload.min.js',
											base_url().'assets/js/angular/services/angular-file-upload-shim.js',
											base_url().'assets/admin/js/angular/modules/activityModule.js');
		$this->load->view('main-template', $data);
 
	}


	public function edit(){

		$data['row_photos'] =  '';
 		$data['header_text'] = 'Edit Activity Media';
 		// $data['experiences'] = $this->global_model->get_rows(array('table'=>'experience'));
 		// $data['activities'] = $this->global_model->get_rows(array('table'=>'activity'));
 		$data['activities'] = $this->db->select()->from('tbl_activity')->join('tbl_experience', 'tbl_experience.experience_id = tbl_activity.experience_id')->get()->result();
 		
 		$data['row'] = $this->global_model->get_row(array('table'=>'activity_media','where'=>$this->input->get()));
 		$data['decisions'] = $this->db->select('decision_left_title, decision_right_title')->from('tbl_activity')->get();

 		$data['main_content'] = $this->load->view('activity-media-form',$data,TRUE);
		$data['nav'] = $this->nav_items();
		$data['header_js_assets'] = array(base_url().'assets/js/angular/libraries/angular.min.js');
		$data['footer_js_assets'] = array(base_url().'assets/js/angular/services/textAngular-sanitize.min.js',
											base_url().'assets/js/angular/services/textAngular.min.js',
											base_url().'assets/js/angular/services/angular-file-upload.min.js',
											base_url().'assets/js/angular/services/angular-file-upload-shim.js',
											base_url().'assets/admin/js/angular/modules/activityModule.js');
		$this->load->view('main-template', $data);

	}

	public function save()
	{
		$this->load->library('SimpleImage');
		// $config['upload_path'] = './uploads/activity_media';
		// $config['allowed_types'] = '*';
		// $this->load->library('upload', $config);

		if( ! $_POST['activity_media_id']) {
			if($this->input->post('media_type') == 1) {
				$config['upload_path'] = './uploads/activity_media';
				$config['allowed_types'] = '*';
				$this->load->library('upload', $config);
				$image1 = 'media';
				if($this->upload->do_upload($image1)) {
					$image1_data = $this->upload->data();
					unset($_POST['media']);
					$_POST['media'] = base_url().'uploads/activity_media/'.$image1_data['file_name'];
				}
				$this->db->insert('tbl_activity_media', $_POST);
			} else if($this->input->post('media_type') == 2) {
				$config['upload_path'] = './uploads/videos';
				$config['allowed_types'] = '*';
				$this->load->library('upload', $config);
				$video = 'media';
				if($this->upload->do_upload($video)) {
					$config['upload_path'] = './uploads/videos';
					$config['allowed_types'] = '*';
					$this->load->library('upload', $config);
					$video_data = $this->upload->data();
					$realpath = realpath($config['upload_path']);
					if($realpath) {
						$realpath = str_replace("\\", '/', rtrim($realpath, DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR);
						$thumbfilename = pathinfo($video_data['file_name'], PATHINFO_FILENAME).'.jpg';
						exec('ffmpeg -i '.$realpath.$video_data['file_name'].' -ss 00:00:03.000 -f image2 -vframes 1 '.$realpath.'thumb/'.$thumbfilename);
						$_POST['media'] = base_url().'uploads/videos/thumb/'.$video_data['file_name'];
						$_POST['preview_if_video'] = base_url().'uploads/videos/thumb/thumb_'.$thumbfilename;
						$this->simpleimage->load('./uploads/videos/thumb/'.$thumbfilename);
						$this->simpleimage->resize(512, 310);
						$this->simpleimage->save('./uploads/videos/thumb/thumb_'.$thumbfilename);
						$_POST['video_duration'] = get_video_duration($realpath.$video_data['file_name']);
					}
					$this->db->insert('tbl_activity_media', $_POST);
				}
			}			
		} else {
			if($this->input->post('media_type') == 1) {
				$config['upload_path'] = './uploads/activity_media';
				$config['allowed_types'] = '*';
				$this->load->library('upload', $config);
				$image1 = 'media';
				if($this->upload->do_upload($image1)) {
					$image1_data = $this->upload->data();
					unset($_POST['media']);
					$_POST['media'] = base_url().'uploads/'.$image1_data['file_name'];
				} else {
					unset($_POST['media']);
				}
				$_POST['preview_if_video'] = '';
				$this->db->update('tbl_activity_media', $_POST, array('activity_media_id'=>$_POST['activity_media_id']));
			} else if($this->input->post('media_type') == 2) {
				$config['upload_path'] = './uploads/videos';
				$config['allowed_types'] = '*';
				$this->load->library('upload', $config);
				$video = 'media';
				if($this->upload->do_upload($video)) {
					$video_data = $this->upload->data();
					$realpath = realpath($config['upload_path']);
					if($realpath) {
						$realpath = str_replace("\\", '/', rtrim($realpath, DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR);
						$thumbfilename = pathinfo($video_data['file_name'], PATHINFO_FILENAME).'.jpg';
						exec('ffmpeg -i '.$realpath.$video_data['file_name'].' -ss 00:00:03.000 -f image2 -vframes 1 '.$realpath.'thumb/'.$thumbfilename);
						$_POST['media'] = base_url().'uploads/videos/thumb/'.$video_data['file_name'];
						$_POST['preview_if_video'] = base_url().'uploads/videos/thumb/thumb_'.$thumbfilename;
						$this->simpleimage->load('./uploads/videos/thumb/'.$thumbfilename);
						$this->simpleimage->resize(512, 310);
						$this->simpleimage->save('./uploads/videos/thumb/thumb_'.$thumbfilename);
						$_POST['video_duration'] = get_video_duration($realpath.$video_data['file_name']);
					}
				}
				$this->db->update('tbl_activity_media', $_POST, array('activity_media_id'=>$_POST['activity_media_id']));
			}
			
		}
		redirect('activity_media');
	}


	public function upload(){

		$this->load->helper('upload');

		$response = array('new_image'=>null,'thumbnail'=>null,'error'=>null);
		$upload_path = './uploads/activity';

		if(!is_dir($upload_path)){
			mkdir($upload_path,0777,true);
		}

		$params = array('upload_path'=>$upload_path,'allowed_types'=>'png|jpg|gif','file_name'=>uniqid(),'max_size'=>'2097152','max_width'=>0,'max_height'=>0,'do_upload'=>'image');
	    $file = upload($params);
 
	    if(is_array($file)){

	    	$this->load->helper('resize');
	    	$params = array('width'=>50,'height'=>50,'source_image'=>$upload_path.'/'.$file['file_name'],'new_image_path'=>$upload_path.'/resized/','file_name'=>$file['file_name']);
 		    //resize($params);

 		    $response['new_image'] = $file['file_name'];
 
	    }else
	    	$response['error'] = $file;
	    
	    $this->load->view('json',array('data'=>$response));

	}

	public function delete()
	{

 		$this->global_model->delete('activity_media',$this->input->get());
		redirect($_SERVER["HTTP_REFERER"]);

	}

	public function validate_form()
	{

		$this->load->library('form_validation');

		$rules = array(
		   array(
				 'field'   => 'title',
				 'label'   => 'Title',
				 'rules'   => 'required'
			  )

		);

		$this->form_validation->set_rules($rules);
		
		return $this->form_validation->run();
	}

	public function get_filters()
	   {
		   
		   $where_filters = array('DATE(date_added) >='=>'from_date_added','DATE(date_added) <='=>'to_date_added');
		   $like_filters = array('name'=>'name');
		   $query_strings = array();
		   
		   $valid_where_filters = array();
		   $valid_like_filters = array();
		   
		   foreach($where_filters as $column_field=>$filter){
			    
 				if($this->input->get($filter) || $this->input->get($filter) ==='0'){
 					$valid_where_filters[$column_field] = trim($this->input->get($filter));
 					$query_strings[$filter] = $this->input->get($filter);
 				}
									 
				  
 		   } 
		    
		   
		   foreach($like_filters as $column_field=>$filter){
			   
 				if($this->input->get($filter)){
 					$valid_like_filters[$column_field] = trim($this->input->get($filter));
 					$query_strings[$filter] = $this->input->get($filter);
 				}
					
				 
				   
		   } 

 		   $query_strings = ($query_strings) ? http_build_query($query_strings) : '';
 		   
		   return array('where_filters'=>$valid_where_filters,'like_filters'=>$valid_like_filters,'query_strings'=>'?'.$query_strings);		   
		   
	   } 


	   public function export()
	   {
	   		$this->load->library('to_excel_array');

		   	$filters = $this->get_filters();
		   	$params = 
		   	$query = $this->global_model->get_rows(array('fields'=>"name,date_added",
		   														'table'=>'activity',
											   					'where'=>array_merge($filters['where_filters'],array('is_deleted'=>0)),
											   					'like'=>$filters['like_filters'],
 											   					)
		   													);
		   	$res[] = array('Regions,Date Added');

		   	if($query->num_rows()){
		   		foreach($query->result() as $v){
 		   			$res[] = array($v->name,$v->date_added);
		   		}
		   	}
		   	$this->to_excel_array->to_excel($res,  $this->uri->segment(1).date("Y-m-d_H-i-s"));

	   }
 
}