<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_birthday extends CI_Controller {
	
	var $_table;
	public function __construct() {
		parent::__construct();
		$this->_table = 'tbl_user_birthday_prizes';
		$this->load->model('explore_model');
	}
	
	public function index()
	{
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
 	}
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('navigations', $data, true);		
	}
	
	private function main_content() {

		$this->load->helper(array('paging','text'));
 		$offset = (int)$this->input->get('per_page');
	    $limit = PER_PAGE;
	    $filters = $this->get_filters();

		//$access = $this->module_model->check_access('user_birthday');
		$total_rows = $this->explore_model->get_rows(array('table'=>'tbl_user_birthday_prizes',
															 'where'=>$filters['where_filters'],
														     'like'=>$filters['like_filters'],
														      'fields'=>"prize_id"
														     )
														    )->num_rows(); 
		$data['header_text'] = 'User Birthday Prizes';
		$data['edit'] = 1;// $access['edit'];
		$data['delete'] = 1;//$access['delete'];
		$data['add'] = 1;//$access['add'];
		$data['total_rows'] = $total_rows;

		$data['rows'] = $this->explore_model->get_rows(array('table'=>'tbl_user_birthday_prizes',
															 'where'=>$filters['where_filters'],
														     'like'=>$filters['like_filters'],
														     'limit'=>$limit,
														     'offset'=>$offset
														     )
														);
		$data['query_strings'] = $filters['query_strings'];
		$data['pagination'] = paging($total_rows,$limit,$offset,SITE_URL.'/'.$this->uri->segment(1).$filters['query_strings']); 
		return $this->load->view('user_birthday/index',$data,TRUE);
		

	}

	public function get_filters()
   {
	   
	   $where_filters = array('prize_status'=>'prize_status',
	   							'send_type'=>'send_type',
	   							'DATE(date_created) >='=>'fromavail',
	   							'DATE(date_created) <='=>'toavail',
	   							'DATE(date_claimed) >='=>'fromredeem',
	   							'DATE(date_claimed) <='=>'toredeem');
	   $like_filters = array("registrant_name"=>'registrant_name','prize_name'=>'prize_name');
	   $query_strings = array();
		   
		   $valid_where_filters = array();
		   $valid_like_filters = array();
		   
		   foreach($where_filters as $column_field=>$filter){
			    
 				if($this->input->get($filter) || $this->input->get($filter) ==='0'){
 					$valid_where_filters[$column_field] = trim($this->input->get($filter));
 					$query_strings[$filter] = $this->input->get($filter);
 				}
									 
				  
 		   } 
		    
		   
		   foreach($like_filters as $column_field=>$filter){
			   
 				if($this->input->get($filter)){
 					$valid_like_filters[$column_field] = trim($this->input->get($filter));
 					$query_strings[$filter] = $this->input->get($filter);
 				}
					
				 
				   
		   } 

 		   $query_strings = ($query_strings) ? http_build_query($query_strings) : '';
		   
	   return array('where_filters'=>$valid_where_filters,'like_filters'=>$valid_like_filters,'query_strings'=>'?'.$query_strings);		   
	   
   }

	public function export() {
		$this->load->library('to_excel_array');
		$like = array();
		$where = array();
		if(isset($_GET['search'])) {
			$like = $this->input->get();
			if($like['fromavail'] && $like['toavail']) {
				$from = $like['fromavail'] > $like['toavail'] ?  $like['toavail'] :  $like['fromavail'];
				$to = $like['toavail'] < $like['fromavail'] ?  $like['fromavail'] :  $like['toavail'];
				$where['DATE(' . $this->_table . '.date_created) <='] = $to;
				$where['DATE(' . $this->_table . '.date_created) >='] = $from;
			}
			if($like['fromredeem']&& $like['toredeem']) {
				$from = $like['fromredeem'] > $like['toredeem'] ?  $like['toredeem'] :  $like['fromredeem'];
				$to = $like['toredeem'] < $like['fromredeem'] ?  $like['fromredeem'] :  $like['toavail'];
				$where['DATE(date_claimed) <='] = $to;
				$where['DATE(date_claimed) >='] = $from;
			}
			if($like['prize_name']) {
				$like["tbl_birthday_offers.prize_name"] = $like['prize_name'];
			}

			if($like['send_type']) {
				$like["tbl_birthday_offers.send_type"] = $like['send_type'];
			}

			if($like['registrant_name']) {
				$like["registrant_name"] = $like['registrant_name'];
			}

			unset($like['registrant_name']);
			unset($like['search']);
			unset($like['send_type']);
			unset($like['prize_name']);
			unset($like['fromredeem']);
			unset($like['fromavail']);
			unset($like['toavail']);
			unset($like['toredeem']);

		}
		/*if(!$where)
		{
			$where = '';
		}
		if(!in_array($like))
		{
			$like = '';
		}*/
		$param['like'] = $like;
		$param['table'] = $this->_table;
		$param['where'] = $where;
		$param['fields'] = '*, tbl_cities.city as cityname, tbl_provinces.province as provincename, tbl_user_birthday_prizes.date_created as date_availed';
		$param['join'] = array(array('table'=>'registrants','on'=>'registrants.registrant_id=user_birthday_prizes.registrant_id'),
					     		array('table'=>'birthday_offers','on'=>'birthday_offers.prize_id=user_birthday_prizes.prize_id'),
					     		array('table'=>'provinces','on'=>'provinces.province_id=registrants.province'),
					     		array('table'=>'cities','on'=>'cities.city_id=registrants.city')
					     		);
/* array('tbl_registrants' => 'tbl_registrants.registrant_id = ' . $this->_table . '.registrant_id',
							  'tbl_birthday_offers' => 'tbl_birthday_offers.prize_id = ' . $this->_table . '.prize_id',
							  'tbl_provinces'	=> 'tbl_provinces.province_id = tbl_registrants.province',
							  'tbl_cities'	=> 'tbl_cities.city_id = tbl_registrants.city');*/
		$records = $this->global_model->get_rows($param)->result_array();

		$row[] = array('Name', 
					   'Street',
					   'Barangay',
					   'City',
					   'Province',
					   'Mobile',
					   'Prize',
					   'Type',
					   'Status',
					   'Date Availed',
					   'Date Claimed');
		if($records) {
			foreach($records as $k => $v) {
				$status = 'Pending';
				if($v['prize_status'] == 1) {
					if($v['send_type'] == 'Delivery') {
						$status = 'Delivered';
					} else {
						$status = 'Redeemed';
					}
				} 
				$date_delivered = $v['prize_status'] == 1 ? $v['date_claimed'] : 'N/A';
				$row[] = array($v['first_name'] . ' ' . $v['third_name'],
							  $v['street_name'], 
							  $v['barangay'], 
							  $v['cityname'], 
							  $v['provincename'],
							  $v['mobile_phone'],
							  $v['prize_name'],
							  $v['send_type'],
							  $status,
							  $v['date_availed'], 
							  $date_delivered);
			}
		}
		$this->to_excel_array->to_excel($row, 'user_birthday_prizes'.date("YmdHis"));
	}

	public function edit()
	{
		$id = $this->uri->segment(3);
		$data['main_content'] = $this->edit_content($id);
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
	}

	private function edit_content($id) {
		$error = false;
		$param['table'] = $this->_table;
		/*
		$param['join'] = array('tbl_registrants' => 'tbl_registrants.registrant_id = ' . $this->_table . '.registrant_id',
							  'tbl_birthday_offers' => 'tbl_birthday_offers.prize_id = ' . $this->_table . '.prize_id');
							  */
		$param['where'] = array('tbl_user_birthday_prizes.user_birthday_prize_id'	=> $id);
		$prize = (array)$this->global_model->get_row($param);
			
		if($this->input->post('submit')) {
			$param['prize_status'] = $this->input->post('prize_status');
			//if($param['prize_status'] == 1) {
				$param['date_modified'] = array(array('field'=>'date_claimed','value'=>date('Y-m-d H:i:s')));
			//}
			$where = array('user_birthday_prize_id'	=> $prize['user_birthday_prize_id']);
			$table = 'tbl_user_birthday_prizes';
			$this->global_model->update($table, $param, $where);
			redirect('user_birthday');
		}

		$month_label = array('', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
		$data['record'] = $prize;
		$data['error'] = $error;
		$data['month'] = $month_label;
		return $this->load->view('user_birthday/add', $data, true);		
	}

	public function update_status()
 	{

 		$id = $this->uri->segment(3);
 		$token = $this->uri->segment(4);
 		$status = $this->uri->segment(5);
 		$row = $this->explore_model->get_rows(array('table'=>'user_birthday_prizes',
 													'where'=>array('user_birthday_prizes.user_birthday_prize_id'=>$id,'user_birthday_prizes.prize_status'=>0),
 													'join'=>array(array('table'=>'tbl_registrants as r','on'=>'r.registrant_id=user_birthday_prizes.registrant_id'),
													     		array('table'=>'tbl_flash_offers as p','on'=>'p.prize_id=user_birthday_prizes.prize_id')),
 													'fields'=>'user_birthday_prizes.*,r.registrant_id,r.first_name,r.third_name,p.send_type'
 													)
 											);

 		if($row && strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/'.$this->uri->segment(1)) !== false && $token == md5($id . ' ' .	$this->config->item('encryption_key'))) {
 			$this->explore_model->update('tbl_user_birthday_prizes',array('prize_status'=>$status,'date_modified'=>array(array('field'=>'date_claimed','value'=>'NOW()'))),array('user_birthday_prize_id'=>$id));
 			$name = ($row->first_name || $row->third_name) ? ucwords($row->first_name.' '.$row->third_name) : '(No name)';
 			$action = ($row->send_type=='Delivery') ? 'delivered' : 'redeemed';
 			$post['url'] = $_SERVER['HTTP_REFERER'];
			$post['description'] = 'update '.$name.'\'s birthday prize to '.$action.' status.';
			$post['table'] = 'tbl_user_birthday_prizes';
			$post['record_id'] = $id;
			$this->module_model->save_audit_trail($post);
		}

		redirect($this->router->class);

 	}

	 
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */