<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

class hidden_packs extends CI_Controller {
	

	public function __construct() {
		parent::__construct();
		$this->load->model('explore_model');
		
 	} 

 	 
 	public function index()
	{
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
 
 	}
 	 
	
	private function nav_items() {
		return $this->load->view('navigations', NULL, true);		
	}


	public function main_content()
	{

		$this->load->helper('paging');

		$start_date = '2014-07-09';
		$end_date = '2014-07-10';
		$where = array("(DATE(start_date) >= '".$start_date."' AND DATE(end_date) <= '".$end_date."') OR
					    (DATE(start_date) < '".$start_date."' AND DATE(end_date) > '".$end_date."') "=>null,'is_deleted'=>0);
 		
		$row = $this->explore_model->get_row(array('table'=>'tbl_hidden_packs','where'=>$where));

		$offset = (int)$this->input->get('per_page');
	    $limit = PER_PAGE;
	    $filters = $this->get_filters(); 

	//	$access = $this->module_model->check_access('hidden_packs');
		$total_rows = $this->explore_model->get_rows(array('table'=>'tbl_hidden_packs',
																 'where'=>array_merge($filters['where_filters'],array('is_deleted'=>0)),
																 'like'=>$filters['like_filters']																 
																 )
														    )->num_rows();
 		$data['header_text'] = 'Hidden Packs';
		$data['edit'] = true;
		$data['delete'] = true;
		$data['add'] = true;
		$data['total_rows'] = $total_rows;
		$data['query_strings'] = $filters['query_strings'];
 		
		$data['rows'] = $this->explore_model->get_rows(array('table'=>'tbl_hidden_packs',
															 'where'=>array_merge($filters['where_filters'],array('is_deleted'=>0)),
														     'like'=>$filters['like_filters'],
														     'order_by' => array('field'=>'date_added','order'=>'DESC'),
														     'offset'=>$offset,
														     'limit'=>$limit
 														     )
														);
 		$data['pagination'] = paging($total_rows,$limit,$offset,SITE_URL.'/'.$this->uri->segment(1).$filters['query_strings']);
 		return $this->load->view('hidden_pack/list',$data,TRUE);

	}


	public function upload_photo()
	{

		$this->load->helper(array('upload','resize'));


 		$upload_path = 'uploads/hidden_packs/';
 		$upload_path_thumbnail = $upload_path;

 		if(!is_dir($upload_path))
 			mkdir($upload_path,0777,true);

 		$params = array('upload_path'=>$upload_path,'allowed_types'=>FILE_UPLOAD_ALLOWED_TYPES,'file_name'=>uniqid(),'max_size'=>FILE_UPLOAD_LIMIT,'max_width'=>0,'max_height'=>0,'do_upload'=>'myFile');
		$file = upload($params);
 
		if(is_array($file)){

			$response = array('image_pack'=>$file['file_name'],'error'=>false);
 
			$params = array('width'=>100,'height'=>100,'source_image'=>$upload_path.$file['file_name'],'new_image_path'=>$upload_path_thumbnail,'file_name'=>$file['file_name']);
			resize($params);

		}else{

			$response = array('image_pack'=>$file,'error'=>$file);

		}

		echo json_encode($response);		

	}
 

	public function add()
	{

		$error = '';
		$row = '';

		if($this->input->post()) {

			$post_data = $this->input->post();	
 

			if($this->validate_form()) {
 				
 				$data = $post_data; 
 				$data['page'] = implode(',',$this->input->post('page'));
				$data['date_added'] = array(array('field'=>'start_date','value'=>$this->input->post('start_date')),array('field'=>'end_date','value'=>$this->input->post('end_date')),array('field'=>'date_added','value'=>'NOW()'));

  				$id = $this->explore_model->insert('tbl_hidden_packs',$data); 

	   		    $post['url'] = SITE_URL . '/'.$this->uri->segment(1).'/add';
				$post['description'] = 'added a hidden marlboro pack';
				$post['table'] = 'tbl_hidden_packs';
				$post['record_id'] = $id;
				$post['type'] = 'add';
			//	$this->module_model->save_audit_trail($post);

	   		    redirect($this->uri->segment(1)); 
 
			} else {

				$post_data['image'] = '';
				$error = validation_errors();
				$row = json_decode(json_encode($post_data),false);

			}
 
		} 
		

		$data['error'] = $error;
		$data['row'] = $row;
		$data['row_photos'] =  '';
 		$data['header_text'] = 'Hidden Pack';
 		$data['action'] = SITE_URL.'/'.$this->router->class.'/add';
 		$data['main_content'] = $this->load->view('hidden_pack/form',$data,TRUE);
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
 
	} 



	public function edit()
	{

		$error = '';
		$id = $this->uri->segment(3);
		$row = $this->explore_model->get_row(array('table'=>'tbl_hidden_packs','where'=>array('hidden_pack_id'=>$id,'is_deleted'=>0)));
 
		if($this->input->post())
		{
			$post_data = $this->input->post();
var_dump($post_data);
			if($this->validate_form()){

				$photo = json_decode(json_encode($row),TRUE);

				/* Insert action history  */

				$fields = array('title','description','page','start_date','end_date','image');
				$status = array('', 'Published', 'Unpublished');

				$new_content = array();
				$old_content = array();

				foreach($photo as $k => $v) {
					if(in_array($k, $fields)) {
						if($photo[$k] != $this->input->post($k)) {

							if($k == 'status') {
								
								$new_content[$k] = $status[$this->input->post($k)];
								$old_content[$k] = $status[$photo[$k]];

							}else {
								$new_content[$k] = $this->input->post($k);
								$old_content[$k] = $photo[$k];
							}

						}
					}
				}

				$post['url'] = SITE_URL . '/'.$this->uri->segment(1).'/edit/' . $id;
				$post['description'] = 'updated a hidden marlboro pack.';
				$post['table'] = 'tbl_hidden_packs';
				$post['record_id'] = $id;
				$post['type'] = 'edit';
				$post['field_changes'] = serialize(array('old'	=> $old_content,
														 'new'	=> $new_content));
			//	$this->module_model->save_audit_trail($post);
				/*  End Insert action history */

				$post_data['page'] = implode(',',$this->input->post('page'));
				$post_data['image'] = $this->input->post('image') ? $post_data['image'] : $post_data['current_image'];
				$post_data['date_updated']	= TRUE; // Set NOW() value to column date_updated
 

 				$this->explore_model->update('tbl_hidden_packs',$post_data,array('hidden_pack_id'=>$id,'is_deleted'=>0));
				redirect($this->uri->segment(1));

			}else{

				$error = validation_errors();
 				$row = json_decode(json_encode($post_data),false);

			}


		}
 		
  
		$data['row'] = $row; 
		$data['error'] = $error;
		$data['header_text'] = 'Hidden pack';
		$data['action'] = SITE_URL.'/'.$this->router->class.'/edit/'.$id;
		$data['main_content'] = $this->load->view('hidden_pack/form',$data,TRUE);
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);

		//$this->output->enable_profiler(TRUE);

	}


	public function delete()
	{

 		$id = $this->uri->segment(3);
 		$token = $this->uri->segment(4);


if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/'.$this->uri->segment(1)) !== false && $token == md5($id . ' ' .	$this->config->item('encryption_key'))) {
 			
 			$this->explore_model->update('tbl_hidden_packs',array('is_deleted'=>1),array('hidden_pack_id'=>$id));
 			$post['url'] = @$_SERVER['HTTP_REFERER'];
			$post['description'] = 'deleted a hidden marlboro pack';
			$post['table'] = 'tbl_hidden_packs';
			$post['record_id'] = $id;
			//$this->module_model->save_audit_trail($post);

		}
		redirect($this->uri->segment(1));

	}

	 

	public function validate_form()
	{

		$this->load->library('form_validation');
		 
		$rules = array(
		   array(
				 'field'   => 'title',
				 'label'   => 'Title',
				 'rules'   => 'required'
			  ),
		   array(
				 'field'   => 'description',
				 'label'   => 'Description',
				 'rules'   => 'required'
			  ),
		   array(
				 'field'   => 'page',
				 'label'   => 'Page',
				 'rules'   => 'required'
			  ),
		   array(
				 'field'   => 'start_date',
				 'label'   => 'Start Date',
				 'rules'   => 'required'
			  ),
		   array(
				 'field'   => 'end_date',
				 'label'   => 'End Date',
				 'rules'   => 'required|callback_validate_date_range'
			  )

		); 
		 

		$this->form_validation->set_rules($rules);		
		return $this->form_validation->run();
	}


	public function validate_date_range()
	{

		
		
 		$start_date = $this->input->post('start_date');
		$end_date = $this->input->post('end_date');

		/*
		$id = $this->uri->segment(3);
 		$like = array();

		$row = $this->explore_model->get_row(array('table'=>'tbl_hidden_packs','where'=>array('hidden_pack_id'=>$id)));

		 
		$where = array("((DATE(start_date) >= '".$start_date."' AND DATE(end_date) <= '".$end_date."') OR
					    (DATE(start_date) < '".$start_date."' AND DATE(end_date) > '".$end_date."')) "=>null,'is_deleted'=>0);
		

 
		if($row){
			$where['hidden_pack_id !=']=$id;
			$like = array('page'=>implode(',',$this->input->post('page')));
		}
 
		$row = $this->explore_model->get_rows(array('table'=>'tbl_hidden_packs','where'=>$where,'like'=>$like));
		
		if($row->num_rows()){
			$this->form_validation->set_message('validate_date_range','Conflict with the other hidden pack\'s start or end date.'); 
			return false;
		}
		*/

		if(strtotime($this->input->post('start_date')) > strtotime($this->input->post('end_date'))){			
			$this->form_validation->set_message('validate_date_range','Invalid Start Date.'); 
			return false;
		}
		 

		return true;

	}
 	

 	 
	public function get_filters()
   {
	   
	   $where_filters = array('DATE(date_added) >='=>'from_date_added','DATE(date_added) <='=>'to_date_added','DATE(start_date) >='=>'start_date','DATE(end_date) <='=>'end_date');
	   $like_filters = array('title'=>'title','description'=>'description','page'=>'page');
	   $query_strings = array();
	   
	   $valid_where_filters = array();
	   $valid_like_filters = array();
	   
	   foreach($where_filters as $column_field=>$filter){
		    
				if($this->input->get($filter) || $this->input->get($filter) ==='0'){
					$valid_where_filters[$column_field] = trim($this->input->get($filter));
					$query_strings[$filter] = $this->input->get($filter);
				}
								 
			  
		   } 
	    
	   
	   foreach($like_filters as $column_field=>$filter){
		   
				if($this->input->get($filter)){
					$valid_like_filters[$column_field] = trim($this->input->get($filter));
					$query_strings[$filter] = $this->input->get($filter);
				}
				
			 
			   
	   } 

		   $query_strings = ($query_strings) ? http_build_query($query_strings) : '';
		   
	   return array('where_filters'=>$valid_where_filters,'like_filters'=>$valid_like_filters,'query_strings'=>'?'.$query_strings);		   
	   
   } 


	   public function export()
	   {

	   		$this->load->library('to_excel_array');

		   	$filters = $this->get_filters();
		   	$params = 
		   	$query = $this->explore_model->get_rows(array('fields'=>"title,description,start_date,end_date,image,date_added",
	   														'table'=>'tbl_hidden_packs',
										   					'where'=>array_merge($filters['where_filters'],array('is_deleted'=>0)),
										   					'like'=>$filters['like_filters'],
									   					)
   													);
		   	$res[] = array('Title','Description','Start Date','End Date','Image','Date Added');

		   	if($query->num_rows()){
		   		foreach($query->result() as $v){
		   			$image = BASE_URL.'uploads/hidden_packs/'.$v->image;
 		   			$res[] = array($v->title,$v->description,date('Y-m-d',strtotime($v->start_date)),date('Y-m-d',strtotime($v->end_date)),$image,$v->date_added);
		   		}
		   	}

		   	$this->to_excel_array->to_excel($res,  $this->uri->segment(1).date("Y-m-d_H-i-s"));


	   }


}