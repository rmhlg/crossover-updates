<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pack_Codes extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		ini_set('memory_limit', '-1');
		set_time_limit(0);
	}

	public function index()
	{
		$data = array(
			"title" => "Pack Codes",
			"main_content" => $this->main_content(),
			"nav" => $this->nav_items()
		);
		$this->load->view("main-template", $data);
	}

	public function add()
	{
		$data = array(
			"title" => "Add Pack Code",
			"main_content" => $this->add_content(),
			"nav" => $this->nav_items()
		);
 		$this->load->view('main-template', $data);
	}

	public function generate()
	{
		if($this->input->is_ajax_request()) {
			$codes_to_generate = $this->input->post("unit") ? (int) $this->input->post("quantity") : 1;

			$km_points = (int) $this->input->post("km_points");
			$stock = (int) $this->input->post("stock");
			$start_date = $this->input->post("start_date");
			// $end_date = $this->input->post("end_date");
			$end_date = "2015-06-30 11:59:59";
			$notes = $this->input->post("notes") ? $this->input->post("notes") : "";
			$category = $this->input->post("category");

			$this->benchmark->mark('start');

			for($i = 1; $i <= $codes_to_generate; $i++) {
				$code = $this->generate_and_validate_pack_code();
				$this->db->insert('tbl_pack_code', array('code'=>$code, 'km_points'=>$km_points, 'stock'=>$stock, 'start_date'=>$start_date, 'end_date'=>$end_date, 'notes'=>$notes, 'date_created'=>date('Y-m-d H:i:s'), 'category'=>$category));
				file_put_contents('ajax_logs.txt', "$i / $codes_to_generate");
			}
			$this->benchmark->mark('end');
			exit($this->benchmark->elapsed_time('start', 'end'));
		}		
	}

	public function generate2()
	{
		// if($this->input->is_ajax_request()) {
			$codes_to_generate = 250000;
			$disallowed	= array("M", "W", "O", "0", "D");
			$format = "abcdefghijklmnopqrstuvwxyzABCEFGHIJKLNPQRSTUVXYZ123456789";
			$format = str_replace($disallowed, "", str_shuffle($format));

			$km_points		= 100;
			$stock		= 10;
			$start_date		= "2015-04-01";
			$end_date		= "2015-05-01";
			$notes		= "Pack Code - Purchase of special packs";

			// $db_data = $this->db->select("code")->from("tbl_pack_code")->get()->result_array();
			$batch = array();

			$this->benchmark->mark('start');

			for($i = 1; $i <= $codes_to_generate; $i++) {


				$code = $this->generate_code($batch);
				array_push($batch, array("code"=>$code,"km_points"=>$km_points,"stock"=>$stock,"start_date"=>$start_date,"end_date"=>$end_date,"notes"=>$notes,"date_created"=>date("Y-m-d H:i:s")));

				// array_push($batch, array("code"=>$code));

				// $code = $this->generate_code($format);
				// if( ! $this->search_in_array($code, $db_data)) {
					// if( ! $this->search_in_array($code, $batch)) {
					// // 	$data = array("code"=>$code,"km_points"=>$km_points,"stock"=>$stock,"start_date"=>$start_date,"end_date"=>$end_date,"notes"=>$notes,"date_created"=>date("Y-m-d H:i:s"));
					// 	array_push($batch, array("code"=>$code,"km_points"=>$km_points,"stock"=>$stock,"start_date"=>$start_date,"end_date"=>$end_date,"notes"=>$notes,"date_created"=>date("Y-m-d H:i:s")));
					// } else {
					// 	$dupes = $dupes + 1;
					// }		
				// }

				if($i % 5000 == 0) {
					// echo '<pre>';
					// print_r($batch);
					// print_r($dupes);
					// exit;
					$this->db->insert_batch("tbl_pack_code", $batch);
					unset($batch);
					// unset($db_data);
					$batch = array();
					// $db_data = $this->db->select("code")->from("tbl_pack_code")->get()->result_array();
				}

				// $this->db->insert("tbl_pack_code", array("code"=>$code,"km_points"=>$km_points,"stock"=>$stock,"start_date"=>$start_date,"end_date"=>$end_date,"notes"=>$notes,"date_created"=>date("Y-m-d H:i:s")));
			}
			if($batch) {
				$this->db->insert_batch("tbl_pack_code", $batch);
				exit;
			}
			$this->benchmark->mark('end');
			exit($this->benchmark->elapsed_time('start', 'end'));

		// } else {
		// 	exit('Invalid request');
		// }		
	}

	public function generate_and_validate_pack_code()
	{
		$chars = 'abcdefghijkmnopqrstuvwxyzABCEFGHJKLNPQRSTUVXYZ123456789';

		$str = '';
		$max = strlen($chars) - 1;

		for ($i=0; $i < 8 ; $i++) {
			$str .= $chars[mt_rand(0, $max)];
		}

		$is_existing = $this->db->select()->from('tbl_pack_code')->where('code', $str)->get()->num_rows();

		if($is_existing) {
			return $this->generate_and_validate_pack_code();
		} else {
			return $str;
		}
	}

	public function delete()
	{
		if($_POST) {
			foreach($_POST['ids'] as $k => $v) {
				$this->db->where('pack_code_id', $v)->delete('tbl_pack_code');
			}
		}
	}

	public function edit($id)
	{
		$data = array( 'title'			=> "Edit User",
					   'main_content'	=> $this->edit_content($id));
 		$this->load->view('main-template', $data);
	}

	// public function delete($id, $token = ''){
	// 	$user = $this->db->get_where('tbl_cms_users', array('user_id'	=> $id))->result_array();	
	// 	if(!is_numeric($id)) {
	// 		die('Oops... Sorry, the page you are looking for is not available at the moment');
	// 	}
		
	// 	if($token == md5($this->config->item('encryption_key') . $id)) {
	// 		$this->db->where('user_id', $id);
	// 		$this->db->delete('tbl_cms_users');
	// 	}
		
	// 	$params['page'] = 'accounts';
	// 	$params['action_text'] = $this->session->userdata('user_name') . ' deleted an account: ' . $user[0]['uname'];
	// 	$this->main_model->save_audit_log($params);
		
 // 		redirect('accounts');
	// }
	
	public function my_account()
	{
		$data = array( 'title'			=> "Edit User",
					   'main_content'	=> $this->my_account_content(),
					   'nav'			=> $this->nav_items());
 		$this->load->view('main-template', $data);	
	}

	public function add_category()
	{
		$this->_add_category();
	}

	public function export()
	{
		$this->_export();
	}
	
	private function nav_items() {
		return $this->load->view('navigations', '', true);		
	}


	/******************************************************/

	public function main_content()
	{
		$page = $this->uri->segment(2, 1);
		$limit = isset($_GET['limit']) ? $_GET['limit'] : 15;
		$data['offset'] = ($page - 1) * $limit;

		$this->db->start_cache();
		$this->db->select()->from('tbl_pack_code');
		if(isset($_GET['code']) && $_GET['code']) {
			$this->db->like('code', $_GET['code']);
		}
		if(isset($_GET['stock']) && $_GET['stock']) {
			$this->db->where('stock', (int)$_GET['stock']);
		}
		if(isset($_GET['km_points']) && $_GET['km_points']) {
			$this->db->where('km_points', (int)$_GET['km_points']);
		}
		if(isset($_GET['start_date']) && $_GET['start_date']) {
			$this->db->where('DATE(start_date) >=', $_GET['start_date']);
		}
		if(isset($_GET['end_date']) && $_GET['end_date']) {
			$this->db->where('DATE(end_date) <=', $_GET['end_date']);
		}
		$this->db->order_by('pack_code_id', 'DESC');
		$data['count'] = $this->db->count_all_results();

		$this->db->limit($limit, $data['offset']);
		$data['items'] = $this->db->get()->result_array();
		$this->db->stop_cache();
		$this->db->flush_cache();
		$data['pagination'] = $this->global_model->pagination($data['count'], $page, SITE_URL.'/pack_codes', $limit);

		$content = $this->load->view('pack_codes/index', $data, TRUE);
		return $content;
	}

	public function add_content()
	{
		$data["title"] = "Add Pack Code";
		$data["action"] = SITE_URL."pack_codes/generate";

		$content = $this->load->view('pack_codes/add', $data, TRUE);
		return $content;
	}

	public function edit_content($id)
	{
		$data['title'] = 'Edit User';
		$user = $this->db->get_where('tbl_cms_users', array('user_id'	=> $id))->result_array();
		$data['user']  = @$user[0];
		
		if(isset($_POST['submit'])) {
			$params['page'] = 'accounts';
			$params['action_text'] = $this->session->userdata('user_name') . ' updated ' . $data['user']['account_name'] . '\'s account';
			$this->main_model->save_audit_log($params);
			
			$this->save_account(true);
		}
		
		$main = $this->load->view('accounts/add', $data, TRUE);
		return $main;
	}

	public function _add_category()
	{
		if($_POST) {
			$name = $this->input->post('name');
			$this->db->insert('tbl_pack_code_category', array('name'=>$name));
			redirect('pack_codes/add');
		}
	}

	public function _export()
	{
		$this->load->library('to_excel_array');

		// $this->db->start_cache();
		// $this->db->select()->from('tbl_pack_code');
		// if(isset($_GET['code']) && $_GET['code']) {
		// $this->db->like('code', $_GET['code']);
		// }
		// if(isset($_GET['stock']) && $_GET['stock']) {
		// $this->db->where('stock', (int)$_GET['stock']);
		// }
		// if(isset($_GET['km_points']) && $_GET['km_points']) {
		// $this->db->where('km_points', (int)$_GET['km_points']);
		// }
		// if(isset($_GET['start_date']) && $_GET['start_date']) {
		// $this->db->where('DATE(start_date) >=', $_GET['start_date']);
		// }
		// if(isset($_GET['end_date']) && $_GET['end_date']) {
		// $this->db->where('DATE(end_date) <=', $_GET['end_date']);
		// }
		// $this->db->order_by('pack_code_id', 'DESC');
		// $export_items = $this->db->get()->result_array();
		// $this->db->stop_cache();
		// $this->db->flush_cache();

		$row[] = array(
			'#',
			'Code',
			'Points',
			'Max Usage',
			'Valid Start Date',
			'Valid End Date'
		);
		$export_items = $this->db->select()->from('tbl_pack_code')
			->where('notes', 'For Family Mart')
			->get()
			->result_array();

		if($export_items) {
			$i = 0;
			foreach($export_items as $k => $v) {
				$i++;
				$row[] = array(
					$i,
					$v['code'],
					$v['km_points'],
					$v['stock'],
					$v['start_date'],
					$v['end_date']
				);
			}
		}
		$this->to_excel_array->to_excel($row, 'pack_codes_'.date('YmdHis'));
	}

	public function _remap($params)
	{
		if(is_numeric($params)) {
			$this->index();
		} else {
			$this->{$params}();
		}
	}
}

?>
