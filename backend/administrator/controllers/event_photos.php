<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

class Event_photos extends CI_Controller {
	

	public function __construct() {
		parent::__construct();
		$this->load->model('explore_model');
 	} 

 	 
 	public function index()
	{
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
  	}
 	 
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);

	}


	public function main_content()
	{
		$this->load->helper('paging');

		$about_event_id = $this->input->get('about_event_id');
		$offset = $this->input->get('per_page') ? (int)$this->input->get('per_page') : (int)$this->session->userdata('offset');
	    $limit = 10;
	    $filters = $this->get_filters();

		$access = $this->module_model->check_access('event_photos');
		$total_rows = $this->explore_model->get_rows(array('table'=>'tbl_about_events_photos as p',
																 'where'=>array_merge($filters['where_filters'],array('p.is_deleted'=>0)),
															     'like'=>$filters['like_filters'],
															     'join'=>array(array('table'=>'tbl_registrants as r','on'=>'r.registrant_id=p.uploader_id','left'),
															     				array('table'=>'tbl_cms_users as c','on'=>'c.cms_user_id=p.uploader_id','left')
															     			),
															     'fields'=>'p.photo_id'
			 													)
														    )->num_rows();

		if($offset)
			$this->session->set_userdata('offset',$offset);
		else
			$this->session->unset_userdata(array('offset'=>''));

		$data['about_event_id'] = $about_event_id;
 		$data['header_text'] = 'Events';
		$data['edit'] = $access['edit'];
		$data['delete'] = $access['delete'];
		$data['add'] = $access['add'];
		$data['total_rows'] = $total_rows;
 
 		$data['events'] = $this->explore_model->get_rows(array('table'=>'tbl_about_events',
															 'where'=>array('is_deleted'=>0)
 														     )
														);
		$data['rows'] = $this->explore_model->get_rows(array('table'=>'tbl_about_events_photos as p',
													 'where'=>array_merge($filters['where_filters'],array('p.is_deleted'=>0)),
												     'like'=>$filters['like_filters'],
												     'join'=>array('table'=>'tbl_about_events as e','on'=>'e.about_event_id=p.about_event_id'),
												     'order_by'=>array('field'=>'p.date_added','order'=>'desc'),
												     'limit'=>$limit,
												     'offset'=>$offset,
												     'order_by'=>array('field'=>'p.date_added','order'=>'DESC'),
												     'fields'=>'p.photo_id,p.uploader_id,p.uploader_name,p.about_event_id,p.image,p.user_type,p.status,p.winner_status,p.date_added,e.title as event_title'
 													)
												);
		$data['query_strings'] = $filters['query_strings'];  
 		$data['pagination'] = paging($total_rows,$limit,$offset,SITE_URL.'/'.$this->uri->segment(1).$filters['query_strings']);
 		return $this->load->view('about/events/photos_list_moderator',$data,TRUE);


	}


	public function approve()
	{
		$photo_id = $this->input->get('photo_id');
		$about_event_id = $this->input->get('about_event_id');
		$token = $this->input->get('token');

		if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/'.$this->uri->segment(1)) !== false && $token == md5($photo_id . ' ' .	$this->config->item('encryption_key'))) {

			$entry = $this->explore_model->get_row(array('table'=>'tbl_about_events_photos','where'=>array('photo_id'=>$photo_id, 'user_type' => 'registrant')));
			if ($entry) {
				$event = $this->explore_model->get_row(array('table'=>'tbl_about_events','where'=>array('about_event_id'=>$about_event_id)));
				if ($event) {
					require_once 'application/models/points_model.php';
					$points_model = new Points_Model();
					if (!$entry->status) {
						$points_model->earn(ABOUT_PHOTOS, array(
							'suborigin' => $entry->photo_id,
							'registrant' => $entry->uploader_id,
							'remarks' => '{{ name }} uploaded a photo on '.$event->title
						));
					} elseif ($entry->status == 2) {
						$points_model->activate($entry->uploader_id, array(
							array(
								'suborigin' => $entry->photo_id,
								'origin' => ABOUT_PHOTOS
							)
						));
					}
				}
			}

 			$this->explore_model->update('tbl_about_events_photos',array('status'=>1),array('photo_id'=>$photo_id));
 			$post['url'] = @$_SERVER['HTTP_REFERER'];
			$post['description'] = 'approved an event photo';
			$post['table'] = 'tbl_about_events_photos';
			$post['record_id'] = $photo_id;
			$this->module_model->save_audit_trail($post);
  		}

		redirect($this->uri->segment(1).'?per_page='.$this->input->get('per_page'));
	}

	public function disapprove()
	{
		$photo_id = $this->input->get('photo_id');
		$about_event_id = $this->input->get('about_event_id');
		$token = $this->input->get('token');

		if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/'.$this->uri->segment(1)) !== false && $token == md5($photo_id . ' ' .	$this->config->item('encryption_key'))) {
 			
 			$entry = $this->explore_model->get_row(array('table'=>'tbl_about_events_photos','where'=>array('photo_id'=>$photo_id, 'user_type' => 'registrant')));
			if ($entry) {
				$event = $this->explore_model->get_row(array('table'=>'tbl_about_events','where'=>array('about_event_id'=>$about_event_id)));
				if ($event) {
					require_once 'application/models/points_model.php';
					$points_model = new Points_Model();
					if (!$entry->status) {
						$points_model->earn(ABOUT_PHOTOS, array(
							'suborigin' => $entry->photo_id,
							'registrant' => $entry->uploader_id,
							'remarks' => '{{ name }} uploaded a photo on '.$event->title
						));
					} elseif ($entry->status == 1) {
						$points_model->deactivate($entry->uploader_id, array(
							array(
								'suborigin' => $entry->photo_id,
								'origin' => ABOUT_PHOTOS
							)
						));
					}
				}
			}

			$this->explore_model->update('tbl_about_events_photos',array('status'=>2),array('photo_id'=>$photo_id));
			$post['url'] = @$_SERVER['HTTP_REFERER'];
			$post['description'] = 'disapproved an event photo';
			$post['table'] = 'tbl_about_events_photos';
			$post['record_id'] = $photo_id;

		}

		redirect($this->uri->segment(1).'?per_page='.$this->input->get('per_page'));
	}

 
	public function add()
	{

		$id = (int)$this->uri->segment(3);

		$error = '';
 
		if($this->input->post()) {

			$post_data = $this->input->post();		

			if($this->validate_form()) {
 
				 
				$photos = $this->do_upload();
				$temp = 1;

				if($photos){
					foreach($photos as $v){
						$this->explore_model->insert('tbl_about_events_photos',$v);
						$temp++;
					}
				}				   
				

	   		    $post['url'] = SITE_URL . '/'.$this->uri->segment(1).'/add';
				$post['description'] = ($temp > 1) ? 'added a new photos' : 'added a new photo';
				$post['table'] = 'tbl_about_events';
				$post['record_id'] = $id;
				$post['type'] = 'add';
				$this->module_model->save_audit_trail($post);

	   		    redirect($this->uri->segment(1).'?about_event_id='.$this->uri->segment(3)); 

			} else {

				$post_data['image'] = '';
				$error = validation_errors();
				$row = json_decode(json_encode($post_data),false);

			}
 
		} 
		

		$data['error'] = $error;
		$data['event'] = $this->explore_model->get_row(array('table'=>'tbl_about_events','where'=>array('about_event_id'=>$id),'fields'=>'title,about_event_id'));
		$data['row_photos'] =  '';
 		$data['header_text'] = 'Event';
 		$data['about_event_id'] = $id;

 		$data['main_content'] = $this->load->view('about/events/photos_form',$data,TRUE);
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
 
	} 



	 


	public function do_upload()
	{

		   $this->load->helper(array('resize','upload'));

		   $max = (int)$this->input->post('image_counter');
  
		   

		   $i = 1;
		   $temp = 1;
		   $thumb = '_';		   
		   $id = (int)$this->explore_model->get_last_id('tbl_about_events_photos','photo_id') + 1;	   

		   $images = array();
		   $about_event_id = $this->uri->segment(3);
 		   $user = $this->login_model->extract_user_details();
		   

  		   while($i<=$max){
			   

			   if(isset($_FILES["slider-image".$i]["error"]) && $_FILES["slider-image".$i]["error"] < 1){ 	   
 

 				   $params = array('upload_path'=>'uploads/about/photos/',
 				   					'allowed_types'=>'png|jpg|gif',
 				   					'file_name'=>'',
 				   					'max_size'=>'2097152',
 				   					'max_width'=>0,
 				   					'max_height'=>0,
 				   					'do_upload'=>'slider-image'.$i);
				   $file = upload($params);    

				   if(is_array($file)){


					   $new_filename = strtolower($id.$thumb.$i.$file['file_ext']);	/// New filename			   
					   while(file_exists('uploads/about/photos/'.$new_filename)){ //Check file exists
					 	  $temp++;
					 	  $new_filename = strtolower($id.$thumb.$temp.$file['file_ext']);
					    }		   

					   rename('uploads/about/photos/'.$file['file_name'],'uploads/about/photos/'.$new_filename); // Rename file



 				   	  $params = array('width'=>100,'height'=>100,'source_image'=>'uploads/about/photos/'.$new_filename,'new_image_path'=>'uploads/about/photos/thumbnails/','file_name'=>$new_filename);
					  resize($params);	

					  $params = array('width'=>331,'height'=>176,'source_image'=>'uploads/about/photos/'.$new_filename,'new_image_path'=>'uploads/about/photos/thumbnails/','file_name'=>$new_filename);
					  resize($params);

					  $data[] = array('about_event_id'=>$about_event_id,
				   					'submitted_by'=>$user['cms_user_id'],
				   					'user_type'=>'administrator',
 				   					'is_deleted'=>0,
				   					'status'=>0,
				   					'image'=>$new_filename,
				   					'date_added'=>TRUE);
 				   }

			   }

			   $i++;			

		   }

		   return $data;


 	   } 


	public function delete()
	{

 		$id = $this->uri->segment(3);
 		$token = $this->uri->segment(4);

				if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . 'admin/'.$this->uri->segment(1)) !== false && $token == md5($id . ' ' .	$this->config->item('encryption_key'))) {
if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . 'admin/'.$this->uri->segment(1)) !== false && $token == md5($id . ' ' .	$this->config->item('encryption_key'))) {
 			$this->explore_model->update('tbl_about_events',array('is_deleted'=>1),array('about_event_id'=>$id));

 			$post['url'] = @$_SERVER['HTTP_REFERER'];
			$post['description'] = 'deleted a photo - About';
			$post['table'] = 'tbl_about_events';
			$post['record_id'] = $id;
			$this->module_model->save_audit_trail($post);

		}
		redirect($this->uri->segment(1));

	}

	public function validate_form()
	{

		$this->load->library('form_validation');

		$rules = array();

		$this->form_validation->set_rules($rules);
		
		//return $this->form_validation->run();
		return true;
	}



	

	public function get_filters()
	   {
		   
		   $where_filters = array('p.about_event_id'=>'about_event_id','p.status'=>'status','p.winner_status'=>'winner_status','DATE(p.date_added) >='=>'from_date_added','DATE(p.date_added) <='=>'to_date_added');
		   $like_filters = array('p.uploader_name'=>'submitted_by');
		   $query_strings = array();
		   
		   $valid_where_filters = array();
		   $valid_like_filters = array();
		   
		   foreach($where_filters as $column_field=>$filter){
			    
 				if($this->input->get($filter) || $this->input->get($filter) ==='0'){
 					$valid_where_filters[$column_field] = trim($this->input->get($filter));
 					$query_strings[$filter] = $this->input->get($filter);
 				}
									 
				  
 		   } 
		    
		   
		   foreach($like_filters as $column_field=>$filter){
			   
 				if($this->input->get($filter)){
 					$valid_like_filters[$column_field] = trim($this->input->get($filter));
 					$query_strings[$filter] = $this->input->get($filter);
 				}
					
				 
				   
		   } 

 		   $query_strings = ($query_strings) ? http_build_query($query_strings) : '';
 		   
		   return array('where_filters'=>$valid_where_filters,'like_filters'=>$valid_like_filters,'query_strings'=>'?default=0'.$query_strings);		   
		   
	   } 


	   public function export()
	   {

		   	$filters = $this->get_filters();
		   	$params = 
		   	$data['query'] = $this->explore_model->get_rows(array('fields'=>"a.title,a.description,b.category_name,date_added",
		   														'table'=>'tbl_about_events as a',
											   					'where'=>$filters['where_filters'],
											   					'like'=>$filters['like_filters'],
											   					'join'=>array('table'=>'tbl_categories as b',
											   								'on'=>'a.category_id = b.category_id')
											   					)
		   													);
		   	$data['filename'] = $this->uri->segment(1).'_'.date('M-d-Y');
		    $this->load->view($this->uri->segment(1).'/export-to-excell',$data);
	   }

	   public function view_photo()
	   {
	   	 $id = $this->uri->segment(3);
	   	 $data['row'] = $this->explore_model->get_rows(array('table'=>'tbl_about_events_photos as p',
													  'where'=>array('p.photo_id'=>$id),
													  'join'=>array('table'=>'tbl_about_events as e','on'=>'e.about_event_id=p.about_event_id'),
													  'fields'=>'e.title,p.image,p.uploader_name'
														)
												)->row();
	   	 $this->load->view('about/events/view-photo',$data);

	   }

 
}