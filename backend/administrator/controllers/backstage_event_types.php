<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

class Backstage_event_types extends CI_Controller {
	

	public function __construct() {
		parent::__construct();
		$this->load->model('explore_model');
 	} 

 	 
 	public function index()
	{
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);

 	}
 	 
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('nav', $data, true);		
	}


	public function main_content()
	{
		$this->load->helper('paging');
		$offset = (int)$this->input->get('per_page');
	    $limit = PER_PAGE;
	    $filters = $this->get_filters(); 

		$access = $this->module_model->check_access('regions');
		$total_rows = $this->explore_model->get_rows(array('table'=>'tbl_backstage_event_types',
																 'where'=>array_merge($filters['where_filters'],array('is_deleted'=>0)),
																 'like'=>$filters['like_filters']																 
																 )
														    )->num_rows();
 		$data['header_text'] = 'Event Types';
		$data['edit'] = $access['edit'];
		$data['delete'] = $access['delete'];
		$data['add'] = $access['add'];
		$data['total_rows'] = $total_rows;
		$data['query_strings'] = $filters['query_strings'];
 		
		$data['rows'] = $this->explore_model->get_rows(array('table'=>'tbl_backstage_event_types ',
															 'where'=>array_merge($filters['where_filters'],array('is_deleted'=>0)),
														     'like'=>$filters['like_filters'],
														     'order_by' => array('field'=>'date_added','order'=>'DESC'),
														     'offset'=>$offset,
														     'limit'=>$limit
 														     )
														);
  
 		$data['pagination'] = paging($total_rows,$limit,$offset,SITE_URL.'/'.$this->uri->segment(1).$filters['query_strings']);
 		return $this->load->view('backstage/event_types/list',$data,TRUE);

	}
 


	public function add()
	{

		$error = '';
		$row = '';

		if($this->input->post()) {

			$post_data = $this->input->post();	
 

			if($this->validate_form()) {
 
				
 				$data = $post_data; 
				$data['date_added'] = TRUE; // Set NOW() value to date_added column field
  				$id = $this->explore_model->insert('tbl_backstage_event_types',$data); 

	   		    $post['url'] = SITE_URL . '/'.$this->uri->segment(1).'/add';
				$post['description'] = 'added a new event type for backstage pass';
				$post['table'] = 'tbl_backstage_event_types';
				$post['record_id'] = $id;
				$post['type'] = 'add';
				$this->module_model->save_audit_trail($post);

	   		    redirect($this->uri->segment(1)); 

			} else {

				$post_data['image'] = '';
				$error = validation_errors();
				$row = json_decode(json_encode($post_data),false);

			}
 
		} 
		

		$data['error'] = $error;
		$data['row'] = $row;
		$data['row_photos'] =  '';
 		$data['header_text'] = 'Event Type'; 

 		$data['main_content'] = $this->load->view('backstage/event_types/form',$data,TRUE);
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
 
	} 



	public function edit()
	{

		$error = '';
		$id = $this->uri->segment(3);
		$row = $this->explore_model->get_row(array('table'=>'tbl_backstage_event_types','where'=>array('event_type_id'=>$id,'is_deleted'=>0)));

		if($this->input->post())
		{
			$post_data = $this->input->post();

			if($this->validate_form()){

				$photo = json_decode(json_encode($row),TRUE);


				/* Insert action history */

				$fields = array('name','status');
				$status = array('', 'Published', 'Unpublished');

				$new_content = array();
				$old_content = array();

				foreach($photo as $k => $v) {
					if(in_array($k, $fields)) {
						if($photo[$k] != $this->input->post($k)) {

							if($k == 'status') {
								
								$new_content[$k] = $status[$this->input->post($k)];
								$old_content[$k] = $status[$photo[$k]];

							}else {
								$new_content[$k] = $this->input->post($k);
								$old_content[$k] = $photo[$k];
							}

						}
					}
				}

				$post['url'] = SITE_URL . '/'.$this->uri->segment(1).'/edit/' . $id;
				$post['description'] = 'updated an event type for backstage pass';
				$post['table'] = 'tbl_backstage_event_types';
				$post['record_id'] = $id;
				$post['type'] = 'edit';
				$post['field_changes'] = serialize(array('old'	=> $old_content,
														 'new'	=> $new_content));
				$this->module_model->save_audit_trail($post);

				/* End Insert action history */



				$post_data['date_updated']	= TRUE; // Set NOW() value to column date_updated
 				$this->explore_model->update('tbl_backstage_event_types',$post_data,array('event_type_id'=>$id,'is_deleted'=>0));
				redirect($this->uri->segment(1));

			}else{

				$error = validation_errors();
 				$row = json_decode(json_encode($post_data),false);

			}


		}
 		
  
		$data['row'] = $row; 
		$data['error'] = $error;
		$data['header_text'] = 'Region';
		$data['main_content'] = $this->load->view('backstage/event_types/form',$data,TRUE);
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);

		//$this->output->enable_profiler(TRUE);

	}


	public function delete()
	{

 		$id = $this->uri->segment(3);
 		$token = $this->uri->segment(4);

				if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . 'admin/'.$this->uri->segment(1)) !== false && $token == md5($id . ' ' .	$this->config->item('encryption_key'))) {
if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . 'admin/'.$this->uri->segment(1)) !== false && $token == md5($id . ' ' .	$this->config->item('encryption_key'))) {
 			
 			$this->explore_model->update('tbl_backstage_event_types',array('is_deleted'=>1),array('event_type_id'=>$id));

 			$post['url'] = @$_SERVER['HTTP_REFERER'];
			$post['description'] = 'deleted an event type for backstage pass';
			$post['table'] = 'tbl_backstage_event_types';
			$post['record_id'] = $id;
			$this->module_model->save_audit_trail($post);

		}
		redirect($this->uri->segment(1));

	}

	 

	public function validate_form()
	{

		$this->load->library('form_validation');

		$rules = array(
		   array(
				 'field'   => 'name',
				 'label'   => 'Type',
				 'rules'   => 'required'
			  )
		);

		$this->form_validation->set_rules($rules);
		
		return $this->form_validation->run();
	}

	 
 

	public function get_filters()
	   {
		   
		   $where_filters = array('DATE(date_added) >='=>'from_date_added','DATE(date_added) <='=>'to_date_added');
		   $like_filters = array('name'=>'name');
		   $query_strings = array();
		   
		   $valid_where_filters = array();
		   $valid_like_filters = array();
		   
		   foreach($where_filters as $column_field=>$filter){
			    
 				if($this->input->get($filter) || $this->input->get($filter) ==='0'){
 					$valid_where_filters[$column_field] = trim($this->input->get($filter));
 					$query_strings[$filter] = $this->input->get($filter);
 				}
									 
				  
 		   } 
		    
		   
		   foreach($like_filters as $column_field=>$filter){
			   
 				if($this->input->get($filter)){
 					$valid_like_filters[$column_field] = trim($this->input->get($filter));
 					$query_strings[$filter] = $this->input->get($filter);
 				}
					
				 
				   
		   } 

 		   $query_strings = ($query_strings) ? http_build_query($query_strings) : '';
 		   
		   return array('where_filters'=>$valid_where_filters,'like_filters'=>$valid_like_filters,'query_strings'=>'?'.$query_strings);		   
		   
	   } 


	   public function export()
	   {
	   		$this->load->library('to_excel_array');

		   	$filters = $this->get_filters();
		   	$params = 
		   	$query = $this->explore_model->get_rows(array('fields'=>"name,date_added",
		   														'table'=>'tbl_backstage_event_types',
											   					'where'=>$filters['where_filters'],
											   					'like'=>$filters['like_filters'],
 											   					)
		   													);
		   	$res[] = array('Event Types','Date Added');

		   	if($query->num_rows()){
		   		foreach($query->result() as $v){
 		   			$res[] = array($v->name,$v->date_added);
		   		}
		   	}
		   	$this->to_excel_array->to_excel($res,  $this->uri->segment(1).date("Y-m-d_H-i-s"));

	   }


	   

 
}