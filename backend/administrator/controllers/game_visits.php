<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Game_visits extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('report_model');
	}
	
	public function index()
	{
		if(!$this->input->get('fromdate') || !$this->input->get('todate')) {
			redirect('game_visits?fromdate=' . date('Y-m-d', strtotime('-30 days')) .  '&todate=' . date('Y-m-d'));
		}
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
	}

	public function export()
	{
		$this->load->library('to_excel_array');
		$records = $this->report_model->get_page_demographics('games', $from, $to)->result_array();
		$row[] = $from && $to ? array('Game Visits (' . date('F d, Y', strtotime($from)) . ' - ' . date('F d, Y', strtotime($to)) . ')') : array('Game Visits');

		$row[] = array('Day', 'Visits', 'Visitors');
		if($records) {
			foreach($records as $k => $v) {
				$row[] = array(date('F d, Y', strtotime($v['dv'])),
							   $v['count'],
							   $v['visitors']);
			}
		}
		$this->to_excel_array->to_excel($row, 'game_visits_'.date("YmdHis"));
	}
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('navigations', $data, true);		
	}
	
	private function main_content() {
		$data['records'] = $this->report_model->get_page_demographics('games')->result_array();
		return $this->load->view('visits/game', $data, true);		
	}

	public function _remap($method) {
		if($method == 'edit')
			$this->edit($this->uri->segment(3));
		elseif($method == 'delete')
			$this->delete($this->uri->segment(3));
		elseif($method == 'add')
			$this->add();
		elseif($method == 'export')
			$this->export();
		else
			$this->index();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */