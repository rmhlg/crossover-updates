<?php 

class Lamp_permissions extends CI_Controller {
	
	var $_table;
	public function __construct() {
		parent::__construct();
		$this->_table = 'tbl_lamp_permissions';
 	}
	
	public function index($page = 1)
	{
		//$this->output->enable_profiler(TRUE);
		$data['main_content'] = $this->main_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);
	}
	
	private function nav_items() {
		$data = $this->module_model->get_nav_data();
		return $this->load->view('navigations', $data, true);		
	}
	
	private function main_content() {
		$page = $this->uri->segment(2, 1);
		$like = array();
		$data['offset'] = ($page - 1) * PER_PAGE;
		if(isset($_GET['search'])) {
			$like = $this->input->get();
			unset($like['search']);
		}
		$param['offset'] = $data['offset'];
		$param['limit'] = PER_PAGE;
		$param['table'] = $this->_table;
$param['join'] = array(array('table'=>'tbl_cms_user','on'=>'tbl_lamp_permissions.cms_user_id = tbl_cms_user.user_id'),
			     		array('table'=>'tbl_lamps','on'=>'tbl_lamps.lamp_id = tbl_lamp_permissions.lamp_id')
					     		);

		$param['order_by'] = array('field'=>'tbl_lamp_permissions.created_date','order'=>'DESC');
		//$param['where'] = array('tbl_lamp_permissions.is_deleted' => 0);
		//$param['having'] = array('challenges LIKE ' => '%' . $challenge . '%', 'is_deleted' => 0);
		$param['like'] = $like;
		$param['fields'] ='*';
		$data['categories'] = $this->global_model->get_rows($param)->result_array();
	
		if(isset($_GET['search'])) { 
			//count all data in search without limit parameters jeromejose 03-11-2015
		$param123['table'] = $this->_table;
		$param123['join'] = array(array('table'=>'tbl_cms_user','on'=>'tbl_lamp_permissions.cms_user_id = tbl_cms_user.user_id'),
			     		array('table'=>'tbl_lamps','on'=>'tbl_lamps.lamp_id = tbl_lamp_permissions.lamp_id')
					     		);
		$param123['order_by'] = array('field'=>'tbl_lamp_permissions.created_date','order'=>'DESC');
		$param123['like'] = $like;
		$param123['fields'] ='*';
			$count = $this->global_model->get_rows($param123)->result_array();
				$records = count($count);
		}else
		{
				$records = $this->global_model->get_total_rows($param);
		}

	

		$data['pagination'] = $this->global_model->pagination($records, $page , SITE_URL . '/lamp_permissions');
		//$access = $this->module_model->check_access('lamp_permissions');
		$data['edit'] = 1;//$access['edit'];
		$data['delete'] = 1;//= $access['delete'];
		$data['add'] = 1;//$access['add'];
		$data['total'] = $records;
		return $this->load->view('perks/reserve/lamps/permissions/index', $data, true);		
	}

	public function export() {
		$like = array();
		if(isset($_GET['search'])) {
			$like = $this->input->get();
			unset($like['search']);
		}

		$param123['table'] = $this->_table;
		$param123['join'] = array(array('table'=>'tbl_cms_user','on'=>'tbl_lamp_permissions.cms_user_id = tbl_cms_user.user_id'),
			     		array('table'=>'tbl_lamps','on'=>'tbl_lamps.lamp_id = tbl_lamp_permissions.lamp_id')
					     		);
		$param123['order_by'] = array('field'=>'tbl_lamp_permissions.created_date','order'=>'DESC');
		$param123['like'] = $like;
		$param123['fields'] ='*';
			$records = $this->global_model->get_rows($param123)->result_array();

		$row[] = array(	'Name',
						'LAMP');
		if($records) {
				foreach($records as $k => $v) {
					$row[] = array($v['name'], $v['lamp_name']);
				}
			}		
		$this->load->library('to_excel_array');
		$this->to_excel_array->to_excel($row, 'lamps_'.date("YmdHis"));
	}
	
	public function add() {
		$data['main_content'] = $this->add_content();
		$data['nav'] = $this->nav_items();
		$this->load->view('main-template', $data);	
	}
	
	private function add_content() {
		$error = '';
		if($this->input->post('submit')) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules($this->get_rules());
			$valid = $this->form_validation->run();	
			if($valid) {
				$post = $this->input->post();
				if(!$error) {
					$id = $this->global_model->insert($this->_table, $post);
					$post = array();
					$post['url'] = SITE_URL . '/move_fwd/add';
					$post['description'] = 'added a new move fwd item';
					$post['table'] = 'tbl_move_forward';
					$post['record_id'] = $id;
					$post['type'] = 'add';
					$this->module_model->save_audit_trail($post);
					redirect('lamp_permissions');	
				}
			} else {
				$error = validation_errors();
			}
		}
		$data['error'] = $error;
		$data['record'] = $_POST;
		$data['lamps'] = $this->get_lamps();
		$data['users'] = $this->get_user();
		return $this->load->view('perks/reserve/lamps/permissions/add', $data, true);			
	}

	public function get_avail_users() {
		$lamp_id = $this->input->post('id');
		$query = "SELECT * FROM tbl_cms_users 
				  WHERE cms_user_id NOT IN (SELECT cms_user_id FROM tbl_lamp_permissions WHERE lamp_id = $lamp_id)
				  ORDER BY name";
		$users = $this->global_model->custom_query($query)->result_array();
		$data['users'] = $users;
		$this->load->view('perks/reserve/lamps/permissions/user', $data);	

	}

	public function get_lamps() {
		$param['table'] = 'tbl_lamps';
		$param['order_by'] = array('field'	=> 'lamp_name', 'order'	=> 'ASC');
		$param['where'] = array('status'	=> 1, 'is_deleted'	=> 0);
		$lamps = $this->global_model->get_rows($param)->result_array();
		return $lamps;
	}
	public function get_user() {
		$param['table'] = 'tbl_cms_user';
		$param['order_by'] = array('field'	=> 'user_id', 'order'	=> 'ASC');
		$lamps = $this->global_model->get_rows($param)->result_array();
		return $lamps;
	}

	public function delete() {
		$table = $this->_table;
		$id = $this->uri->segment(3);
		$field = 'lamp_permission_id';
		$token = $this->uri->segment(4);
		if(strrpos(@$_SERVER['HTTP_REFERER'], SITE_URL . '/lamp_permissions') !== false && $token == md5($id . ' ' .	$this->config->item('encryption_key'))) {
			$where[$field] = $id;
			$this->global_model->delete($table, $where);
		}
		redirect($_SERVER['HTTP_REFERER']);
	}
	
	private function get_rules() {
		$config = array(
		   array(
				 'field'   => 'cms_user_id',
				 'label'   => 'user',
				 'rules'   => 'required'
			  ),
		   array(
				 'field'   => 'lamp_id',
				 'label'   => 'LAMP',
				 'rules'   => 'required'
			  )
		);
		return $config;
	}

	
	
	public function _remap($method) {
		if($method == 'edit')
			$this->edit($this->uri->segment(3));
		elseif($method == 'delete')
			$this->delete($this->uri->segment(3));
		elseif($method == 'add')
			$this->add();
		elseif($method == 'export')
			$this->export();
		elseif(is_numeric($method))
			$this->index();
		else
			$this->{$method}();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */