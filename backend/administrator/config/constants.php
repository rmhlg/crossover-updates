<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

define('PROD_MODE', $_SERVER['SERVER_NAME'] == 'marlboro.ph' || $_SERVER['SERVER_NAME'] == 'www.marlboro.ph');
define('DEV_MODE', !in_array($_SERVER['SERVER_NAME'], array('marlboro-stage2.yellowhub.com')) && !PROD_MODE);
define('FILE_UPLOAD_ALLOWED_TYPES', 'png|gif|jpg|jpeg');
define('SPICE_FILE_UPLOAD_ALLOWED_TYPES', 'png|bmp|jpg|jpeg|pdf');
define('NUWORKS_IP', '124.107.151.158');
define('HTML_ESCAPE_CONFIG', ENT_NOQUOTES);
/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');
define('PER_PAGE', 10);

// define('SITE_URL' ,'http://localhost:81/webdevelopment/marlboro/marlboro-crossover/');
// define('BASE_URL' ,'http://localhost:81/webdevelopment/marlboro/marlboro-crossover/');

/*
|--------------------------------------------------------------------------
| Site Configuration
|--------------------------------------------------------------------------
|
*/
define('TRACKING_CODE_EXPIRATION',              '+7 day');
define('FORGOT_TOKEN_EXPIRATION',               3600); // 1 hour
define('DEFAULT_IMAGE',                         'images/no_image.png');
define('GIID_FILE_TYPES',                       FILE_UPLOAD_ALLOWED_TYPES.'|pdf');
define('VIEW_LIMIT_PER_DAY',                    1);
define('SESSION_NAMESPACE',                     'marlboro_front_sess');
define('LOGIN_ATTEMPTS',                        7);
define('LOGIN_RETURN',                          '+3 day');
define('FILE_UPLOAD_LIMIT',                     '3072');
define('VIDEO_UPLOAD_LIMIT',                    '307200');
define('MINIMUM_AGE',                           18);
define('CAPTCHA_FONT',                          './fonts/captcha_font.ttf');
define('ERROR_MESSAGE',                         'An Error Occured');
define('LAUNCH_DATE',                           '2014-04-02');

/*
|--------------------------------------------------------------------------
| ORIGIN IDS
|--------------------------------------------------------------------------
|
*/
define('MOVE_FWD',                              1);
define('EXPLORE',                               2);
define('BIRTHDAY_OFFERS',                       3);
define('FLASH_OFFERS',                          4);
define('WEBGAMES',                              5);
define('COMMENT',                               6);
define('PERKS_BUY',                             7);
define('PERKS_BID',                             8);
define('PERKS_RESERVE',                         9);
define('HIDDEN_MARLBORO',                       10);
define('USER_STATEMENT',                        11);
define('REGISTRATION',                          12);
define('LOGIN',                                 13);
define('ABOUT_NEWS',                            14);
define('ABOUT_VIDEOS',                          15);
define('ABOUT_PHOTOS',                          24);
define('COMMENT_REPLY',                         17);
define('MOVE_FWD_GALLERY',                      18);
define('MOVE_FWD_COMPLETE',                     19);
define('MOVE_FWD_PLEDGE_ENTRY',                 20);
define('COMMENT_RECEIVE',                       21);
define('MOVE_FWD_WINNER',                       22);
define('COMMENT_REPLY_RECEIVE',                 23);
define('BACKSTAGE_PHOTOS',                      ABOUT_PHOTOS);
define('PROFILE_PHOTO',                         25);
define('MOVE_FWD_COMMENTS_PHOTOS',              26);
define('MOVE_FWD_COMMENT_REPLIES_PHOTOS',       27);
define('PREMIUM_HIDDEN_MARLBORO',               28);
define('BACKSTAGE_VIDEOS',                      29);
define('REFERRAL',                              30);
define('PROFILING',                             31);
define('REFEREE_FIRSTLOGIN',                    32);
define('REFERER_FIRSTLOGIN',                    33);
define('MOVE_FWD_ACCOMPLISHED_WINNER',          34);
define('MY_PROFILE',                            35);
define('RESET_PASSWORD',                        36);

/*
|--------------------------------------------------------------------------
| Account status
|--------------------------------------------------------------------------
|
| These statuses are used for user accounts 
|
*/

define('VERIFIED',                              1);
define('CSR_APPROVED',                          2);
define('PENDING_GIID',                          0);
define('PENDING_CSR',                           4);
define('REJECTED_NO_GIID',                      5);
define('REJECTED_GIID',                         6);
define('DEACTIVATED',                           7);
define('ARCLIGHT_FAILED',                       8);
define('DELETED',                               3);
define('MYM_APPROVED',                          9);
define('ARCLIGHT_IN_PROCESS',                   10);

define('PENDING',                               0);
define('APPROVED',                              1);
define('DISAPPROVED',                           2);

define('ACCESS_GRANTED_STATUS',                 VERIFIED);

/*
|--------------------------------------------------------------------------
| Brand History Configuration
|--------------------------------------------------------------------------
|
*/
define('BRAND_HISTORY_FILE_UPLOAD_TYPES',           FILE_UPLOAD_ALLOWED_TYPES.'|mp4');

/*
|--------------------------------------------------------------------------
| Product Info Configuration
|--------------------------------------------------------------------------
|
*/
define('PRODUCT_INFO_FILE_UPLOAD_TYPES',            BRAND_HISTORY_FILE_UPLOAD_TYPES);

/*
|--------------------------------------------------------------------------
| DBAM Configuration
|--------------------------------------------------------------------------
|
*/
define('DBAM_FILE_UPLOAD_TYPES',                    BRAND_HISTORY_FILE_UPLOAD_TYPES);

/*
|--------------------------------------------------------------------------
| API CREDENTIALS
|--------------------------------------------------------------------------
|
*/
define('ADMIN_USERNAME',                                                    'CMSPHSysUser');
define('ADMIN_PASSWORD',                                                      'Default#14');
define('APP_ID',                                                                    '1054');
define('API_URL',      'https://qa.mrm-pmi.com/Services/PMI.MRM.Services.RESTService.svc/');

/* =^=^=^=^=^=^=^=^=^=^=^=^=^=^= Marlboro Crossover =^=^=^=^=^=^=^=^=^=^=^=^=^=^= */

/*
|--------------------------------------------------------------------------
| ORIGIN IDs
|--------------------------------------------------------------------------
|
*/
define('INVALID_ORIGIN_ID', 					99);
define('ACTIVITY',						1);
define('ACTIVITY_READ',						2);
define('HOME',							3);
define('CODE',							4);
define('HOME_LOGIN',						5);
define('ACTIVITY_SELECT',					6);
define('USER_APPROVED_PHOTOS',				7);
define('GAMES',							8);

/*
|--------------------------------------------------------------------------
| SUB ORIGIN IDs
|--------------------------------------------------------------------------
|
*/
define('READ',							1);
define('WATCH',							2);
define('DECISION_LEFT',						3);
define('DECISION_RIGHT',					4);
define('CODE_PACK_CODE',					5);
define('CODE_LAMP_DRIVE',					6);
define('CODE_ELD',						7);
define('CODE_LEAVE_BEHIND',					8);
define('CODE_KA',							9);

/*
|--------------------------------------------------------------------------
| USER ACTIVITY
|--------------------------------------------------------------------------
|
*/
define('UA_SELECTED_ACTIVITIES',				'Selected Activities');
define('UA_GAINED_CREDITES',					'Gained Credits');
define('UA_CLAIMED_PRIZES',					'Claimed Prizes');
define('UA_INVITED_USERS',					'Invitied Users');
define('UA_INTEREST_IN_EVENT',				'Interest in Event');
define('UA_ENTERED_CODES',					'Entered Codes');
define('UA_WATCH',						'Watched Video');
define('UA_VIEW_CONTENT',					'View Content');
define('UA_HIDDEN_PACK',					'Hidden Pack');
define('UA_FLASH_OFFER',					'Flash Offer');
define('PLAY_GAME',						'Play Game');

/*
|--------------------------------------------------------------------------
| USER ACTIVITY PAGES
|--------------------------------------------------------------------------
|
*/
define('HOME_PAGE',						1);
define('MAP_PAGE',						2);
define('GIFT_PAGE',						3);
define('EVENT_PAGE',						4);
define('HOW_TO_PLAY_PAGE',					5);
define('ACTIVITY_PAGE',						6);

/*
|--------------------------------------------------------------------------
| GIFT TYPES
|--------------------------------------------------------------------------
|
*/
define('GIFT_TYPE_ACTIVITY',					1);
define('GIFT_TYPE_SPECIAL',					2);

/*
|--------------------------------------------------------------------------
| REDEMPTION TYPES
|--------------------------------------------------------------------------
|
*/
define('GIFT_REDEMPTION_DELIVERY',				1);
define('GIFT_REDEMPTION_REDEMPTION',			2);

/*
|--------------------------------------------------------------------------
| REGIONS
|--------------------------------------------------------------------------
|
*/
define('REGION_CALIFORNIA',					1);
define('REGION_ALASKA',						2);
define('REGION_CANADA',						3);
define('REGIONS_ALL',						serialize(array('', 'California', 'Alaska', 'Canada')));

/*
|--------------------------------------------------------------------------
| CODE LIMITS
|--------------------------------------------------------------------------
|
*/
define('PACK_CODE_LIMIT',					30);
define('LAMP_DRIVE_LIMIT',					24);
define('ELD_LIMIT',						12);
define('LEAVE_BEHIND_LIMIT',					1);
define('KA_LIMIT',						1);
define('NO_LIMIT',						1000000);

/*
|--------------------------------------------------------------------------
| CODE TYPES
|--------------------------------------------------------------------------
|
*/
define('PACK_CODE',						'Pack Codes');
define('LAMP_DRIVE',						'LAMP Drive');
define('ELD',							'ELD');
define('LEAVE_BEHIND',						'Leave Behind');
define('KA',							'KA Codes');

/*
|--------------------------------------------------------------------------
| REGION POINTS
|--------------------------------------------------------------------------
|
*/
define('CALIFORNIA_START_POINTS', 				0);
define('ALASKA_START_POINTS', 				1399);
define('CANADA_START_POINTS', 				3999);

/*
|--------------------------------------------------------------------------
| ACTIVITY_TYPE
|--------------------------------------------------------------------------
|
*/
define('TYPE_BIKING', 						1);
define('TYPE_HIKING',		 				2);
define('TYPE_RUNNING',		 				3);

/*
|--------------------------------------------------------------------------
| ACTIVITY POINTS
|--------------------------------------------------------------------------
|
*/
define('ACTIVITIES_EARN_POINTS',				30);
define('HOMEPAGE_EARN_POINTS',				50);
define('ACTIVITY_SELECT_POINTS',				40);

/*
|--------------------------------------------------------------------------
| GAMES
|--------------------------------------------------------------------------
|
*/
define('GRAB',				1);
define('LANDSAIL',			2);
define('PADDLING',			3);
define('PHOTOGRAPHY',			4);
define('RAFTING',				5);
define('ROCKCLIMBER',			6);
define('SNOWMOBILE',			7);
define('ZIPLINE',				8);

define('ALL_GAMES',			serialize(array("Grab", "Land Sail", "Paddling", "Photography", "Rafting", "Rock Climber", "Snow Mobile", "Zipline")));

/*
|--------------------------------------------------------------------------
| GAMES POINTS
|--------------------------------------------------------------------------
|
*/
define('BRONZE_POINTS',				100);
define('SILVER_POINTS',				200);
define('GOLD_POINTS',				300);

/*
|--------------------------------------------------------------------------
| ETC
|--------------------------------------------------------------------------
|
*/
define('BRONZE',				'bronze');
define('SILVER',				'silver');
define('GOLD',				'gold');
define('READ_CONTENT',						1);
define('WATCH_VIDEO',						2);
define('EXPERIENCE_HEADER_LIMIT',				3);

/*
|--------------------------------------------------------------------------
| URLs
|--------------------------------------------------------------------------
|
*/
define('CROSSOVER_STAGING_LOGOUT',				'http://marlboro-stage2.yellowhub.com/spice/welcome/logout');
define('CROSSOVER_PRODUCTION_LOGOUT',			'https://marlboro.ph/welcome/logout');
define('STAGING_BASE_URL',					'http://marlboro-stage2.yellowhub.com/spice');
define('PRODUCTION_BASE_URL',					'https://marlboro.ph');

/* End of file constants.php */
/* Location: ./application/config/constants.php */

define('WEB_ONLY',						1);
define('MOBILE_ONLY',						2);
define('BOTH_PLATFORM',						3);


define('BACKSTAGE_PHOTOS_APPROVE_BASE_POINT',		50);
define('BACKSTAGE_PHOTOS_APPROVE_DAILY_LIMIT',		200);
define('BACKSTAGE_PHOTOS_APPROVE_MONTHLY_LIMIT',	6000);

define('POINT_ACTIVE_STATUS',					1);
define('POINT_REJECTED_STATUS',				0);

define('IS_CROSSOVER', 1);
