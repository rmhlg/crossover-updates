<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| DATABASE CONNECTIVITY SETTINGS
| -------------------------------------------------------------------
| This file will contain the settings needed to access your database.
|
| For complete instructions please consult the 'Database Connection'
| page of the User Guide.
|
| -------------------------------------------------------------------
| EXPLANATION OF VARIABLES
| -------------------------------------------------------------------
|
|	['hostname'] The hostname of your database server.
|	['username'] The username used to connect to the database
|	['password'] The password used to connect to the database
|	['database'] The name of the database you want to connect to
|	['dbdriver'] The database type. ie: mysql.  Currently supported:
				 mysql, mysqli, postgre, odbc, mssql, sqlite, oci8
|	['dbprefix'] You can add an optional prefix, which will be added
|				 to the table name when using the  Active Record class
|	['pconnect'] TRUE/FALSE - Whether to use a persistent connection
|	['db_debug'] TRUE/FALSE - Whether database errors should be displayed.
|	['cache_on'] TRUE/FALSE - Enables/disables query caching
|	['cachedir'] The path to the folder where cache files should be stored
|	['char_set'] The character set used in communicating with the database
|	['dbcollat'] The character collation used in communicating with the database
|				 NOTE: For MySQL and MySQLi databases, this setting is only used
| 				 as a backup if your server is running PHP < 5.2.3 or MySQL < 5.0.7
|				 (and in table creation queries made with DB Forge).
| 				 There is an incompatibility in PHP with mysql_real_escape_string() which
| 				 can make your site vulnerable to SQL injection if you are using a
| 				 multi-byte character set and are running versions lower than these.
| 				 Sites using Latin-1 or UTF-8 database character set and collation are unaffected.
|	['swap_pre'] A default table prefix that should be swapped with the dbprefix
|	['autoinit'] Whether or not to automatically initialize the database.
|	['stricton'] TRUE/FALSE - forces 'Strict Mode' connections
|							- good for ensuring strict SQL while developing
|
| The $active_group variable lets you choose which connection group to
| make active.  By default there is only one group (the 'default' group).
|
| The $active_record variables lets you determine whether or not to load
| the active record class
*/

/* Marlboro Crossover Database Settings */


if( in_array( $_SERVER['REMOTE_ADDR'], array( '127.0.0.1', '::1' ) ) ) 
{ //check if localhost or in staging / prod server
	$active_group = 'localhost';
}else
{
	$active_group = 'staging';
}




$active_record = TRUE;

$db['localhost']['hostname'] = 'localhost'; //'192.168.1.167';
$db['localhost']['username'] = 'root';
$db['localhost']['password'] = '';
$db['localhost']['database'] = 'marlboro_crossover';
$db['localhost']['dbdriver'] = 'mysql';
$db['localhost']['dbprefix'] = 'tbl_';
$db['localhost']['pconnect'] = TRUE;
$db['localhost']['db_debug'] = TRUE;
$db['localhost']['cache_on'] = FALSE;
$db['localhost']['cachedir'] = '';
$db['localhost']['char_set'] = 'utf8';
$db['localhost']['dbcollat'] = 'utf8_general_ci';
$db['localhost']['swap_pre'] = '';
$db['localhost']['autoinit'] = TRUE;
$db['localhost']['stricton'] = FALSE;

$db['staging']['hostname'] = 'localhost';
$db['staging']['username'] = 'crossoverstage';
$db['staging']['password'] = 'DZk26SgN64t3ykqHbPHNRzbQsc3m3n';
$db['staging']['database'] = 'crossoverstage';
$db['staging']['dbdriver'] = 'mysql';
$db['staging']['dbprefix'] = 'tbl_';
$db['staging']['pconnect'] = TRUE;
$db['staging']['db_debug'] = TRUE;
$db['staging']['cache_on'] = FALSE;
$db['staging']['cachedir'] = '';
$db['staging']['char_set'] = 'utf8';
$db['staging']['dbcollat'] = 'utf8_general_ci';
$db['staging']['swap_pre'] = '';
$db['staging']['autoinit'] = TRUE;
$db['staging']['stricton'] = FALSE;

$db['production']['hostname'] = '10.66.16.204';
$db['production']['username'] = 'crossover';
$db['production']['password'] = 'c8U9u92VN848a44Q9p6gb93e45s3hm';
$db['production']['database'] = 'crossover';
$db['production']['dbdriver'] = 'mysql';
$db['production']['dbprefix'] = '';
$db['production']['pconnect'] = TRUE;
$db['production']['db_debug'] = TRUE;
$db['production']['cache_on'] = FALSE;
$db['production']['cachedir'] = '';
$db['production']['char_set'] = 'utf8';
$db['production']['dbcollat'] = 'utf8_general_ci';
$db['production']['swap_pre'] = '';
$db['production']['autoinit'] = TRUE;
$db['production']['stricton'] = FALSE;


/* Marlboro Database Settings */
$db['marlboro_local']['hostname'] = '192.168.1.181'; //'192.168.1.176';
$db['marlboro_local']['username'] = 'root';
$db['marlboro_local']['password'] = '';
$db['marlboro_local']['database'] = 'marlboro_2013';
$db['marlboro_local']['dbdriver'] = 'mysql';
$db['marlboro_local']['dbprefix'] = '';
$db['marlboro_local']['pconnect'] = TRUE;
$db['marlboro_local']['db_debug'] = TRUE;
$db['marlboro_local']['cache_on'] = FALSE;
$db['marlboro_local']['cachedir'] = '';
$db['marlboro_local']['char_set'] = 'utf8';
$db['marlboro_local']['dbcollat'] = 'utf8_general_ci';
$db['marlboro_local']['swap_pre'] = '';
$db['marlboro_local']['autoinit'] = TRUE;
$db['marlboro_local']['stricton'] = FALSE;

$db['marlboro_staging']['hostname'] = 'localhost';
$db['marlboro_staging']['username'] = 'stagemarlboro';
$db['marlboro_staging']['password'] = 'uST49JBmeDFZpbrf';
$db['marlboro_staging']['database'] = 'stagemarlboro';
$db['marlboro_staging']['dbdriver'] = 'mysql';
$db['marlboro_staging']['dbprefix'] = '';
$db['marlboro_staging']['pconnect'] = TRUE;
$db['marlboro_staging']['db_debug'] = TRUE;
$db['marlboro_staging']['cache_on'] = FALSE;
$db['marlboro_staging']['cachedir'] = '';
$db['marlboro_staging']['char_set'] = 'utf8';
$db['marlboro_staging']['dbcollat'] = 'utf8_general_ci';
$db['marlboro_staging']['swap_pre'] = '';
$db['marlboro_staging']['autoinit'] = TRUE;
$db['marlboro_staging']['stricton'] = FALSE;

$db['marlboro_production']['hostname'] = '10.66.16.202';
$db['marlboro_production']['username'] = 'pmftc';
$db['marlboro_production']['password'] = 'BUmlHksWepNsT8rqFnDwlJrHLDtBUjxcBcwY';
$db['marlboro_production']['database'] = 'pmftc';
$db['marlboro_production']['dbdriver'] = 'mysql';
$db['marlboro_production']['dbprefix'] = '';
$db['marlboro_production']['pconnect'] = TRUE;
$db['marlboro_production']['db_debug'] = TRUE;
$db['marlboro_production']['cache_on'] = FALSE;
$db['marlboro_production']['cachedir'] = '';
$db['marlboro_production']['char_set'] = 'utf8';
/* End of file database.php */
/* Location: ./application/config/database.php */