/*  jquery Roundabout plugin by 
    Jay Ramirez
    May 5, 2014
*/

jQuery.fn.roundabout = function(callback) {
 	var Self    = this,
        Items   = [],
        itemPos = {};
 	
 	this.next = function(){
        if(Items.length < 2) return;
 		var nextItem = Items[2] ? Items[2] : Items[Items.length-1];
        var curItem  = Items[Items.length-1];
 		
        Items[0].css(itemPos.left);
 		Items[1].css(itemPos.center);
 		curItem.css(itemPos.back);
 		nextItem.css(itemPos.right);

 		Items.push(Items[0]);
 		Items.shift();
        callback(Items[0]);
 	}

 	this.prev = function(){
        if(Items.length < 2) return;
 		var nextItem = Items[Items.length-2];
        var curItem  = Items[Items.length-1];

 		Items[0].css(itemPos.right);
        Items[1].css(itemPos.back);
 		curItem.css(itemPos.center);
 		nextItem.css(itemPos.left);

 		Items.unshift(Items[Items.length-1]);
 		Items.pop();
        callback(Items[0]);
 	}

    this.init = function(){
        Items[0].css(itemPos.center);

        if(Items.length < 2) return;

        Items[1].css(itemPos.right)
        Items[Items.length-1].css(itemPos.left);

        for( i = 2; i< Items.length-1; i++){
            Items[i].css(itemPos.back);
        }

    }

    this.resized = function(){
        var el = $(Self[0]),
            parentWidth = el.width(),
            childWidth = Items[0].width();

        itemPos.center = {
            top: 0,
            width : childWidth,
            left : (parentWidth/2) - (childWidth/2),
            opacity : 1,
            zIndex : 10
        }

        itemPos.left = {
            top: '15%',
            left : 0,
            width : childWidth/2,
            opacity : 0.5,
            zIndex : itemPos.center.zIndex - 1
        }

        itemPos.right = {
            top: '15%',
            left : parentWidth - (childWidth/2),
            width : childWidth/2,
            opacity : 0.5,
            zIndex : itemPos.center.zIndex - 2
        }

        itemPos.back = {
            left : parentWidth/2 - 20,
            width : 40,
            top: '30%',
            opacity : 0,
            zIndex : itemPos.center.zIndex - 3
        }

        Self.init();
    }

    return this.each(function() {
        var self = $(this),
            Window = $(window),
        	children = self.children(),
        	childCount = children.length;


        	children.each(function(){
        		var child = $(this);
        			Items.push(child)
        		child.css({position : 'absolute'})
        	})

        	
            Self.resized();
            Window.resize(function(){
                Self.resized()
            });

            setTimeout(function(){
                children.css({
                  transition: 'all 300ms'
                })
            },100);

        callback(Items[0]);
    });
};