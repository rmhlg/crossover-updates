$(document).ready( function() {

	$("#delete-selected-notifications").click( function() {
		var messages = [];
		var checked = $(".notification-message:checked");
		checked.each( function(key, item) {
			messages.push($(item).val());
		});
		var self = $(this);
		var origHtml = self.addClass("disabled").prop("disabled", true).html();
		self.html("<i>Deleting...</i>");
		$.post(BASE_URL + "notifications/delete", { ids : messages }, function(response) {
			checked.parent().remove();
			setTimeout( function() {
				self.html(origHtml).removeClass("disabled").prop("disabled", false);
			}, 300);
		});
	});

	$("#delete-notification").click( function() {
		var self = $(this);
		
	});

});