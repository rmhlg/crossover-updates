/*
  _                       _   _                         _       
 | |   ___     __ _    __| | (_)  _ __     __ _        (_)  ___ 
 | |  / _ \   / _` |  / _` | | | | '_ \   / _` |       | | / __|
 | | | (_) | | (_| | | (_| | | | | | | | | (_| |  _    | | \__ \
 |_|  \___/   \__,_|  \__,_| |_| |_| |_|  \__, | (_)  _/ | |___/
                                          |___/      |__/       
*/

$(function() {

  $(window).bind("pageshow", function(event) {
    if(event.originalEvent.persisted) {
      window.location.reload() 
    }
  });

  
});

var i18nInit = function(){
	//Load I18N file
	i18n.init({lng: "en",fallbackLng : false, 
    customLoad : function(lng, ns, options, loadComplete){
        $.get("translations/en.translation.txt")
        .success(function(data, textStatus, jqXHR){
          contents = JSON.parse(data);
          loadComplete(null, contents);
          
          $("html [data-i18n]").i18n();
          //Hack to prevent wrong sizes on some elements
          $("<style></style>").appendTo($(document.body)).remove();
          handlers();
        });
      }});
}
/*
                     _              __     _                       _   _                         _       
   ___   _ __     __| |     ___    / _|   | |   ___     __ _    __| | (_)  _ __     __ _        (_)  ___ 
  / _ \ | '_ \   / _` |    / _ \  | |_    | |  / _ \   / _` |  / _` | | | | '_ \   / _` |       | | / __|
 |  __/ | | | | | (_| |   | (_) | |  _|   | | | (_) | | (_| | | (_| | | | | | | | | (_| |  _    | | \__ \
  \___| |_| |_|  \__,_|    \___/  |_|     |_|  \___/   \__,_|  \__,_| |_| |_| |_|  \__, | (_)  _/ | |___/
                                                                                   |___/      |__/       
*/