/*
  _   _           _       _           _       
 | | (_)   __ _  | |__   | |_        (_)  ___ 
 | | | |  / _` | | '_ \  | __|       | | / __|
 | | | | | (_| | | | | | | |_   _    | | \__ \
 |_| |_|  \__, | |_| |_|  \__| (_)  _/ | |___/
          |___/                    |__/       

*/

//If the window size is over 767, it will remove the "mobile" content and download the new one
var ww = window.innerWidth;
if(window.outerWidth!=0){
	ww = window.outerWidth<ww?window.outerWidth:ww;
}
var contentLoaded = false;
if(ww>=767 && screen.width >= 767){
	var request = new XMLHttpRequest();
	var what = document.body.getAttribute("data-content");
	request.open("GET", "desktop/"+what+".html", true);
	request.onload = function(){
	  if(request.status >= 200 && request.status < 400){
	    var data = request.responseText;//Get data

		//Random background
		var _which = parseInt(Math.random()*3);
		switch (_which){
			case(1):
				data = data.replace("img/_california/home_banner.jpg", "img/_alaska/home_banner.jpg");
				data = data.replace('<b data-i18n="california.surname" data-random="imgLegend"></b>', '<b data-i18n="alaska.surname"></b>');
				data = data.replace('<span data-i18n="california.location" data-random="imgLegend"></span>', '<span data-i18n="alaska.location"></span>');
				data = data.replace('<span data-i18n="california.country" data-random="imgLegend"></span>','<span data-i18n="alaska.country"></span>');
			break;
			case(2):
				data = data.replace("img/_california/home_banner.jpg", "img/_canada/home_banner.jpg");
				data = data.replace('<b data-i18n="california.surname" data-random="imgLegend"></b>', '<b data-i18n="canada.surname"></b>');
				data = data.replace('<span data-i18n="california.location" data-random="imgLegend"></span>', '<span data-i18n="canada.location"></span>');
				data = data.replace('<span data-i18n="california.country" data-random="imgLegend"></span>','<span data-i18n="canada.country"></span>');
			break;
		}


	   document.getElementsByClassName("mainWrapper")[0].outerHTML = data;//Place data in the body

	    i18nInit();

	    FastClick.attach(document.body);
	  }
	};
	request.send();
}
else{
	i18nInit();
}

var lightHandlers = function(){

	//Random background mobile
	if(isMobile){
		var _random = parseInt(Math.random()*3);
		switch (_random){
			case(0):
				$("#home_banner").css("background-image","url(img/_california/home_banner_m.jpg)")
			break;
			case(1):
				$("#home_banner").css("background-image","url(img/_alaska/home_banner_m.jpg)")
			break;
			case(2):
				$("#home_banner").css("background-image","url(img/_canada/home_banner_m.jpg)")
			break;
		}
	}

	/* HEADER SLIDER ACTIVITIES */
	$(".sliderActivities").unbind("click");
	$(".sliderActivities").on("click", function(e){
		e.preventDefault();
		$("header").toggleClass("active");//open / close activities slider
	});

	if(firstTime)actMoreDetails();
	setHeightWrapper();
};

var actMoreDetails = function(){
	$("[data-seeMore], [data-seeMore] + .moreInfos ").on('click',function(e){
		e.preventDefault();
		$(this).parent().toggleClass("active");
	});
};

var setHeightWrapper = function(){
	/* HEIGHT OF CONTENT */
	if(!isMobile){
		var _header = $("header").height();
		var _footer = $("footer").height();
		var height = window.innerHeight - _header - _footer;
		$(".mainWrapper").css("height", height);
	}

	/* Last section padding-bottom */
	var _top = (window.innerHeight*0.15);
	_top += $(".mainWrapper section:last-of-type").is(".makeChoice")?24:0;
	if(!isMobile){
		_top += 24;
	}
	$(".mainWrapper section:last-of-type").css("padding-bottom", _top);
}


/*
                     _                __       _   _           _       _           _       
   ___   _ __     __| |       ___    / _|     | | (_)   __ _  | |__   | |_        (_)  ___ 
  / _ \ | '_ \   / _` |      / _ \  | |_      | | | |  / _` | | '_ \  | __|       | | / __|
 |  __/ | | | | | (_| |     | (_) | |  _|     | | | | | (_| | | | | | | |_   _    | | \__ \
  \___| |_| |_|  \__,_|      \___/  |_|       |_| |_|  \__, | |_| |_|  \__| (_)  _/ | |___/
                                                       |___/                    |__/       
*/