(function () {
    'use strict';
    var gametitle = function () {};

    gametitle.prototype = {
        create: function () {
            console.log('%c Title screen ', 'color: white; background:blue;');

            var gametitle = this.game.add.button(this.game.world.centerX, this.game.world.centerY, 'gametitle', this.startGame, this);
            gametitle.anchor.setTo(0.5);
        },
        startGame: function () {
            this.game.state.start('Game');
        }
    };

    window['rafting'] = window['rafting'] || {};
    window['rafting'].gametitle = gametitle;
})();