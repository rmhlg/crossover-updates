(function () {
    'use strict';

    function Preloader() {
        this.ready = false;
    }

    Preloader.prototype = {

        preload: function () {
            this.load.onLoadComplete.addOnce(this.onLoadComplete, this);
            this.loadResources();

            $(".loader").addClass("visible");
        },

        loadResources: function () {

            this.load.spritesheet('character', 'img/games/zipline/character.png', 480, 400, 17);
            this.load.spritesheet('point', 'img/games/zipline/points_spritesheet.png', 100, 142);
            this.load.image('sky', 'img/games/zipline/Sky.jpg');
            this.load.image('cloud', 'img/games/zipline/Cloud.png');

            this.load.spritesheet('rope', 'img/games/zipline/rope.png', 10, 654);
            this.load.image('boom1', 'img/games/zipline/boom1.png');
            this.load.image('boom2', 'img/games/zipline/boom2.png');
            this.load.image('arrival', 'img/games/zipline/arrival.png');

            this.load.image('landscape1', 'img/games/zipline/1_landscape.png');
            this.load.image('landscape3', 'img/games/zipline/3_landscape.png');
            this.load.image('landscape5', 'img/games/zipline/5_landscape.png');

            this.load.image('scorebg', 'img/games/zipline/pointsback.png');

            this.load.image('letsgo', 'img/games/CTA.png');
        },

        create: function () {},

        update: function () {
            if (!!this.ready) {
                this.game.state.start('GameTitle');
            }
        },

        onLoadComplete: function () {
            this.ready = true;

            $(".loader").removeClass("visible");
        }
    };

    window['zipline'] = window['zipline'] || {};
    window['zipline'].Preloader = Preloader;

}());