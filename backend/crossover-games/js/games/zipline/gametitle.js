(function () {
    'use strict';

    var GameTitle = function () {};

    GameTitle.prototype = {
        create: function () {
           var startBG = this.add.image(360, 220, 'letsgo');
            startBG.inputEnabled = true;
            startBG.input.useHandCursor = true;
            startBG.events.onInputDown.addOnce(this.startGame, this);
        },
        startGame: function () {
            this.game.state.start('game');
        }
    };

    window['zipline'] = window['zipline'] || {};
    window['zipline'].GameTitle = GameTitle;
})();