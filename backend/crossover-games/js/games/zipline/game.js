(function () {

  'use strict';

  function Game() {
    this.player = null;
    this.timer = null;
    this.end = null;
    this.arrival = null;
    this.rope = null;
  }

  Game.prototype = {

    create: function () {

      this.physics.startSystem(Phaser.Physics.P2JS);
      this.physics.p2.gravity.y = 5000;

      this.setupVariables();

      this.createBackground();
      this.createTrees();
      this.createPoints();
      this.createEnd();
      this.createPlayer();
      this.createInterface();

      this.timer = this.time.create();
      this.timer.start();

      if (this.game.device.desktop) {
        this.cursors = this.input.keyboard.createCursorKeys();
        this.time.events.loop(200, this.spawnTree, this);
      } else {
        this.time.events.loop(333, this.spawnTree, this);
      }
    },

    setupVariables: function () {
      this.score = 0;
      this.currentSpeed = 1;
      this.tickCount = 0;
      this.distance = 0;
      this.ended = false;
      this.gameOver = false;
      this.spawnCount = 0;
      this.spawnPointTime = 1 + Math.random();
      this.timeSinceSpawn = 0;
      this.pointsSpawned = 0;
    },

    createBackground: function () {

      this.backgroundGroup = this.add.group();

      this.backgroundGroup.create(0, 0, 'sky');
      this.clouds = this.backgroundGroup.create(200, 0, 'cloud');

      var bg = this.backgroundGroup.create(0, 0, 'landscape5');
      this.add.tween(bg).to({
        y: -30
      }, 50000, Phaser.Easing.Linear.None, true, 0, 0, false);

      bg = this.backgroundGroup.create(this.game.width * 0.5, this.game.height * 0.55, 'landscape3');
      bg.anchor.set(0.5);

      this.add.tween(bg.scale).to({
        x: 1.5,
        y: 1.5
      }, 50000, Phaser.Easing.Linear.None, true, 0, 0, false);

      bg = this.backgroundGroup.create(this.game.width * 0.5, this.game.height, 'landscape1');
      bg.anchor.setTo(0.5, 1.0);
      bg.scale.set(0.6);

      this.add.tween(bg.scale).to({
        x: 1,
        y: 1
      }, 50000, Phaser.Easing.Linear.None, true, 0, 0, false);

      this.rope = this.add.sprite(this.world.centerX - 5, 0, 'rope');
      this.rope.animations.add('anim');
      this.rope.animations.play('anim', 24, true);
    },

    createPlayer: function () {

      this.player = this.add.sprite(this.world.centerX, this.world.centerY, 'character');
      this.player.anchor.setTo(0.5);
      this.player.frame = 8;

      var anchor = this.add.sprite(this.world.centerX, 120);
      anchor.anchor.set(0.5);

      this.physics.p2.enable([this.player, anchor]);

      anchor.body.static = true;
      anchor.body.setCircle(5);

      this.player.body.setRectangle(150, 320, 0, -25);
      this.player.body.mass = 50;

      this.physics.p2.createSpring(anchor, this.player, 50, 10000, 50, null, null, [0, 0], [0, 0]);
    },

    createPoints: function () {
      this.points = this.add.group();
      for (var i = 0; i < 10; ++i) {
        var point = this.points.create(-500, -500, 'point');
        point.animations.add('turn');
        point.anchor.setTo(0.5, 0.5);
        point.kill();
      }
    },

    createTrees: function () {
      this.trees = this.add.group();
      var tree = null;

      var i = 0;
      for (; i < 80; ++i) {
        if (i % 2 === 0) {
          tree = this.trees.create(this.world.centerX, 540, 'boom1');
        } else {
          tree = this.trees.create(this.world.centerX, 540, 'boom2');
        }

        tree.anchor.setTo(0.5, 0.5);
        tree.kill();
      }

      for (i = 0; i < 30; ++i) {
        this.spawnStart();
      }
      this.spawnTree();
    },

    createEnd: function () {
      this.end = this.add.group();
      this.end.x = this.game.width * 0.5;
      this.end.y = 540;

      this.end.pivot.x = this.game.width * 0.5;
      this.end.pivot.y = 540;

      /*var tree = this.end.create(this.game.width * 0.5 - 450, 540, 'end_left');
      tree.anchor.set(0.5, 0.35);

      tree = this.end.create(this.game.width * 0.5 + 350, 540, 'end_right');
      tree.anchor.set(0.5, 0.35);

      tree = this.end.create(this.game.width * 0.5 - 200, 515, 'flag_left');
      tree.anchor.set(0.5, 1);
      tree.animations.add('flag');
      tree.animations.play('flag', 24, true);

      tree = this.end.create(this.game.width * 0.5 + 150, 515, 'flag_right');
      tree.anchor.set(0.5, 1);
      tree.animations.add('flag');
      tree.animations.play('flag', 24, true);*/

      this.end.scale.set(0.01, 0.01);
      this.end.visible = false;

      var arrival = this.end.create(this.game.width * 0.5, 520, 'arrival');
      arrival.scale.set(0.45);
      arrival.anchor.set(0.5);
    },

    createInterface: function () {

      this.add.sprite(-80, 15, 'scorebg');

      this.scoreText = this.add.text(45, 50, '0', {
        font: '48px bebas-neue',
        fill: '#000000',
        align: 'center'
      });
      this.scoreText.anchor.set(0.5);

      this.add.text(65, 44, '/18', {
        font: '24px bebas-neue',
        fill: '#000000',
        align: 'center'
      });

      var point = this.add.sprite(0, 30, 'point');
      point.animations.add('turn');
      point.scale.set(0.25);
      point.animations.play('turn', 12, true);

    },

    update: function () {

      if (!this.gameOver) {
        if (this.distance >= 160) {
          this.onGameOver();
        } else {

          if (this.distance > 135 && !this.ended) {
            this.spawnEnd();
            this.ended = true;
          }

          this.updatePlayer();
          this.updatePoints();

          var d = this.time.physicsElapsed;

          if (this.distance < 135) {
            this.timeSinceSpawn += d;

            if (this.timeSinceSpawn > this.spawnPointTime) {
              this.spawnPoint();
              this.spawnPointTime = 0.85 + Math.random();
              this.timeSinceSpawn = 0;
            }
          }

          this.distance += d * 5;

        }
      } else {
        this.player.body.velocity.x *= 0.9;
        this.updateFrame();
      }
    },

    updatePlayer: function () {

      this.updateFrame();

      if (Math.abs(this.player.body.velocity.x) < 450) {
        if (this.tickCount > 5) {
          var force = 40;

          if (this.game.device.desktop) {
            this.clouds.x -= 0.1;
            if (this.cursors.left.isDown) {
              this.player.body.velocity.x -= force;
              this.tickCount = 0;
            } else if (this.cursors.right.isDown) {
              this.player.body.velocity.x += force;
              this.tickCount = 0;
            }
          } else {
            var pointer = this.game.input.activePointer;
            if (pointer.isDown) {
              if (pointer.x < this.game.height * 0.5) {
                this.player.body.velocity.x -= force;
                this.tickCount = 0;
              } else {
                this.player.body.velocity.x += force;
                this.tickCount = 0;
              }
            }
          }
        }
      }

      ++this.tickCount;
    },

    updateFrame: function () {
      var frame = 8 - Math.floor(this.player.body.angle / 6);
      this.player.frame = Math.max(0, Math.min(16, frame));
    },

    updatePoints: function () {

      this.points.forEachAlive(function (point) {
        if (point.scale.x > 0.8 && this.checkHitPlayer(point)) {
          this.scoreText.setText(++this.score);
          this.tweens.removeFrom(point);
          point.kill();
        }
      }, this);

    },

    checkHitPlayer: function (point) {
      var arr = this.physics.p2.hitTest(point.world, [this.player.body]);
      return (arr && arr.length > 0);
    },

    spawnTree: function () {

      ++this.spawnCount;

      var left = this.spawnCount % 2 === 0;
      this.spawnSingleTree(1, left);
      this.spawnSingleTree(2 + Math.random() * 3, left);
      this.spawnSingleTree(5 + Math.random() * 3, !left);

      this.trees.children.sort(function (a, b) {
        if (a.scale.x < b.scale.x) {
          return -1;
        } else {
          return 1;
        }
      });
    },

    spawnSingleTree: function (r, left) {

      var tree = this.trees.getFirstDead();
      if (tree) {

        var startDistance = 20 * r;
        var endDistance = 500 * r;

        tree.x = this.game.width * 0.5 + (left ? -startDistance : startDistance);
        tree.y = 530;
        tree.angle = r - 5;
        tree.scale.set(0.01 + 0.01 * r);
        tree.revive();

        var tween = this.add.tween(tree.scale);
        tween.to({
          x: 1 + r,
          y: 1 + r
        }, 5000, Phaser.Easing.Exponential.In);
        tween.start();

        tween = this.add.tween(tree).to({
          x: (left ? -endDistance : this.game.width + endDistance),
          y: 500
        }, 5000, Phaser.Easing.Exponential.In);
        tween.onComplete.add(function () {
          tree.kill();
        });
        tween.start();

      }
    },

    spawnStart: function () {

      var tree = this.trees.getFirstDead();
      if (tree) {

        var r = Math.floor(Math.random() * 10);

        var startDistance = r * 50;
        var endDistance = 500;

        tree.x = this.game.width * 0.5 + (r % 2 === 0 ? -startDistance : startDistance);
        tree.y = 520;
        tree.scale.set(r * 0.1);
        tree.revive();

        var tween = this.add.tween(tree.scale);
        tween.to({
          x: 2.2,
          y: 2.2
        }, (12 - r) * 320, Phaser.Easing.Exponential.In);
        tween.start();

        tween = this.add.tween(tree).to({
          x: (r % 2 === 0 ? -endDistance : this.game.width + endDistance),
          y: 500
        }, (12 - r) * 320, Phaser.Easing.Exponential.In);
        tween.onComplete.add(function () {
          tree.kill();
        });
        tween.start();

      }
    },

    spawnEnd: function () {
      var tween = this.add.tween(this.end.scale);
      tween.to({
        x: 2,
        y: 2
      }, 5000, Phaser.Easing.Exponential.In);
      tween.start();

      this.end.visible = true;
    },

    spawnPoint: function () {
      var point = this.points.getFirstDead();
      if (point && this.pointsSpawned < 18) {

        ++this.pointsSpawned;
        point.x = this.game.width * 0.5;
        point.y = 530;

        var r = 50 + Math.random() * 924;
        point.scale.set(0.01);
        point.animations.play('turn', 10, true);
        point.revive();

        var tween = this.add.tween(point.scale);
        tween.to({
          x: 1,
          y: 1
        }, 5000, Phaser.Easing.Exponential.In);
        tween.start();

        tween = this.add.tween(point).to({
          x: r,
          y: -100
        }, 5500, function (k) {
          return k === 0 ? 0 : Math.pow(4096, k - 1);
        });
        tween.onComplete.add(function () {
          point.kill();
        });
        tween.start();
      }
    },

    onGameOver: function () {

            // Stop Game
            this.gameOver = true;
            this.time.removeAll();
            this.tweens.removeAll();

            this.rope.animations.stop('anim');
            this.points.forEach(function (point) {
              point.animations.stop('turn');
            });


            var grade = 'none';
            if (this.score > gold) {
                grade="gold";
            } else if (this.score > silver) {
                grade="silver";
            } else if (this.score > bronze) {
                grade="bronze";
            }

           gameOver(grade, this.score);
            

          }
        };

        window['zipline'] = window['zipline'] || {};
        window['zipline'].Game = Game;


      })();