(function () {
    'use strict';

    var gametitle = function () {};

    gametitle.prototype = {
        create: function () {
           var startBG = this.add.image(360, 220, 'letsgo');
            startBG.inputEnabled = true;
            startBG.input.useHandCursor = true;
            startBG.events.onInputDown.addOnce(this.startGame, this);
        },
        startGame: function () {
            this.game.state.start('Game');
        }
    };

    window['landsail'] = window['landsail'] || {};
    window['landsail'].gametitle = gametitle;
})();