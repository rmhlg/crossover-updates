(function () {

    'use strict';

    var preload = function () {
        this.ready = false;
    };

    preload.prototype = {
        preload: function () {
            console.log('%c Preloading assets ', 'color: white; background:red;');

            $(".loader").addClass("visible");
            this.load.onLoadComplete.addOnce(this.onLoadComplete, this);
            this.loadResources();
        },
        loadResources: function () {
            console.log('loading resources');
            this.game.load.spritesheet('road', 'img/games/landsail/road_9frames_spritesheet_1024.jpg', 1024, 390);
            this.game.load.spritesheet('sail', 'img/games/landsail/landyacht_spritesheet_v2_1024.png', 408, 408);
            this.game.load.spritesheet('dust', 'img/games/landsail/dust.png', 60, 49);
            this.game.load.spritesheet('points', 'img/games/landsail/points.png', 76, 107);
            this.game.load.image('sky', 'img/games/landsail/sky.jpg');
            this.load.image('letsgo', 'img/games/CTA.png');
            this.game.load.image('mountain', 'img/games/landsail/mountain.png');
            this.game.load.image('cloud', 'img/games/landsail/cloud.png');
            this.game.load.image('rock1', 'img/games/landsail/rock1.png');
            this.game.load.image('rock2', 'img/games/landsail/rock2.png');
            this.game.load.image('rock3', 'img/games/landsail/rock3.png');
            this.game.load.image('rock4', 'img/games/landsail/rock4.png');
            this.game.load.image('rock5', 'img/games/landsail/rock5.png');
            this.game.load.image('rock6', 'img/games/landsail/rock6.png');
            this.game.load.image('rock7', 'img/games/landsail/rock7.png');
            this.game.load.image('rock8', 'img/games/landsail/rock8.png');
            this.game.load.image('rock9', 'img/games/landsail/rock9.png');
            this.game.load.image('cactus1', 'img/games/landsail/cactus1.png');
            this.game.load.image('cactus2', 'img/games/landsail/cactus2.png');
            this.game.load.image('cactus3', 'img/games/landsail/cactus3.png');
            this.game.load.image('cactus4', 'img/games/landsail/cactus4.png');
            this.game.load.image('cactus5', 'img/games/landsail/cactus5.png');
            this.game.load.image('cactus6', 'img/games/landsail/cactus6.png');
            this.game.load.image('scorebg', 'img/games/landsail/pointsback.png');
        },
        create: function () {},
        update: function () {
            if (!!this.ready) {
                this.game.state.start('GameTitle');
            }
        },

        onLoadComplete: function () {
            $(".loader").removeClass("visible");
            this.ready = true;
        }
    };

    window['landsail'] = window['landsail'] || {};
    window['landsail'].preload = preload;
})();