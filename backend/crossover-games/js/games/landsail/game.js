(function () {
    'use strict';

    var sailgame = function () {
        this.currentSpeed = 0;
        this.maxSpeed = 15;
        this.roadWidth = 1024;
        this.carFrameCount = 7;
        this.sailFrameCount = 21;
        this.debug = 0;
        this.secondsPerLevel = 20;
        this.ticksPerSecond = 10;
        this.timePlayed = 0;
        this.ticks = 0;
        this.level = 0;
        this.rockXCoords = [];
        this.rockHitSizes = [];
        this.rocktimers = [3, 2, 1.5, 1, 0.5, 0.25];
        this.rocktimer = 0;
        this.rocks = [];
        this.pointerLeft = false;
        this.pointerRight = false;
        this.roadSpeed = 45;
        this.cactii = [];
        this.cactusGroup = null;
        this.rockGroup = null;
        this.clouds = null;
        this.gameover = false;
        this.distance = null;
        this.loopTimer = null;
        this.plusscore = null;
    };

    sailgame.prototype = {
        create: function () {
            if(this.rockGroup != null){
                this.rockGroup.destroy();
            }
            if(this.cactusGroup != null){
                this.cactusGroup.destroy();
            }
            this.clouds = null;
            this.gameover = false;
            this.plusscore = null;
            this.cactusGroup = null;
            this.rockGroup = null;
            this.roadSpeed = 45;
            this.currentSpeed = 0;
            this.maxSpeed = 15;
            this.distance = 0;

            this.rocks = [];
            this.cactii = [];
            this.ticks = 0;
            this.level = 0;
            this.rockXCoords = [];
            
            this.debug = this.getQueryVariable('debug');

            this.initStage();
            this.createControls();
            this.createBackground();
            this.createRoad();
            this.createCactii();

            this.createRocks();

            this.createEmitters();

            this.createPoints();

            this.createSail();

            this.createUI();

            this.loopTimer = this.game.time.events.loop(Phaser.Timer.SECOND / this.ticksPerSecond, this.timer, this);

        },

        initStage: function () {
            this.game.physics.startSystem(Phaser.Physics.ARCADE);
            this.game.stage.backgroundColor = '#c6e5f1';
        },

        createControls: function () {
            if (this.game.device.desktop) {
                this.cursors = this.game.input.keyboard.createCursorKeys();
            } else {
                this.game.input.onDown.add(this.handlePointerDown, this);
                this.game.input.onUp.add(this.handlePointerUp, this);
            }
        },

        createBackground: function () {

            this.game.add.sprite(0, 0, 'sky');

            this.clouds = this.game.add.tileSprite(0, 0, 1024, 150, 'cloud');
            this.stage.backgroundColor = 0x000000;
            var mountain = this.game.add.sprite(this.game.world.centerX, 150, 'mountain');
            mountain.anchor.set(0.5, 1);
            mountain.scale.set(0.5, 0.5);

            this.game.add.tween(mountain.scale).to({
                x: 1,
                y: 1
            }, 150000, Phaser.Easing.Linear.None, true, 0, -1, true);
        },

        createRoad: function () {
            this.road = this.game.add.sprite(0, 150, 'road');
            this.roadSegmentWidth = Math.round(this.roadWidth / this.sailFrameCount);
            this.roadAnim = this.road.animations.add('moving', [0, 1, 2, 3, 4, 5, 6, 7, 8], this.roadSpeed, true);
            this.road.play('moving');
        },

        createSail: function () {
            this.sail = this.game.add.sprite(this.game.world.width / 2, 340, 'sail');
            this.sail.anchor.set(0.5);
            this.game.physics.enable(this.sail, Phaser.Physics.ARCADE);
            this.sail.body.setSize(160, 30, 25, 150);

            this.sail.body.collideWorldBounds = true;
            this.sail.body.bounce.set(1);
            this.sail.body.immovable = true;

            this.sail.frame = 10;

            this.zPoint = (this.game.world.width / 2) - (this.roadWidth / 2);

            this.sailTween = this.game.add.tween(this.sail).to({
                y: 338
            }, 100, Phaser.Easing.Linear.None, true, 0, -1, true);
        },

        createUI: function () {

            this.plusscore = this.game.add.text(this.game.world.centerX - 20, 100, '+10', {
                font: '40px bebas-neue',
                fill: '#ffffff',
                align: 'center'
            });
            this.plusscore.alpha = 0;

            this.createScoreBox();
        },

        createScoreBox: function () {

            this.add.sprite(-80, 15, 'scorebg');

            this.distance = this.add.text(72, 50, '0', {
                font: '48px bebas-neue',
                fill: '#000000',
                align: 'right'
            });
            this.distance.anchor.setTo(1, 0.5);

            this.add.text(78, 45, 'm', {
                font: '24px bebas-neue',
                fill: '#000000',
                align: 'center'
            });
        },

        createCactii: function () {
            this.cactii[0] = 'cactus1';
            this.cactii[1] = 'cactus2';
            this.cactii[2] = 'cactus3';
            this.cactii[3] = 'cactus4';
            this.cactii[4] = 'cactus5';
            this.cactii[5] = 'cactus6';
            this.cactii[6] = 'rock1';

            this.cactusGroup = this.game.add.group();

            for (var i = 0; i < 20; ++i) {
                var cactus = this.cactusGroup.create(-2000, -2000, this.cactii[Math.floor(Math.random() * 6)]);
                cactus.kill();
            }

            this.spawnCactus();

        },

        createPoints: function () {
            this.pointsGroup = this.game.add.group();

            for (var i = 0; i < 5; ++i) {
                var point = this.pointsGroup.create(-2000, -2000, 'points');
                point.animations.add('turn');
                point.animations.play('turn', 15, true);
                this.game.physics.enable(point, Phaser.Physics.ARCADE);
                point.body.setSize(30, 20, 0, 40);
                point.kill();
            }

            this.spawnPoint();
        },

        createRocks: function () {
            this.rocktimer = this.rocktimers[this.level];

            this.rocks[0] = 'rock1';
            this.rocks[1] = 'rock2';
            this.rocks[2] = 'rock3';
            this.rocks[3] = 'rock4';
            this.rocks[4] = 'rock5';
            this.rocks[5] = 'rock6';
            this.rocks[6] = 'rock7';
            this.rocks[7] = 'rock8';
            this.rocks[8] = 'rock9';
            this.rocks[9] = 'cactus1';
            this.rocks[10] = 'cactus2';
            this.rocks[11] = 'cactus3';
            this.rocks[12] = 'cactus4';

            this.rockXCoords[0] = this.game.world.width / 2 + 200;
            this.rockXCoords[1] = 1600;
            this.rockXCoords[2] = -500;
            this.rockXCoords[3] = this.game.world.width / 2 - 200;
            this.rockXCoords[4] = 1200;
            this.rockXCoords[5] = 0;
            this.rockXCoords[6] = 650;
            this.rockXCoords[7] = 955;
            this.rockXCoords[8] = 300;
            this.rockXCoords[9] = this.game.world.width / 2;
            this.rockXCoords[10] = -500;
            this.rockXCoords[11] = 1600;
            this.rockXCoords[12] = 450;

            this.rockGroup = this.game.add.group();

            for (var i = 0; i < 20; ++i) {

                var rnd = Math.floor(Math.random() * (this.rocks.length));
                var rock = this.rockGroup.create(-2000, -2000, this.rocks[rnd]);
                this.game.physics.enable(rock, Phaser.Physics.ARCADE);

                if (rnd < 9) {
                    rock.body.setSize(rock.texture.width * 0.70, 20, 0, 40);
                } else {
                    rock.body.setSize(rock.texture.width * 0.1, 10, 0, 88);
                }
                rock.kill();
            }

            this.spawnRock();

        },

        createEmitters: function () {
            this.leftEmitter = this.game.add.emitter(0, 0, 1000);
            this.leftEmitter.makeParticles('dust', [0, 1, 2, 3, 4, 5, 6, 7]);
            this.leftEmitter.lifespan = 500;
            this.leftEmitter.maxParticleSpeed = new Phaser.Point(25, 500);
            this.leftEmitter.minParticleSpeed = new Phaser.Point(-25, 400);
            this.rightEmitter = this.game.add.emitter(0, 0, 1000);
            this.rightEmitter.makeParticles('dust', [0, 1, 2, 3, 4, 5, 6, 7]);
            this.rightEmitter.lifespan = 500;
            this.rightEmitter.maxParticleSpeed = new Phaser.Point(25, 500);
            this.rightEmitter.minParticleSpeed = new Phaser.Point(-25, 400);
            this.midEmitter = this.game.add.emitter(0, 0, 1000);
            this.midEmitter.makeParticles('dust', [0, 1, 2, 3, 4, 5, 6, 7]);
            this.midEmitter.lifespan = 250;
            this.midEmitter.maxParticleSpeed = new Phaser.Point(25, 500);
            this.midEmitter.minParticleSpeed = new Phaser.Point(-25, 400);
        },

        timer: function () {
            this.ticks++;
            if (this.ticks % this.ticksPerSecond === 0) {
                this.timePlayed++;
            }

            if (this.distance) {
                this.distance.setText(this.ticks);
            }

            if (this.ticks % (this.secondsPerLevel * this.ticksPerSecond) === 0) {
                this.level++;
                if (this.level === this.rocktimers.length) {
                    this.rocktimer = this.rocktimers[this.rocktimers.length - 1];
                } else {
                    this.rocktimer = this.rocktimers[this.level];
                }
            }
        },

        update: function () {
            this.clouds.tilePosition.x -= 0.1;

            if (!this.gameover) {

                if (this.game.device.desktop) {
                    if (this.cursors.left.isDown) {
                        this.currentSpeed = -this.maxSpeed;
                    } else if (this.cursors.right.isDown) {
                        this.currentSpeed = this.maxSpeed;
                    } else {
                        this.currentSpeed = this.currentSpeed / 1.1;
                    }
                }

                if (this.pointerLeft) {
                    this.currentSpeed = -this.maxSpeed;
                } else if (this.pointerRight) {
                    this.currentSpeed = this.maxSpeed;
                } else {
                    this.currentSpeed = this.currentSpeed / 1.1;
                }

                this.sail.x += this.currentSpeed;
                var rX = this.sail.x - this.zPoint;
                this.sail.frame = Math.floor(rX / this.roadSegmentWidth);

                this.rockGroup.forEachAlive(function (rock) {
                    this.physics.arcade.overlap(this.sail, rock, this.overlapHandler, null, this);
                }, this);

                this.pointsGroup.forEachAlive(function (point) {
                    this.physics.arcade.overlap(this.sail, point, this.pointOverlapHandler, null, this);
                }, this);

                this.updateEmitters();
            }
        },

        updateEmitters: function () {
            var offset = (this.sail.x * (40 / this.game.world.width)) - 20;
            var midOffset = (this.sail.x * (130 / this.game.world.width)) - 65;

            this.leftEmitter.x = this.sail.x - 65 + offset;
            this.leftEmitter.y = this.sail.y + 160;
            this.rightEmitter.x = this.sail.x + 110 + offset;
            this.rightEmitter.y = this.sail.y + 160;
            this.midEmitter.x = (this.sail.x + 15) - midOffset;
            this.midEmitter.y = this.sail.y + 100;

            var tmp = (this.sail.x * (500 / 768)) - 250;

            this.leftEmitter.maxParticleSpeed = new Phaser.Point(tmp + 25, 500);
            this.leftEmitter.minParticleSpeed = new Phaser.Point(tmp - 25, 400);
            this.rightEmitter.maxParticleSpeed = new Phaser.Point(tmp + 25, 500);
            this.rightEmitter.minParticleSpeed = new Phaser.Point(tmp - 25, 400);
            this.midEmitter.maxParticleSpeed = new Phaser.Point(tmp + 25, 500);
            this.midEmitter.minParticleSpeed = new Phaser.Point(tmp - 25, 400);

            this.leftEmitter.emitParticle();
            this.rightEmitter.emitParticle();
            this.midEmitter.emitParticle();
        },

        spawnRock: function () {
            if (!this.gameover) {

                var rndTimer = 250 + Math.floor(Math.random() * this.rocktimer) * Phaser.Timer.SECOND;
                this.game.time.events.add(rndTimer, this.spawnRock, this).autoDestroy = true;

                var rock = this.rockGroup.getFirstDead();

                if (rock) {
                    var rnd = Math.floor(Math.random() * (this.rocks.length));

                    rock.x = this.game.world.centerX + (((this.rockXCoords[rnd] / 2) - 800) / 120);
                    rock.y = 150;

                    rock.anchor.setTo(0.5);
                    rock.scale.set(0.01, 0.01);

                    var rockScaleTween = this.game.add.tween(rock.scale);
                    rockScaleTween.to({
                        x: 2,
                        y: 2
                    }, 7500, this.ultratween, true, 0, 0);
                    var rockYTween = this.game.add.tween(rock);
                    rockYTween.to({
                        x: this.rockXCoords[rnd],
                        y: 1000
                    }, 7500, this.ultratween, true, 0, 0);

                    rockScaleTween.onComplete.add(function () {
                        rock.kill();
                    }, this);

                    rock.revive();
                }
            }
        },

        spawnCactus: function () {
            if (!this.gameover) {
                var rndTimer = Math.floor(Math.random() * 250);
                this.game.time.events.add(rndTimer, this.spawnCactus, this).autoDestroy = true;

                var cactus = this.cactusGroup.getFirstDead();
                if (cactus) {

                    var rndPosition = Math.floor(Math.random() * 500) - 250;
                    if (rndPosition < 0) {
                        rndPosition = -10 + rndPosition;
                    } else {
                        rndPosition = 10 + rndPosition;
                    }

                    cactus.x = this.game.world.centerX + rndPosition;
                    cactus.y = 150;
                    cactus.anchor.setTo(0.5);
                    cactus.scale.set(0.02, 0.02);

                    var cactusScaleTween = this.game.add.tween(cactus.scale);
                    cactusScaleTween.to({
                        x: 3,
                        y: 3
                    }, 3500, Phaser.Easing.Exponential.In, true, 0, 0);

                    var cactusYTween = this.game.add.tween(cactus);

                    if (rndPosition < 0) {
                        cactusYTween.to({
                            x: -1000 + (rndPosition * 100),
                            y: 1000
                        }, 3500, Phaser.Easing.Exponential.In, true, 0, 0);
                    } else {
                        cactusYTween.to({
                            x: 2280 + (rndPosition * 100),
                            y: 1000
                        }, 3500, Phaser.Easing.Exponential.In, true, 0, 0);
                    }

                    cactusScaleTween.onComplete.add(function () {
                        cactus.kill();
                    }, this);

                    cactus.revive();
                }
            }
        },

        spawnPoint: function () {

            if (!this.gameover) {

                var rndTimer = 250 + Math.floor(Math.random() * 20) * Phaser.Timer.SECOND;
                this.game.time.events.add(rndTimer, this.spawnPoint, this).autoDestroy = true;

                var point = this.pointsGroup.getFirstDead();
                if (point) {

                    var rnd = Math.floor(Math.random() * 12);

                    var px = this.game.world.centerX + (((this.rockXCoords[rnd] / 2) - 700) / 100) ;
                    point.x = px;
                    point.alpha = 1;
                    point.y = 150;

                    point.anchor.setTo(0.5);
                    point.scale.set(0.01, 0.01);

                    var pointScaleTween = this.game.add.tween(point.scale);
                    pointScaleTween.to({
                        x: 2,
                        y: 2
                    }, 7500, this.ultratween, true, 0, 0);
                    var pointYTween = this.game.add.tween(point);

                    pointYTween.to({
                        x: this.rockXCoords[rnd] + 150,
                        y: 1100
                    }, 7500, this.ultratween, true, 0, 0);

                    pointScaleTween.onComplete.add(function () {
                        point.kill();
                    }, this);
                    point.revive();
                }
            }
        },

        pointOverlapHandler: function (sail, point) {

            point.kill();

            this.plusscore.alpha = 1;
            this.plusscore.y = 300;
            var plustween = this.game.add.tween(this.plusscore);
            plustween.to({
                y: 250,
                alpha: 0
            }, 2000, Phaser.Easing.Circular.Out, true, 0, 0);

            this.ticks += 10;
        },

        overlapHandler: function () {

            this.game.time.events.remove(this.loopTimer);

            this.road.animations.stop(null, true);
            this.game.tweens.pauseAll();
            this.gameover = true;
            this.showEndScore();

        },

        showEndScore: function () {

            var grade="none";
            if (this.ticks > gold) {
                grade="gold";
            } else if (this.ticks > silver) {
                grade="silver";
            } else if (this.ticks > bronze) {
                grade ="bronze";
            }
            gameOver(grade,this.ticks);
        },

        render: function () {
            if (this.debug === '1') {

                this.game.debug.body(this.sail);

                this.rockGroup.forEachAlive(function (rock) {
                    this.game.debug.body(rock);
                }, this);
            }
        },
        handlePointerDown: function (pointer) {
            if (pointer.x < this.game.width * 0.5) {
                this.pointerLeft = true;
                this.pointerRight = false;
            } else {
                this.pointerLeft = false;
                this.pointerRight = true;
            }
        },
        handlePointerUp: function () {
            this.pointerLeft = false;
            this.pointerRight = false;
        },

        ultratween: function (k) {
            return Math.pow(k, 20);
        },

        getQueryVariable: function (variable) {
            var query = window.location.search.substring(1);
            var vars = query.split('&');
            for (var i = 0; i < vars.length; i++) {
                var pair = vars[i].split('=');
                if (pair[0] === variable) {
                    return pair[1];
                }
            }
            return (false);
        }
    };

    window['landsail'] = window['landsail'] || {};
    window['landsail'].sailgame = sailgame;

})();