(function () {
    'use strict';

    function Game() {
        this.player = null;
        this.wheelFront = null;
        this.wheelBack = null;

        this.bonuses = null;
        this.carGroup = null;
        this.levelGroup = null;

        this.timer = null;
        this.timeText = null;
        this.emitter = null;
        this.touchingGround = false;
        this.endPoint = 18200;
    }

    Game.prototype = {

        create: function () {

            this.debug = this.getQueryVariable('debug');

            this.world.setBounds(0, -135, 19000, this.game.height);
            this.camera.y = -135;
            this.stage.backgroundColor = '#a0c0cf';

            this.createPhysics();

            this.createBackground();
            this.createTrees();
            this.createBonuses();
            this.createTerrain();
            this.createTileMap();
            this.createPlayer();
            this.createSnow();
            this.createTreesFront();
            this.createFlag();

            this.createInterface();

            this.wheelFront.body.collides(this.levelGroup);
            this.wheelBack.body.collides(this.levelGroup);

            this.flyCount = 0;
            this.maxTime = 0;
            this.currentBonus = 0;
            this.touchingGround = false;
            this.frontTouchingGround = false;

            if (this.game.device.desktop) {
                this.cursors = this.game.input.keyboard.createCursorKeys();
            }

            this.timer = this.time.create();
            this.timer.loop(Phaser.Timer.SECOND, this.updateTime, this);
            this.timer.start();
        },

        createPhysics: function () {
            this.game.physics.startSystem(Phaser.Physics.P2JS);
            this.game.physics.p2.gravity.y = 1500;
            this.game.physics.p2.friction = 5;
            this.game.physics.p2.restitution = 0.05;

            this.levelGroup = this.physics.p2.createCollisionGroup();
            this.carGroup = this.physics.p2.createCollisionGroup();
            this.physics.p2.updateBoundsCollisionGroup();
        },

        createBackground: function () {
            this.sky = this.add.sprite(0, 0, 'sky');
            this.sky.fixedToCamera = true;

            this.mountains = this.add.tileSprite(-200, -35, 3000, 350, 'mountains');

            this.backgroundGroup = this.add.group();
            var tree = this.add.tileSprite(-200, 100, 8000, 420, 'snowBackground');
            this.backgroundGroup.add(tree);

            tree = this.backgroundGroup.create(2000, 0, 'tree1');
            tree.scale.set(0.5);
            tree = this.backgroundGroup.create(2500, 10, 'tree3');
            tree.scale.set(0.5);
            tree = this.backgroundGroup.create(3200, 5, 'tree1');
            tree.scale.set(0.5);
            tree = this.backgroundGroup.create(4000, 15, 'tree3');
            tree.scale.set(0.6);
            tree = this.backgroundGroup.create(5000, 10, 'tree3');
            tree.scale.set(0.5);
            tree = this.backgroundGroup.create(5600, 25, 'tree1');
            tree.scale.set(0.55);
            tree = this.backgroundGroup.create(6000, 20, 'tree1');
            tree.scale.set(0.55);
        },

        createTrees: function () {

            this.trees = this.add.group();

            this.trees.create(1050, -50, 'tree1');
            this.trees.create(5000, 0, 'tree1');
            this.trees.create(9000, -50, 'tree1');
            this.trees.create(13100, 0, 'tree1');
            this.trees.create(17000, -50, 'tree1');
            this.trees.create(21000, 0, 'tree1');
            this.trees.create(2000, 190, 'tree3');
            this.trees.create(3500, 120, 'tree3');
            this.trees.create(3700, 110, 'tree3');
            this.trees.create(7800, 190, 'tree3');
            this.trees.create(10600, 180, 'tree3');
            this.trees.create(11000, 190, 'tree3');
            this.trees.create(15500, 190, 'tree3');
        },

        createTreesFront: function () {
            this.add.sprite(5400, 110, 'tree3');
            this.add.sprite(7400, 100, 'tree3');
            this.add.sprite(9800, -100, 'tree1');
            this.add.sprite(14000, -100, 'tree1');
        },

        createTerrain: function () {
            this.terrain = this.add.group();
            for (var i = 0; i < 20; ++i) {
                this.terrain.create(i * 950, 0, 'terrain' + i);
            }
        },

        createTileMap: function () {
            var map = this.add.tilemap('map');

            var walls = this.physics.p2.convertCollisionObjects(map, 'physics');

            var l = walls.length;
            for (var i = 0; i < l; ++i) {
                var wall = walls[i];
                wall.static = true;
                wall.setCollisionGroup(this.levelGroup);
                wall.collides(this.carGroup);
            }
        },

        createPlayer: function () {

            var x = this.game.width * 0.5;
            var y = this.game.height * 0.1;

            this.player = this.add.sprite(x, y, 'player');
            this.player.anchor.setTo(0.5, 0.6);

            var xoffset = 80;
            var yoffset = 20;

            this.wheelFront = this.add.sprite(x + xoffset, y + yoffset);
            this.wheelFront.anchor.set(0.5);

            this.wheelBack = this.add.sprite(x - xoffset, y + yoffset);
            this.wheelBack.anchor.set(0.5);

            this.physics.p2.enable([this.player, this.wheelFront, this.wheelBack]);
            this.player.body.clearShapes();

            // Setup Wheel Front

            this.wheelFront.body.setCircle(22);
            this.wheelFront.body.mass = 1;
            this.wheelFront.body.setCollisionGroup(this.carGroup);
            this.wheelFront.body.collideWorldBounds = false;

            this.game.physics.p2.createSpring(this.player, this.wheelFront, 80, 50, 30, null, null, [xoffset, yoffset], [0, 0]);

            var constraintFront = this.game.physics.p2.createPrismaticConstraint(this.player, this.wheelFront, false, [xoffset, yoffset + 10], [0, 0], [0, 1]);
            constraintFront.lowerLimitEnabled = constraintFront.upperLimitEnabled = true;
            constraintFront.upperLimit = -1;
            constraintFront.lowerLimit = -50;

            // We keep track if the rear wheel is on the ground
            this.wheelFront.body.onBeginContact.add(this.frontTouchGround, this);
            this.wheelFront.body.onEndContact.add(this.frontLeaveGround, this);

            // Setup Wheel Back

            this.wheelBack.body.setCircle(22);
            this.wheelBack.body.mass = 2;
            this.wheelBack.body.setCollisionGroup(this.carGroup);
            this.wheelBack.body.collideWorldBounds = false;

            this.game.physics.p2.createSpring(this.player, this.wheelBack, 80, 50, 30, null, null, [-xoffset, yoffset], [0, 0]);

            var constraintBack = this.game.physics.p2.createPrismaticConstraint(this.player, this.wheelBack, false, [-xoffset, yoffset], [0, 0], [0, 1]);
            constraintBack.lowerLimitEnabled = constraintBack.upperLimitEnabled = true;
            constraintBack.upperLimit = -1;
            constraintBack.lowerLimit = -10;

            // We keep track if the rear wheel is on the ground
            this.wheelBack.body.onBeginContact.add(this.touchGround, this);
            this.wheelBack.body.onEndContact.add(this.leaveGround, this);

            // Add Children to snowmobile
            this.pilot = this.add.sprite(-20, -110, 'pilot');
            this.pilot.animations.add('stand');
            this.pilot.animations.add('sit', [3, 2, 1, 0]);
            this.player.addChild(this.pilot);

            this.back = this.add.sprite(-95, 20, 'back');
            this.back.animations.add('drive');
            this.player.addChild(this.back);

            this.front = this.add.sprite(40, 7, 'front');
            this.player.addChild(this.front);
        },

        createSnow: function () {
            this.snow = this.add.sprite(-300, 0, 'snow');
            this.snow.animations.add('snow');
            this.player.addChild(this.snow);
            this.snow.kill();
        },

        createFlag: function () {
            this.flag = this.add.sprite(18230, -110, 'flag');
            //this.flag = this.add.sprite(500, -10, 'flag');
            this.flag.scale.set(0.75);
            this.flag.animations.add('flaganim', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9], 20, true);
            this.flag.play('flaganim');
        },

        createBonuses: function () {
            this.bonuses = this.add.group();
            this.bonuses.create(2200, -20, 'bonus');
            this.bonuses.create(3425, -20, 'bonus');
            this.bonuses.create(4900, 175, 'bonus');
            this.bonuses.create(6400, 50, 'bonus');
            this.bonuses.create(9500, 0, 'bonus');
            this.bonuses.create(10500, 150, 'bonus');
            this.bonuses.create(12400, 0, 'bonus');
            this.bonuses.create(14400, 30, 'bonus');
            this.bonuses.create(16400, 50, 'bonus');
            this.bonuses.create(18000, 50, 'bonus');

            this.bonuses.forEach(function (bonus) {
                bonus.scale.set(0.5);
                bonus.animations.add('turn');
                bonus.animations.play('turn', 10, true);
            }, this);

        },

        createInterface: function () {

            this.add.sprite(-80, 15, 'scorebg').fixedToCamera = true;

            this.timeText = this.add.text(95, 50, '0', {
                font: '48px bebas-neue',
                fill: '#000000',
                align: 'right'
            });
            this.timeText.anchor.setTo(1, 0.5);
            this.timeText.fixedToCamera = true;

            // var txt = this.add.text(50, 90, 'time', {
            //     font: '28px bebas-neue',
            //     fill: '#FFFFFF',
            //     align: 'center'
            // });
            // txt.anchor.set(0.5);
            // txt.fixedToCamera = true;


        },

        update: function () {

            this.mountains.x = -this.game.width + this.player.x * 0.95;
            this.backgroundGroup.x = -this.game.width + this.player.x * 0.75;

            this.camera.x = this.player.x - 300;

            if (this.player.x > this.endPoint) {
                this.onGameOver();
            }

            this.updatePlayer();
            this.updateBonuses();
        },

        updateTime: function () {
            var t = Math.floor(this.timer.seconds) - this.currentBonus;
            /*if (t <= 0) {
                this.timeText.setText(0);
                this.onGameOver();
            } else {
                this.timeText.setText(t);
            }*/
            /*if(t<0) {
                t = 0;
            }*/

            if(t < 0) {
                this.currentBonus += t;
                t = 0;
            }

            this.timeText.setText(t);
        },

        updatePlayer: function () {

            if (this.player.angle < -100 || this.player.angle > 100) {
                this.onGameOver();
            }

            if (!this.frontTouchingGround && !this.touchingGround) {
                ++this.flyCount;
                if (this.flyCount === 10 && this.pilot.animations.name === 'sit') {
                    this.pilot.animations.play('stand', 12);
                }
            }

            var right = false;
            var left = false;

            if (this.game.device.desktop) {
                if (this.cursors.right.isDown) {
                    right = true;
                } else if (this.cursors.left.isDown) {
                    left = true;
                }
            } else {
                var pointer = this.game.input.activePointer;
                if (pointer.isDown) {
                    if (pointer.x < this.game.width * 0.5) {
                        left = true;
                    } else {
                        right = true;
                    }
                }
            }

            if (right) {
                this.back.animations.play('drive', 20, true);
                if (this.touchingGround) {
                    this.snow.revive();
                    this.snow.animations.play('snow', 24);
                }
                this.wheelBack.body.angularVelocity = 50;
            } else {
                this.back.animations.stop('drive');
                if (left) {
                    this.wheelBack.body.angularVelocity = 0;
                }
            }
        },
        render: function () {
            if (this.debug === '1') {
                this.game.debug.spriteBounds(this.player);
                this.bonuses.forEachAlive(function (bonus) {
                    this.game.debug.spriteBounds(bonus);
                }, this);
            }
            
        },

        updateBonuses: function () {
            var playerBounds = this.player.getBounds().scale(0.8);
            this.bonuses.forEachAlive(function (bonus) {
                if (Phaser.Rectangle.intersects(playerBounds, bonus.getBounds())) {
                    this.updateScore();
                    bonus.kill();
                }
            }, this);
        },

        updateScore: function () {
            this.currentBonus += 2;
            this.updateTime();
            //this.timeText.setText(this.maxTime - Math.floor(this.timer.seconds));
        },

        frontTouchGround: function () {
            this.frontTouchingGround = true;
            this.pilotSit();
        },

        frontLeaveGround: function () {
            this.frontTouchingGround = false;
        },

        touchGround: function () {
            this.touchingGround = true;
            this.pilotSit();
        },
        leaveGround: function () {
            this.snow.animations.stop('snow');
            this.snow.kill();
            this.touchingGround = false;
        },

        pilotSit: function () {
            if (this.flyCount > 15) {
                this.pilot.animations.play('sit', 12);
            }
            this.flyCount = 0;
        },

        onGameOver: function () {

            var grade = 'none';
            if (this.player.x > this.endPoint) {
                var dif = Math.floor(this.timer.seconds) - this.currentBonus;
                if (dif <= gold) {
                    grade = 'gold';
                } else if (dif <= silver) {
                    grade = 'silver';
                } else if (dif > bronze) {
                    grade = 'bronze';
                }
            }

            gameOver(grade, dif);
        },
        getQueryVariable: function (variable) {
          var query = window.location.search.substring(1);
          var vars = query.split('&');
          for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split('=');
            if (pair[0] === variable) {
              return pair[1];
            }
          }
          return (false);
        }
    };

    window['snowscooter'] = window['snowscooter'] || {};
    window['snowscooter'].Game = Game;

}());