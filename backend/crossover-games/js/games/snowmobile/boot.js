(function () {
    'use strict';

    function Boot() {}

    Boot.prototype = {

        create: function () {
            this.game.input.maxPointers = 1;
            this.time.desiredFps = 30;

            this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;

            this.scale.minWidth = 480;
            this.scale.minHeight = 260;

            this.scale.maxWidth = 1024;
            this.scale.maxHeight = 540;

            this.game.scale.forceOrientation(false, false);
            this.game.scale.compatibility.scrollTo = false;

            this.game.state.start('preloader');
        }
    };

    window['snowscooter'] = window['snowscooter'] || {};
    window['snowscooter'].Boot = Boot;

}());