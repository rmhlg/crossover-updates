(function () {
    'use strict';
    
    
    function GameTitle() {
        this.startPrompt = null;
        this.ding = null;
    }

    GameTitle.prototype = {

        create: function () {

            var startBG = this.add.image(360, 220, 'letsgo');
            startBG.inputEnabled = true;
            startBG.input.useHandCursor = true;
            startBG.events.onInputDown.addOnce(this.startGame, this);
            
        },

        startGame: function () {
             this.game.state.start('game');
        }
    };

    window['grabgame'] = window['grabgame'] || {};
    window['grabgame'].GameTitle = GameTitle;

})();