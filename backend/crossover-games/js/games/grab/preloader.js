(function () {
    'use strict';

    function Preloader() {
        this.ready = false;
    }

    Preloader.prototype = {

        preload: function () {
            this.load.onLoadComplete.addOnce(this.onLoadComplete, this);
            this.loadResources();
            $(".loader").addClass("visible");
        },

        loadResources: function () {
            this.load.image('rock1', 'img/games/grab/rockbg_02.jpg');
            this.load.image('rock2', 'img/games/grab/rockbg_01.jpg');
            this.load.image('rope', 'img/games/grab/rope.png');
            this.load.image('letsgo', 'img/games/CTA.png');
           

            this.load.spritesheet('cracks', 'img/games/grab/cracks.png', 175, 175);
            this.load.spritesheet('fixations', 'img/games/grab/fixations.png', 50, 63);
            // this.inputEnabled = true;
            // this.input.useHandCursor = true;

            this.load.spritesheet('hero', 'img/games/grab/girl.png', 113, 205);
            this.load.image('pointsback', 'img/games/grab/pointsback.png');
            this.load.image('ground', 'img/games/grab/platform.png');
        },

        update: function () {
           if (!!this.ready) {
                 this.ready = true;
                this.state.start('GameTitle');
            }
            
        },

        onLoadComplete: function () {
            this.ready = true;
            $(".loader").removeClass("visible");
        }
    };

    window['grabgame'] = window['grabgame'] || {};
    window['grabgame'].Preloader = Preloader;

}());