function Preloader() {
	
	this.percent = 0;
	this.preloadAssets = { 'toLoad':0, 'loaded':0 };
	this.assets = new Array();
	
	gRegisteredForAnimation.push(this);
	
	this.preloadImg = preloadImg;
	function preloadImg(tName, tExt) {
	
		if (this.assets.indexOf(tName)<0) {
			this.assets.push(tName);
	
			if (!tExt) { tExt = '.png'; }
			
			// create DOM //
			var i = document.createElement('img'); // or new Image()
			// Set DOM attributes //
			i.id = tName;
			
			var self = this;
			i.onload = function() {
				// Add to assets //
				var container = document.getElementById('assets');
				container.appendChild(this);
				
				// console.log('loaded'+i.src);
				
				// Update progress //
				self.preloadAssets.loaded++;
				self.updateLoading();
			};
			i.src = 'img/games/photography/'+tName+tExt;
			// console.log(i.src);
			this.preloadAssets.toLoad++;
		}
	}

	this.preloadSound = preloadSound;
	function preloadSound(tName, tExt, tLoop, tVolume, tAutoplay) {
		
		if (this.assets.indexOf(tName)<0) {
			this.assets.push(tName);
		
			if (!tExt) { tExt = '.mp3'; }
			if (!tLoop) { tLoop = 'true'; }
			if (!tVolume) { tVolume = '0.5'; }
			if (!tAutoplay) { tAutoplay = 'false'; }
			
			// create DOM //
			var i = document.createElement('audio'); // or new Image()
			// Set DOM attributes //
			i.loop = tLoop;
			i.autoplay = tAutoplay;
			i.volume = tVolume;
			i.id = tName;
			i.preload = 'auto';
			
			// create child DOM //
			var s = document.createElement('source');
			// Set child DOM attributes //
			s.type = 'audio/'+tExt.substring(1);
			s.src = 'Assets/'+tName+tExt;	
			i.appendChild(s);
			
			var self = this;
			i.onloadeddata = function() {
				// Add to assets //
				var container = document.getElementById('assets');
				container.appendChild(this);
				
				this.play();
				
				console.log('loaded'+s.src);
				
				// Update progress //				
				self.preloadAssets.loaded++;
				self.updateLoading();
			};
				
			console.log(s.src);
			
			this.preloadAssets.toLoad++;
		
		}
	}

	this.updateLoading = updateLoading;
	function updateLoading() {
		this.percent = this.preloadAssets.loaded/this.preloadAssets.toLoad;
				
		if (this.percent>=1) {						
			
			// show 100% //
			this.animate();
			$(".loader").removeClass("visible");
			
			// delete from animation //
			gRegisteredForAnimation.splice(indexOfObj(gRegisteredForAnimation, this), 1);
			
			// call init global //
			initPhoto();
			
		}
	}
	
	this.animate = animate;
	function animate() {
		
		// Draw progress line //
		var lineWidth = 10;
		var lineTop = (gScene.height/2)-(lineWidth/2);
		var context = gScene.getContext('2d');
		// context.clearRect(0, 0, gScene.width, gScene.height);
		// context.beginPath();
		// context.moveTo(0, lineTop);
		// context.lineTo(gScene.width*this.percent, lineTop);
		// context.lineWidth = 10;	
		// context.strokeStyle = '#ff0000';
		// context.stroke();
		
		// Write progress txt //
		// context.font = "20px 'bebas-neue'";
		// context.textBaseline = 'top';
		// context.fillStyle = '#000000';
		// context.textAlign = 'center';
		// context.fillText('Loading '+Math.round(this.percent*100)+' %', 512, lineTop-30);
		
	}
	
}