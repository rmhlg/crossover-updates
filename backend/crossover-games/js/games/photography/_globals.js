var gParams;
var gFrames;
var gRegisteredForAnimation;
var mySpritesManager;
var myGame;
var gScene;
var gMousePos;
var myPreloader;
var gInactiveTimeout;
var gFramerate;
var gTime;
var gFrameTime;

var gPause;

var resetAllVar = function(){
	gParams;
	gFrames = 0;
	gRegisteredForAnimation = new Array();
	mySpritesManager = new SpritesManager();
	myGame = null;
	gScene;
	gMousePos = new Object();
	myPreloader = new Preloader();
	gInactiveTimeout;
	gFramerate = 66;
	gTime = 0;
	gFrameTime = Math.round(1000/gFramerate);

	gPause = false;
}

var gRestartFunction = function() { gRegisteredForAnimation.push(mySpritesManager); initPhoto(); };
var gGameMouseupFunction = function(e) { myGame.mouseUp(e); };

//var gLastGroundSpeed = 0;


//Function called the first time the user click on Play
var game;
var initGame = function(){

	$(".loader").addClass("visible");
	resetAllVar();
    preloadPhotographyGame();
	if ('addEventListener' in document) {
		document.addEventListener('DOMContentLoaded', function() {
			FastClick.attach(document.getElementById('scene'));
			//alert('FastClick');
		}, false);
	}

	resetGame();
    
};

var resetGame = function(){

	//If we are on a mobile
    if(isMobile){
        $(".canvasGame").show();//Show the canvas
    }
    else{
        $(".teaserGame").removeClass("showOutro howToOn").addClass("gameOn");
    }

    gameTitle();

};

//Function called whenever the player lose the game
var gameOver = function(grade, score){

    //Change the copy on the outro pannel
    //Three var needs to be passed
    //grade = "gold", "silver", "bronze" or "none"
    //score = score of the player convert in correct unit
    pages.activities.showScores(grade, score);

    $(".teaserGame").removeClass("gameOn").addClass("showOutro");

    if(isMobile){
        $(".canvasGame").hide();//Hide the canvas
        $(window).scrollTop($(".teaserGame").offset().top);//Scroll to the outro
    }

    gameTitle();
    
};

function preloadPhotographyGame() {
	
	// console.log('startMovie');
				
	gScene = document.getElementById('scene')
	// console.log('gScene '+gScene);
	
	// Load font //
	loadJSON('js/games/photography/params.txt', function(result) {
		gParams = result;		
		// console.log('gParams '+gParams);
		
		preloadAssets();

		// startAnimation();
		
	}, 
	function() { alert('error while loading parameters'); }
	);
	
}

function preloadAssets() {
				
	// Preload images //	
	for (var b=0;b<gParams.backgrounds.length;b++) {
		var tBG = gParams.backgrounds[b];
		var tExt = '.png';
		if (b==0) { tExt = '.jpg'; }
		myPreloader.preloadImg(tBG.name, tExt);
	}
			
	myPreloader.preloadImg('viewfinder');
	myPreloader.preloadImg('scorerect');
	myPreloader.preloadImg('picture-bg');
	for (var o=0;o<gParams.objects.length;o++) {
		var tObject = gParams.objects[o];
		for (var n=1;n<=tObject.frames;n++) {
			var tNum = n;
			if (tNum<10) { tNum = '0'+tNum; }
			myPreloader.preloadImg(tObject.name+'_'+tNum);
		}
	}

	// Preload sounds //
	//myPreloader.preloadSound('sleep');
					
	
}

function startAnimation() {
	
	gScene.removeEventListener("click", startAnimation);
	// Set framerate //
	if (gParams.showFPS==true) { setInterval(fps, 1000); }
	setInterval(animate, gFrameTime);
}
function initPhoto() {
	
	myPreloader = null;
		
	// Capture mouse position //
	window.onmousemove = function(e){
		updateMousePos(e);
	};
	// Capture mouse position //
	window.onmousedown = function(e){
		updateMousePos(e);
	};
		
	// Create game //			
	// console.log('Create game');						
	myGame = new Photography();
	
	// Remove loading screen //
	// document.getElementById('loader').className = 'loader';
	
    gameTitle();

}

function updateMousePos(e) {
	gMousePos = {			
		left: (e.pageX - $(gScene).offset().left) * (1024/gScene.offsetWidth),
        top: (e.pageY - $(gScene).offset().top) * (540/gScene.offsetHeight)
	};
						
	clearTimeout(gInactiveTimeout);
	gInactiveTimeout = setTimeout(endGame, gParams.timer*1000);
}

function endGame(closeGame) {
	
	// console.log('endGame');
	
	window.onmousemove = function(e){};	
	window.onmousedown = function(e){};
	
	gScene.removeEventListener('click', gGameMouseupFunction);
	gScene.addEventListener('click', gRestartFunction);
	
	gRegisteredForAnimation.splice(indexOfObj(gRegisteredForAnimation, mySpritesManager), 1);
	mySpritesManager.draw();
	
	clearTimeout(gInactiveTimeout);
	
	if(myGame){
		var tScore = myGame.score;
		myGame.destroy();
		myGame = null;
	}
	
	var grade = 'none';
    if (tScore>=gParams.medals.gold) {
        grade = 'gold';
    } else if (tScore>=gParams.medals.silver) {
        grade = 'silver';
    } else if (tScore >= gParams.medals.bronze) {
        grade = 'bronze';
    }

    if(!closeGame){
    	gameOver(grade, tScore);
    }
	
}

function animate() {
	
	if (gPause==false) {
	
		var d = new Date();
		var n = d.getTime();
		
		if (gTime==0) { gTime = n-gFrameTime; }
		
		var tDeltaTime = (n-gTime)/gFrameTime;	
			
		gTime = n;
		
		gPause = true;
		
		// Call animate every registered object //
		for (var r=0;r<gRegisteredForAnimation.length;r++) {
			gRegisteredForAnimation[r].animate(tDeltaTime);		
		}
		
		// Delete every object set to null //
		while(gRegisteredForAnimation.indexOf(null)>=0) {
			gRegisteredForAnimation.splice(gRegisteredForAnimation.indexOf(null), 1);
		}
		
		gPause = false;
		
	} else {
		//alert('paused');
	}
	gFrames++;			
		
}
function fps() {
	if (myGame!=null) {
		myGame.fps.DOM.innerHTML = gFrames;
	}
	gFrames=0;
}

var CTAimage, ctx;
function gameTitle(){
	ctx = gScene.getContext("2d");
	ctx.clearRect(0, 0, gScene.width, gScene.height);
	ctx.fillStyle = "black";
	ctx.fillRect(0, 0, gScene.width,gScene.height);
	gScene.removeEventListener('click', gGameMouseupFunction);

	if(CTAimage==undefined){
		CTAimage = new Image();
		CTAimage.src = "img/games/CTA.png"
		CTAimage.onload = function(){
	    	ctx.drawImage(CTAimage,(gScene.width/2-136),(gScene.height/2-38));
			gScene.addEventListener('click', initEvents);
		}
	}
	else{
		ctx.drawImage(CTAimage,(gScene.width/2-136),(gScene.height/2-38));
		gScene.addEventListener('click', initEvents);
	}
}

function initEvents(){
	startAnimation();
	gScene.addEventListener('click', gGameMouseupFunction);
	gScene.removeEventListener('click',initEvents);
}