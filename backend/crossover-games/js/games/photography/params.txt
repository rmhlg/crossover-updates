{
	"speed": 2,	
	"timer": 60,
	"medals": { "bronze": 50, "silver": 100, "gold": 200 },
	"radius": 20,	
	"nb_pictures": 10,
	"camera": { "width": 240, "height": 160 },	
	"doubleTake": 0.25,
	"showFPS": false,
	"miss": "MISS",
	
	"backgrounds": [
		{
			"name": "6_background",
			"width": 2766, 
			"height": 267, 
			"y": 133, 
			"speed": 0.1
		},	
		{
			"name": "1_sky",
			"width": 1148, 
			"height": 273, 
			"y": 136, 
			"speed": 0.5
		},
		{
			"name": "5_mountain",
			"width": 1368, 
			"height": 166, 
			"y": 252, 
			"speed": 1
		},
		{
			"name": "4_mountain",
			"width": 1005, 
			"height": 242, 
			"y": 298, 
			"speed": 1.5
		},
		{
			"name": "3_sol",
			"width": 1002, 
			"height": 77, 
			"y": 375, 
			"speed": 2
		},
		{
			"name": "2_sol",
			"width": 1765, 
			"y": 457, 
			"height": 165, 
			"speed": 3
		}	
	],
	"objects": [
		{
			"name": "tree004a",
			"frames": 1,
			"value": 0, 
			"width": 195,
			"height": 126,
			
			"min_frequency": 3000,
			"max_frequency": 9000,
						
			"background": "2_sol",
			"max_speed": 0,
			"min_speed": 0,
			
			"min_y": 390,
			"max_y": 540
			
		},
		{
			"name": "tree004b",
			"frames": 1,
			"value": 0, 
			"width": 199,
			"height": 116,
			
			"min_frequency": 3000,
			"max_frequency": 9000,
			
			"background": "2_sol",
			"max_speed": 0,
			"min_speed": 0,
			
			"min_y": 390,
			"max_y": 540
			
		},
		{
			"name": "tree037_green_shortened_resized",
			"frames": 1,
			"value": 0, 
			"width": 257,
			"height": 810,
			
			"min_frequency": 3000,
			"max_frequency": 10000,
			
			"background": "2_sol",
			"max_speed": 0,
			"min_speed": 0,
			
			"min_y": 200,
			"max_y": 380
			
		},
		{
			"name": "tree026b_shortened_resized",
			"frames": 1,
			"value": 0, 
			"width": 446,
			"height": 570,
			
			"min_frequency": 5000,
			"max_frequency": 15000,
			
			"background": "2_sol",
			"max_speed": 0,
			"min_speed": 0,
			
			"min_y": 270,
			"max_y": 324
			
		},
		{
			"name": "tree038_green_shortened",
			"frames": 1,
			"value": 0, 
			"width": 569,
			"height": 788,
			
			"min_frequency": 10000,
			"max_frequency": 20000,
			
			"background": "2_sol",
			"max_speed": 1,
			"min_speed": 0,
			
			"min_y": 390,
			"max_y": 410
			
		},
		
		{
			"name": "eagle",
			"frames": 21,
			"value": 20, 
			"width": 51,
			"height": 120,
			
			"min_frequency": 5000,
			"max_frequency": 15000,
			
			"background": "2_sol",
			"max_speed": 3,
			"min_speed": 1,
			
			"min_y": 132,
			"max_y": 252
			
		},
		{
			"name": "bison",
			
			"frames": 9,
			"loop": false,
			"start": 1000,
			
			"value": 15, 
			"width": 200,
			"height": 124,
			
			"min_frequency": 5000,
			"max_frequency": 20000,
			
			"background": "2_sol",
			"max_speed": 0,
			"min_speed": 0,			
			
			"min_y": 378,
			"max_y": 475
			
		},		
		
		{
			"name": "ram",
			"frames": 7,
			"value": 10, 
			"width": 105,
			"height": 80,
			
			"min_frequency": 5000,
			"max_frequency": 10000,
			
			"background": "2_sol",
			"max_speed": 2,
			"min_speed": 0.5,
			
			"min_y": 375,
			"max_y": 484
			
		},
		{
			"name": "reindeer",
			"frames": 12,
			"value": 15, 
			"width": 143,
			"height": 120,
			
			"min_frequency": 5000,
			"max_frequency": 15000,
			
			"background": "2_sol",
			"max_speed": -9,
			"min_speed": -7,	
			
			"min_y": 365,
			"max_y": 475
			
		}
		
		
	]
}