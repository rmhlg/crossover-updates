(function () {
    'use strict';

    var Boot = function () {};

    Boot.prototype = {
        preload: function () {
           
        },

        create: function () {
            if (this.game.device.desktop) {
                this.game.scale.pageAlignHorizontally = true;
            } else {
                this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
                this.game.scale.minWidth = 480;
                this.game.scale.minHeight = 260;
                this.game.scale.maxWidth = 1024;
                this.game.scale.maxHeight = 540;
                this.game.scale.forceLandscape = true;
                this.game.scale.pageAlignHorizontally = true;
                this.game.scale.forceOrientation(false, false);
                this.game.scale.compatibility.scrollTo = false;
            }
            this.game.state.start('Preloader');
        }
    };

    window['rockclimber'] = window['rockclimber'] || {};
    window['rockclimber'].Boot = Boot;
})();