(function () {
    'use strict';

    var GameTitle = function () {
        this.startPrompt = null;
        this.ding = null;
    };

    GameTitle.prototype = {

        create: function () {

            var startBG = this.add.image(360, 220, 'letsgo');
            startBG.inputEnabled = true;
            startBG.input.useHandCursor = true;
            startBG.events.onInputDown.addOnce(this.startGame, this);

            
        },

        startGame: function () {
            this.state.start('Game', true, false);
        }
    };

    window['rockclimber'] = window['rockclimber'] || {};
    window['rockclimber'].GameTitle = GameTitle;

})();